import campo from "../assets/icons/map/default/campo.svg";
import casa from "../assets/icons/map/default/casa.svg";
import cochera from "../assets/icons/map/default/cochera.svg";
import departamento from "../assets/icons/map/default/departamento.svg";
import galpon from "../assets/icons/map/default/galpon.svg";
import local from "../assets/icons/map/default/local.svg";
import oficina from "../assets/icons/map/default/oficina.svg";
import ph from "../assets/icons/map/default/ph.svg";
import terreno from "../assets/icons/map/default/terreno.svg";

import campoLiked from "../assets/icons/map/liked/campo.svg";
import casaLiked from "../assets/icons/map/liked/casa.svg";
import cocheraLiked from "../assets/icons/map/liked/cochera.svg";
import departamentoLiked from "../assets/icons/map/liked/departamento.svg";
import galponLiked from "../assets/icons/map/liked/galpon.svg";
import localLiked from "../assets/icons/map/liked/local.svg";
import oficinaLiked from "../assets/icons/map/liked/oficina.svg";
import phLiked from "../assets/icons/map/liked/ph.svg";
import terrenoLiked from "../assets/icons/map/liked/terreno.svg";

import campoHovered from "../assets/icons/map/hovered/campo.svg";
import casaHovered from "../assets/icons/map/hovered/casa.svg";
import cocheraHovered from "../assets/icons/map/hovered/cochera.svg";
import departamentoHovered from "../assets/icons/map/hovered/departamento.svg";
import galponHovered from "../assets/icons/map/hovered/galpon.svg";
import localHovered from "../assets/icons/map/hovered/local.svg";
import oficinaHovered from "../assets/icons/map/hovered/oficina.svg";
import phHovered from "../assets/icons/map/hovered/ph.svg";
import terrenoHovered from "../assets/icons/map/hovered/terreno.svg";

export const defaultMapIcons: { [key: string]: string } = {
  HO: casa,
  PH: ph,
  AP: departamento,
  LA: terreno,
  LO: local,
  CO: galpon,
  GL: galpon,
  SR: local,
  OF: oficina,
  GA: cochera,
  CS: campo,
  BU: departamento,
  LOT: terreno,
};

export const likedMapIcons: { [key: string]: string } = {
  HO: casaLiked,
  PH: phLiked,
  AP: departamentoLiked,
  LA: terrenoLiked,
  LO: localLiked,
  CO: galponLiked,
  GL: galponLiked,
  SR: localLiked,
  OF: oficinaLiked,
  GA: cocheraLiked,
  CS: campoLiked,
  BU: departamentoLiked,
  LOT: terrenoLiked,
};

export const hoveredMapIcons: { [key: string]: string } = {
  HO: casaHovered,
  PH: phHovered,
  AP: departamentoHovered,
  LA: terrenoHovered,
  LO: localHovered,
  CO: galponHovered,
  GL: galponHovered,
  SR: localHovered,
  OF: oficinaHovered,
  GA: cocheraHovered,
  CS: campoHovered,
  BU: departamentoHovered,
  LOT: terrenoHovered,
};
