import { Sort } from "../contexts/FiltersContext";
import { PropertiesTypes } from "../interfaces/properties";

export const sortQuery: { value: Sort; query: string }[] = [
  { value: "operationSell.priceUSDmin:ASC", query: "venta-menor-precio" },
  { value: "operationRent.priceUSDmin:ASC", query: "alquiler-menor-precio" },
  { value: "operationSell.priceUSDmax:DESC", query: "venta-mayor-precio" },
  { value: "operationRent.priceUSDmax:DESC", query: "alquler-mayor-precio" },
  { value: "creationDate:DESC", query: "mas-nuevas" },
  { value: "totalSurfaceMax:DESC", query: "mayor-superficio" },
  { value: "totalSurfaceMax:ASC", query: "menor-superficie" },
];

export const operationTypeOptions = [
  {
    value: "Venta",
    label: "Venta",
    query: "venta",
  },
  {
    value: "Alquiler",
    label: "Alquiler",
    query: "alquiler",
  },
];

export const propertyTypeOptions: Array<{
  value: PropertiesTypes;
  label: string;
  query: string;
}> = [
  { value: PropertiesTypes.AP, label: "Departamentos", query: "departamentos" },
  { value: PropertiesTypes.HO, label: "Casas", query: "casas" },
  {
    value: PropertiesTypes.BU,
    label: "Emprendimientos",
    query: "emprendimientos",
  },
  { value: PropertiesTypes.PH, label: "P.H", query: "p.h" },
  { value: PropertiesTypes.LA, label: "Terrenos", query: "terrenos" },
  { value: PropertiesTypes.LO, label: "Locales", query: "locales" },
  { value: PropertiesTypes.OF, label: "Oficinas", query: "oficinas" },
  { value: PropertiesTypes.GL, label: "Galpones", query: "galpones" },
  { value: PropertiesTypes.GA, label: "Cocheras", query: "cocheras" },
  { value: PropertiesTypes.CS, label: "Campos", query: "campos" },
  { value: PropertiesTypes.LOT, label: "Loteos", query: "loteos" },
];

export const roomAmountOptions = [
  { value: "0:=", label: "Monoambientes", query: "monoambientes" },
  { value: "1:=", label: "1 Dormitorio", query: "1-dormitorio" },
  { value: "2:=", label: "2 Dormitorios", query: "2-dormitorios" },
  { value: "3:=", label: "3 Dormitorios", query: "3-Dormitorios" },
  { value: "4:>=", label: "4 Dormitorios o más", query: "4-dormitorios-o-más" },
];

export const ageOptions = [
  { value: "-1:=", label: "En construcción", query: "en-construccion" },
  { value: "0:=", label: "A estrenar", query: "a-estrenar" },
  { value: "0:>,10:<=", label: "Hasta 10 años", query: "hasta-10-años" },
  {
    value: "10:>,35:<=",
    label: "Entre 10 y 35 años",
    query: "entre-10-y-35-años",
  },
  { value: "35:>", label: "Más de 35 años", query: "mas-de-35-años" },
];

export const amenitiesOptions = [
  { value: "Cochera Opcional", label: "Cochera", query: "cochera" },
  { value: "Pileta", label: "Pileta", query: "pileta" },
  { value: "Patio", label: "Patio", query: "patio" },
  { value: "Parrillero", label: "Parrilla", query: "parrilla" },
  { value: "Quincho", label: "Quincho", query: "quincho" },
  { value: "Jardín", label: "Jardín", query: "jardín" },
  { value: "Terraza", label: "Terraza", query: "terraza" },
];

export const otherOptions = [
  { value: "Financiación", label: "Financiación", query: "financiacion" },
  {
    value: "Apto crédito",
    label: "Apto crédito Hipotecario",
    query: "apto-credito-hipotecario",
  },
  { value: "Acepta Permuta", label: "Acepta Permuta", query: "acepta-permuta" },
];
