import { useCallback, useContext, useEffect, useState } from "react";
import { GetServerSidePropsResult, NextPage, NextPageContext } from "next";
import InfiniteScroll from "react-infinite-scroller";
import Router, { useRouter } from "next/router";
import classnames from "classnames";
import Head from "next/head";

import { fetchProperties } from "../../data-fetch/fetchProperties";
import { FeedPageProps } from "./type";
import PropertyCard from "../../components/PropertyCard";
import { PROPERTIES_FEED_LIMIT } from "./constants";
import { useGetProperties } from "../../hooks/useGetProperties";
import FiltersBar from "../../components/FiltersBar";
import {
  FiltersContext,
  parseFiltersFromQuery,
} from "../../contexts/FiltersContext";
import Loading from "../../components/Loading";
import SortPopup from "../../components/SortPopup";
import FloatingConsultButton from "../../components/FloatingConsultButton";
import HighlightProperty from "../../components/HighlightProperty";
import { useGetHighlightProperties } from "../../hooks/useGetHighlightProperties";
import SortDrawer from "../../components/SortDrawer";
import { DrawerContext } from "../../contexts/DrawerContext";
import NoProperties from "../../components/NoProperties";
import { propertyTypeOptions } from "../../constants/filters";
import MobileActions from "../../components/MobileActions";

import Key from "../../assets/icons/feed/key.svg";
import Map from "../../assets/icons/feed/map.svg";
import FiltersButton from "../../assets/icons/feed/filter-button.svg";
import Filters from "../../assets/icons/feed/filters.svg";
import FiltersMobile from "../../assets/icons/feed/filters-mobile.svg";

import styles from "./FeedPage.module.scss";

export const FeedPage: NextPage<FeedPageProps> = ({
  initialProperties,
  initialPropertiesCount,
  loadingPage,
}) => {
  const {
    properties,
    propertiesCount,
    getProperties,
    hasMore,
    fetchingProperties,
  } = useGetProperties(initialProperties, initialPropertiesCount);
  const { getHighlightProperties, highligthProperties } =
    useGetHighlightProperties(2);
  const { push } = useRouter();

  const [loading, setLoading] = useState(false);
  const [visibleSortDrawer, setVisibleSortDrawer] = useState(false);
  const { setDrawer } = useContext(DrawerContext);
  const { filters } = useContext(FiltersContext);

  const setLoadingOff = useCallback(() => {
    setLoading(false);
  }, [setLoading]);
  const setLoadingOn = useCallback(() => {
    setLoading(true);
  }, [setLoading]);

  useEffect(() => {
    Router.events.on("routeChangeStart", setLoadingOn);
    Router.events.on("routeChangeComplete", setLoadingOff);
    Router.events.on("routeChangeError", setLoadingOff);

    return () => {
      Router.events.off("routeChangeStart", setLoadingOn);
      Router.events.off("routeChangeComplete", setLoadingOff);
      Router.events.off("routeChangeError", setLoadingOff);
    };
  }, [setLoadingOff, setLoadingOn]);

  const mobileActions = (
    <MobileActions
      className={styles["feed-page__mobile-actions"]}
      actions={[
        {
          img: Map,
          label: "Mapa",
          action: () => {
            push("/mapa");
          },
        },
        {
          img: FiltersButton,
          label: "Filtros",
          action: () => {
            setDrawer("filters", true);
          },
        },
      ]}
    />
  );

  return !properties.length && !loading ? (
    <div className={styles["feed-page__no-properties"]}>
      <FiltersBar className={styles["feed-page__filters-bar"]} />
      {mobileActions}
      <NoProperties />
    </div>
  ) : (
    <div className={styles["feed-page"]}>
      <Head>
        <title>Inmobiliaria en Rosario. Venta de propiedades | Vanzini</title>
        <meta
          name="description"
          content={
            filters.propertyType && filters.propertyType.length
              ? `${filters.propertyType
                  .map((propType) => {
                    const option = propertyTypeOptions.find(
                      (opt) => opt.value === propType
                    );
                    return option && option.label;
                  })
                  .join(
                    ", "
                  )} en Venta en Rosario. Buscá con el filtro de los/as mejores ${propertyTypeOptions
                  .map((opt) => opt.label)
                  .join(", ")}  | Vanzini Propiedades`
              : "Inmobiliaria en Rosario. Venta de propiedades, departamentos, casas, terrenos, oficinas, galpones y campos. Más de 50 años de EXPERIENCIA inmobiliaria."
          }
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "https://schema.org",
              "@type": "Corporation",
              "name": "Vanzini Propiedades",
              "url": "https://www.vanzini.com.ar/",
              "logo": "https://www.vanzini.com.ar/logo-vanzini.svg",
              "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "03414255215",
                "contactType": "customer service",
                "areaServed": "AR",
                "availableLanguage": "es"
              },
              "sameAs": [
                "https://www.facebook.com/vanzini.prop",
                "https://twitter.com/VanziniProp",
                "https://www.instagram.com/vanzinipropiedades/",
                "https://www.youtube.com/user/VanziniPropiedades",
                "https://ar.pinterest.com/vanzinipropiedades/"
              ]
            }
              `,
          }}
        />
      </Head>
      <FloatingConsultButton />
      <FiltersBar className={styles["feed-page__filters-bar"]} />
      {mobileActions}
      <SortDrawer
        visible={visibleSortDrawer}
        setVisible={setVisibleSortDrawer}
      />
      <div className={styles["feed-page__actions-mobile"]}>
        <span className={styles["feed-page__actions-mobile--count"]}>
          <b>{propertiesCount}</b> propiedades en venta
        </span>
        <span
          onClick={() => setVisibleSortDrawer(true)}
          className={styles["feed-page__actions-mobile--sort"]}
        >
          <img src={FiltersMobile} alt="Icono de filtros" />
          Ordenar
        </span>
      </div>
      <div className={styles["feed-page__page-container"]}>
        {!!properties.length && !loading && (
          <div className={styles["feed-page__actions"]}>
            <span className={styles["feed-page__actions--item"]}>
              <img src={Key} alt="Llave" />
              {propertiesCount} propiedades en venta
            </span>
            <SortPopup>
              <span className={styles["feed-page__actions--item"]}>
                <img src={Filters} alt="Icono de filtros" />
                Ordenar
              </span>
            </SortPopup>
          </div>
        )}
        <InfiniteScroll
          pageStart={0}
          initialLoad={false}
          loadMore={() => {
            if (!fetchingProperties) {
              getProperties(properties.length, PROPERTIES_FEED_LIMIT);
              getHighlightProperties();
            }
          }}
          threshold={100}
          hasMore={hasMore}
          className={styles["feed-page__feed"]}
        >
          {!loading &&
            properties.map((property, index) => {
              if ((index + 1) % 6 === 0) {
                const highlightIndex = (index + 1) / 6 - 1;

                return [
                  <PropertyCard
                    className={classnames(
                      styles["feed-page__property-card"],
                      (index + 1) % 2 === 0
                        ? styles["feed-page__property-card--even"]
                        : styles["feed-page__property-card--odd"]
                    )}
                    key={property._id}
                    property={property}
                  />,
                  highligthProperties[highlightIndex] && (
                    <HighlightProperty
                      key={highligthProperties[highlightIndex]._id}
                      className={styles["feed-page__highlighted-property"]}
                      property={highligthProperties[highlightIndex]}
                    />
                  ),
                ];
              } else {
                return (
                  <PropertyCard
                    className={classnames(
                      styles["feed-page__property-card"],
                      (index + 1) % 2 === 0
                        ? styles["feed-page__property-card--even"]
                        : styles["feed-page__property-card--odd"]
                    )}
                    key={property._id}
                    property={property}
                  />
                );
              }
            })}
        </InfiniteScroll>
        {(loading || fetchingProperties) && !loadingPage && (
          <Loading className={styles["feed-page__loading"]} />
        )}
      </div>
    </div>
  );
};

export async function feedGetServerSideProps(
  context: NextPageContext
): Promise<GetServerSidePropsResult<FeedPageProps>> {
  const filters = parseFiltersFromQuery(context.query);

  const {
    properties: initialProperties,
    propertiesCount: initialPropertiesCount,
  } = await fetchProperties(0, PROPERTIES_FEED_LIMIT, filters);

  return {
    props: { initialProperties, initialPropertiesCount },
  };
}
