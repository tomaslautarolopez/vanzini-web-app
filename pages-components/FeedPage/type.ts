import { Property } from "../../interfaces/properties";

export interface FeedPageProps {
  initialProperties: Array<Property>;
  initialPropertiesCount: number;
  loadingPage?: boolean;
}
