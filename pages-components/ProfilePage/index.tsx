import { NextPage } from "next";
import { useRouter } from "next/router";
import { useContext } from "react";
import MobileActions from "../../components/MobileActions";

import PageFooter from "../../components/PageFooter";
import ProfileLikes from "../../components/Profile/ProfileLikes";
import ProfileLogOut from "../../components/Profile/ProfileLogOut";
import ProfileNewsletter from "../../components/Profile/ProfileNewsletter";
import ProfilePasswordChange from "../../components/Profile/ProfilePasswordChange";
import ProfileUpdate from "../../components/Profile/ProfileUpdate";
import ProfileResume from "../../components/ProfileResume";
import { UserContext } from "../../contexts/UserContext";
import Map from "../../assets/icons/feed/map.svg";
import FiltersButton from "../../assets/icons/feed/filter-button.svg";
import { DrawerContext } from "../../contexts/DrawerContext";
import PropertiesListsBanner from "../../components/PropertiesListsBanner";
import ProfileAlerts from "../../components/Profile/ProfileAlerts";

import styles from "./ProfilePage.module.scss";

export const ProfilePage: NextPage<{}> = () => {
  const {
    user: { data },
  } = useContext(UserContext);
  const { setDrawer } = useContext(DrawerContext);

  const { push } = useRouter();

  return (
    <div className={styles["profile-page"]}>
      <MobileActions
        className={styles["profile-page__mobile-actions"]}
        actions={[
          {
            img: Map,
            label: "Mapa",
            action: () => {
              push("/mapa");
            },
          },
          {
            img: FiltersButton,
            label: "Filtros",
            action: () => {
              setDrawer("filters", true);
            },
          },
        ]}
      />
      <PropertiesListsBanner />
      <div className={styles["profile-page__row"]}>
        <div className={styles["profile-page__column"]}>
          <div className={styles["profile-page__resume"]}>
            <ProfileResume />
          </div>
        </div>
        <div className={styles["profile-page__column"]}>
          <ProfileLikes />
          <ProfileAlerts />
          <div className={styles["profile-page__profile-update"]}>
            <ProfileUpdate />
          </div>
          {data?.provider === "local" && (
            <div className={styles["profile-password-update"]}>
              <ProfilePasswordChange />
            </div>
          )}
          <div className={styles["profile-page__newsletter"]}>
            <ProfileNewsletter />
          </div>
          <div className={styles["profile-page__log=out"]}>
            <ProfileLogOut />
          </div>
        </div>
      </div>
      <PageFooter />
    </div>
  );
};
