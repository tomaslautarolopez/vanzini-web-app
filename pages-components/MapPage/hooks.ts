import { useContext, useEffect, useState } from "react";
import { Filters, FiltersContext } from "../../contexts/FiltersContext";
import { fetchMapProperties } from "../../data-fetch/fetchProperties";
import { Property } from "../../interfaces/properties";

export const useMap = () => {
  const { filters } = useContext(FiltersContext);
  const [loading, setLoading] = useState(false);
  const [mapState, setMapState] = useState<{
    mapProperties: Array<Property>;
    mapPropertiesCount: number;
  }>({
    mapProperties: [],
    mapPropertiesCount: 0,
  });

  const fetch = async (filters: Filters) => {
    setLoading(true);
    const newMapState = await fetchMapProperties(filters);
    setLoading(false);

    setMapState(newMapState);
  };

  useEffect(() => {
    fetch(filters);
  }, [filters]);

  return {
    ...mapState,
    loading,
  };
};
