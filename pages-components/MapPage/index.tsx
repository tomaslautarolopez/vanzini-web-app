import { NextPage } from "next";
import InfiniteScroll from "react-infinite-scroller";
import { useContext, useMemo, useState } from "react";
import dynamic from "next/dynamic";
import { DownOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import Head from "next/head";
import { Drawer } from "antd";
import { isPointInPolygon } from "geolib";

import ArrowButton from "../../assets/icons/property-details/arrow.svg";
import { MapPageProps } from "./type";
import FiltersBar from "../../components/FiltersBar";
import { useMap } from "./hooks";
import MapPropertyCard from "../../components/MapPropertyCard";
import SortPopup from "../../components/SortPopup";
import Loading from "../../components/Loading";
import FloatingConsultButton from "../../components/FloatingConsultButton";
import MobileActions from "../../components/MobileActions";
import { Property } from "../../interfaces/properties";
import FiltersButton from "../../assets/icons/feed/filter-button.svg";
import PropertyCard from "../../components/PropertyCard";
import { DrawerContext } from "../../contexts/DrawerContext";

import styles from "./MapPage.module.scss";
import { LatLngBounds } from "leaflet";

const MapWithNoSSR = dynamic(() => import("../../components/PropertiesMap"), {
  ssr: false,
});

const PAGE_SIZE = 12;

export const MapPage: NextPage<MapPageProps> = ({}) => {
  const { mapProperties, mapPropertiesCount, loading } = useMap();
  const [searchedBounds, setSearchedBounds] = useState<
    LatLngBounds | undefined
  >();
  const [mobilePropertyClicked, setMobilePropertyClicked] = useState<
    Property | undefined
  >();
  const [page, setPage] = useState(PAGE_SIZE);
  const { back } = useRouter();
  const { setDrawer } = useContext(DrawerContext);
  const [hoveredProperty, setHoveredProperty] = useState<
    Property | undefined
  >();

  searchedBounds?.getNorthEast().lat;

  const showingProperties = useMemo(
    () =>
      searchedBounds
        ? mapProperties.filter((prop) =>
            isPointInPolygon(
              {
                latitude: prop.geoLocation.coordinates[0],
                longitude: prop.geoLocation.coordinates[1],
              },
              [
                {
                  latitude: searchedBounds.getNorthEast().lat,
                  longitude: searchedBounds.getNorthEast().lng,
                },
                {
                  latitude: searchedBounds.getSouthEast().lat,
                  longitude: searchedBounds.getSouthEast().lng,
                },
                {
                  latitude: searchedBounds.getSouthWest().lat,
                  longitude: searchedBounds.getSouthWest().lng,
                },
                {
                  latitude: searchedBounds.getNorthWest().lat,
                  longitude: searchedBounds.getNorthWest().lng,
                },
              ]
            )
          )
        : mapProperties,
    [searchedBounds, mapProperties]
  );

  return (
    <div className={styles["map-page"]}>
      <Head>
        <title>
          Propiedades en Venta, mapa de Rosario | Vanzini Propiedades
        </title>
        <meta
          name="description"
          content="Propiedades en Venta, mapa de Rosario con departamentos, casas, terrenos, locales comerciales. Las mejores ubicaciones y opciones en los barrios: centro, pichincha, echesortu, puerto norte, fisherton, funes y abasto."
        />
      </Head>
      <Drawer
        closable={false}
        className="map-page__mobile-drawer"
        visible={!!mobilePropertyClicked}
        onClose={() => setMobilePropertyClicked(undefined)}
        placement="bottom"
        height="400px"
      >
        {mobilePropertyClicked && (
          <PropertyCard mapCard property={mobilePropertyClicked} />
        )}
      </Drawer>
      <FiltersBar className={styles["map-page__filters-bar"]} />
      {loading && (
        <div className={styles["map-page__loading"]}>
          <Loading />
        </div>
      )}
      <MobileActions
        className={styles["map-page__mobile-actions"]}
        actions={[
          {
            img: ArrowButton,
            label: "Atras",
            action: () => {
              back();
            },
          },
          {
            img: FiltersButton,
            label: "Filtros",
            action: () => {
              setDrawer("filters", true);
            },
          },
        ]}
      />
      <FloatingConsultButton showOnMobile />
      <div className={styles["map-page__container"]}>
        <div className={styles["map-page__feed"]}>
          <div className={styles["map-page__feed--header"]}>
            <h6 className={styles["map-page__feed--header---amount"]}>
              <b>{mapPropertiesCount}</b> propiedades en venta
            </h6>
            <SortPopup>
              <h6 className={styles["map-page__feed--header---sort"]}>
                Acciones <DownOutlined />
              </h6>
            </SortPopup>
          </div>
          <div className={styles["map-page__feed--container"]}>
            <InfiniteScroll
              initialLoad={false}
              loadMore={() => {
                setPage(page + PAGE_SIZE);
              }}
              hasMore={page < showingProperties.length}
              useWindow={false}
            >
              {showingProperties.slice(0, page).map((property) => (
                <MapPropertyCard
                  setHoveredProperty={setHoveredProperty}
                  key={property._id}
                  property={property}
                  className={styles["map-page__property-card"]}
                />
              ))}
            </InfiniteScroll>
          </div>
        </div>
        <div className={styles["map-page__map"]}>
          <MapWithNoSSR
            hoveredProperty={hoveredProperty}
            setSearchedBounds={setSearchedBounds}
            setMobilePropertyClicked={setMobilePropertyClicked}
            mapProperties={showingProperties}
          />
        </div>
      </div>
    </div>
  );
};

// export async function feedGetServerSideProps(
//   context: NextPageContext
// ): Promise<GetServerSidePropsResult<FeedPageProps>> {
//   const filters = parseFiltersFromQuery(context.query);

//   const {
//     properties: initialProperties,
//     propertiesCount: initialPropertiesCount,
//   } = await fetchProperties(0, PROPERTIES_FEED_LIMIT, filters);

//   return {
//     props: { initialProperties, initialPropertiesCount },
//   };
// }
