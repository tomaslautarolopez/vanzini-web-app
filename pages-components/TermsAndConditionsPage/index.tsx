import { Button } from "antd";
import { NextPage } from "next";
import { useRef } from "react";
import { RightOutlined } from "@ant-design/icons";
import Link from "next/link";

import LogInBanner from "../../components/LogInBanner";
import PageFooter from "../../components/PageFooter";
import background from "../../assets/images/TermsAndConditions/terms-and-conditions-bg.jpeg";
import FiltersButton from "../../assets/icons/feed/filter-button.svg";
import MobileActions from "../../components/MobileActions";
import Map from "../../assets/icons/feed/map.svg";

import styles from "./TermsAndConditionsPage.module.scss";

export const TermsAndConditionsPage: NextPage<{}> = () => {
  const ref = useRef<HTMLDivElement>(null);

  return (
    <div className={styles["terms-and-condition-page"]}>
      <MobileActions
        className={styles["terms-and-condition-page__mobile-actions"]}
        actions={[
          {
            img: Map,
            label: "Mapa",
            action: () => {},
          },
          {
            img: FiltersButton,
            label: "Filtros",
            action: () => {},
          },
        ]}
      />
      <LogInBanner />
      <div className={styles["terms-and-condition-page__header"]}>
        <h1>Terminos y condiciones</h1>
        <h5>
          A continuación se encuentra los terminos y condiciones de uso para
          nuestro sitio web.
        </h5>
        <Button
          onClick={() =>
            ref.current?.scrollIntoView({
              behavior: "smooth",
            })
          }
          type="primary"
        >
          Leer más
        </Button>
        <img src={background} alt="Foto de la ciudad de Rosario" />
      </div>
      <div ref={ref} className={styles["terms-and-condition-page__body"]}>
        <h5>INTRODUCCIÓN</h5>
        <p>
          El presente contrato estipula los términos y condiciones (en adelante,
          los "Términos y Condiciones") aplicables a los servicios ofrecidos por
          Vanzini Black S.A., CUIT 33-71612808-9, (en adelante, los "Servicios")
          en el sitio web de su titularidad, identificado con la URL
          vanzini.com.ar (en adelante, el “Sitio Web”), como así también las
          relaciones e interacciones derivadas y/o vinculadas al mismo, ya sea a
          través de las cuentas oficiales de sus redes sociales o cualquier otro
          canal electrónico que resulte expresamente informado a través del
          “sitio web” (en adelante, “Redes Sociales”). Toda persona humana y/o
          jurídica (en adelante, “Usuario”) que ingrese, utilice y/o de modo
          alguno interactúe con el Sitio Web, las Redes Sociales o se sirva de
          los Servicios que ofrezca o provea Vanzini Black S.A. (en adelante,
          “Vanzini”), deberá realizarlo obligatoriamente bajo aceptación de los
          Términos y Condiciones. Mediante la utilización de los Servicios, el
          Sitio Web y/o las Redes Sociales, el Usuario declara que ha tomado
          conocimiento mediante la lectura y comprensión de los Términos y
          Condiciones, aceptando expresa y libremente los mismos. En caso de no
          encontrarse de acuerdo con los Términos y Condiciones, el Usuario
          deberá abstenerse de toda utilización y/o interacción de forma
          inmediata. Los Términos y Condiciones podrán ser modificados por
          Vanzini a su sola voluntad y sin necesidad de notificación previa al
          Usuario, resultando vinculante para el Usuario la versión que se
          encontrare vigente y publicada en el Sitio Web, al momento que utilice
          el Servicio. Versión actual del contrato: 1.0 – 09/03/2020.
        </p>
        <h5>CLÁUSULAS GENERALES</h5>
        <h6>Primera:</h6>
        <p>
          A los efectos de la utilización de los Servicios, el Sitio Web y las
          Redes Sociales, el Usuario deberá contar con plena capacidad legal
          para celebrar contratos, conforme la legislación vigente. En caso de
          no contar con capacidad o contar con capacidad restringida, la
          utilización se encuentra vedada en su totalidad y el Usuario deberá
          abstenerse de cualquier tipo de uso.
        </p>
        <h6>Segunda:</h6>
        <p>
          El Usuario podrá utilizar el sitio sin necesidad de registrarse,
          limitado a los Servicios que así se encuentren habilitados para ello y
          bajo las mismas obligaciones fijadas por los Términos y Condiciones.
          Para aquellas funciones, servicios, prestaciones o características que
          lo requieran, el Usuario deberá, para acceder a las mismas, completar
          el formulario de registro, proveyendo la información que le sea
          solicitada de forma verídica, exacta y completa. El Usuario se
          compromete a mantener los mismos actualizados, en ocasión que ocurran
          modificaciones respecto de los que hubiera informado, considerándose
          válidos los ingresados, a todos sus efectos en la medida que así no lo
          cumplimente. En tal sentido, Vanzini carece de toda responsabilidad
          respecto de la exactitud, actualización o veracidad de los datos
          ingresados por el Usuario, siendo éste responsable respecto de la
          información que hubiera ingresado. Vanzini podrá utilizar todo tipo de
          herramientas legales, técnicas, informáticas y/o electrónicas, a su
          solo criterio, para corroborar e identificar al Usuario y los datos
          que hubiera ingresado, lo cual es aceptado y reconocido por el
          Usuario. Vanzini podrá bajo su exclusivo criterio, eliminar, suspender
          o prohibir el ingreso de cualquier Usuario a sus Servicios, Sitio Web
          y/o Redes Sociales, sin necesidad de expresión de motivos ni
          justificación alguna, siendo tal decisión inapelable, no requiriendo
          para ello informarlo de forma previa al Usuario, ni generando derecho
          patrimonial y/o indemnizatorio alguno, de ninguna índole. Vanzini
          podrá exigir al Usuario la información y/o elementos documentales que
          a su solo criterio considere necesarios, a fin de ampliar y/o
          corroborar la información provista por el Usuario, reservándose el
          derecho de su suspensión, eliminación o prohibición de ingreso, de no
          aportarse los datos requeridos, o de no resultar éstos suficientes o
          convincentes, al solo y exclusivo criterio de Vanzini, siendo la
          decisión definitiva e inapelable, no generando ello derecho a
          indemnización o reparación alguna a favor del Usuario. El Usuario
          podrá, opcionalmente, registrarse y/o identificarse en el Sitio Web, a
          través de las API provistas por Google y/o Facebook, utilizando los
          datos de su cuenta personal en los respectivos servicios. El Usuario
          declara conocer y aceptar que Vanzini podrá recopilar sus datos de
          e-mail, nombre y apellido, teléfono, foto de perfil y/u otros datos
          provistos por el Usuario y/o las API que el Usuario opte por utilizar
          para identificarse en el Sitio Web para su uso conforme los Términos y
          Condiciones. El Usuario podrá optar por suscribirse a la recepción de
          un correo electrónico enviado periódicamente por Vanzini, mediante la
          provisión de su cuenta de correo electrónico por cualquiera de los
          medios descriptos, u otros que se incorporen en el futuro. Asimismo,
          podrá solicitar su desuscripción en cualquier momento, por medio de
          los canales dispuestos a tal efecto en el mismo correo electrónico. El
          Usuario acepta la utilización de cookies para diferentes funciones del
          sitio, incluidas pero no limitadas a su identificación y salvado de
          preferencias.
        </p>
        <h6>Tercera:</h6>
        <p>
          El Usuario registrado deberá utilizar para su ingreso, su correo
          electrónico y contraseña elegida al momento de registrarse, o bien
          identificarse por medio de las API de Google y/o Facebook. El usuario
          se compromete a mantener secreta y no compartir con persona alguna su
          contraseña. El Usuario acepta que su cuenta de usuario resulta
          personal, única e intransferible. Se encuentra prohibido utilizar la
          cuenta por más de una persona y Vanzini se reserva el derecho de
          eliminar cualquier cuenta de Usuario y vedar su ingreso al Sitio Web,
          que incumpla tal obligación. Se encuentra prohibida la registración de
          más de un usuario por persona. El Usuario se responsabiliza por toda
          actividad que realice en el Sitio Web, encontrándose registrado o no,
          aceptando que Vanzini podrá utilizar diferentes medios y registros
          técnicos a fin de su identificación. El Usuario se obliga a informar
          fehacientemente y de forma inmediata a Vanzini en caso que haya
          ocurrido cualquier uso de su Cuenta por una tercera persona y/o
          cualquier uso no autorizado.
        </p>
        <h6>Cuarta:</h6>
        <p>
          Los Servicios contemplan la posibilidad de acceder y consultar una
          base de datos de bienes inmuebles titularidad de terceras personas,
          publicitados por Vanzini y/o asociados para su venta y/o alquiler,
          como así también servicios relacionados, tales como, a modo
          enunciativo, pero no limitado a, ofrecimiento de servicios varios de
          intermediación inmobiliaria y tasaciones de propiedades. Los inmuebles
          publicados en el Sitio Web, Redes Sociales o correos electrónicos
          remitidos por Vanzini, se exhiben al mero efecto ilustrativo, a fin de
          permitir al Usuario acceder a fotografías, videos, planimetría,
          renderizados y detalles técnicos, entre otros datos, de los mismos, al
          solo efecto de su conocimiento.. En ningún caso y bajo ninguna
          circunstancia, las publicaciones de inmuebles y/o cualquier otro
          Servicio ofrecido, podrá ser considerado oferta en los términos del
          Art. 972, ni invitación a ofertar del Art. 973 del Código Civil y
          Comercial de la Nación. El Usuario acepta y reconoce que la
          información publicada resulta ilustrativa y no contractual. Asimismo,
          acepta que Vanzini carecerá de toda responsabilidad respecto de
          inexactitudes, omisiones o errores en las publicaciones, no existiendo
          a favor del Usuario derecho patrimonial y/o indemnizatorio alguno, de
          ninguna índole. El Usuario deberá en toda circunstancia, contactar a
          Vanzini por cualquiera de las modalidades indicadas en el Sitio Web
          y/o las Redes Sociales, o solicitar ser contactado, para mayor
          información y consultas sobre los Servicios ofrecidos, sus condiciones
          y costos. Todo acto jurídico que eventualmente se celebre entre el
          Vanzini y el Usuario, respecto de los inmuebles publicados y/o demás
          Servicios, se regirá por las condiciones que oportunamente se acuerden
          entre las partes. En ningún caso la información publicada en el Sitio
          Web y/o sus Redes Sociales, importará obligación alguna de Vanzini a
          favor del Usuario, ni promesa y/o garantía de calidad, exactitud ni
          cualquier otra característica. El Usuario acepta que se encuentra
          prohibido la utilización de cualquier medio o técnica a fin de ubicar
          a los titulares registrales de los bienes publicados, a fin de su
          contacto directo. La violación de ello importara a favor de Vanzini el
          derecho de reclamar al Usuario, en caso de eventualmente ocurrir
          cualquier acto contractual entre el Usuario y el titular registral del
          bien publicado (dentro del plazo acordado con el titular registral del
          bien inmueble para su ofrecimiento), la correspondiente comisión que
          le hubiere correspondido de intermediar en el negocio jurídico, con
          más sus gastos extrajudiciales, judiciales, costas en general e
          intereses que correspondieren. En el caso de los servicios de
          tasaciones ofertados, los mismos no se presumen en ningún caso a
          título gratuito, debiendo el Usuario contactar a Vanzini a través de
          los medios publicitados, al efecto de consultar sus costos y
          condiciones, al efecto de su eventual contratación.
        </p>
        <h6>Quinta:</h6>
        <p>
          Vanzini resulta titular y/o licenciatario autorizado de los logos,
          isologotipos, slogans y demás imágenes y textos identificatorios de su
          marca utilizados en el Sitio Web. Se encuentra prohibido el uso por
          cualquier medio y forma de los mismos, salvo expresa autorización
          escrita de Vanzini o su correspondiente titular. Las fotografías,
          videos, planos, renderizados y demás elementos insertos en las
          publicaciones de bienes inmuebles publicitados, resultan de
          titularidad exclusiva de Vanzini -salvo en aquellos casos provistos
          por terceros, en los cuales se regirán por sus propias licencias-, no
          pudiendo persona alguna descargarlas y/o utilizarlas de ninguna forma
          ni por ningún medio, salvo expresa autorización de Vanzini o su
          correspondiente titular.
        </p>
        <h6>Sexta:</h6>
        <p>
          El Usuario se compromete a utilizar el Sitio Web y los Servicios
          exclusivamente del modo diseñado. El Sitio Web y su contenido resulta
          titularidad de Vanzini, bajo la protección de la Ley 11.723 de
          Propiedad Intelectual y sus modificatorias, encontrándose prohibido su
          copia, descarga, modificación, reutilización o alteración de modo
          alguno, ya sea de su diseño y/o código fuente y/o contenido
          multimedia. El Usuario acepta que en caso de violar la prohibición
          indicada, será pasible de las sanciones legales contempladas en la
          ley, sin perjuicio de responder por los daños y perjuicios
          patrimoniales que correspondan.
        </p>
        <h6>Septima:</h6>
        <p>
          El Usuario acepta y se obliga a interactuar con el Sitio Web, los
          Servicios, sus bases de datos, servidores y/o conexiones, en un todo
          de acuerdo a la ley de la República Argentina y de los demás países
          y/u organismos internacionales que correspondieren. Toda violación a
          la normativa vigente resultará de su exclusiva responsabilidad,
          obligándose a mantener indemne a Vanzini de cualquier tipo de reclamo.
          Asimismo se compromete a responder por los daños y perjuicios que su
          accionar generen hacia Vanzini y/o terceros.
        </p>
        <h6>Octava:</h6>
        <p>
          La información provista por los Usuarios, de modo directo y/o
          indirecto, será procesada y almacenada bajo estándares internacionales
          de seguridad y conforme las exigencias legales vigentes. El Usuario
          podrá ejercer su derecho de acceso a su información de modo gratuito
          en períodos no menores a seis meses, debiendo solicitarlo por escrito
          y acreditando su identidad a Vanzini. En cualquier momento el Usuario
          podrá eliminar y/o solicitar la eliminación de sus datos, mediante
          solicitud escrita y previa acreditación de su identidad, o mediante
          los medios técnicos que se dispongan a tal efecto.
        </p>
        <h6>Novena:</h6>
        <p>
          El Usuario presta conformidad para el tratamiento de su información
          con la finalidad de prestar un servicio personalizado y mejorar los
          Servicios ofrecidos. En tal sentido, el Usuario acepta y autoriza a
          Vanzini a utilizar los datos recabados en servicios y sistemas de
          marketing y remarketing propios y de terceros. El Usuario podrá
          solicitar en cualquier momento la remoción de su información conforme
          lo indicado en cláusula octava.
        </p>
        <h6>Décima:</h6>
        <p>
          Vanzini no se responsabiliza por ningún tipo de daño, perjuicio o
          pérdida que sufriera el Usuario por fallas en el sistema, sus
          servidores o conexiones de Internet. Vanzini no será responsable por
          cualquier tipo de virus que pudiera infectar o dañar el equipamiento
          del Usuario en virtud del ingreso o uso que realizara del Sitio Web
          y/o Redes Sociales. El Usuario renuncia expresamente a reclamar
          cualquier tipo de pago, por el concepto que fuere a Vanzini, en virtud
          de fallas o dificultades técnicas en los Servicios o Internet. Vanzini
          no garantiza el acceso al Sitio Web, Servicios y/o Redes Sociales
          resulte continuado o ininterrumpido, sin perjuicio que intentará
          reparar aquellas fallas que detectara oportunamente.
        </p>
        <h6>Décima primera:</h6>
        <p>
          El Sitio Web podrá contener accesos a sitios webs de terceros. Vanzini
          no controla los mismos ni será responsable por sus contenidos,
          acciones y/o servicios, como así tampoco respecto de los daños o
          pérdidas que el uso de éstos pudiera eventualmente causar al Usuario.
          Los enlaces no implican vinculación contractual y/o societaria de
          ningún tipo de Vanzini hacia los terceros. Los proveedores de
          servicios sobre los cuales Vanzini tiene presencia en Redes Sociales,
          como así también los sitios enlazados, se rigen por su propios
          términos y condiciones, los cuales el usuario declara conocer y
          aceptar como condición para su utilización. Vanzini en ningún caso
          será responsable por el uso indebido y/o ilegal que el Usuario realice
          de los mismos, obligándose a mantener indemne a Vanzini de cualquier
          reclamo que terceros pudieran efectuar.
        </p>
        <h6>Décima segunda:</h6>
        <p>
          El Usuario se obliga a mantener indemne a Vanzini y/o terceros
          relacionados de cualquier modo, de cualquier reclamo de otro Usuario,
          terceros y/u organismos de cualquier tipo, en relación a su actividad
          en el Sitio Web y/o vinculada a los Servicios y/o Redes Sociales, como
          así también del cumplimiento o no de los Términos y Condiciones, y
          respecto de la violación de la normativa vigente o derechos que
          correspondieren a terceros.
        </p>
        <h6>Décima tercera:</h6>
        <p>
          Los Términos y Condiciones se rigen por las leyes vigentes en la
          República Argentina. Cualquier controversia o discusión derivada del
          presente, será sometida a la Justicia Ordinaria de la ciudad de
          Rosario, renunciando el Usuario a cualquier otro fuero y/o
          jurisdicción, inclusive la Federal.
        </p>
        <h6>Décima cuarta:</h6>
        <p>
          Vanzini fija domicilio a todos los efectos del presente en calle Brown
          2063 Piso 4 oficina 2 de la ciudad de Rosario, Provincia de Santa Fe,
          República Argentina.
        </p>
      </div>
      <div className={styles["terms-and-condition-page__back-home"]}>
        <Link href="/">
          <Button type="primary">
            Volver al home <RightOutlined />
          </Button>
        </Link>
      </div>
      <PageFooter />
    </div>
  );
};
