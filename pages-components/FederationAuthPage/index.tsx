import { NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect } from "react";

export const FederationAuthPage: NextPage<{}> = () => {
  const router = useRouter();

  useEffect(() => {
    if (router.query.token) {
      window.opener.postMessage(
        {
          token: router.query.token,
        },
        "*"
      );

      window.close();
    }
  }, [router.query.token]);

  return <div />;
};
