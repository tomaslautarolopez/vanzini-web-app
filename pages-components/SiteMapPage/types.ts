import { Property } from "../../interfaces/properties";

export type SiteMapPageProps = {
  properties: Array<Property>;
  page: number;
};
