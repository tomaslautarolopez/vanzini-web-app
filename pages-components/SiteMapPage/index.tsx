import React from "react";
import { GetServerSidePropsResult, NextPage, NextPageContext } from "next";
import Link from "next/link";
import { buildUrlForProperty } from "../../utils/buildUrlForProperty";
import { fetchProperties } from "../../data-fetch/fetchProperties";

import { SiteMapPageProps } from "./types";

export const SiteMapPage: NextPage<SiteMapPageProps> = ({
  properties,
  page,
}) => {
  return (
    <div
      className="site-map-view"
      style={{
        paddingTop: 100,
      }}
    >
      <ul>
        {properties.map((prop) => (
          <li key={prop._id}>
            <Link
              href="/propiedades/[operationType-propertyType]/[propertyURI]"
              as={buildUrlForProperty(prop)}
            >
              <a>{prop.publicationTitle}</a>
            </Link>
          </li>
        ))}
      </ul>
      <div>
        {properties.length === 50 && (
          <Link href="/site-map/[page]" as={`/site-map/${page + 1}`}>
            <a>Siguiente página</a>
          </Link>
        )}
      </div>
    </div>
  );
};

export async function siteMapGetServerSideProps(
  context: NextPageContext
): Promise<GetServerSidePropsResult<SiteMapPageProps>> {
  const {
    query: { page },
  } = context;
  const { properties } = await fetchProperties(
    page ? Number(page) * 50 : undefined,
    50
  );

  return {
    props: {
      properties,
      page: page ? Number(page) : 0,
    },
  };
}
