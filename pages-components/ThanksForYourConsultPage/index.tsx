import { NextPage } from "next";
import Link from "next/link";

import PageFooter from "../../components/PageFooter";
import NewsletterBanner from "../../components/NewsletterBanner";
import ThanksForConsultLayout from "../../components/ThanksForConsultLayout";
import background from "../../assets/images/ThanksForYourConsult/thanks-for-your-consult-bg.jpg";

import styles from "./ThanksForYourConsultPage.module.scss";

export const ThanksForYourConsultPage: NextPage<{}> = () => {
  return (
    <div className={styles["thanks-for-your-consult-page"]}>
      <NewsletterBanner />
      <ThanksForConsultLayout
        background={background}
        contentClassName={styles["thanks-for-your-consult-page__content"]}
      >
        <h3 className="fw-700">Gracias por su consulta!</h3>
        <p>Uno de nuestros agentes se estara contactando a la brevedad.</p>
        <div
          className={
            styles[
              "thanks-for-your-consult-page__content--keep-searching-button"
            ]
          }
        >
          <Link href="/feed">
            <button className="black-custom-button">SEGUIR BUSCANDO</button>
          </Link>
          <h6>
            Ver{" "}
            <a href="/terminos-y-condiciones" target="_blank">
              términos y condiciones de uso.
            </a>
          </h6>
        </div>
      </ThanksForConsultLayout>
      <PageFooter />
    </div>
  );
};
