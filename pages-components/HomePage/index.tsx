import { NextPage } from "next";
import Head from "next/head";

import LogInBanner from "../../components/LogInBanner";
import PageFooter from "../../components/PageFooter";
import NewsletterSuscribeHero from "../../components/NewsletterSuscribeHero";
import HomeWhatSearchFor from "../../components/Home/HomeWhatSearchFor";
import HomeAboutUs from "../../components/Home/HomeAboutUs";
import HomeCategories from "../../components/Home/HomeCategories";
import hero from "../../assets/images/Home/Hero.jpg";
import PropertiesListFeedBanner from "../../components/PropertiesListFeedBanner";
import HomeMap from "../../components/Home/HomeMap";
import HomeFilters from "../../components/Home/HomeFilters";
import ResetPasswordModal from "../../components/ResetPasswordModal";
import HomeParallax from "../../components/Home/HomeParallax";

import styles from "./HomePage.module.scss";

export const HomePage: NextPage<{}> = () => {
  return (
    <div className={styles["home-page"]}>
      <Head>
        <title>Inmobiliaria en Rosario. Venta de propiedades | Vanzini</title>
        <meta
          name="description"
          content="Inmobiliaria en Rosario. Venta de propiedades, departamentos, casas, terrenos, oficinas, galpones y campos. Más de 50 años de EXPERIENCIA inmobiliaria."
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "https://schema.org",
              "@type": "Corporation",
              "name": "Vanzini Propiedades",
              "url": "https://www.vanzini.com.ar/",
              "logo": "https://www.vanzini.com.ar/logo-vanzini.svg",
              "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "03414255215",
                "contactType": "customer service",
                "areaServed": "AR",
                "availableLanguage": "es"
              },
              "sameAs": [
                "https://www.facebook.com/vanzini.prop",
                "https://twitter.com/VanziniProp",
                "https://www.instagram.com/vanzinipropiedades/",
                "https://www.youtube.com/user/VanziniPropiedades",
                "https://ar.pinterest.com/vanzinipropiedades/"
              ]
            }
              `,
          }}
        />
      </Head>
      <ResetPasswordModal />
      <HomeParallax>
        <div className={styles["home-page__header"]}>
          <h1>Expertos en Rosario</h1>
          <HomeFilters />
          <h5>
            Te damos las herramientas digitales para vender o comprar tu
            propiedad
          </h5>
        </div>
      </HomeParallax>
      <LogInBanner />
      <div className={styles["home-page__what-search-for"]}>
        <HomeWhatSearchFor />
      </div>
      <div className={styles["home-page__about-us"]}>
        <HomeAboutUs />
      </div>
      <div className={styles["home-page__categories"]}>
        <HomeCategories />
      </div>
      <div className={styles["home-page__properties-list"]}>
        <PropertiesListFeedBanner
          className={styles["feed-page__properties-list-banner"]}
        />
      </div>
      <div className={styles["home-page__hero"]}>
        <img src={hero} alt="Foto aerea de la ciudad de Rosario" />
      </div>
      <div className={styles["home-page__map"]}>
        <HomeMap />
      </div>
      <NewsletterSuscribeHero />
      <PageFooter />
    </div>
  );
};
