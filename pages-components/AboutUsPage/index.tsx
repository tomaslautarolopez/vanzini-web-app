import { Button } from "antd";
import { NextPage } from "next";
import { useRef } from "react";
import Head from "next/head";

import LogInBanner from "../../components/LogInBanner";
import PageFooter from "../../components/PageFooter";
import background from "../../assets/images/AboutUs/about-us-hero.jpg";
import city from "../../assets/images/AboutUs/city.jpg";
import AboutUsLocation from "../../components/AboutUs/AboutUsLocation";
import AboutUsEmployees from "../../components/AboutUs/AboutUsEmployees";
import AboutUsSocialMedia from "../../components/AboutUs/AboutUsSocialMedia";
import NewsletterSuscribeHero from "../../components/NewsletterSuscribeHero";

import styles from "./AboutUsPage.module.scss";

export const AboutUsPage: NextPage<{}> = () => {
  const ref = useRef<HTMLDivElement>(null);

  return (
    <div className={styles["about-us-page"]}>
      <Head>
        <title>
          Inmobiliaria en Rosario, contáctate con nosotros | Vanzini Propiedades
        </title>
        <meta
          name="description"
          content="About us: Inmobiliaria en Rosario, contáctate con nosotros. Desde 1965 liderando el mercado inmobiliario, ofreciendo una experiencia única en la comercialización de inmuebles. Expertos en Rosario | Vanzini Propiedades"
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "https://schema.org",
              "@type": "Corporation",
              "name": "Vanzini Propiedades",
              "url": "https://www.vanzini.com.ar/",
              "logo": "https://www.vanzini.com.ar/logo-vanzini.svg",
              "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "03414255215",
                "contactType": "customer service",
                "areaServed": "AR",
                "availableLanguage": "es"
              },
              "sameAs": [
                "https://www.facebook.com/vanzini.prop",
                "https://twitter.com/VanziniProp",
                "https://www.instagram.com/vanzinipropiedades/",
                "https://www.youtube.com/user/VanziniPropiedades",
                "https://ar.pinterest.com/vanzinipropiedades/"
              ]
            }
              `,
          }}
        />
      </Head>
      <LogInBanner />
      <div className={styles["about-us-page__header"]}>
        <h1>Sobre nosotros</h1>
        <h5>
          Desde 1965 liderando el mercado inmobiliario de Rosario y alrededores,
          ofreciendo una experiencia distinta
          <br /> en la comercialización de propiedades.
        </h5>
        <Button
          onClick={() =>
            ref.current?.scrollIntoView({
              behavior: "smooth",
            })
          }
          type="primary"
        >
          Conocer más
        </Button>
        <img src={background} alt="Foto de la ciudad de Rosario" />
      </div>
      <div ref={ref} className={styles["about-us-page__location"]}>
        <AboutUsLocation />
      </div>
      <div className={styles["about-us-page__city"]}>
        <img src={city} />
      </div>
      <div className={styles["about-us-page__employees"]}>
        <AboutUsEmployees />
      </div>
      <div className={styles["about-us-page__social-media"]}>
        <AboutUsSocialMedia />
      </div>
      <NewsletterSuscribeHero />
      <PageFooter />
    </div>
  );
};
