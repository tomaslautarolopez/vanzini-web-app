import { NextPage } from "next";
import Link from "next/link";

import PageFooter from "../../components/PageFooter";
import ThanksForConsultLayout from "../../components/ThanksForConsultLayout";
import background from "../../assets/images/ThanksForYourSuscription/thanks-for-your-suscription-bg.jpg";
import AssessBanner from "../../components/AssessBanner";

import styles from "./ThanksForYourSuscriptionPage.module.scss";

export const ThanksForYourSuscriptionPage: NextPage<{}> = () => {
  return (
    <div className={styles["thanks-for-your-suscription-page"]}>
      <AssessBanner />
      <ThanksForConsultLayout
        background={background}
        contentClassName={styles["thanks-for-your-suscription-page__content"]}
      >
        <h3 className="fw-700">Gracias por su suscripción!</h3>
        <p>
          Estaras recibiendo las últimas ofertas inmobiliarias a tu casilla de
          email.
        </p>
        <div
          className={
            styles[
              "thanks-for-your-suscription-page__content--keep-searching-button"
            ]
          }
        >
          <Link href="/feed">
            <button className="black-custom-button">SEGUIR BUSCANDO</button>
          </Link>
          <h6>
            Ver{" "}
            <a href="/terminos-y-condiciones" target="_blank">
              términos y condiciones de uso.
            </a>
          </h6>
        </div>
      </ThanksForConsultLayout>
      <PageFooter />
    </div>
  );
};
