import { Button } from "antd";
import { NextPage } from "next";
import { useContext } from "react";
import Head from "next/head";

import LogInBanner from "../../components/LogInBanner";
import PageFooter from "../../components/PageFooter";
import background from "../../assets/images/Assess/assess-page-bg.jpeg";
import hero from "../../assets/images/Assess/hero.jpg";
import NewsletterSuscribeHero from "../../components/NewsletterSuscribeHero";
import AssessMarketing from "../../components/Assess/AssessMarketing";
import AssessSocialMedia from "../../components/Assess/AssessSocialMedia";
import AssessNumbers from "../../components/Assess/AssessNumbers";
import AssessHowTo from "../../components/Assess/AssessHowTo";
import AssessQualityPictures from "../../components/Assess/AssessQualityPictures";
import AssessModal from "../../components/AssessModal";
import { ModalContext } from "../../contexts/ModalContext";

import styles from "./AssessPage.module.scss";

export const AssessPage: NextPage<{}> = () => {
  const { setModal } = useContext(ModalContext);
  return (
    <div className={styles["assess-page"]}>
      <Head>
        <title>Tasación de propiedades para venta | Vanzini Propiedades</title>
        <meta
          name="description"
          content="Tasación de propiedades para venta inmobiliaria. Valuamos tu departamento, casa, oficina, terreno y galpón con tecnología de vanguardia."
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "https://schema.org",
              "@type": "Corporation",
              "name": "Vanzini Propiedades",
              "url": "https://www.vanzini.com.ar/",
              "logo": "https://www.vanzini.com.ar/logo-vanzini.svg",
              "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "03414255215",
                "contactType": "customer service",
                "areaServed": "AR",
                "availableLanguage": "es"
              },
              "sameAs": [
                "https://www.facebook.com/vanzini.prop",
                "https://twitter.com/VanziniProp",
                "https://www.instagram.com/vanzinipropiedades/",
                "https://www.youtube.com/user/VanziniPropiedades",
                "https://ar.pinterest.com/vanzinipropiedades/"
              ]
            }
              `,
          }}
        />
      </Head>
      <LogInBanner />
      <AssessModal />
      <div className={styles["assess-page__header"]}>
        <h1>Tasaciones en 48 hs</h1>
        <h5>
          Bienvenido al portal inmobiliario más grande de Rosario y la región.
        </h5>
        <Button onClick={() => setModal("assessModal", true)} type="primary">
          Vender mi propiedad{" "}
        </Button>
        <img src={background} alt="Foto de la ciudad de Rosario" />
      </div>
      <div className={styles["assess-page__marketing"]}>
        <AssessMarketing />
      </div>
      <div className={styles["assess-page__how-to"]}>
        <AssessHowTo />
      </div>
      <div className={styles["assess-page__quality-pictures"]}>
        <AssessQualityPictures />
      </div>
      <div className={styles["assess-page__city"]}>
        <img src={hero} />
      </div>
      <div className={styles["assess-page__social-media"]}>
        <AssessSocialMedia />
      </div>
      <div className={styles["assess-page__numbers"]}>
        <AssessNumbers />
      </div>
      <NewsletterSuscribeHero />
      <PageFooter />
    </div>
  );
};
