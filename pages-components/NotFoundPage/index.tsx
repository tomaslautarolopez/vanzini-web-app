import { NextPage } from "next";
import Link from "next/link";

import PageFooter from "../../components/PageFooter";
import ThanksForConsultLayout from "../../components/ThanksForConsultLayout";
import background from "../../assets/images/NotFound/not-found-bg.jpg";
import AlertIcon from "../../assets/icons/not-found/Alert.svg";

import styles from "./NotFoundPage.module.scss";

export const NotFoundPage: NextPage<{}> = () => {
  return (
    <div className={styles["not-found-page"]}>
      <ThanksForConsultLayout
        hideBackButton
        background={background}
        contentClassName={styles["not-found-page__content"]}
      >
        <h6 className={styles["not-found-page__header"]}>
          <img src={AlertIcon} alt="Escudo de armas vanzini" /> ERROR 404
        </h6>
        <h3 className="fw-700">Parece que estas perdido!</h3>
        <p>
          No encontramos la página que estabas buscando. Puede ser que se haya
          retirado de la
          <br /> venta o el link esta roto.
        </p>
        <div className={styles["not-found-page__content--go-to-home"]}>
          <Link href="/">
            <button className="black-custom-button">IR AL HOME</button>
          </Link>
          <h6>
            Ver{" "}
            <a href="/terminos-y-condiciones" target="_blank">
              términos y condiciones de uso.
            </a>
          </h6>
        </div>
      </ThanksForConsultLayout>
      <PageFooter />
    </div>
  );
};
