import { GetServerSidePropsResult, NextPage, NextPageContext } from "next";
import { useContext, useState } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import Head from "next/head";

import { PropertyDetailsPageProps } from "./type";
import { fetchProperties } from "../../data-fetch/fetchProperties";
import PropertyDetailsTitleAndCarousel from "../../components/PropertyDetails/PropertyDetailsTitleAndCarousel";
import FloatingConsultButton from "../../components/FloatingConsultButton";
import PropertyDetailsActions from "../../components/PropertyDetails/PropertyDetailsActions";
import PropertyDetailsPrice from "../../components/PropertyDetails/PropertyDetailsPrice";
import PropertyDetailsMessures from "../../components/PropertyDetails/PropertyDetailsMessures";
import PropertyDetailsCharacteristics from "../../components/PropertyDetails/PropertyDetailsCharacteristics";
import PropertyDetailsAgeBar from "../../components/PropertyDetails/PropertyDetailsAgeBar";
import PropertyDetailsCheckbox from "../../components/PropertyDetails/PropertyDetailsCheckbox";
import {
  roomsAndDistributionTags,
  amenitiesTags as amenitiesTagsConstant,
  airConditioningTags as airConditioningTagsConstant,
  hotWaterTags as hotWaterTagsConstant,
  securityTags as securityTagsConstant,
  servicesTags as servicesTagsConstant,
} from "../../constants/tags";
import PropertyDetailsDescription from "../../components/PropertyDetails/PropertyDetailsDescription";
import PropertyDetailsServicesTag from "../../components/PropertyDetails/PropertyDetailsServicesTag";
import PropertyDetailsRecommendations from "../../components/PropertyDetails/PropertyDetailsRecommendations";
import PageFooter from "../../components/PageFooter";
import ConsultForm from "../../components/ConsultForm";
import PropertyDetailsSideBarActions from "../../components/PropertyDetails/PropertyDetailsSideBarActions";
import PropertyDetailsMobileFloatingActions from "../../components/PropertyDetails/PropertyDetailsMobileFloatingActions";
import PropertyDetailsDevelopmentsTables from "../../components/PropertyDetails/PropertyDetailsDevelopmentsTables";
import { isDevelopment } from "../../utils/isDevelopment";
import MobileActions from "../../components/MobileActions";
import { DrawerContext } from "../../contexts/DrawerContext";
import { buildRoomsAmountString } from "../../utils/buildRoomsAmountString";
import { propertyTypeOptions } from "../../constants/filters";

import BlueBulletIcon from "../../assets/icons/property-details/bullet-blue.svg";
import MagentaBulletIcon from "../../assets/icons/property-details/bullet-magenta.svg";
import GrayBulletIcon from "../../assets/icons/property-details/bullet-gray.svg";
import RedBulletIcon from "../../assets/icons/property-details/bullet-red.svg";
import FiltersButton from "../../assets/icons/feed/filter-button.svg";
import ArrowButton from "../../assets/icons/property-details/arrow.svg";
import NewsletterBanner from "../../components/NewsletterBanner";
import NoImages from "../../assets/images/no-images.jpg";

import styles from "./PropertyDetailsPage.module.scss";
import { UserContext } from "../../contexts/UserContext";
import { useEffect } from "react";
import { propertyLocationFinder } from "../../utils/propertyLocationFinder";

const MapWithNoSSR = dynamic(
  () => import("../../components/PropertyDetails/PropertyDetailsMap"),
  {
    ssr: false,
  }
);

export const PropertyDetailsPage: NextPage<PropertyDetailsPageProps> = ({
  property,
}) => {
  const { back } = useRouter();
  const { setDrawer } = useContext(DrawerContext);
  const { markPropertyAsSeen, user } = useContext(UserContext);
  const [units, setUnits] = useState<string[]>([]);

  const location = propertyLocationFinder(property);

  useEffect(() => {
    markPropertyAsSeen(property._id);
  }, [user.data?._id]);

  const distributionTags = property.tags.filter((tag) =>
    roomsAndDistributionTags.includes(tag)
  );

  const amenitiesTags = property.tags.filter((tag) =>
    amenitiesTagsConstant.includes(tag)
  );

  const hotWaterTags = property.tags.filter((tag) =>
    hotWaterTagsConstant.includes(tag)
  );
  const securityTags = property.tags.filter((tag) =>
    securityTagsConstant.includes(tag)
  );
  const airConditioningTags = property.tags.filter((tag) =>
    airConditioningTagsConstant.includes(tag)
  );
  const servicesTags = property.tags.filter((tag) =>
    servicesTagsConstant.includes(tag)
  );

  const categoryTitle = propertyTypeOptions.find(
    (opt) => opt.value === property.propertyType
  );

  const fstImage =
    property && property.photos && property.photos[0]
      ? property.photos[0].image
      : NoImages;

  return (
    <div className={styles["property-details-page"]}>
      <Head>
        <title>
          {`Venta de ${
            categoryTitle ? categoryTitle.label : "inmueble"
          } ${buildRoomsAmountString(property)} en ${
            property.firstSubDivision
          }, ${property.secondSubDivision} | Vanzini Propiedades`}
        </title>
        <meta
          name="description"
          content={`${property.address}, venta de ${
            categoryTitle ? categoryTitle.label : "inmueble"
          } | ${property.description.slice(0, 90)}`}
        />
        <meta property="og:image" content={`${fstImage}`} />
      </Head>
      <FloatingConsultButton
        className={styles["property-details-page__floating-button"]}
      />
      <NewsletterBanner
        className={styles["property-details-page__newsletter-banner"]}
      />
      <MobileActions
        className={styles["property-details-page__mobile-actions"]}
        actions={[
          {
            img: ArrowButton,
            label: "Atras",
            action: () => back(),
          },
          {
            img: FiltersButton,
            label: "Filtros",
            action: () => {
              setDrawer("filters", true);
            },
          },
        ]}
      />
      <div className={styles["property-details-page__details"]}>
        <div className={styles["property-details-page__details--container"]}>
          <PropertyDetailsTitleAndCarousel property={property} />
          <PropertyDetailsActions
            property={property}
            className={styles["property-details-page__details--actions"]}
          />
          {isDevelopment(property.propertyType) && (
            <PropertyDetailsDevelopmentsTables
              setUnits={setUnits}
              property={property}
            />
          )}
          {!isDevelopment(property.propertyType) && (
            <div
              className={
                styles["property-details-page__details--price-and-messures"]
              }
            >
              <PropertyDetailsPrice
                property={property}
                className={styles["property-details-page__details--price"]}
              />
              <PropertyDetailsMessures
                property={property}
                className={styles["property-details-page__details--messures"]}
              />
            </div>
          )}
          {!isDevelopment(property.propertyType) && (
            <PropertyDetailsCharacteristics property={property} />
          )}
          <PropertyDetailsAgeBar
            className={styles["property-details-page__details--age"]}
            property={property}
          />
          <div
            className={
              styles[
                "property-details-page__details--distribution-and-amenities"
              ]
            }
          >
            {!!distributionTags.length && (
              <PropertyDetailsCheckbox
                title="Distribución"
                color="blue"
                iconImg={BlueBulletIcon}
                tags={distributionTags}
                className={
                  styles["property-details-page__details--distribution"]
                }
              />
            )}
            {!!amenitiesTags.length && (
              <PropertyDetailsCheckbox
                title="Amenities"
                color="magenta"
                iconImg={MagentaBulletIcon}
                tags={amenitiesTags}
                className={styles["property-details-page__details--amenities"]}
              />
            )}
          </div>
          <PropertyDetailsDescription property={property} />
          <div className={styles["property-details-page__details--services"]}>
            <PropertyDetailsServicesTag
              iconImg={BlueBulletIcon}
              color="blue"
              title="Agua Caliente"
              services={hotWaterTags}
            />
            <PropertyDetailsServicesTag
              iconImg={GrayBulletIcon}
              color="gray"
              title="Climatización"
              services={airConditioningTags}
            />
            <PropertyDetailsServicesTag
              iconImg={RedBulletIcon}
              color="red"
              title="Seguridad"
              services={securityTags}
            />
            <PropertyDetailsServicesTag
              iconImg={MagentaBulletIcon}
              color="magenta"
              title="Servicios"
              services={servicesTags}
            />
          </div>
          <MapWithNoSSR property={property} />
          <PropertyDetailsRecommendations
            className={
              styles["property-details-page__details--recommendations"]
            }
            property={property}
          />
        </div>
        <div
          className={
            styles["property-details-page__details--consult-and-likes"]
          }
        >
          <div
            className={styles["property-details-page__details--consult-form"]}
          >
            <ConsultForm
              location={location}
              propertiesId={[
                {
                  id: property._id,
                  propertyType: property.propertyType,
                  units,
                },
              ]}
            />
          </div>
          <PropertyDetailsSideBarActions property={property} />
        </div>
      </div>
      <PropertyDetailsMobileFloatingActions property={property} />
      <PageFooter />
    </div>
  );
};

export async function propertyDetailsGetServerSideProps(
  context: NextPageContext
): Promise<GetServerSidePropsResult<PropertyDetailsPageProps>> {
  const { propertySlug } = context.query;
  const propertyId = (propertySlug as string).split("__")[1];

  const { properties } = await fetchProperties(0, 1, {
    id: [propertyId],
  });

  return {
    props: { property: properties[0] },
  };
}
