import { Property } from "../../interfaces/properties";

export interface PropertyDetailsPageProps {
  property: Property;
}
