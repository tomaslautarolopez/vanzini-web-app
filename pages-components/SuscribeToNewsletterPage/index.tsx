import { NextPage } from "next";

import PageFooter from "../../components/PageFooter";
import NewsletterSuscribeHero from "../../components/NewsletterSuscribeHero";

import styles from "./SuscribeToNewsletterPage.module.scss";

export const SuscribeToNewsletterPage: NextPage<{}> = () => {
  return (
    <div className={styles["suscribe-to-newsletter-page"]}>
      <NewsletterSuscribeHero />
      <PageFooter />
    </div>
  );
};
