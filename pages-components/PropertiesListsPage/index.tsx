import { useContext, useState } from "react";
import { GetServerSidePropsResult, NextPage } from "next";
import { Pagination } from "antd";
import Head from "next/head";

import star from "../../assets/icons/properties-lists/star.svg";
import background from "../../assets/images/PropertiesLists/properties-lists-header-bg.jpg";
import { PropertiesListsPageProps } from "./type";
import { fetchPropertiesLists } from "../../data-fetch/fetchPropertiesLists";
import NewsletterBanner from "../../components/NewsletterBanner";
import PropertiesListCard from "../../components/PropertiesListCard";
import PageFooter from "../../components/PageFooter";
import MobileActions from "../../components/MobileActions";
import Map from "../../assets/icons/feed/map.svg";
import FiltersButton from "../../assets/icons/feed/filter-button.svg";
import { DrawerContext } from "../../contexts/DrawerContext";

import styles from "./PropertiesListsPage.module.scss";

const PAGE_SIZE = 6;

export const PropertiesListsPage: NextPage<PropertiesListsPageProps> = ({
  propertiesLists,
}) => {
  const [page, setPage] = useState(1);
  const { setDrawer } = useContext(DrawerContext);

  return (
    <div className={styles["properties-lists-page"]}>
      <Head>
        <title>Clasificado de propiedades en venta en Rosario</title>
        <meta
          name="description"
          content="Selección de propiedades en venta en la ciudad de Rosario. Financiación, terminados, de pozo y de contado. Calidad constructiva y ubicaiones mas buscadas de la ciudad de Rosario."
        />
      </Head>
      <NewsletterBanner />
      <MobileActions
        className={styles["properties-lists-page__mobile-actions"]}
        actions={[
          {
            img: Map,
            label: "Mapa",
            action: () => {},
          },
          {
            img: FiltersButton,
            label: "Filtros",
            action: () => {
              setDrawer("filters", true);
            },
          },
        ]}
      />
      <div className={styles["properties-lists-page__header"]}>
        <div className={styles["properties-lists-page__header--content"]}>
          <img src={star} />
          <h1>Listados Vanzini Propiedades</h1>
          <h5>
            Casas, departamentos, oficinas, terrenos y emprendimientos en
            Rosario y la región, actualizada semanalmente.
          </h5>
        </div>
        <img
          src={background}
          className={styles["properties-lists-page__header--background"]}
        />
      </div>
      <div className={styles["properties-lists-page__list"]}>
        <div className={styles["properties-lists-page__list--amount"]}>
          <h6>
            <b>{propertiesLists.length}</b> Clasificados
          </h6>
        </div>
        {propertiesLists
          .slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE)
          .map((propertiesList) => (
            <PropertiesListCard
              className={styles["properties-lists-page__list--card"]}
              propertiesList={propertiesList}
            />
          ))}
        <Pagination
          onChange={(page) => {
            setPage(page);
          }}
          className={styles["properties-lists-page__list--pagination"]}
          defaultCurrent={1}
          defaultPageSize={PAGE_SIZE}
          total={propertiesLists.length}
        />
      </div>
      <PageFooter />
    </div>
  );
};

export async function propertiesListsGetServerSideProps(): Promise<
  GetServerSidePropsResult<PropertiesListsPageProps>
> {
  const propertiesLists = await fetchPropertiesLists();

  return {
    props: { propertiesLists },
  };
}
