import { PropertiesList } from "../../hooks/useGetPropertiesListForFeed";

export interface PropertiesListsPageProps {
  propertiesLists: PropertiesList[];
}
