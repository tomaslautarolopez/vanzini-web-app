import { PropertiesList } from "../../hooks/useGetPropertiesListForFeed";

export interface PropertiesListsViewPageProps {
  propertiesList: PropertiesList;
  propertiesLists: PropertiesList[];
}
