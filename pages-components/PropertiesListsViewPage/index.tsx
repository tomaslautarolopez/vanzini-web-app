import { useContext, useRef, useState } from "react";
import { GetServerSidePropsResult, NextPage, NextPageContext } from "next";
import { useTransition, animated } from "react-spring";
import { Button } from "antd";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  ShareAltOutlined,
  UnorderedListOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import Head from "next/head";

import star from "../../assets/icons/properties-lists/star.svg";
import { PropertiesListsViewPageProps } from "./type";
import {
  fetchPropertiesList,
  fetchPropertiesLists,
} from "../../data-fetch/fetchPropertiesLists";
import NewsletterBanner from "../../components/NewsletterBanner";
import PageFooter from "../../components/PageFooter";
import MobileActions from "../../components/MobileActions";
import Map from "../../assets/icons/feed/map.svg";
import FiltersButton from "../../assets/icons/feed/filter-button.svg";
import { DrawerContext } from "../../contexts/DrawerContext";
import ShareButtons from "../../components/ShareButtons";
import { useOutsideAlerter } from "../../hooks/useOutsideAlerter";
import PropertyCard from "../../components/PropertyCard";
import { webURL } from "../../axios";

import styles from "./PropertiesListsViewPage.module.scss";

export const PropertiesListsViewPage: NextPage<PropertiesListsViewPageProps> = ({
  propertiesList,
  propertiesLists,
}) => {
  const { setDrawer } = useContext(DrawerContext);
  const shareRef = useRef<HTMLDivElement>(null);
  const [toggle, setToggle] = useState(false);

  useOutsideAlerter(shareRef, () => setToggle(false));

  const transitions = useTransition(toggle, null, {
    from: { position: "absolute", opacity: 0, zIndex: 100 },
    enter: { opacity: 1, zIndex: 100 },
    leave: { opacity: 0, zIndex: 100 },
  });
  const prevIndex =
    propertiesLists.findIndex((pl) => pl.slug === propertiesList.slug) - 1;
  const postIndex =
    propertiesLists.findIndex((pl) => pl.slug === propertiesList.slug) + 1;

  return (
    <div className={styles["properties-list-view-page"]}>
      <Head>
        <title>Clasificado de {propertiesList.title} en venta en Rosario</title>
        <meta name="description" content={propertiesList.description} />
        <meta property="og:image" content={`${propertiesList.portrait}`} />
      </Head>
      <NewsletterBanner />
      <MobileActions
        className={styles["properties-list-view-page__mobile-actions"]}
        actions={[
          {
            img: Map,
            label: "Mapa",
            action: () => {},
          },
          {
            img: FiltersButton,
            label: "Filtros",
            action: () => {
              setDrawer("filters", true);
            },
          },
        ]}
      />
      <div className={styles["properties-list-view-page__header"]}>
        <div className={styles["properties-list-view-page__header--content"]}>
          <img src={star} />
          <h1>{propertiesList.title}</h1>
          <h5>{propertiesList.description}</h5>
        </div>
        <div className={styles["properties-list-view-page__header--actions"]}>
          <div
            className={
              styles["properties-list-view-page__header--share-and-like"]
            }
          >
            <div
              ref={shareRef}
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              {transitions.map(({ item, props }) =>
                item ? (
                  <animated.div style={props}>
                    <ShareButtons
                      url={`${webURL}/listados/${propertiesList.slug}`}
                      text={`Te comparto esta lista de propiedades de Vanzini Inmobiliaria: ${propertiesList.title}`}
                    />
                  </animated.div>
                ) : (
                  <animated.div style={props}>
                    <Button onClick={() => setToggle(true)} type="dashed">
                      <ShareAltOutlined /> Compartir Listado
                    </Button>
                  </animated.div>
                )
              )}
            </div>
            <Link href="/listados">
              <Button type="dashed">
                <UnorderedListOutlined /> Otros Listados
              </Button>
            </Link>
          </div>
          <div>
            {prevIndex >= 0 && (
              <Link href={`/listados/${propertiesLists[prevIndex].slug}`}>
                <button
                  className={styles["properties-list-view-page__gray-button"]}
                >
                  <ArrowLeftOutlined /> Anterior
                </button>
              </Link>
            )}
            {postIndex <= propertiesLists.length - 1 && (
              <Link href={`/listados/${propertiesLists[postIndex].slug}`}>
                <button
                  className={styles["properties-list-view-page__gray-button"]}
                >
                  Siguiente <ArrowRightOutlined />
                </button>
              </Link>
            )}
          </div>
        </div>
        <img
          src={propertiesList.portrait}
          className={styles["properties-list-view-page__header--background"]}
        />
      </div>
      <div className={styles["properties-list-view-page__list"]}>
        {propertiesList.properties?.map((property) => (
          <PropertyCard
            property={property}
            className={styles["properties-list-view-page__property-card"]}
          />
        ))}
      </div>
      <PageFooter />
    </div>
  );
};

export async function propertiesListViewGetServerSideProps(
  context: NextPageContext
): Promise<GetServerSidePropsResult<PropertiesListsViewPageProps>> {
  const { slug } = context.query;

  const propertiesList = await fetchPropertiesList(slug as string);
  const propertiesLists = await fetchPropertiesLists();

  return {
    props: { propertiesList, propertiesLists },
  };
}
