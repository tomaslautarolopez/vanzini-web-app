export enum Location {
  FISHERTON = 'FISHERTON',
  HEAD_OFFICE = 'HEAD_OFFICE',
}

export interface Employee {
  _id: string;
  location: Location,
  email: string;
  phonenumber: string;
  name: string;
  employeeType: string; // This is the role of the employee inside the organization
  pictureUrl: string;
}
