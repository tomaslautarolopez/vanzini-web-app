import { Property } from "./properties";

export enum NOTIFICATIONS_TYPE {
  LOWER_PRICE = "LOWER_PRICE",
}

export interface NotificationBase {
  creationDate: number;
  seen: boolean;
  type: NOTIFICATIONS_TYPE;
  userId: string;
  _id: string;
}

export interface LowerPriceNotification extends NotificationBase {
  information: {
    property: Property;
  };
  type: NOTIFICATIONS_TYPE.LOWER_PRICE;
}

export type Notification = LowerPriceNotification;
