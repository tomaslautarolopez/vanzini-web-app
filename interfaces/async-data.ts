export interface AsyncData<T, M = boolean> {
  data: T | null;
  loading: M;
  error: any;
}
