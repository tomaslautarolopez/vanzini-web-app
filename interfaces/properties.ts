export enum PropertiesTypes {
  HO = "HO",
  PH = "PH",
  AP = "AP",
  LA = "LA",
  LO = "LO",
  CO = "CO",
  GL = "GL",
  SR = "SR",
  OF = "OF",
  GA = "GA",
  CS = "CS",
  BU = "BU",
  LOT = "LOT",
}

export interface Branch {
  address: string;
  alternative_phone: string;
  alternative_phone_area: string;
  alternative_phone_country_code: string;
  alternative_phone_extension: string;
  branch_type: string;
  contact_time: string;
  created_date: string;
  display_name: string;
  email: string;
  geo_lat: string;
  geo_long: string;
  gm_location_type: string;
  id: number;
  is_default: boolean;
  logo: string;
  name: string;
  pdf_footer_text: string;
  phone: string;
  phone_area: string;
  phone_country_code: string;
  phone_extension: string;
  use_pdf_footer: boolean;
}


export interface Video {
  description: string;
  id: number;
  order: number;
  player_url: string;
  provider: string;
  provider_id: number;
  title: string;
  url: string;
  video_id: string;
};

export type Operation = {
  priceUSDmin: number;
  priceUSDmax: number;
  pricePesosMin: number;
  pricePesosMax: number;
  currencyFromTokko: Currency;
};

export type Tag = {
  id: number;
  name: string;
  type: number;
};

export type Photo = {
  description: string;
  image: string;
  is_blueprint: boolean;
  is_front_cover: boolean;
  order: number;
  original: string;
  thumb: string;
};

export type Producer = {
  cellphone: string;
  email: string;
  id: number;
  name: string;
  phone: string;
  picture: string;
};

export type DevelopmentProperty = {
  address: string;
  id: string;
  surface: number;
  totalSurface: number;
  roofedSurface: number;
  propertyType: PropertiesTypes;
  semiroofedSurface: number;
  operationSell: Operation;
  operationRent: Operation;
  roomAmount: number;
  webPrice: boolean;
  addressSlug: string;
  creationDate: number;
  age: number;
  producer: Producer;
  rent: boolean;
  sell: boolean;
};

export type CustomTagsInfo = {
  id: number;
  group_name: string;
  name: string;
  public_name: string;
};

export type Property = {
  _id: string;
  branch?: Branch;
  roomAmount: number;
  bathroomAmount: number;
  toiletAmount: number;
  garageAmount: number;
  publicationTitle: string;
  operationType: string;
  situation: string;
  disposition: string;
  expenses: number;
  age: number;
  constructionStatus: number;
  webPrice: boolean;
  hasGarage: boolean;
  geoLocation: {
    type: string;
    coordinates: [number, number];
  };
  sell: boolean;
  rent: boolean;
  subPropertiesTypes?: Array<PropertiesTypes>;
  creationDate: number;
  location: string;
  urbanLocation: string;
  neighborhood: string;
  producer: Producer;
  operationSell: Operation;
  operationRent: Operation;
  lastOperationSell: Operation;
  constructionDate: string;
  unroofedSurface: number;
  roofedSurface: number;
  semiroofedSurface: number;
  totalSurfaceMin: number;
  totalSurfaceMax: number;
  surface: number;
  description: string;
  tags: Array<string>;
  videos?: Array<Video>;
  tagsInfo: Array<Tag>;
  customTags?: Array<string>;
  customTagsInfo?: Array<CustomTagsInfo>;
  address: string;
  addressSlug: string;
  propertyType: PropertiesTypes;
  photos: Array<Photo>;
  developmentProperties: Array<DevelopmentProperty>;
  propertyCondition: string;
  orientation?: string;
  ambientAmount?: number;
  floorsAmount?: number;
  firstSubDivision?: string;
  secondSubDivision?: string;
  country?: string;
  zonification: string;
};

export enum Currency {
  USD = "USD",
  ARS = "ARS",
}
