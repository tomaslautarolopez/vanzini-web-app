/// <reference types="next" />
/// <reference types="next/types/global" />
declare module "*.svg" {
  const content: string;
  export default content;
}
declare module "*.jpg" {
  const content: string;
  export default content;
}
declare module "*.jpeg" {
  const content: string;
  export default content;
}
declare module "*.png" {
  const content: string;
  export default content;
}
declare module "react-leaflet-markercluster" {
  import { Component } from "react";
  export default class MarkerClusterGroup extends Component {}
}
declare module "react-image-lightbox-rotate";
