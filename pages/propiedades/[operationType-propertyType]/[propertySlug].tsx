import {
  PropertyDetailsPage,
  propertyDetailsGetServerSideProps,
} from "../../../pages-components/PropertyDetailsPage";

export const getServerSideProps = propertyDetailsGetServerSideProps;

export default PropertyDetailsPage;
