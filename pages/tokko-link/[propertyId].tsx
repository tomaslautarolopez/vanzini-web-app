import { GetServerSidePropsResult, NextPageContext } from "next";
import { fetchProperties } from "../../data-fetch/fetchProperties";
import { PropertyDetailsPage } from "../../pages-components/PropertyDetailsPage";
import { PropertyDetailsPageProps } from "../../pages-components/PropertyDetailsPage/type";

export async function getServerSideProps(
  context: NextPageContext
): Promise<GetServerSidePropsResult<PropertyDetailsPageProps>> {
  const { propertyId } = context.query;

  const { properties } = await fetchProperties(0, 1, {
    id: [propertyId as string],
  });

  return {
    props: { property: properties[0] },
  };
}

export default PropertyDetailsPage;
