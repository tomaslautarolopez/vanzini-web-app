import {
  PropertiesListsPage,
  propertiesListsGetServerSideProps,
} from "../pages-components/PropertiesListsPage";

export const getServerSideProps = propertiesListsGetServerSideProps;

export default PropertiesListsPage;
