import { useEffect, useState } from "react";
import type { AppProps } from "next/app";
import Router, { useRouter } from "next/router";
import dayjs from "dayjs";
import NProgress from "nprogress";
import Head from "next/head";
import getConfig from "next/config";

import { FiltersContextProvider } from "../contexts/FiltersContext";
import { UserContextProvider } from "../contexts/UserContext";
import { ModalContextProvider } from "../contexts/ModalContext";
import { EmployeeContextProvider } from "../contexts/EmployeeContext";
import NavBar from "../components/NavBar";
import MobileNavBar from "../components/MobileNavBar";
import ConsultModal from "../components/ConsultModal";
import NewsletterModal from "../components/NewsletterModal";
import FiltersMobile from "../components/FiltersMobile";
import { DrawerContextProvider } from "../contexts/DrawerContext";
import Loading from "../components/Loading";
import LogInModal from "../components/LogInModal";
import SignUpModal from "../components/SignUpModal";
import LogOutModal from "../components/LogOutModal";
import ForgotPasswordModal from "../components/ForgotPasswordModal";
import AlertModal from "../components/AlertModal";
import RemoveAlertModal from "../components/RemoveAlertModal";

import "dayjs/locale/es";
import "nprogress/nprogress.css";
import "antd/dist/antd.css";
import "../assets/styles/global.scss";
import "../assets/styles/animations.scss";
import "../assets/styles/antd-refactor.scss";
import "../assets/styles/leaflet-refactor.scss";
import "../assets/styles/react-modal-refactor.scss";

const logo = require("../assets/images/logo.png");

dayjs.locale("es");

const {
  publicRuntimeConfig: { ENVIROMENT },
} = getConfig();

const MyApp = ({ Component, pageProps }: AppProps) => {
  const [loadingPage, setLoadingPage] = useState(false);
  const { pathname } = useRouter();

  useEffect(() => {
    Router.events.on("routeChangeStart", () => {
      NProgress.start();
      setLoadingPage(true);
    });
    Router.events.on("routeChangeComplete", () => {
      NProgress.done();
      setLoadingPage(false);
    });
    Router.events.on("routeChangeError", () => {
      NProgress.done();
      setLoadingPage(false);
    });
  }, []);

  return (
    <UserContextProvider>
      <ModalContextProvider>
        <FiltersContextProvider>
          <DrawerContextProvider>
            <EmployeeContextProvider>
              <Head>
                <link
                  rel="apple-touch-icon"
                  sizes="57x57"
                  href="/apple-icon-57x57.png"
                />
                <link
                  rel="apple-touch-icon"
                  sizes="60x60"
                  href="/apple-icon-60x60.png"
                />
                <link
                  rel="apple-touch-icon"
                  sizes="72x72"
                  href="/apple-icon-72x72.png"
                />
                <link
                  rel="apple-touch-icon"
                  sizes="76x76"
                  href="/apple-icon-76x76.png"
                />
                <link
                  rel="apple-touch-icon"
                  sizes="114x114"
                  href="/apple-icon-114x114.png"
                />
                <link
                  rel="apple-touch-icon"
                  sizes="120x120"
                  href="/apple-icon-120x120.png"
                />
                <link
                  rel="apple-touch-icon"
                  sizes="144x144"
                  href="/apple-icon-144x144.png"
                />
                <link
                  rel="apple-touch-icon"
                  sizes="152x152"
                  href="/apple-icon-152x152.png"
                />
                <link
                  rel="apple-touch-icon"
                  sizes="180x180"
                  href="/apple-icon-180x180.png"
                />
                <link
                  rel="icon"
                  type="image/png"
                  sizes="192x192"
                  href="/android-icon-192x192.png"
                />
                <link
                  rel="icon"
                  type="image/png"
                  sizes="32x32"
                  href="/favicon-32x32.png"
                />
                <link
                  rel="icon"
                  type="image/png"
                  sizes="96x96"
                  href="/favicon-96x96.png"
                />
                <link
                  rel="icon"
                  type="image/png"
                  sizes="16x16"
                  href="/favicon-16x16.png"
                />
                <link rel="manifest" href="/manifest.json" />
                <meta name="msapplication-TileColor" content="#ffffff" />
                <meta
                  name="msapplication-TileImage"
                  content="/ms-icon-144x144.png"
                />
                <meta name="theme-color" content="#ffffff" />
                {pathname !==
                  "/propiedades/[operationType-propertyType]/[propertySlug]" && (
                  <meta property="og:image" content={`${logo}`} />
                )}
                {ENVIROMENT === "production" && (
                  <>
                    <script
                      dangerouslySetInnerHTML={{
                        __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-MK7J5RN');`,
                      }}
                    />
                    <noscript
                      dangerouslySetInnerHTML={{
                        __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MK7J5RN"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe>`,
                      }}
                    />
                  </>
                )}
              </Head>
              <div className="app-page">
                <RemoveAlertModal />
                <ForgotPasswordModal />
                <LogOutModal />
                <LogInModal />
                <SignUpModal />
                <NavBar />
                <MobileNavBar />
                <ConsultModal />
                <AlertModal />
                <FiltersMobile />
                <NewsletterModal />
                <Component loadingPage={loadingPage} {...pageProps} />
                {loadingPage && (
                  <div className="app-page__loading">
                    <Loading />
                  </div>
                )}
              </div>
            </EmployeeContextProvider>
          </DrawerContextProvider>
        </FiltersContextProvider>
      </ModalContextProvider>
    </UserContextProvider>
  );
};
export default MyApp;
