import {
  SiteMapPage,
  siteMapGetServerSideProps,
} from "../pages-components/SiteMapPage";

export const getServerSideProps = siteMapGetServerSideProps;

export default SiteMapPage;
