import { FeedPage, feedGetServerSideProps } from "../pages-components/FeedPage";

export const getServerSideProps = feedGetServerSideProps;

export default FeedPage;
