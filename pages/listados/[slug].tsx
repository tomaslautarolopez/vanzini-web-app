import {
  PropertiesListsViewPage,
  propertiesListViewGetServerSideProps,
} from "../../pages-components/PropertiesListsViewPage";

export const getServerSideProps = propertiesListViewGetServerSideProps;

export default PropertiesListsViewPage;
