import {
  Operation,
  Property,
  Currency,
  DevelopmentProperty,
} from "../interfaces/properties";

const getMaxOperationCurrentPrice = (
  operation: Operation
): number | undefined => {
  const { currencyFromTokko } = operation;

  if (currencyFromTokko === Currency.ARS) {
    return operation.pricePesosMax;
  } else if (currencyFromTokko === Currency.USD) {
    return operation.priceUSDmax;
  }
};

export const getPropertyPrice = (
  property: Property | DevelopmentProperty
): {
  price: number | undefined;
  currency: Currency;
  operationType: String;
} | null => {
  const { rent, sell, operationRent, operationSell, webPrice } = property;

  if (!webPrice) {
    return null;
  }

  if (rent) {
    return {
      price: getMaxOperationCurrentPrice(operationRent),
      currency: operationRent.currencyFromTokko,
      operationType: "ALQUILER",
    };
  } else if (sell) {
    return {
      price: getMaxOperationCurrentPrice(operationSell),
      currency: operationSell.currencyFromTokko,
      operationType: "VENTA",
    };
  }

  return null;
};
