import { PropertiesTypes } from "../interfaces/properties";

export const shouldConsiderRooms = (propertyType: PropertiesTypes) =>
  propertyType === "AP" || propertyType === "HO";
