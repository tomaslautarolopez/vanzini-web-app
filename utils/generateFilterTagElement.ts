export const generateFilterTagElement = (
  options: Array<{ value: any; label: string }>,
  values: Array<string> | undefined,
  setter: (values: Array<any>) => void
) =>
  values
    ? options
        .filter((opt) => !!values.find((val) => val === opt.value))
        .map(({ value, label }) => ({
          value,
          label,
          onRemove: (value: string) =>
            setter(values.filter((val) => val !== value)),
        }))
    : [];
