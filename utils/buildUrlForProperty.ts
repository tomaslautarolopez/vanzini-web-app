import { Property, PropertiesTypes } from "../interfaces/properties";
import { isDevelopment } from "./isDevelopment";
import { propertyTypeToString } from "./propertyTypeToString";

export const buildUrlForProperty = ({
  sell,
  rent,
  propertyType,
  roomAmount,
  firstSubDivision,
  secondSubDivision,
  addressSlug,
  _id,
}: Property): string => {
  const operationType =
    sell && rent ? "venta-o-alquiler" : rent ? "alquiler" : "venta";
  const type = propertyTypeToString(propertyType);
  const isPropDevelopment = isDevelopment(propertyType);
  const showRoomsType =
    propertyType === PropertiesTypes.HO ||
    propertyType == PropertiesTypes.PH ||
    propertyType === PropertiesTypes.AP;

  const rooms =
    isPropDevelopment || !showRoomsType
      ? ""
      : roomAmount === 0
      ? "monoambiente-"
      : `${roomAmount}-dormitorios-`;
  const urbanLocation = firstSubDivision
    ? `${firstSubDivision.toLowerCase()}-`
    : "";
  const neighborhood = secondSubDivision
    ? `${secondSubDivision.toLowerCase()}-`
    : "";

  const url = `/propiedades/${operationType}-${type}/${[
    rooms,
    urbanLocation,
    neighborhood,
    addressSlug,
  ]
    .filter((x) => !!x)
    .join("-")}__${_id}`.replace(/ /g, "-");

  return url;
};
