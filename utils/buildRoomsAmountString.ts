import { Property } from "../interfaces/properties";
import { shouldConsiderRooms } from "./shouldConsiderRooms";

export const buildRoomsAmountString = (property: Property) =>
  shouldConsiderRooms(property.propertyType)
    ? property.roomAmount > 0
      ? `${property.roomAmount} dormitorios`
      : "Monoambiente"
    : "---";
