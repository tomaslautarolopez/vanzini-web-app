import { Location } from "../interfaces/employee";
import { Property } from "../interfaces/properties";

export const propertyLocationFinder = (property: Property): Location => {
  const location = property.branch?.display_name;

  switch (location) {
    case "Vanzini Fisherton":
      return Location.FISHERTON;
    case "Vanzini":
      return Location.HEAD_OFFICE;

    default:
      return Location.HEAD_OFFICE;
  }
};
