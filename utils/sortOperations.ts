import { OperationType, Sort } from "../contexts/FiltersContext";

export const leastPriceFilterSort = (
  operationType: OperationType
): Sort => {
  switch (operationType) {
    case "Venta":
      return "operationSell.priceUSDmin:ASC";
    case "Alquiler":
      return "operationRent.priceUSDmin:ASC";
  }
};

export const mostPriceFilterSort = (
  operationType: OperationType
): Sort => {
  switch (operationType) {
    case "Venta":
      return "operationSell.priceUSDmax:DESC";
    case "Alquiler":
      return "operationRent.priceUSDmax:DESC";
  }
};
