import { PropertiesTypes } from "../interfaces/properties";

import CampoIcon from "../assets/icons/property-details/tipology-icons/Campo.svg";
import CasaIcon from "../assets/icons/property-details/tipology-icons/Casa.svg";
import CocheraIcon from "../assets/icons/property-details/tipology-icons/Cochera.svg";
import DepartamentoIcon from "../assets/icons/property-details/tipology-icons/Departamento.svg";
import GalponIcon from "../assets/icons/property-details/tipology-icons/Galpon.svg";
import LocalIcon from "../assets/icons/property-details/tipology-icons/Local.svg";
import OficinaIcon from "../assets/icons/property-details/tipology-icons/Oficina.svg";
import PasilloIcon from "../assets/icons/property-details/tipology-icons/Pasillo.svg";
import TerrenoIcon from "../assets/icons/property-details/tipology-icons/Terreno.svg";
// import IndustrialIcon from "../assets/icons/property-details/tipology-icons/Industrial.svg";

export const propertyTypeToIcon = (propertyType: PropertiesTypes) => {
  switch (propertyType) {
    case "HO":
      return CasaIcon;
    case "PH":
      return PasilloIcon;
    case "AP":
      return DepartamentoIcon;
    case "LA":
      return TerrenoIcon;
    case "LO":
      return LocalIcon;
    case "CO":
      return GalponIcon;
    case "GL":
      return GalponIcon;
    case "SR":
      return LocalIcon;
    case "OF":
      return OficinaIcon;
    case "GA":
      return CocheraIcon;
    case "CS":
      return CampoIcon;
    case "BU":
      return DepartamentoIcon;
    case "LOT":
      return CampoIcon;
    default:
      return CasaIcon;
  }
};
