export const isMobileWeb = () => process.browser && window.innerWidth < 1024;
