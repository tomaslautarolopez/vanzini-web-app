import { Property } from "../interfaces/properties";
import { validateWithKey } from "./asserts";

export function validateProperty(value: unknown): asserts value is Property {
  validateWithKey("developmentProperties", value);
}

export function validateDevelopment(value: unknown): asserts value is Property {
  validateProperty(value);

  if (value.developmentProperties.length === 0) {
    throw new TypeError("Property is not development");
  }
}
