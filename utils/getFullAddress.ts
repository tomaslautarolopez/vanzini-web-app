import { Property } from "../interfaces/properties";

export const getFullAddress = (property: Property) => {
  return `${property.address}, ${property.firstSubDivision || ""}, ${
    property.secondSubDivision || ""
  }`;
};
