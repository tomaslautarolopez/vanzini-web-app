import { PropertiesTypes } from "../interfaces/properties";

export const propertyTypeToString = (propertyType: PropertiesTypes) => {
  switch (propertyType) {
    case "HO":
      return "casa";
    case "PH":
      return "pasillo";
    case "AP":
      return "departamento";
    case "LA":
      return "terreno";
    case "LO":
      return "local";
    case "CO":
      return "galpon";
    case "GL":
      return "galpon";
    case "SR":
      return "local";
    case "OF":
      return "oficina";
    case "GA":
      return "cochera";
    case "CS":
      return "campo";
    case "BU":
      return "emprendimiento";
    case "LOT":
      return "loteo";
    default:
      return "Genérico";
  }
};
