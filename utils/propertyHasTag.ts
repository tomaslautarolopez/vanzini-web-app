import { Property } from "../interfaces/properties";

export const propertyHasTag = (prop: Property, tag: string): boolean => {
  return tag === prop.tags.find((x) => x === tag);
};

export const propertyHasCustomTag = (prop: Property, tag: string): boolean => {
  return tag === prop.customTags?.find((x) => x === tag);
};
