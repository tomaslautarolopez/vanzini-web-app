import { Operation } from "../interfaces/properties";

export function priceSorter(a: Operation, b: Operation) {
  if (a.currencyFromTokko === "USD") {
    return a.priceUSDmax - b.priceUSDmin;
  } else {
    return a.pricePesosMax - b.pricePesosMax;
  }
}
