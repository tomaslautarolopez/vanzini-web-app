import { PropertiesTypes } from "../interfaces/properties";

export const isDevelopment = (propertyType: PropertiesTypes): boolean =>
  propertyType === PropertiesTypes.BU || propertyType === PropertiesTypes.LOT;
