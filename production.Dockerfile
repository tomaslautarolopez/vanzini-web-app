FROM node:16.13.0
# Create back-office directory

RUN mkdir -p /usr/src/application
WORKDIR /usr/src/application

ADD ./ /usr/src/application

EXPOSE 3000

RUN yarn install
RUN yarn build
CMD yarn start:prod