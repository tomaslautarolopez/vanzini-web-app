import { axiosVanziniApiInstance } from "../axios";
import { API_URL_PROPERTIES } from "../axios/apiEndPoints";
import { Filters } from "../contexts/FiltersContext";
import { Property } from "../interfaces/properties";

const MAP_LIMIT = 50;

export const fetchProperties = async (
  offset = 0,
  limit?: number,
  filters?: Filters,
  fields?: string[]
): Promise<{
  properties: Array<Property>;
  propertiesCount: number;
}> => {
  try {
    const {
      data: { payload: properties, count: propertiesCount },
    } = await axiosVanziniApiInstance.get(API_URL_PROPERTIES, {
      params: {
        ...filters,
        limit,
        offset,
        fields,
      },
    });
    return { properties, propertiesCount };
  } catch (error) {
    console.log(error);
    return { properties: [], propertiesCount: 0 };
  }
};

export const fetchMapProperties = async (
  filters?: Filters
): Promise<{
  mapProperties: Array<Property>;
  mapPropertiesCount: number;
}> => {
  try {
    const {
      data: { realCount, count: mapPropertiesCount },
    } = await axiosVanziniApiInstance.get(API_URL_PROPERTIES, {
      params: {
        ...filters,
        limit: 1,
        fields: ["_id"],
      },
    });

    const iterations = Math.ceil(realCount / MAP_LIMIT);

    const mapProperties = await Promise.all(
      Array.apply(null, Array(iterations)).map(async (_, index) => {
        const {
          data: { payload: properties },
        } = await axiosVanziniApiInstance.get<{
          payload: Property[];
        }>(API_URL_PROPERTIES, {
          params: {
            ...filters,
            offset: index * MAP_LIMIT,
            limit: MAP_LIMIT,
            fields: [
              "_id",
              "publicationTitle",
              "geoLocation",
              "address",
              'photos|{"$slice": 1}',
              "operationSell",
              "urbanLocation",
              "neighborhood",
              "webPrice",
              "totalSurfaceMax",
              "propertyType",
              "roomAmount",
              "addressSlug",
              "sell",
              "rent",
              "firstSubDivision",
              "secondSubDivision",
            ],
          },
        });

        return properties;
      })
    );

    return {
      mapProperties: mapProperties.reduce(
        (prevv, current) => [...prevv, ...current],
        []
      ),
      mapPropertiesCount,
    };
  } catch (error) {
    console.log(error);
    return { mapProperties: [], mapPropertiesCount: 0 };
  }
};
