import { axiosVanziniApiInstance } from "../axios";
import {
  API_URL_GET_PROPERTIES_LIST_FOR_FEED,
  API_URL_GET_PROPERTIES_LIST,
} from "../axios/apiEndPoints";
import { PropertiesList } from "../hooks/useGetPropertiesListForFeed";

export const fetchPropertiesLists = async (): Promise<PropertiesList[]> => {
  try {
    const {
      data: { payload: propertiesList },
    } = await axiosVanziniApiInstance.get<{ payload: PropertiesList[] }>(
      API_URL_GET_PROPERTIES_LIST_FOR_FEED
    );

    return propertiesList;
  } catch (error) {
    console.error(error);

    throw error;
  }
};

export const fetchPropertiesList = async (
  slug: string
): Promise<PropertiesList> => {
  try {
    const {
      data: { payload: propertiesList },
    } = await axiosVanziniApiInstance.get<{ payload: PropertiesList }>(
      `${API_URL_GET_PROPERTIES_LIST}/${slug}`
    );

    return propertiesList;
  } catch (error) {
    console.error(error);

    throw error;
  }
};
