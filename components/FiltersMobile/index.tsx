import React, { useContext } from "react";
import { Checkbox, Radio, InputNumber, Select, Drawer, Button } from "antd";

import { FiltersMobileProps } from "./types";
import FiltersCollapse from "../FiltersCollapse";
import { useFilters } from "../../hooks/useFilters";
import {
  ageOptions,
  operationTypeOptions,
  propertyTypeOptions,
  roomAmountOptions,
  amenitiesOptions,
  otherOptions,
} from "../../constants/filters";
import { DrawerContext } from "../../contexts/DrawerContext";
import { useCountProperties } from "../../hooks/useCountProperties";
import FiltersIcon from "../../assets/icons/filters-mobile/filter.svg";
import FiltersMobileHeader from "../FiltersMobileHeader";
import { numberWithDotsAndCommas } from "../../utils/numberWithDotsAndCommas";
import { generateFilterTagElement } from "../../utils/generateFilterTagElement";

import styles from "./FiltersMobile.module.scss";
import { useRouter } from "next/router";
import OperationTypeFilterRadioGroup from "../OperationTypeFilterRadioGroup";

const { Option } = Select;

const FiltersMobile: React.FC<FiltersMobileProps> = () => {
  const {
    resetFilters,
    countryOptions,
    secondSubDivisionOptions,
    firstSubDivisionOptions,
    country,
    setCountry,
    setFirstSubDivision,
    setSecondSubDivision,
    currency,
    setCurrency,
    fromPrice,
    setFromPrice,
    toPrice,
    setToPrice,
    operationType,
    setOperationType,
    propertyType,
    setPropertyType,
    age,
    setAge,
    roomAmount,
    setRoomAmount,
    amenities,
    setAmenities,
    other,
    setOther,
    applyFilters,
    temporaryFilters,
    secondSubDivision,
    firstSubDivision,
  } = useFilters();

  const {
    drawers: { filters },
    setDrawer,
  } = useContext(DrawerContext);

  const { propertiesCount } = useCountProperties(temporaryFilters);
  const { pathname } = useRouter();

  const tagList = [
    fromPrice !== undefined && {
      value: fromPrice,
      label: `Desde: ${numberWithDotsAndCommas(fromPrice)} ${currency}`,
      onRemove: () => setFromPrice(undefined),
    },
    toPrice !== undefined && {
      value: toPrice,
      label: `hasta: ${numberWithDotsAndCommas(toPrice)} ${currency}`,
      onRemove: () => setToPrice(undefined),
    },
    country !== undefined && {
      value: country,
      label: country,
      onRemove: () => {
        setCountry("Argentina");
        setFirstSubDivision(undefined);
        setSecondSubDivision(undefined);
      },
    },
    firstSubDivision !== undefined && {
      value: firstSubDivision,
      label: firstSubDivision,
      onRemove: () => {
        setFirstSubDivision(undefined);
        setSecondSubDivision(undefined);
      },
    },
    secondSubDivision !== undefined && {
      value: secondSubDivision,
      label: secondSubDivision,
      onRemove: () => {
        setSecondSubDivision(undefined);
      },
    },
    ...generateFilterTagElement(
      operationTypeOptions,
      operationType && [operationType],
      (newValue) => setOperationType(newValue[0])
    ),
    ...generateFilterTagElement(propertyTypeOptions, propertyType, (newValue) =>
      setPropertyType(newValue)
    ),
    ...generateFilterTagElement(roomAmountOptions, roomAmount, (newValue) =>
      setRoomAmount(newValue)
    ),
    ...generateFilterTagElement(ageOptions, age, (newValue) =>
      setAge(newValue)
    ),
    ...generateFilterTagElement(amenitiesOptions, amenities, (newValue) =>
      setAmenities(newValue)
    ),
    ...generateFilterTagElement(otherOptions, other, (newValue) =>
      setOther(newValue)
    ),
  ];

  return (
    <Drawer
      headerStyle={{
        backgroundColor: "var(--color-blue-1)",
      }}
      placement="right"
      title={
        <div>
          <h5>
            <img
              style={{
                marginRight: "8px",
              }}
              src={FiltersIcon}
            />{" "}
            Filtros
          </h5>
        </div>
      }
      width="100vw"
      onClose={() => setDrawer("filters", false)}
      visible={filters}
      destroyOnClose
    >
      <div className={styles["filters-mobile"]}>
        <div className={styles["filters-mobile__header"]}>
          <FiltersMobileHeader
            clearFilters={resetFilters}
            tags={
              tagList.filter((x) => !!x) as Array<{
                label: string;
                value: string | number;
                onRemove: (value: string | number) => undefined;
              }>
            }
          />
        </div>
        <div className={styles["filters-mobile__filters-container"]}>
          <FiltersCollapse
            title="Tipo de operación"
            className={styles["filters-mobile__filter-collapse"]}
          >
            <OperationTypeFilterRadioGroup
              operationType={operationType}
              setOperationType={setOperationType}
            />
          </FiltersCollapse>
          <FiltersCollapse
            title="Tipología"
            className={styles["filters-mobile__filter-collapse"]}
            showCollapse
          >
            <Checkbox.Group
              value={propertyType}
              onChange={(values: any) => setPropertyType(values)}
              className={styles["filters-mobile__options"]}
            >
              {propertyTypeOptions.map((option) => (
                <Checkbox value={option.value} key={option.value}>
                  {option.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </FiltersCollapse>
          <FiltersCollapse
            title="Dormitorios"
            className={styles["filters-mobile__filter-collapse"]}
          >
            <Checkbox.Group
              value={roomAmount}
              onChange={(values: any) => setRoomAmount(values)}
              className={styles["filters-mobile__options"]}
            >
              {roomAmountOptions.map((option) => (
                <Checkbox value={option.value} key={option.value}>
                  {option.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </FiltersCollapse>
          <FiltersCollapse
            title="Precio"
            className={styles["filters-mobile__filter-collapse"]}
          >
            <div className={styles["filters-mobile__options--price"]}>
              <Radio.Group
                value={currency}
                onChange={(e) => setCurrency(e.target.value)}
                className={styles["ant-radio-group"]}
              >
                <Radio value={"ARS"}>Pesos</Radio>
                <Radio value={"USD"}>U$D</Radio>
              </Radio.Group>
              <div className={styles["filters-mobile__options--price-inputs"]}>
                <InputNumber
                  value={fromPrice}
                  onChange={(value) => setFromPrice(Number(value))}
                  style={{ width: 100, textAlign: "center" }}
                  placeholder="Desde"
                />
                {" - "}
                <InputNumber
                  value={toPrice}
                  onChange={(value) => setToPrice(Number(value))}
                  className="site-input-right"
                  style={{
                    width: 100,
                    textAlign: "center",
                  }}
                  placeholder="Hasta"
                />
              </div>
              <Checkbox.Group
                value={other}
                onChange={(values: any) => setOther(values)}
              >
                {otherOptions.map((option) => (
                  <div key={option.value}>
                    <Checkbox
                      value={option.value}
                      className={styles["ant-checkbox-wrapper"]}
                    >
                      {option.label}
                    </Checkbox>
                  </div>
                ))}
              </Checkbox.Group>
            </div>
          </FiltersCollapse>
          <FiltersCollapse
            title="Antigüedad"
            className={styles["filters-mobile__filter-collapse"]}
          >
            <Checkbox.Group
              value={age}
              onChange={(values: any) => setAge(values)}
              className={styles["filters-mobile__options"]}
            >
              {ageOptions.map((option) => (
                <Checkbox value={option.value} key={option.value}>
                  {option.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </FiltersCollapse>
          <FiltersCollapse
            title="Ubicación"
            className={styles["filters-mobile__filter-collapse"]}
          >
            <div>
              <Radio.Group
                value={country}
                onChange={(e) => setCountry(e.target.value)}
                className={styles["ant-radio-group"]}
              >
                {countryOptions.map((option) => (
                  <Radio value={option} key={option}>
                    {option}
                  </Radio>
                ))}
              </Radio.Group>
              <div
                className={styles["filters-mobile__options--location-select"]}
                id="location-filter-mobile-fsd"
              >
                <div>Ciudad</div>
                <Select
                  style={{ width: "100%" }}
                  disabled={!firstSubDivisionOptions}
                  placeholder="Rosario"
                  onChange={(value: string) => setFirstSubDivision(value)}
                  getPopupContainer={() =>
                    document.getElementById("location-filter-mobile-fsd") ||
                    document.body
                  }
                >
                  {firstSubDivisionOptions &&
                    firstSubDivisionOptions.map(
                      ({ value, amount }: { value: string; amount: number }) =>
                        value !== "amount" && (
                          <Option value={value} key={value}>
                            {value} ({amount})
                          </Option>
                        )
                    )}
                </Select>
              </div>
              <div
                className={styles["filters-mobile__options--location-select"]}
                id="location-filter-mobile-ssd"
              >
                <div>Barrio</div>
                <Select
                  style={{ width: "100%" }}
                  disabled={!secondSubDivisionOptions}
                  placeholder="Centro"
                  onChange={(value: string) => setSecondSubDivision(value)}
                  getPopupContainer={() =>
                    document.getElementById("location-filter-mobile-ssd") ||
                    document.body
                  }
                >
                  {secondSubDivisionOptions &&
                    secondSubDivisionOptions.map(
                      ({ value, amount }: { value: string; amount: number }) =>
                        value !== "amount" && (
                          <Option value={value} key={value}>
                            {value} ({amount})
                          </Option>
                        )
                    )}
                </Select>
              </div>
            </div>
          </FiltersCollapse>
          <FiltersCollapse
            title="Amenities"
            className={styles["filters-mobile__filter-collapse"]}
            showCollapse
          >
            <Checkbox.Group
              value={amenities}
              onChange={(values: any) => setAmenities(values)}
              className={styles["filters-mobile__options"]}
            >
              {amenitiesOptions.map((option) => (
                <Checkbox value={option.value} key={option.value}>
                  {option.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </FiltersCollapse>
        </div>
        <div className={styles["filters-mobile__button-container"]}>
          <Button
            type="primary"
            className={styles["filters-mobile__button"]}
            onClick={() => {
              applyFilters(pathname === "/");
              setDrawer("filters", false);
            }}
          >
            VER {propertiesCount} PROPIEDADES
          </Button>
        </div>
      </div>
    </Drawer>
  );
};

export default FiltersMobile;
