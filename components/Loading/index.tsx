import React from "react";
import classnames from "classnames";

import LogoShield from "../../assets/icons/general/vanzini-shield.svg";
import { LoadingProps } from "./types";

import styles from "./Loading.module.scss";

const Loading: React.FC<LoadingProps> = ({ className }) => {
  return (
    <div className={classnames(styles.loading, className)}>
      <img
        className="breathing"
        src={LogoShield}
        alt="Escudo de armas vanzini"
      />
    </div>
  );
};

export default Loading;
