import React, { useContext, useEffect, useState } from "react";
import { MobileNavBarProps } from "./types";
import { Avatar, Drawer, Input } from "antd";
import Link from "next/link";
import { MenuOutlined, SearchOutlined } from "@ant-design/icons";
import classnames from "classnames";
import { useRouter } from "next/router";
import debounce from "lodash.debounce";

import Logo from "../../assets/icons/general/vanzini-logo.svg";
import LogoWhite from "../../assets/icons/general/vanzini-logo-white.svg";
import UserIcon from "../../assets/icons/general/user.svg";
import UserIconWhite from "../../assets/icons/general/user-white.svg";
import { UserContext } from "../../contexts/UserContext";
import { ModalContext } from "../../contexts/ModalContext";
import Cross from "../../assets/icons/nav-bar/cross.svg";
import Glass from "../../assets/icons/nav-bar/glass.svg";
import { FiltersContext } from "../../contexts/FiltersContext";
import MobileMenu from "../MobileMenu";

import styles from "./MobileNavBar.module.scss";

const MobileNavBar: React.FC<MobileNavBarProps> = () => {
  const { setModal } = useContext(ModalContext);
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  const [search, setSearch] = useState(false);
  const { user } = useContext(UserContext);
  const [address, setAddress] = useState<string | undefined>();
  const { changeFilters, filters, constructUrlWithFilter } = useContext(
    FiltersContext
  );
  const { pathname } = useRouter();
  const [scrolledHome, setScrolledHome] = useState(false);

  const picture = user.data?.picture;
  const name = user.data?.name;

  useEffect(() => {
    const listener = debounce(() => {
      if (window.scrollY > 518) {
        setScrolledHome(true);
      } else {
        setScrolledHome(false);
      }
    }, 100);

    window.addEventListener("scroll", listener);

    return () => {
      window.removeEventListener("scroll", listener);
    };
  }, []);

  return (
    <div
      className={classnames(
        styles["mobile-nav-bar"],
        search && styles["open-search-input"],
        pathname === "/" && !scrolledHome && styles["mobile-nav-bar__home"],
        pathname === "/" && scrolledHome && styles["mobile-nav-bar__shadow"],
        pathname !== "/feed" &&
          pathname !== "/feed/[propertyType]" &&
          pathname !== "/" &&
          pathname !== "/mapa" &&
          styles["mobile-nav-bar__shadow"]
      )}
    >
      <Drawer
        width="100vw"
        placement="left"
        onClose={() => setMenuIsOpen(false)}
        visible={menuIsOpen}
      >
        <MobileMenu onClose={() => setMenuIsOpen(false)} />
      </Drawer>
      {search ? (
        <div className={styles["mobile-nav-bar__search"]}>
          <form
            onSubmit={(event) => {
              event.preventDefault();
              changeFilters({
                ...filters,
                address,
              });
            }}
          >
            <Input
              placeholder="Buscar por dirección"
              className={classnames(
                styles["mobile-nav-bar__search--inputs"],
                "dp2-shadow"
              )}
              value={address}
              onChange={(event) => setAddress(event.target.value)}
              addonBefore={
                <img
                  onClick={() => {
                    changeFilters({
                      ...filters,
                      address,
                    });
                  }}
                  src={Glass}
                  alt="Lupa azul"
                />
              }
              addonAfter={
                <img
                  onClick={() => {
                    setSearch(false);
                    setAddress(undefined);
                    changeFilters({
                      ...filters,
                      address: undefined,
                    });
                  }}
                  src={Cross}
                  alt="Cruz negra"
                />
              }
            />
          </form>
        </div>
      ) : (
        <>
          <Link href={constructUrlWithFilter("/feed")}>
            <a>
              <div className={styles["mobile-nav-bar__logo"]}>
                <img
                  src={pathname === "/" && !scrolledHome ? LogoWhite : Logo}
                  alt="Logo de inmobiliaria Vanzini"
                />
              </div>
            </a>
          </Link>
          <div className={styles["mobile-nav-bar__buttons"]}>
            <SearchOutlined
              onClick={() => setSearch(true)}
              className={styles["mobile-nav-bar__buttons--search"]}
              style={{ fontSize: 24 }}
            />
            <MenuOutlined
              onClick={() => setMenuIsOpen(true)}
              className={styles["mobile-nav-bar__buttons--menu"]}
              style={{ fontSize: 24 }}
            />

            {user.data ? (
              <Link href="/perfil">
                {picture ? (
                  <Avatar size={32} src={picture} />
                ) : (
                  <Avatar
                    size={32}
                    src={picture}
                    style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
                  >
                    {name ? name[0].toUpperCase() : "User"}
                  </Avatar>
                )}
              </Link>
            ) : (
              <img
                onClick={() => setModal("logInModal", true)}
                src={
                  pathname === "/" && !scrolledHome ? UserIconWhite : UserIcon
                }
                alt="Icono de usuario"
              />
            )}
          </div>
        </>
      )}
    </div>
  );
};

export default MobileNavBar;
