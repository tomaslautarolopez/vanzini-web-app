import React, { useContext } from "react";

import { NewsletterBannerProps } from "./types";
import { ModalContext } from "../../contexts/ModalContext";
import Banner from "../Banner";
import { COLORS } from "../Banner/types";

const NewsletterBanner: React.FC<NewsletterBannerProps> = ({ className }) => {
  const { setModal } = useContext(ModalContext);

  return (
    <Banner
      className={className}
      color={COLORS.MAGENTA}
      buttonContent={"Suscribite"}
      onClick={() => setModal("newsletterModal", true)}
      textContent={
        <>
          Suscribite al newsletter
          <br /> y recibí las novedades del mercado.
        </>
      }
      titleContent={
        <>
          <b>Suscribite al newsletter</b> y recibí las novedades del mercado.
        </>
      }
    />
  );
};

export default NewsletterBanner;
