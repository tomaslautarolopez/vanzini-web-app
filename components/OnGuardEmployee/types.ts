import { Employee } from "../../interfaces/employee";

export type OnGuardEmployeeProps = {
  onGuardEmployee: Employee;
};
