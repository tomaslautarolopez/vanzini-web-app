import React from "react";
import { Avatar } from "antd";

import { OnGuardEmployeeProps } from "./types";
import Number from "../../assets/icons/consults/number.svg";

import styles from "./OnGuardEmployee.module.scss";

const OnGuardEmployee: React.FC<OnGuardEmployeeProps> = ({
  onGuardEmployee,
}) => {
  return (
    <div className={styles["on-guard-employee"]}>
      <div className={styles["on-guard-employee__avatar"]}>
        <img
          src={Number}
          alt="Burbuja de dialogo con teléfono"
          className={styles["floating-icon"]}
        />
        <Avatar size={64} src={onGuardEmployee.pictureUrl} />
      </div>
      <div>
        <h5>{onGuardEmployee.name}</h5>
        <p>Vendedor de guardia</p>
      </div>
    </div>
  );
};

export default OnGuardEmployee;
