import React, { useContext } from "react";
import classnames from "classnames";
import {
  BellFilled,
  BellOutlined,
  HeartFilled,
  HeartOutlined,
  LoadingOutlined,
} from "@ant-design/icons";

import { MapPropertyCardProps } from "./types";
import NoImages from "../../assets/images/no-images.jpg";
import { propertyTypeToString } from "../../utils/propertyTypeToString";
import { getFullAddress } from "../../utils/getFullAddress";
import { useGeneratePriceString } from "../../hooks/useGeneratePriceString";
import { useLikeProperty } from "../../hooks/useLikeProperty";
import { ModalContext } from "../../contexts/ModalContext";
import Consult from "../../assets/icons/consult/send.svg";
import { buildUrlForProperty } from "../../utils/buildUrlForProperty";
import { usePriceAlerts } from "../../hooks/usePriceAlerts";
import { UserContext } from "../../contexts/UserContext";
import ResponsiveLink from "../ResponsiveLink";

import styles from "./MapPropertyCard.module.scss";
import { numberWithDotsAndCommas } from "../../utils/numberWithDotsAndCommas";

const MapPropertyCard: React.FC<MapPropertyCardProps> = ({
  property,
  className,
  setHoveredProperty,
}) => {
  const { setModal, setConsultModalData, setAlertModalData } =
    useContext(ModalContext);
  const { removeUserPriceAlert, loading: alertLoading } = usePriceAlerts();

  const { photos, propertyType, totalSurfaceMax, _id } = property;
  const {
    likeProperty,
    dislikeProperty,
    loading: likeLoading,
  } = useLikeProperty();

  const {
    user: { data },
  } = useContext(UserContext);
  const liked = data?.likes.has(property._id);
  const alertSetted = data?.alerts.has(property._id);

  const url = buildUrlForProperty(property);

  return (
    <div
      onMouseEnter={() => setHoveredProperty?.(property)}
      onMouseLeave={() => setHoveredProperty?.(undefined)}
      className={classnames(styles["map-property-card"], className)}
    >
      <div className={styles["map-property-card__image"]}>
        <ResponsiveLink
          as={url}
          href="/propiedades/[operationType-propertyType]/[propertySlug]"
        >
          <img src={photos?.[0]?.thumb || NoImages} />
        </ResponsiveLink>
      </div>
      <div className={styles["map-property-card__info"]}>
        <div>
          <small className={styles["map-property-card__info--property-type"]}>
            {propertyTypeToString(propertyType)}
          </small>
          <ResponsiveLink
            as={url}
            href="/propiedades/[operationType-propertyType]/[propertySlug]"
          >
            <h5>{property.publicationTitle}</h5>
          </ResponsiveLink>
          <h6 className={styles["map-property-card__info--address"]}>
            {getFullAddress(property)}
          </h6>
          <h6 className={styles["map-property-card__info--price-and-surface"]}>
            {numberWithDotsAndCommas(totalSurfaceMax)}m² | {useGeneratePriceString(property, "Venta")}
          </h6>
        </div>
        <div className={styles["map-property-card__info--actions"]}>
          <button
            onClick={() => {
              if (alertSetted) {
                removeUserPriceAlert(property._id);
              } else {
                setModal("alertModal", true);
                setAlertModalData({ propertyId: property._id });
              }
            }}
            className={styles["map-property-card__info--actions---bell"]}
          >
            {alertLoading ? (
              <LoadingOutlined />
            ) : alertSetted ? (
              <BellFilled />
            ) : (
              <BellOutlined />
            )}
          </button>
          <button
            onClick={() => (liked ? dislikeProperty(_id) : likeProperty(_id))}
          >
            {!likeLoading ? (
              liked ? (
                <HeartFilled />
              ) : (
                <HeartOutlined />
              )
            ) : (
              <LoadingOutlined />
            )}
          </button>
          <button
            onClick={() => {
              setModal("consultModal", true);
              setConsultModalData({
                property,
                propertiesId: [
                  {
                    id: property._id,
                    propertyType: property.propertyType,
                  },
                ],
              });
            }}
          >
            <img src={Consult} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default MapPropertyCard;
