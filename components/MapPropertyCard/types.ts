import { Property } from "../../interfaces/properties";

export type MapPropertyCardProps = {
  property: Property;
  className?: string;
  setHoveredProperty?: (property: Property | undefined) => void;
};
