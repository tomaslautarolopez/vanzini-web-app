import { Property } from "../../interfaces/properties";

export type PropertySimpleMenuProps = {
  property: Property;
  hideShare?: boolean;
  hideDetails?: boolean;
};
