import React, { useContext } from "react";
import {
  BellFilled,
  BellOutlined,
  FormOutlined,
  HeartFilled,
  HeartOutlined,
  LoadingOutlined,
  ShareAltOutlined,
} from "@ant-design/icons";
import { Popover } from "antd";

import { PropertySimpleMenuProps } from "./types";
import { usePriceAlerts } from "../../hooks/usePriceAlerts";
import { useLikeProperty } from "../../hooks/useLikeProperty";
import { UserContext } from "../../contexts/UserContext";
import { ModalContext } from "../../contexts/ModalContext";
import Consult from "../../assets/icons/consult/send.svg";
import ShareButtons from "../ShareButtons";
import { buildUrlForProperty } from "../../utils/buildUrlForProperty";

import styles from "./PropertySimpleMenu.module.scss";

const PropertySimpleMenu: React.FC<PropertySimpleMenuProps> = ({
  property,
  hideShare,
  hideDetails,
}) => {
  const { _id, address, propertyType } = property;
  const {
    dislikeProperty,
    likeProperty,
    loading: likeLoading,
  } = useLikeProperty();
  const { setModal, setConsultModalData, setAlertModalData } = useContext(
    ModalContext
  );
  const { removeUserPriceAlert, loading: alertLoading } = usePriceAlerts();

  const {
    user: { data },
  } = useContext(UserContext);
  const alertSetted = data?.alerts.has(_id);
  const liked = data?.likes.has(_id);

  return (
    <>
      <button
        className={styles["property-simple-menu__button"]}
        onClick={() => (liked ? dislikeProperty(_id) : likeProperty(_id))}
      >
        {likeLoading ? (
          <LoadingOutlined
            style={{
              color: "var(--color-blue-7)",
            }}
          />
        ) : liked ? (
          <HeartFilled
            style={{
              color: "var(--color-blue-7)",
            }}
          />
        ) : (
          <HeartOutlined
            style={{
              color: "var(--color-blue-7)",
            }}
          />
        )}
      </button>
      <Popover
        trigger="click"
        content={
          <ShareButtons
            url={buildUrlForProperty(property)}
            text={`Te comparto esta propiedad desde Vanzini Propiedades: ${address}`}
          />
        }
      >
        {!hideShare && (
          <button className={styles["property-simple-menu__button"]}>
            <ShareAltOutlined
              style={{
                color: "var(--color-blue-7)",
              }}
            />
          </button>
        )}
      </Popover>
      <button
        className={styles["property-simple-menu__button"]}
        onClick={() => {
          if (alertSetted) {
            removeUserPriceAlert(_id);
          } else {
            setModal("alertModal", true);
            setAlertModalData({ propertyId: _id });
          }
        }}
      >
        {alertLoading ? (
          <LoadingOutlined
            style={{
              color: "var(--color-blue-7)",
            }}
          />
        ) : alertSetted ? (
          <BellFilled
            style={{
              color: "var(--color-blue-7)",
            }}
          />
        ) : (
          <BellOutlined
            style={{
              color: "var(--color-blue-7)",
            }}
          />
        )}
      </button>
      {!hideDetails && (
        <a target="_blank" href={buildUrlForProperty(property)}>
          <button className={styles["property-simple-menu__button"]}>
            <FormOutlined
              style={{
                color: "var(--color-blue-7)",
              }}
            />
          </button>
        </a>
      )}
      <button
        className={styles["property-simple-menu__button"]}
        onClick={() => {
          setModal("consultModal", true);
          setConsultModalData({
            property,
            propertiesId: [
              {
                id: _id,
                propertyType: propertyType,
              },
            ],
          });
        }}
      >
        <img src={Consult} />
      </button>
    </>
  );
};

export default PropertySimpleMenu;
