import { AbstractTooltipProps } from "antd/lib/tooltip";
import { Sort } from "../../contexts/FiltersContext";

export type SortPopupProps = {
  setLocalSort?: (sort: Sort) => void;
  placement?: AbstractTooltipProps["placement"];
};
