import React, { useContext, useState } from "react";
import { Popover } from "antd";
import classnames from "classnames";

import { SortPopupProps } from "./types";
import { FiltersContext, Sort } from "../../contexts/FiltersContext";
import {
  leastPriceFilterSort,
  mostPriceFilterSort,
} from "../../utils/sortOperations";

import styles from "./SortPopup.module.scss";

const SortPopup: React.FC<SortPopupProps> = ({ children, setLocalSort, placement }) => {
  const {
    setSort,
    filters: { operationType, sort },
  } = useContext(FiltersContext);
  const [visible, setVisible] = useState(false);

  const applySort = (value: Sort) => {
    if (setLocalSort) {
      setLocalSort(value);
    } else {
      setSort(value);
    }
    setVisible(false);
  };

  return (
    <Popover
      trigger="click"
      visible={visible}
      onVisibleChange={(visible) => {
        setVisible(visible);
      }}
      placement={placement || "bottom"}
      content={
        <div className={styles["sort-popup"]}>
          <h5 className="fw-700">ORDENAR</h5>
          <div
            className={classnames(
              styles["sort-popup__option"],
              (sort === "operationSell.priceUSDmin:ASC" ||
                sort === "operationRent.priceUSDmin:ASC") &&
                styles["selected"]
            )}
            onClick={() => {
              applySort(
                operationType
                  ? leastPriceFilterSort(operationType)
                  : "operationSell.priceUSDmin:ASC"
              );
            }}
          >
            Menor precio {operationType ? `(${operationType})` : ""}
          </div>
          <div
            className={classnames(
              styles["sort-popup__option"],
              (sort === "operationRent.priceUSDmax:DESC" ||
                sort === "operationSell.priceUSDmax:DESC") &&
                styles["selected"]
            )}
            onClick={() => {
              applySort(
                operationType
                  ? mostPriceFilterSort(operationType)
                  : "operationSell.priceUSDmax:DESC"
              );
            }}
          >
            Mayor precio {operationType ? `(${operationType})` : ""}
          </div>
          <div
            className={classnames(
              styles["sort-popup__option"],
              sort === "creationDate:DESC" && styles["selected"]
            )}
            onClick={() => {
              applySort("creationDate:DESC");
            }}
          >
            Recien publicados{" "}
          </div>
          <div
            className={classnames(
              styles["sort-popup__option"],
              sort === "totalSurfaceMax:DESC" && styles["selected"]
            )}
            onClick={() => {
              applySort("totalSurfaceMax:DESC");
            }}
          >
            Mas m <sup>2</sup>
          </div>
          <div
            className={classnames(
              styles["sort-popup__option"],
              sort === "totalSurfaceMax:ASC" && styles["selected"]
            )}
            onClick={() => {
              applySort("totalSurfaceMax:ASC");
            }}
          >
            Menos m <sup>2</sup>
          </div>
        </div>
      }
    >
      {children}
    </Popover>
  );
};

export default SortPopup;
