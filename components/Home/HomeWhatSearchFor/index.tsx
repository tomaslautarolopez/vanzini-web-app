import React from "react";
import Link from "next/link";

import buy from "../../../assets/icons/home/buy.svg"
import invest from "../../../assets/icons/home/invest.svg"
import sell from "../../../assets/icons/home/sell.svg"
import { HomeWhatSearchForProps } from "./types";
import { useCountProperties } from "../../../hooks/useCountProperties";

import styles from "./HomeWhatSearchFor.module.scss";

const HomeWhatSearchFor: React.FC<HomeWhatSearchForProps> = () => {
  const { propertiesCount } = useCountProperties(undefined);

  return (
    <div className={styles["home-what-search-for"]}>
      <span className={styles["home-what-search-for__tag"]}>¿qué buscas?</span>
      <h3>
        Más de <b>{propertiesCount} propiedades</b> a la venta
      </h3>
      <h6 className={styles["home-what-search-for__description"]}>
        La mejor manera de encontrar todas las propiedades en un solo lugar.
      </h6>
      <div className={styles["home-what-search-for__cards-container"]}>
        <Link href="/feed">
          <a>
            <div className={styles["home-what-search-for__cards"]}>
              <div className={styles["home-what-search-for__cards--content"]}>
                <img src={buy} alt="Icono de mano con una casa" />
                <h3 className={styles["blue"]}>Comprar</h3>
                <h6>
                  Navegá la cartera de propiedades más grande de Rosario y la región. Encontra tu hogar con asesoría profesional.
                </h6>
                <a className={styles["blue"]}>
                  Ver más
                </a>
              </div>
            </div>
          </a>
        </Link>
        <Link href="/tasaciones">
          <a>
            <div className={styles["home-what-search-for__cards"]}>
              <div className={styles["home-what-search-for__cards--content"]}>
                <img src={sell} alt="Icono de casa con dinero" />
                <h3 className={styles["pink"]}>Vender</h3>
                <h6>
                  Vendé tu propiedad más rapido mostrándola a más de 50.000 personas por mes y el respaldo de 65 años de trayectoria.
                </h6>
                <a className={styles["pink"]}>
                  Ver más
                </a>
              </div>
            </div>
          </a>
        </Link>
        <Link href="/feed/emprendimientos?operacion=Venta&antiguedad=en-construccion&moneda=ARS">
          <a>
            <div className={styles["home-what-search-for__cards"]}>
              <div className={styles["home-what-search-for__cards--content"]}>
                <img src={invest} alt="Icono de edificios" />
                <h3 className={styles["gray"]}>Inversiones</h3>
                <h6>
                  Conocé todos los nuevos desarrollos inmobiliarios de Rosario, compará y decidí en<br /> cuál invertir.
                </h6>
                <a className={styles["gray"]}>
                  Ver más
                </a>
              </div>
            </div>
          </a>
        </Link>
      </div>
    </div>
  );
};

export default HomeWhatSearchFor;
