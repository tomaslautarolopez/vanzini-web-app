import React from "react";

import { HomeMapProps } from "./types";
import map from "../../../assets/images/Home/Map.png";
import mapAnchor from "../../../assets/icons/home/map-anchor.svg";
import { defaultMapIcons } from "../../../constants/map";
import property from "../../../assets/images/Home/property.jpeg";

import styles from "./HomeMap.module.scss";
import { Button } from "antd";
import Link from "next/link";

const HomeMap: React.FC<HomeMapProps> = () => {
  return (
    <div className={styles["home-map"]}>
      <span className={styles["home-map__tag"]}>herramienta digital</span>
      <h3>
        <b>Mapa interactivo</b> con todas nuestras propiedades
      </h3>
      <h6 className={styles["home-map__description"]}>
        Una manera más fácil de buscar entre todas las propiedades a la venta de
        la ciudad de Rosario y alrededores.
      </h6>
      <div className={styles["home-map__content"]}>
        <div className={styles["home-map__map"]}>
          <div className={styles["home-map__map--property-card"]}>
            <img src={property} alt="Propiedad de la ciudad de Rosario" />
            <div className={styles["home-map__map--property-card---data"]}>
              <small>Departamento</small>
              <h6>Av. Francia 202 bis, Puerto Norte</h6>
            </div>
          </div>
          <img
            className={styles["home-map__map--property-icon"]}
            alt="Icono de edificio"
            src={defaultMapIcons.BU}
          />
          <img
            className={styles["home-map__map--image"]}
            alt="Mapa de Rosario"
            src={map}
          />
        </div>
        <div className={styles["home-map__guide"]}>
          <h3 className={styles["home-map__guide--header"]}>
            Podés encontrar propiedades en el<br /> <b>buscador</b> de la siguiente
            forma:
          </h3>
          <div className={styles["home-map__guide--card"]}>
            <img src={mapAnchor} className="Icono de ancla de mapa" />
            <div>
              <h6 className={styles["home-map__guide--card---title"]}>
                Solo calle: <b>“Moreno”</b>
              </h6>
              <h6>Buscá todas las propiedades sobre la calle mencionada</h6>
            </div>
          </div>
          <div className={styles["home-map__guide--card"]}>
            <img src={mapAnchor} className="Icono de ancla de mapa" />
            <div>
              <h6 className={styles["home-map__guide--card---title"]}>
                Calle y numeración: <b>“Moreno 954”</b>
              </h6>
              <h6>Buscá todas las propiedades en un radio de 4 cuadras</h6>
            </div>
          </div>
          <div className={styles["home-map__guide--card"]}>
            <img src={mapAnchor} className="Icono de ancla de mapa" />
            <div>
              <h6 className={styles["home-map__guide--card---title"]}>
                Calle y Calle: <b>“Moreno y Tucuman”</b>
              </h6>
              <h6>Buscá todas las propiedades en un radio de 4 cuadras</h6>
            </div>
          </div>
          <Link href="/mapa">
            <a>
              <Button type="primary">IR AL MAPA</Button>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default HomeMap;
