import React, { useContext } from "react";
import { Button, Checkbox, Radio } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import Link from "next/link";

import UserIcon from "../../../assets/icons/general/user-white.svg";
import { HomeFiltersProps } from "./types";
import FilterPopup from "../../FilterPopup";
import {
  operationTypeOptions,
  propertyTypeOptions,
  roomAmountOptions,
} from "../../../constants/filters";
import { useFilters } from "../../../hooks/useFilters";
import filter from "../../../assets/icons/home/filter.svg";
import { ModalContext } from "../../../contexts/ModalContext";

import styles from "./HomeFilters.module.scss";

const popUpClass = styles["home-filters__filter-popup"];

const HomeFilters: React.FC<HomeFiltersProps> = () => {
  const {
    operationType,
    setOperationType,
    propertyType,
    setPropertyType,
    roomAmount,
    setRoomAmount,
    applyFilters,
  } = useFilters();

  const { setModal } = useContext(ModalContext);

  return (
    <div className={styles["home-filters"]}>
      <div className={styles["home-filters__desktop"]}>
        <FilterPopup
          className={popUpClass}
          applyOnVisibleChange={false}
          onApplyFilter={applyFilters}
          onClearFilter={() => setOperationType("Venta")}
          title="Tipo de operación"
          label="Operación"
          highlighted={!!operationType?.length}
        >
          <Radio.Group
            value={operationType}
            onChange={(e) => setOperationType(e.target.value)}
          >
            {operationTypeOptions.map((option) => (
              <Radio value={option.value} key={option.value}>
                {option.label}
              </Radio>
            ))}
          </Radio.Group>
        </FilterPopup>
        <FilterPopup
          className={popUpClass}
          applyOnVisibleChange={false}
          onApplyFilter={applyFilters}
          onClearFilter={() => setPropertyType([])}
          title="Tipología"
          label="Tipología"
          highlighted={!!propertyType.length}
        >
          <Checkbox.Group
            value={propertyType}
            onChange={(values: any) => setPropertyType(values)}
            className={styles["home-filters__options"]}
          >
            {propertyTypeOptions.map((option) => (
              <Checkbox value={option.value} key={option.value}>
                {option.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </FilterPopup>
        <FilterPopup
          className={popUpClass}
          applyOnVisibleChange={false}
          onApplyFilter={applyFilters}
          onClearFilter={() => setRoomAmount([])}
          title="Dormitorios"
          label="Dormitorios"
          highlighted={!!roomAmount.length}
        >
          <Checkbox.Group
            value={roomAmount}
            onChange={(values: any) => setRoomAmount(values)}
            className={styles["home-filters__options"]}
          >
            {roomAmountOptions.map((option) => (
              <Checkbox value={option.value} key={option.value}>
                {option.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </FilterPopup>
        <button
          onClick={() => applyFilters(true)}
          className={styles["home-filters__search"]}
        >
          <SearchOutlined /> Buscar
        </button>
      </div>
      <div className={styles["home-filters__mobile"]}>
        <Link href="/feed">
          <Button className={styles["home-filters__search"]} type="primary">
            <img src={filter} alt="Icono de filtro" /> Comenzar búsqueda
          </Button>
        </Link>
        <button
          className={styles["home-filters__log-in"]}
          onClick={() => setModal("logInModal", true)}
        >
          <img src={UserIcon} alt="Icono de usuario" /> Iniciar Sesión
        </button>
      </div>
    </div>
  );
};

export default HomeFilters;
