import React, { useEffect, useRef, useState, ImgHTMLAttributes } from "react";
import { HomeParallaxProps } from "./types";

import sky from "../../../assets/images/Home/parallax/00_Sky.png";
import skyPlaceholder from "../../../assets/images/Home/parallax/00_Sky_placeholder.png";

import background from "../../../assets/images/Home/parallax/01_background-foreground.png";
import backgroundPlaceholder from "../../../assets/images/Home/parallax/01_background-foreground_placeholder.png";

import rightTower from "../../../assets/images/Home/parallax/03_torreDER.png";
import rightTowerPlaceholder from "../../../assets/images/Home/parallax/03_torreDER_placeholder.png";

import middleTower from "../../../assets/images/Home/parallax/04_torreMED.png";
import middleTowerPlaceholder from "../../../assets/images/Home/parallax/04_torreMED_placeholder.png";

import leftTower from "../../../assets/images/Home/parallax/05_torreIZQ.png";
import leftTowerPlaceholder from "../../../assets/images/Home/parallax/05_torreIZQ_placeholder.png";

import city from "../../../assets/images/Home/parallax/ciudad.png";
import cityPlaceholder from "../../../assets/images/Home/parallax/ciudad_placeholder.png";

import styles from "./HomeParallax.module.scss";

const ImageWithPlaceholder: React.FC<
  {
    srcPlaceholder?: string;
  } & ImgHTMLAttributes<HTMLImageElement>
> = ({ src, srcPlaceholder, ...rest }) => {
  const imageRef = useRef<HTMLImageElement | null>(null);
  const [isDownloadingPicture, setIsDownloadingPicture] = useState(false);

  useEffect(() => {
    if (imageRef.current?.complete) {
      setIsDownloadingPicture(true);
    }
  }, []);

  return (
    <>
      <img {...rest} src={isDownloadingPicture ? src : srcPlaceholder} />
      <img
        ref={imageRef}
        style={{ display: "none" }}
        src={src}
        onLoad={() => {
          setIsDownloadingPicture(true);
        }}
      />
    </>
  );
};

const HomeParallax: React.FC<HomeParallaxProps> = ({ children }) => {
  const [propotion, setPropotion] = useState(0);
  const [offset, setOffset] = useState(200);

  useEffect(() => {
    const scroll = () => {
      setPropotion(window.pageYOffset / window.innerHeight);
    };
    window.addEventListener("scroll", scroll);
    setOffset(window.innerWidth / 4);

    return () => {
      window.removeEventListener("scroll", scroll);
    };
  }, []);

  return (
    <div className={styles["home-parallax"]}>
      <div className={styles["home-parallax__content"]}>{children}</div>

      <ImageWithPlaceholder
        srcPlaceholder={skyPlaceholder}
        src={sky}
        style={{
          transform: `scale(1.8, 2)`,
        }}
        alt="Cielo de Rosario"
        className={styles["home-parallax__sky"]}
      />

      <ImageWithPlaceholder
        srcPlaceholder={backgroundPlaceholder}
        src={background}
        style={{
          transform: `scale(1, ${1 - propotion * 0.6})`,
          bottom: -offset,
        }}
        alt="Ciudad de Rosario"
        className={styles["home-parallax__background"]}
      />

      <ImageWithPlaceholder
        srcPlaceholder={cityPlaceholder}
        style={{
          bottom: `calc(${47 * (1 - propotion * 0.7)}vw - ${offset}px)`,
        }}
        src={city}
        alt="Ciudad de Rosario"
        className={styles["home-parallax__city"]}
      />

      <ImageWithPlaceholder
        srcPlaceholder={rightTowerPlaceholder}
        src={rightTower}
        style={{
          bottom: `calc(${23.8 * (1 - propotion * 0.6)}vw - ${offset}px)`,
        }}
        alt="Torres Dolfines"
        className={styles["home-parallax__right-tower"]}
      />

      <ImageWithPlaceholder
        srcPlaceholder={leftTowerPlaceholder}
        src={leftTower}
        className={styles["home-parallax__left-tower"]}
        alt="Torres Dolfines"
        style={{
          bottom: `calc(${37 * (1 - propotion * 0.6)}vw - ${offset}px)`,
        }}
      />

      <ImageWithPlaceholder
        srcPlaceholder={middleTowerPlaceholder}
        src={middleTower}
        alt="Torres Dolfines"
        style={{
          bottom: `calc(${30 * (1 - propotion * 0.6)}vw - ${offset}px)`,
        }}
        className={styles["home-parallax__middle-tower"]}
      />

      <div className={styles["home-parallax__black-background"]} />
    </div>
  );
};

export default HomeParallax;
