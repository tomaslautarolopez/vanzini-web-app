import React from "react";
import Link from "next/link";

import { HomeCategoriesProps } from "./types";
import CampoIcon from "../../../assets/icons/property-details/tipology-icons/Campo.svg";
import CasaIcon from "../../../assets/icons/property-details/tipology-icons/Casa.svg";
import CocheraIcon from "../../../assets/icons/property-details/tipology-icons/Cochera.svg";
import DepartamentoIcon from "../../../assets/icons/property-details/tipology-icons/Departamento.svg";
import GalponIcon from "../../../assets/icons/property-details/tipology-icons/Galpon.svg";
import LocalIcon from "../../../assets/icons/property-details/tipology-icons/Local.svg";
import OficinaIcon from "../../../assets/icons/property-details/tipology-icons/Oficina.svg";
import PasilloIcon from "../../../assets/icons/property-details/tipology-icons/Pasillo.svg";
import TerrenoIcon from "../../../assets/icons/property-details/tipology-icons/Terreno.svg";
import IndustriaIcon from "../../../assets/icons/property-details/tipology-icons/Industrial.svg";

import styles from "./HomeCategories.module.scss";

const HomeCategories: React.FC<HomeCategoriesProps> = () => {
  return (
    <div className={styles["home-categories"]}>
      <span className={styles["home-categories__tag"]}>categorías</span>
      <h3>
        Simplificá tu <b>búsqueda</b> filtrando por categorías
      </h3>
      <h6 className={styles["home-categories__description"]}>
        Elegí cualquiera de las categorías inmobiliarias para refinar tu
        búsqueda.
      </h6>
      <div className={styles["home-categories__categories"]}>
        <Link href="/feed/departamentos">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={DepartamentoIcon} alt="Icono de departamento" />
              </div>
              <small>Deptos.</small>
            </div>
          </a>
        </Link>
        <Link href="/feed/p.h">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={PasilloIcon} alt="Icono de case de pasillo" />
              </div>
              <small>Pasillos</small>
            </div>
          </a>
        </Link>
        <Link href="/feed/casas">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={CasaIcon} alt="Icono de casa" />
              </div>
              <small>Casa</small>
            </div>
          </a>
        </Link>
        <Link href="/feed/cocheras">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={CocheraIcon} alt="Icono de cochera" />
              </div>
              <small>Cocheras</small>
            </div>
          </a>
        </Link>
        <Link href="/feed/oficinas">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={OficinaIcon} alt="Icono de oficina" />
              </div>
              <small>Oficinas</small>
            </div>
          </a>
        </Link>
        <Link href="/feed/terrenos">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={TerrenoIcon} alt="Icono de terreno" />
              </div>
              <small>Terrenos</small>
            </div>
          </a>
        </Link>
        <Link href="/feed/campos">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={CampoIcon} alt="Icono de campo" />
              </div>
              <small>Campo</small>
            </div>
          </a>
        </Link>
        <Link href="/feed/locales">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={LocalIcon} alt="Icono de local" />
              </div>
              <small>Locales</small>
            </div>
          </a>
        </Link>
        <Link href="/feed/galpones">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={GalponIcon} alt="Icono de galpon" />
              </div>
              <small>Galpones</small>
            </div>
          </a>
        </Link>
        <Link href="/feed/departamentos">
          <a>
            <div className={styles["home-categories__category-card"]}>
              <div className={styles["home-categories__category-card--icon"]}>
                <img src={IndustriaIcon} alt="Icono de industria" />
              </div>
              <small>Industria</small>
            </div>
          </a>
        </Link>
      </div>
    </div>
  );
};

export default HomeCategories;
