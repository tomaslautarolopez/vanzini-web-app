import React from "react";
import { Button } from "antd";
import Link from "next/link";

import { HomeAboutUsProps } from "./types";
import background from "../../../assets/images/Home/AboutUs.jpg";

import styles from "./HomeAboutUs.module.scss";

const HomeAboutUs: React.FC<HomeAboutUsProps> = () => {
  return (
    <div className={styles["home-about-us"]}>
      <h1>Sobre nosotros</h1>
      <h5>
        Desde 1965 liderando el mercado inmobiliario de Rosario y alrededores,
        ofreciendo una experiencia distinta
        <br /> en la comercialización de propiedades.
      </h5>
      <Link href="/nosotros">
        <Button type="primary">Conocer más</Button>
      </Link>
      <img src={background} alt="Foto de la ciudad de Rosario" />
    </div>
  );
};

export default HomeAboutUs;
