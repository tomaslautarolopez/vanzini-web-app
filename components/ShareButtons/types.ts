export type ShareButtonsProps = {
  url: string;
  text: string;
};
