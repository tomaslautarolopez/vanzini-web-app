import React from "react";
import {
  FacebookShareButton,
  EmailShareButton,
  WhatsappShareButton,
} from "react-share";
import { notification } from "antd";
import CopyToClipboard from "react-copy-to-clipboard";

import { ShareButtonsProps } from "./types";
import Facebook from "../../assets/icons/social-media/facebook.svg";
import Email from "../../assets/icons/social-media/email.svg";
import Whatsapp from "../../assets/icons/social-media/whatsapp.svg";
import Link from "../../assets/icons/social-media/link.svg";
import styles from "./ShareButtons.module.scss";

const ShareButtons: React.FC<ShareButtonsProps> = ({ url, text }) => {
  return (
    <div className={styles["share-buttons"]}>
      <WhatsappShareButton
        title={text}
        url={url}
        windowWidth={600}
        windowHeight={800}
      >
        <div className={styles["share-buttons__button"]}>
          <img src={Whatsapp} alt="Logo de whatsapp" />
        </div>
      </WhatsappShareButton>
      <FacebookShareButton
        quote={text}
        url={url}
        windowWidth={600}
        windowHeight={800}
      >
        <div className={styles["share-buttons__button"]}>
          <img src={Facebook} alt="Logo de facebook" />
        </div>
      </FacebookShareButton>
      <EmailShareButton url={url} body={text} subject="Vanzini Inmobiliaria">
        <div className={styles["share-buttons__button"]}>
          <img src={Email} alt="Icono de una carta" />
        </div>
      </EmailShareButton>
      <CopyToClipboard
        text={url}
        onCopy={() =>
          notification.success({
            message: "Compartir",
            placement: "bottomRight",
            description: "Link copiado con éxito",
          })
        }
      >
        <div className={styles["share-buttons__button"]}>
          <img src={Link} alt="Icono de copiar link" />
        </div>
      </CopyToClipboard>
    </div>
  );
};

export default ShareButtons;
