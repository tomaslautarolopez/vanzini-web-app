import { Property } from "../../interfaces/properties";

export type MapPopupProps = {
  property: Property;
  liked?: boolean;
};
