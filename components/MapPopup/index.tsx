import React, { useContext, useState } from "react";
import { HeartFilled, HeartOutlined, LoadingOutlined } from "@ant-design/icons";

import { MapPopupProps } from "./types";
import NoImages from "../../assets/images/no-images.jpg";
import { propertyTypeToString } from "../../utils/propertyTypeToString";
import { getFullAddress } from "../../utils/getFullAddress";
import { useGeneratePriceString } from "../../hooks/useGeneratePriceString";
import { useLikeProperty } from "../../hooks/useLikeProperty";
import { ModalContext } from "../../contexts/ModalContext";
import Consult from "../../assets/icons/consult/send.svg";
import openInfo from "../../assets/icons/map/open-info.svg";
import { useFadeInOutWrapper } from "../../hooks/useFadeInOutWrapper";
import { buildUrlForProperty } from "../../utils/buildUrlForProperty";
import ResponsiveLink from "../ResponsiveLink";

import styles from "./MapPopup.module.scss";
import { numberWithDotsAndCommas } from "../../utils/numberWithDotsAndCommas";

const MapPopup: React.FC<MapPopupProps> = ({ property, liked }) => {
  const { propertyType, totalSurfaceMax, _id } = property;
  const { likeProperty, dislikeProperty, loading } = useLikeProperty();
  const [hovered, setHovered] = useState(false);

  const { setModal, setConsultModalData } = useContext(ModalContext);

  const brownComponent = useFadeInOutWrapper(
    <ResponsiveLink
      as={buildUrlForProperty(property)}
      href="/propiedades/[operationType-propertyType]/[propertySlug]"
    >
      <div className={styles["map-popup__image--hovered"]}>
        <img src={openInfo} />
        <p>Abirir ficha</p>
      </div>
    </ResponsiveLink>,
    hovered
  );

  return (
    <div className={styles["map-popup"]}>
      <div
        onMouseOver={() => setHovered(true)}
        onMouseLeave={() => setHovered(false)}
        className={styles["map-popup__image"]}
      >
        {brownComponent}
        <img
          className={styles["map-popup__image--image"]}
          src={property?.photos?.[0]?.thumb || NoImages}
        />
      </div>
      <div className={styles["map-popup__data"]}>
        <small>{propertyTypeToString(propertyType)}</small>
        <h6 className={styles["map-popup__data--address"]}>
          {getFullAddress(property)}
        </h6>
        <h6 className={styles["map-popup__data--price-and-surface"]}>
          {numberWithDotsAndCommas(totalSurfaceMax)}m² | {useGeneratePriceString(property, "Venta")}
        </h6>
      </div>
      <div className={styles["map-popup__actions"]}>
        <button
          onClick={() => (liked ? dislikeProperty(_id) : likeProperty(_id))}
        >
          {!loading ? (
            liked ? (
              <HeartFilled />
            ) : (
              <HeartOutlined />
            )
          ) : (
            <LoadingOutlined />
          )}
        </button>
        <button
          className={styles["map-popup__actions--consult"]}
          onClick={() => {
            setModal("consultModal", true);
            setConsultModalData({
              property,
              propertiesId: [
                {
                  id: property._id,
                  propertyType: property.propertyType,
                },
              ],
            });
          }}
        >
          <img src={Consult} />
        </button>
      </div>
    </div>
  );
};

export default MapPopup;
