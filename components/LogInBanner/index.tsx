import React, { useContext } from "react";

import { LogInBannerProps } from "./types";
import { ModalContext } from "../../contexts/ModalContext";
import Banner from "../Banner";
import { COLORS } from "../Banner/types";

const LogInBanner: React.FC<LogInBannerProps> = () => {
  const { setModal } = useContext(ModalContext);

  return (
    <Banner
      color={COLORS.BLUE}
      buttonContent={"Registrate"}
      onClick={() => setModal("logInModal", true)}
      textContent={
        <>
          Creá una cuenta
          <br /> y utilizá todas las funcionalidades.
        </>
      }
      titleContent={
        <>
          <b>Creá una cuenta</b> y utilizá todas las funcionalidades.
        </>
      }
    />
  );
};

export default LogInBanner;
