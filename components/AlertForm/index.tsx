import React, { useContext } from "react";
import { Form, Input } from "antd";

import { AlertFormProps } from "./types";
import { usePriceAlerts } from "../../hooks/usePriceAlerts";
import { LoadingOutlined } from "@ant-design/icons";
import { UserContext } from "../../contexts/UserContext";

const AlertForm: React.FC<AlertFormProps> = ({ propertyId, callback }) => {
  const { createPriceAlert, loading } = usePriceAlerts();
  const {
    user: { data },
  } = useContext(UserContext);

  return (
    <Form
      id="alert-form"
      name="alert-form"
      layout="vertical"
      onFinish={({ email }) => createPriceAlert(propertyId, email, callback)}
      initialValues={{
        email: data?.email,
      }}
    >
      <Form.Item
        name="email"
        label="EMAIL"
        rules={[
          {
            required: true,
            message: "Por favor, ingrese su email.",
          },
        ]}
      >
        <Input
          placeholder="nombre@email.com"
          style={{ width: "100%", height: "32px" }}
        />
      </Form.Item>
      <button className="black-custom-button" type="submit">
        CREAR ALERTA
        {loading && <LoadingOutlined />}
      </button>
    </Form>
  );
};

export default AlertForm;
