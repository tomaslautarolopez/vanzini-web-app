export type FiltersTagsProps = {
  className?: string;
  tags: Array<{
    label: string;
    value: string | number;
    onRemove: (value: string | number) => undefined;
  }>;
};
