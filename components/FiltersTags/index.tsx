import React from "react";
import { Tag } from "antd";
import classnames from "classnames";

import { FiltersTagsProps } from "./types";

import styles from "./FiltersTags.module.scss";

// const tagCreate = (
//   options: Array<{ value: any; label: string }>,
//   values: Array<string> | undefined,
//   setter: (values: Array<any>) => void
// ) =>
//   values
//     ? options
//         .filter((opt) => !!values.find((val) => val === opt.value))
//         .map((opt) => (
//           <Tag
//             className={styles["filters-tags__tag"]}
//             closable
//             key={opt.value}
//             onClose={(e: any) => {
//               e.preventDefault();
//               setter(values.filter((val) => val !== opt.value));
//             }}
//           >
//             {opt.label}
//           </Tag>
//         ))
//     : [];

// const tagList = [
//   fromPrice !== undefined && (
//     <Tag
//       className={styles["filters-tags__tag"]}
//       closable
//       key="from-price"
//       onClose={(e: any) => {
//         e.preventDefault();
//         setToPrice(undefined);
//       }}
//     >
//       Desde: {numberWithDotsAndCommas(Number(fromPrice))} {currency}
//     </Tag>
//   ),
//   toPrice !== undefined && (
//     <Tag
//       className={styles["filters-tags__tag"]}
//       closable
//       key="to-price"
//       onClose={(e: any) => {
//         e.preventDefault();
//         setFromPrice(undefined);
//       }}
//     >
//       hasta: {numberWithDotsAndCommas(Number(toPrice))} {currency}
//     </Tag>
//   ),
//   ...tagCreate(
//     operationTypeOptions,
//     operationType && [operationType],
//     (newValue) => setOperationType(newValue[0])
//   ),
//   ...tagCreate(propertyTypeOptions, propertyType, (newValue) =>
//     setPropertyType(newValue)
//   ),
//   ...tagCreate(roomAmountOptions, roomAmount, (newValue) =>
//     setRoomAmount(newValue)
//   ),
//   ...tagCreate(ageOptions, age, (newValue) => setAge(newValue)),
//   ...tagCreate(amenitiesOptions, amenities, (newValue) =>
//     setAmenities(newValue)
//   ),
//   ...tagCreate(otherOptions, other, (newValue) => setOther(newValue)),
//   country && (
//     <Tag
//       className={styles["filters-tags__tag"]}
//       closable
//       key={country}
//       onClose={(e: any) => {
//         e.preventDefault();
//         setCountry(undefined);
//         setFirstSubDivision(undefined);
//         setSecondSubDivision(undefined);
//       }}
//     >
//       {country}
//     </Tag>
//   ),
//   firstSubDivision && (
//     <Tag
//       className={styles["filters-tags__tag"]}
//       closable
//       key={firstSubDivision}
//       onClose={(e: any) => {
//         e.preventDefault();
//         setFirstSubDivision(undefined);
//         setSecondSubDivision(undefined);
//       }}
//     >
//       {firstSubDivision}
//     </Tag>
//   ),
//   secondSubDivision && (
//     <Tag
//       className={styles["filters-tags__tag"]}
//       closable
//       key={secondSubDivision}
//       onClose={(e: any) => {
//         e.preventDefault();
//         setSecondSubDivision(undefined);
//       }}
//     >
//       {secondSubDivision}
//     </Tag>
//   ),
// ];

const FiltersTags: React.FC<FiltersTagsProps> = ({ className, tags }) => {
  return (
    <div className={classnames(styles["filters-tags"], className)}>
      {tags.map(({ value, label, onRemove }) => (
        <Tag
          className={styles["filters-tags__tag"]}
          closable
          key={value}
          onClose={(e: any) => {
            e.preventDefault();
            onRemove(value);
          }}
        >
          {label}
        </Tag>
      ))}
    </div>
  );
};

export default FiltersTags;
