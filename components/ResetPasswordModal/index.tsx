import React, { useContext, useEffect } from "react";
import { Form, Input } from "antd";
import { useRouter } from "next/router";

import UserIcon from "../../assets/icons/general/user.svg";
import { ResetPasswordModalProps } from "./types";
import ModalWithImage from "../ModalWithImage";
import { ModalContext } from "../../contexts/ModalContext";
import { LoadingOutlined } from "@ant-design/icons";
import background from "../../assets/images/ForgotPassword/recover-password-bg.jpg";
import { usePasswordRecovery } from "../../hooks/usePasswordRecovery";

import styles from "./ResetPasswordModal.module.scss";

const ResetPasswordModal: React.FC<ResetPasswordModalProps> = () => {
  const {
    setModal,
    modals: { resetPasswordModal },
  } = useContext(ModalContext);

  const {
    query: { "token-recuperacion": token },
  } = useRouter();

  const {
    loading: { resetting: resettingLoading },
    resetPassword,
  } = usePasswordRecovery();

  useEffect(() => {
    if (token) {
      setModal("resetPasswordModal", true);
    }
  }, [token]);

  return (
    <ModalWithImage
      style={{
        width: 840,
      }}
      imageClassname={styles["reset-password-modal__image"]}
      image={background}
      altImage="Fuente Rosario Argentina"
      visible={resetPasswordModal}
      onCancel={() => setModal("resetPasswordModal", false)}
      footer={null}
      header={
        <h6 className={styles["reset-password-modal__header"]}>
          <img src={UserIcon} alt="Icono de usuario" /> RECUPERAR CONTRASEÑA
        </h6>
      }
    >
      <div className={styles["reset-password-modal"]}>
        <h3>¿Te olvidaste la contraseña?</h3>
        <p>Ingresa tu email y te enviaremos los pasos para cambiarla</p>
        <Form
          onFinish={(form) => {
            resetPassword(form.password, token as string, () =>
              setModal("resetPasswordModal", false)
            );
          }}
          className={styles["reset-password-modal__form"]}
          layout="vertical"
        >
          <Form.Item
            label="NUEVA CONTRASEÑA"
            name="password"
            rules={[
              {
                required: true,
                message: "Debes ingresar una contraseña",
              },
            ]}
          >
            <Input.Password placeholder="*********" />
          </Form.Item>
          <Form.Item
            label="CONFIRMAR CONTRASEÑA"
            name="repassword"
            rules={[
              {
                required: true,
                message: "Debes repetir tu contraseña",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject("Las contraseñas deben coincidir");
                },
              }),
            ]}
          >
            <Input.Password placeholder="*********" />
          </Form.Item>
          <button className="black-custom-button" type="submit">
            CAMBIAR CONTRASEÑA {resettingLoading && <LoadingOutlined />}
          </button>
        </Form>
        <h6 className={styles["reset-password-modal__footer"]}>
          No tengo cuenta,{" "}
          <a
            onClick={() => {
              setModal("signUpModal", true);
            }}
          >
            registrarme.
          </a>
        </h6>
      </div>
    </ModalWithImage>
  );
};

export default ResetPasswordModal;
