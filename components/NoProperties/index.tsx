import React, { useContext } from "react";
import { Button } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import classnames from "classnames";

import { NoPropertiesProps } from "./types";
import background from "../../assets/images/Feed/no-properties-bg.jpg";
import ThanksForConsultLayout from "../ThanksForConsultLayout";
import Rotate from "../../assets/icons/feed/rotate.svg";
import { FiltersContext } from "../../contexts/FiltersContext";
import FiltersTags from "../FiltersTags";
import PageFooter from "../PageFooter";
import { generateFilterTagElement } from "../../utils/generateFilterTagElement";
import { numberWithDotsAndCommas } from "../../utils/numberWithDotsAndCommas";
import Send from "../../assets/icons/consults/send.svg";
import { ModalContext } from "../../contexts/ModalContext";

import {
  ageOptions,
  amenitiesOptions,
  operationTypeOptions,
  otherOptions,
  propertyTypeOptions,
  roomAmountOptions,
} from "../../constants/filters";

import styles from "./NoProperties.module.scss";

const NoProperties: React.FC<NoPropertiesProps> = () => {
  const { filters, changeFilters, resetFilters } = useContext(FiltersContext);
  const { setModal } = useContext(ModalContext);

  const {
    fromSellPrice,
    toSellPrice,
    currency,
    country,
    firstSubDivision,
    secondSubDivision,
    age,
    tags,
    customTags,
    propertyType,
    operationType,
    roomAmount,
    address,
  } = filters;

  const tagList = [
    address !== undefined &&
      address !== "" && {
        value: address,
        label: address,
        onRemove: () =>
          changeFilters({
            ...filters,
            address: undefined,
          }),
      },
    fromSellPrice !== undefined && {
      value: fromSellPrice,
      label: `Desde: ${numberWithDotsAndCommas(fromSellPrice)} ${currency}`,
      onRemove: () =>
        changeFilters({
          ...filters,
          fromSellPrice: undefined,
        }),
    },
    toSellPrice !== undefined && {
      value: toSellPrice,
      label: `hasta: ${numberWithDotsAndCommas(toSellPrice)} ${currency}`,
      onRemove: () => changeFilters({
        ...filters,
        toSellPrice: undefined,
      }),
    },
    country !== undefined && {
      value: country,
      label: country,
      onRemove: () => changeFilters({
        ...filters,
        firstSubDivision: undefined,
        secondSubDivision: undefined,
      }),
    },
    firstSubDivision !== undefined && {
      value: firstSubDivision,
      label: firstSubDivision,
      onRemove: () => changeFilters({
        ...filters,
        firstSubDivision: undefined,
        secondSubDivision: undefined,
      }),
    },
    secondSubDivision !== undefined && {
      value: secondSubDivision,
      label: secondSubDivision,
      onRemove: () => changeFilters({
        ...filters,
        secondSubDivision: undefined,
      }),
    },
    ...generateFilterTagElement(
      operationTypeOptions,
      operationType && [operationType],
      (newValue) =>
        changeFilters({
          ...filters,
          operationType: newValue[0],
        })
    ),
    ...generateFilterTagElement(propertyTypeOptions, propertyType, (newValue) =>
      changeFilters({
        ...filters,
        propertyType: newValue,
      })
    ),
    ...generateFilterTagElement(roomAmountOptions, roomAmount, (newValue) =>
      changeFilters({
        ...filters,
        roomAmount: newValue,
      })
    ),
    ...generateFilterTagElement(ageOptions, age, (newValue) =>
      changeFilters({
        ...filters,
        age: newValue,
      })
    ),
    ...generateFilterTagElement(amenitiesOptions, tags, (newValue) =>
      changeFilters({
        ...filters,
        tags: newValue,
      })
    ),
    ...generateFilterTagElement(otherOptions, customTags, (newValue) =>
      changeFilters({
        ...filters,
        customTags: newValue,
      })
    ),
  ];

  return (
    <div className={styles["no-properties"]}>
      <ThanksForConsultLayout background={background} hideBackButton>
        <div className={styles["no-properties__content"]}>
          <h6 className={styles["no-properties__header"]}>
            <img src={Rotate} alt="Icono de reciclado" /> PROBÁ OTROS FILTROS
          </h6>
          <h3>
            No hay propiedades como la
            <br /> que buscás!
          </h3>
          <p>
            Probá eliminando algún filtro para encontrar más resultados
            <br />o contactate con alguno de nuestros asesores de venta.
          </p>
          <FiltersTags
            className={styles["no-properties__tags"]}
            tags={
              tagList.filter((x) => !!x) as Array<{
                label: string;
                value: string | number;
                onRemove: (value: string | number) => undefined;
              }>
            }
          />
          <div
            className={classnames(
              styles["no-properties__actions"],
              styles["no-properties__actions--clear-filters"]
            )}
          >
            <button
              className="white-custom-button"
              onClick={() => resetFilters()}
            >
              <DeleteOutlined
                style={{
                  fontSize: 20,
                  marginRight: 8,
                }}
              />
              Borrar Filtros
            </button>
            <Button
              type="primary"
              onClick={() => setModal("consultModal", true)}
            >
              <img
                style={{
                  width: 24,
                  height: 24,
                  marginRight: 8,
                }}
                src={Send}
                alt="Icono de avion de papel"
              />
              Consultar
            </Button>
          </div>
        </div>
      </ThanksForConsultLayout>
      <PageFooter />
    </div>
  );
};

export default NoProperties;
