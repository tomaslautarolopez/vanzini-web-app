import React from "react";
import classnames from "classnames";

import { PropertiesListFeedBannerProps } from "./types";
import styles from "./PropertiesListFeedBanner.module.scss";
import { useGetPropertiesListForFeed } from "../../hooks/useGetPropertiesListForFeed";
import ResponsiveLink from "../ResponsiveLink";

const PropertiesListFeedBanner: React.FC<PropertiesListFeedBannerProps> = ({
  className,
}) => {
  const { data: propertiesLists } = useGetPropertiesListForFeed();

  const sliceNumber =
    typeof window !== "undefined" && window.innerWidth >= 1200 ? 6 : 4;

  return (
    <div
      className={classnames(styles["properties-list-feed-banner"], className)}
    >
      <h6 className={styles["properties-list-feed-banner__label"]}>
        CLASIFICADOS
      </h6>
      <h3 className={styles["properties-list-feed-banner__title"]}>
        Las <span>mejores</span> selecciones de propiedades
      </h3>
      <p className={styles["properties-list-feed-banner__description"]}>
        La mayor oferta de casas, departamentos, oficinas, terrenos y
        emprendimientos en Rosario la región, actualizada semanalmente.
      </p>
      <div className={styles["properties-list-feed-banner__lists"]}>
        {propertiesLists?.slice(0, sliceNumber).map((pl) => (
          <div
            key={pl._id}
            className={styles["properties-list-feed-banner__list-card"]}
          >
            <div
              className={
                styles["properties-list-feed-banner__list-card--container"]
              }
            >
              <div
                className={classnames(
                  styles["properties-list-feed-banner__list-card--image"],
                  "dp8-shadow"
                )}
              >
                <img src={pl.portrait} alt={pl.title} />
              </div>
              <h6>{pl.title}</h6>
              <p>{pl.description}</p>
            </div>
            <ResponsiveLink href="/listados/[slug]" as={`/listados/${pl.slug}`}>
              Ver listado
            </ResponsiveLink>
          </div>
        ))}
      </div>
      <a
        target="_blank"
        href="/listados"
        className={styles["properties-list-feed-banner__see-all"]}
      >
        Ver todos los listados
      </a>
    </div>
  );
};

export default PropertiesListFeedBanner;
