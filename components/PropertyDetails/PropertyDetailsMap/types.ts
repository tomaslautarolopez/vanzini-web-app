import { Property } from "../../../interfaces/properties";

export type PropertyDetailsMapProps = {
  property: Property;
};
