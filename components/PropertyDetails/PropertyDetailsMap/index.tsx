import React from "react";
import { icon } from "leaflet";
import { MapContainer, Marker, TileLayer } from "react-leaflet";
import { Button } from "antd";
import Link from "next/link";

import { PropertyDetailsMapProps } from "./types";
import { defaultMapIcons } from "../../../constants/map";
import LocationIcon from "../../../assets/icons/map/location.svg";

import styles from "./PropertyDetailsMap.module.scss";

import "leaflet/dist/leaflet.css";

const PropertyDetailsMap: React.FC<PropertyDetailsMapProps> = ({
  property,
}) => {
  const { propertyType, publicationTitle } = property;
  const center = {
    lat: property.geoLocation.coordinates[0],
    lng: property.geoLocation.coordinates[1],
  };

  return (
    <div className={styles["property-details-map"]}>
      <h5>Ubicación</h5>
      <MapContainer
        zoom={18}
        center={center}
        dragging={false}
        scrollWheelZoom={false}
        touchZoom={false}
        style={{ width: "100%", height: 360, zIndex: 0 }}
        zoomControl={false}
      >
        <TileLayer
          url="https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}"
          subdomains={["mt0", "mt1", "mt2", "mt3"]}
          tileSize={256}
        />
        <Marker
          icon={icon({
            iconUrl: defaultMapIcons[propertyType] || defaultMapIcons.HO,
            iconSize: [55, 55],
            iconAnchor: [22.5, 5],
            popupAnchor: [0, 0],
          })}
          position={center}
          title={publicationTitle}
        />
      </MapContainer>
      <Link href={`/mapa?propiedad=${property._id}`}>
        <Button
          className={styles["property-details-map__go-to-map"]}
          type="ghost"
        >
          Ir al mapa <img src={LocationIcon} alt="Pin de mapa" />
        </Button>
      </Link>
    </div>
  );
};

export default PropertyDetailsMap;
