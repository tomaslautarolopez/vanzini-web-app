import React from "react";

import { PropertiesTypes } from "../../../interfaces/properties";
import { PropertyDetailsCharacteristicsProps } from "./types";
import { propertyHasTag } from "../../../utils/propertyHasTag";

import styles from "./PropertyDetailsCharacteristics.module.scss";

const PropertyDetailsCharacteristics: React.FC<PropertyDetailsCharacteristicsProps> = ({
  property,
}) => {
  const {
    roomAmount,
    bathroomAmount,
    ambientAmount,
    floorsAmount,
    zonification,
    disposition,
    orientation,
    propertyCondition,
    propertyType,
    garageAmount,
  } = property;

  const parkings =
    garageAmount ||
    (propertyHasTag(property, "Cochera Opcional") && "Opcional");

  return (
    <div className={styles["property-details-characteristics"]}>
      <h5>Caracteristicas</h5>
      {propertyType !== PropertiesTypes.GA &&
        propertyType !== PropertiesTypes.LA && (
          <div className={styles["property-details-characteristics__table"]}>
            <div
              className={
                styles["property-details-characteristics__table--column"]
              }
            >
              <h6>
                Ambientes: <label>{ambientAmount || "---"}</label>
              </h6>
              <h6>
                Baños: <label>{bathroomAmount || "---"}</label>
              </h6>
              {parkings && (
                <h6>
                  Cocheras: <label>{parkings}</label>
                </h6>
              )}
            </div>
            <div
              className={
                styles["property-details-characteristics__table--column"]
              }
            >
              <h6>
                Dormitorios: <label>{roomAmount || "---"}</label>
              </h6>
              <h6>
                Plantas: <label>{floorsAmount || "---"}</label>
              </h6>
            </div>
          </div>
        )}
      <div>
        <h6>
          Zonificación: <label>{zonification || "---"}</label>
        </h6>
        <h6>
          Disposición: <label>{disposition || "---"}</label>
        </h6>
        <h6>
          Orientación: <label>{orientation || "---"}</label>
        </h6>
        <h6>
          Condición: <label>{propertyCondition || "---"}</label>
        </h6>
      </div>
    </div>
  );
};

export default PropertyDetailsCharacteristics;
