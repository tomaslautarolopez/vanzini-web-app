import { Property } from "../../../interfaces/properties";

export type PropertyDetailsCharacteristicsProps = {
  property: Property;
  className?: string;
};
