import React from "react";
import classnames from "classnames";

import { PropertyDetailsServicesTagProps } from "./types";

import styles from "./PropertyDetailsServicesTag.module.scss";

const PropertyDetailsServicesTag: React.FC<PropertyDetailsServicesTagProps> = ({
  services,
  title,
  color,
  className,
  iconImg,
}) => {
  return (
    <div
      className={classnames(styles["property-details-services-tag"], className)}
    >
      <div
        className={classnames(
          styles["property-details-services-tag__title"],
          styles[color]
        )}
      >
        <img src={iconImg} alt="Check icon" />
        <h5>{title}</h5>
      </div>
      <div className={styles["property-details-services-tag__tags"]}>
        {services.map((tag) => (
          <div
            key={tag}
            className={classnames(
              styles["property-details-services-tag__tags--item"],
              styles[color]
            )}
          >
            {tag}
          </div>
        ))}
      </div>
    </div>
  );
};

export default PropertyDetailsServicesTag;
