export type PropertyDetailsServicesTagProps = {
  services: string[];
  title: string;
  color: "magenta" | "red" | "blue" | "gray";
  className?: string;
  iconImg: string;
};
