export type PropertyDetailsCheckboxProps = {
  tags: string[];
  iconImg: string;
  title: string;
  className: string;
  color: "magenta" | "blue";
};
