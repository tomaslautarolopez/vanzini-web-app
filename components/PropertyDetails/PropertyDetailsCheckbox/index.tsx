import React from "react";
import { Checkbox } from "antd";
import classnames from "classnames";

import { PropertyDetailsCheckboxProps } from "./types";

import styles from "./PropertyDetailsCheckbox.module.scss";

const PropertyDetailsCheckbox: React.FC<PropertyDetailsCheckboxProps> = ({
  tags,
  iconImg,
  title,
  className,
  color,
}) => {
  return (
    <div
      className={classnames(
        styles["property-details-checkbox"],
        className,
        styles[color]
      )}
    >
      <div className={styles["property-details-checkbox__title"]}>
        <img src={iconImg} alt="Check icon" />
        <h5>{title}</h5>
      </div>
      <div className={styles["property-details-checkbox__tags"]}>
        {tags.map((tag) => (
          <Checkbox key={tag} className={color} checked>
            {tag}
          </Checkbox>
        ))}
      </div>
    </div>
  );
};

export default PropertyDetailsCheckbox;
