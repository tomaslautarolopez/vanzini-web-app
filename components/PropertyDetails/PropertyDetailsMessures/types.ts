import { Property } from "../../../interfaces/properties";

export type PropertyDetailsMessuresProps = {
  property: Property;
  className?: string;
};
