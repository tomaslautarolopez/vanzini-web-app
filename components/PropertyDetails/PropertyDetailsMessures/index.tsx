import React from "react";
import classnames from "classnames";

import { PropertyDetailsMessuresProps } from "./types";

import styles from "./PropertyDetailsMessures.module.scss";
import { numberWithDotsAndCommas } from "../../../utils/numberWithDotsAndCommas";

const PropertyDetailsMessures: React.FC<PropertyDetailsMessuresProps> = ({
  property,
  className,
}) => {
  const {
    totalSurfaceMax,
    roofedSurface,
    unroofedSurface,
    surface,
    semiroofedSurface,
  } = property;

  const generateSurfaceString = (surface: number | undefined) =>
    surface ? `${numberWithDotsAndCommas(surface)}m²` : "---";

  return (
    <div className={classnames(styles["property-details-messures"], className)}>
      <h5>Superficie total</h5>
      <h3 className={styles["property-details-messures__blue"]}>
        {generateSurfaceString(totalSurfaceMax)}
      </h3>
      <p>Superficie cubierta</p>
      <h5
        className={classnames(
          styles["property-details-messures__blue"],
          "fw-700"
        )}
      >
        {generateSurfaceString(roofedSurface)}
      </h5>
      <p>Superficie terreno</p>
      <h5
        className={classnames(
          styles["property-details-messures__blue"],
          "fw-700"
        )}
      >
        {generateSurfaceString(surface)}
      </h5>
      <p>
        Superficie descubierta:{" "}
        <span className={styles["property-details-messures__blue"]}>
          {generateSurfaceString(unroofedSurface)}
        </span>
      </p>
      <p>
        Superficie Semicubierta:{" "}
        <span className={styles["property-details-messures__blue"]}>
          {generateSurfaceString(semiroofedSurface)}
        </span>
      </p>
    </div>
  );
};

export default PropertyDetailsMessures;
