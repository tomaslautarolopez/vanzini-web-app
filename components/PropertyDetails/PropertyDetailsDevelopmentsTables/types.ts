import { DevelopmentProperty, Property } from "../../../interfaces/properties";

export type PropertyDetailsDevelopmentsTablesProps = {
  property: Property;
  setUnits?: (units: string[]) => void;
};

export interface TableProps {
  key: string;
  name: string;
  roofedSurface: string;
  semiroofedSurface: string;
  totalSurface: string;
  property: DevelopmentProperty;
}
