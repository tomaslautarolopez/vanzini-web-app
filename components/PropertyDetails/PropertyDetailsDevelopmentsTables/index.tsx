import React, { useContext, useEffect, useState } from 'react';
import { Collapse, Table } from 'antd';
import classnames from 'classnames';
import { Breakpoint } from 'antd/lib/_util/responsiveObserve';

import { PropertyDetailsDevelopmentsTablesProps, TableProps } from './types';
import { DevelopmentProperty } from '../../../interfaces/properties';
import { propertyTypeToIcon } from '../../../utils/propertyTypeToIcon';
import { propertyTypeToString } from '../../../utils/propertyTypeToString';
import { ModalContext } from '../../../contexts/ModalContext';
import { useGeneratePriceFromOperation } from '../../../hooks/useGeneratePriceString';
import CollapseButton from '../../../assets/icons/property-details/collapse-button.svg';
import Consult from '../../../assets/icons/consult/send.svg';
import { priceSorter } from '../../../utils/priceSorter';

import styles from './PropertyDetailsDevelopmentsTables.module.scss';
import { numberWithDotsAndCommas } from '../../../utils/numberWithDotsAndCommas';

const { Panel } = Collapse;

const propertyOrder: { [key: string]: number } = {
  HO: 104,
  PH: 103,
  AP: 103,
  LO: 101,
  SR: 101,
  OF: 102,
  GA: 100,
};

const PropertyDetailsDevelopmentsTables: React.FC<PropertyDetailsDevelopmentsTablesProps> =
  ({ property, setUnits }) => {
    const [activeKey, setActiveKey] = useState<string[]>([]);
    const { setModal, setConsultModalData } = useContext(ModalContext);

    const developmentTypologies: {
      [key: string]: {
        properties: Array<DevelopmentProperty>;
        order: number;
        icon: string;
      };
    } = {};

    const columns = [
      {
        title: 'UNIDADES',
        dataIndex: 'name',
        sorter: (a: TableProps, b: TableProps) => a.name.localeCompare(b.name),
      },
      {
        title: 'CUBIERTA',
        dataIndex: 'roofedSurface',
        sorter: (a: TableProps, b: TableProps) =>
          a.roofedSurface.localeCompare(b.roofedSurface),
        responsive: ['lg', 'xl', 'xxl'] as Breakpoint[],
      },
      {
        title: 'SEMICUBIERTA',
        dataIndex: 'semiroofedSurface',
        sorter: (a: TableProps, b: TableProps) =>
          a.semiroofedSurface.localeCompare(b.semiroofedSurface),
        responsive: ['lg', 'xl', 'xxl'] as Breakpoint[],
      },
      {
        title: 'TOTAL',
        dataIndex: 'totalSurface',
        sorter: (a: TableProps, b: TableProps) =>
          a.totalSurface.localeCompare(b.totalSurface),
      },
      {
        title: 'PRECIO',
        dataIndex: 'property',
        render: (prop: DevelopmentProperty) => {
          const priceString = useGeneratePriceFromOperation(
            prop.operationSell,
            prop.webPrice
          );
          if (priceString === 'CONSULTAR PRECIO') {
            return (
              <span
                onClick={() => {
                  setModal('consultModal', true);
                  setConsultModalData({
                    property,
                    propertiesId: [
                      {
                        id: property._id,
                        propertyType: prop.propertyType,
                      },
                    ],
                  });

                  if (typeof window !== 'undefined') {
                    window.scrollTo(0, 0);
                  }
                }}
                style={{
                  cursor: 'pointer',
                  color: '#1890ff',
                }}
              >
                Consultar
              </span>
            );
          } else {
            return priceString;
          }
        },
        className: styles['property-details-developments-tables__table--price'],
        sorter: (a: TableProps, b: TableProps) =>
          priceSorter(a.property.operationSell, b.property.operationSell),
      },
      {
        dataIndex: 'actions',
      },
    ];

    property.developmentProperties.forEach((prop) => {
      const { propertyType, roomAmount } = prop;
      const key =
        propertyType === 'AP'
          ? roomAmount > 0
            ? `${roomAmount} Dormitorio${roomAmount > 1 ? 's' : ''}`
            : 'Monoambientes'
          : propertyTypeToString(propertyType);
      const typeProps = developmentTypologies[key];
      if (typeProps) {
        developmentTypologies[key] = {
          properties: [...typeProps.properties, prop],
          icon: typeProps.icon,
          order: typeProps.order,
        };
      } else {
        developmentTypologies[key] = {
          properties: [prop],
          order:
            propertyType === 'AP' ? roomAmount : propertyOrder[propertyType],
          icon: propertyTypeToIcon(propertyType),
        };
      }
    });

    const collapses = Object.entries(developmentTypologies)
      .sort(([keyA], [keyB]) => {
        if (keyA === 'Monoambientes') {
          return 1;
        }
        if (keyB === 'Monoambientes') {
          return -1;
        }

        return 0;
      })
      .sort(([, { order: orderA }], [, { order: orderB }]) => {
        return orderA - orderB;
      });

    useEffect(() => {
      setActiveKey(collapses.map(([devTypo]) => devTypo));
    }, []);

    return (
      <div className={styles['property-details-developments-tables']}>
        {collapses.map(([devTypo, { properties, icon }]) => {
          const data: TableProps[] = properties.map((prop) => {
            return {
              key: `${prop.id}_${prop.propertyType}`,
              name: `${prop.address.split("'").pop()}`,
              roofedSurface: `${numberWithDotsAndCommas(prop.roofedSurface)}m²`,
              semiroofedSurface: `${numberWithDotsAndCommas(
                prop.semiroofedSurface
              )}m²`,
              totalSurface: `${numberWithDotsAndCommas(prop.totalSurface)}m²`,
              property: prop,
              actions: (
                <div
                  className={
                    styles[
                      'property-details-developments-tables__table--actions'
                    ]
                  }
                >
                  <button
                    onClick={() => {
                      if (window && window.innerWidth < 1200)
                        setModal('consultModal', true);
                      else {
                        window.scrollTo({
                          top: 0,
                          left: 0,
                          behavior: 'smooth',
                        });
                      }
                      const units = [`${prop.address.split("'").pop()}`];
                      setUnits?.(units);
                      setConsultModalData({
                        property,
                        propertiesId: [
                          {
                            id: property._id,
                            propertyType: prop.propertyType,
                            units,
                          },
                        ],
                      });
                    }}
                  >
                    <img src={Consult} />
                  </button>
                </div>
              ),
            };
          });

          return (
            <div
              key={devTypo}
              className={
                styles['property-details-developments-tables__container']
              }
            >
              <div
                className={
                  styles[
                    'property-details-developments-tables__container--header'
                  ]
                }
                onClick={() =>
                  setActiveKey(
                    activeKey.includes(devTypo)
                      ? activeKey.filter((keys) => keys !== devTypo)
                      : [...activeKey, devTypo]
                  )
                }
              >
                <div
                  className={
                    styles[
                      'property-details-developments-tables__container--header---image'
                    ]
                  }
                >
                  <img src={icon} />
                </div>
                <div
                  className={
                    styles[
                      'property-details-developments-tables__container--header---title'
                    ]
                  }
                >
                  <h6>{devTypo}</h6>
                  <p>{properties.length} Disponibles</p>
                </div>
                <img
                  className={classnames(
                    styles[
                      'property-details-developments-tables__container--header---button'
                    ],
                    activeKey?.includes(devTypo) && styles['expanded-button']
                  )}
                  src={CollapseButton}
                />
              </div>
              <Collapse
                activeKey={activeKey?.includes(devTypo) ? devTypo : undefined}
                className="property-details-developments-tables__collpase"
              >
                <Panel
                  header="This panel can only be collapsed by clicking text"
                  key={devTypo}
                >
                  <Table
                    showSorterTooltip={false}
                    className="property-details-developments-tables__table"
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                  />
                </Panel>
              </Collapse>
            </div>
          );
        })}
      </div>
    );
  };

export default PropertyDetailsDevelopmentsTables;
