import React, { useState } from "react";
import classnames from "classnames";
import { Button, Collapse } from "antd";
import { DownOutlined } from "@ant-design/icons";

import { PropertyDetailsDescriptionProps } from "./types";

import styles from "./PropertyDetailsDescription.module.scss";

const { Panel } = Collapse;

const PropertyDetailsDescription: React.FC<PropertyDetailsDescriptionProps> = ({
  className,
  property,
}) => {
  const [activeKey, setActiveKey] = useState<undefined | "1">("1");

  const paragraphs = property.description
    .split("\n")
    .map((paragraph, idx) => <h6 key={idx}>{paragraph}</h6>);

  const showedParagraphs = paragraphs.slice(0, 1);
  const hiddenParagraphs = paragraphs.slice(1, paragraphs.length);

  return (
    <div
      className={classnames(styles["property-details-description"], className)}
    >
      <h5>Descripción</h5>
      <div>{showedParagraphs}</div>
      <Collapse
        activeKey={activeKey}
        className="property-details-description__collpase"
      >
        <Panel
          header="This panel can only be collapsed by clicking text"
          key="1"
        >
          {hiddenParagraphs}
        </Panel>
      </Collapse>
      {!!hiddenParagraphs.length && (
        <Button
          className={styles["property-details-description__show-more-button"]}
          type="ghost"
          onClick={() => {
            if (!activeKey) {
              setActiveKey("1");
            } else {
              setActiveKey(undefined);
            }
          }}
        >
          {activeKey ? "Ver menos" : "Ver mas"}{" "}
          <DownOutlined
            className={classnames(
              styles["property-details-description__anchor"],
              activeKey && styles["open"]
            )}
          />
        </Button>
      )}
    </div>
  );
};

export default PropertyDetailsDescription;
