import { Property } from "../../../interfaces/properties";

export type PropertyDetailsDescriptionProps = {
  className?: string;
  property: Property;
};
