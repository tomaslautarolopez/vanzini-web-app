import { Property } from "../../../interfaces/properties";

export type PropertyDetailsAgeBarProps = {
  property: Property;
  className?: string;
};
