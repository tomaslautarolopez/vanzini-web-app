import React from 'react';
import classnames from 'classnames';
import dayjs from 'dayjs';

import { PropertyDetailsAgeBarProps } from './types';
import { PropertiesTypes, Property } from '../../../interfaces/properties';
import { isDevelopment } from '../../../utils/isDevelopment';

import styles from './PropertyDetailsAgeBar.module.scss';

const calculateConstructionStatus = ({
  propertyType,
  age,
  constructionStatus,
}: Property) => {
  if (isDevelopment(propertyType)) {
    if (constructionStatus <= 3) {
      return 1;
    } else if (constructionStatus <= 4) {
      return 2;
    } else {
      return 3;
    }
  } else {
    if (age < -1) {
      return 1;
    } else if (age < 0) {
      return 2;
    } else {
      return 3;
    }
  }
};

const constructionLabel = (age: number) => {
  if (age <= -1) {
    return 'En Construcción';
  } else if (age <= 0) {
    return 'A estrenar';
  } else {
    return 'Usado';
  }
};

const dateLabel = (
  age: number,
  constructionDate: string,
  propertyType: PropertiesTypes
) => {
  if (age <= -1) {
    return isDevelopment(propertyType)
      ? `Fecha estimada de entrega: ${dayjs(constructionDate).format(
          'MMMM'
        )} de ${dayjs(constructionDate).format('YYYY')}`
      : 'Consultar fecha estimada de entrega'; 
  } else if (age <= 0) {
    return 'Entrega inmediata';
  } else {
    return `+${age} años de antigüedad`;
  }
};

const PropertyDetailsAgeBar: React.FC<PropertyDetailsAgeBarProps> = ({
  property,
  className,
}) => {
  const { constructionDate, age, propertyType } = property;
  const constructionStatus = calculateConstructionStatus(property);

  return (
    <div className={classnames(styles['property-details-age-bar'], className)}>
      <h5>{constructionLabel(age)}</h5>
      <div className={styles['property-details-age-bar__age-bar']}>
        <div className={styles['property-details-age-bar__age-bar--bar']}>
          <div
            className={classnames(
              styles['property-details-age-bar__dot'],
              constructionStatus >= 1 && styles['completed']
            )}
          >
            <p
              className={classnames(
                constructionStatus >= 1 && styles['completed']
              )}
            >
              Pozo
            </p>
          </div>
          <div
            className={classnames(
              styles['property-details-age-bar__line'],
              constructionStatus >= 2 && styles['completed']
            )}
          ></div>
          <div
            className={classnames(
              styles['property-details-age-bar__dot'],
              constructionStatus >= 2 && styles['completed']
            )}
          >
            <p
              className={classnames(
                constructionStatus >= 2 && styles['completed']
              )}
            >
              Construcción
            </p>
          </div>
          <div
            className={classnames(
              styles['property-details-age-bar__line'],
              constructionStatus >= 3 && styles['completed']
            )}
          ></div>
          <div
            className={classnames(
              styles['property-details-age-bar__dot'],
              constructionStatus >= 3 && styles['completed']
            )}
          >
            <p
              className={classnames(
                constructionStatus >= 3 && styles['completed']
              )}
            >
              Terminado
            </p>
          </div>
        </div>
      </div>
      <p className={styles['property-details-age-bar__date']}>
        {dateLabel(age, constructionDate, propertyType)}
      </p>
    </div>
  );
};

export default PropertyDetailsAgeBar;
