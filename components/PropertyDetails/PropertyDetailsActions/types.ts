import { Property } from "../../../interfaces/properties";

export type PropertyDetailsActionsProps = {
  property: Property;
  className?: string;
};
