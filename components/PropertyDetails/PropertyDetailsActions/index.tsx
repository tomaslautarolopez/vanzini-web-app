import React, { useContext, useRef, useState } from "react";
import { Button } from "antd";
import classnames from "classnames";

import { PropertyDetailsActionsProps } from "./types";
import OnGuardEmployee from "../../OnGuardEmployee";
import ShareLikeAndNotificationButtons from "../../ShareLikeAndNotificationButtons";
import { useOutsideAlerter } from "../../../hooks/useOutsideAlerter";
import { buildUrlForProperty } from "../../../utils/buildUrlForProperty";
import { webURL } from "../../../axios";
import { ModalContext } from "../../../contexts/ModalContext";
import { EmployeeContext } from "../../../contexts/EmployeeContext";
import { propertyLocationFinder } from "../../../utils/propertyLocationFinder";

import styles from "./PropertyDetailsActions.module.scss";

const PropertyDetailsActions: React.FC<PropertyDetailsActionsProps> = ({
  property,
  className,
}) => {
  const shareRef = useRef<HTMLDivElement>(null);
  const [toggle, setToggle] = useState(false);

  const location = propertyLocationFinder(property);

  const { employeesOnGuard } = useContext(EmployeeContext);
  const onGuardEmployee = employeesOnGuard[location];

  const { setModal, setConsultModalData } = useContext(ModalContext);
  useOutsideAlerter(shareRef, () => setToggle(false));

  const propertyUrl = buildUrlForProperty(property);

  return (
    <div className={classnames(styles["property-details-actions"], className)}>
      {onGuardEmployee && <OnGuardEmployee onGuardEmployee={onGuardEmployee} />}
      <div
        ref={shareRef}
        className={styles["property-details-actions__actions"]}
      >
        <ShareLikeAndNotificationButtons
          propertyId={property._id}
          setToggle={setToggle}
          toggle={toggle}
          url={`${webURL}${propertyUrl}`}
          shareText={`Te comparto esta propiedad desde Vanzini Propiedades: ${property.address}`}
        />
        <Button
          type="primary"
          onClick={() => {
            setModal("consultModal", true);
            setConsultModalData({
              propertiesId: [
                {
                  id: property._id,
                  propertyType: property.propertyType,
                },
              ],
              property,
            });
          }}
        >
          Consultar
        </Button>
      </div>
      <Button
        type="primary"
        className={styles["property-details-actions__actions--button"]}
        onClick={() => {
          setModal("consultModal", true);
          setConsultModalData({
            propertiesId: [
              {
                id: property._id,
                propertyType: property.propertyType,
              },
            ],
            property,
          });
        }}
      >
        Consultar
      </Button>
    </div>
  );
};

export default PropertyDetailsActions;
