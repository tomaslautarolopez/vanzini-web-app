import { Property } from "../../../interfaces/properties";

export type PropertyDetailsRecommendationsProps = {
  className?: string;
  property: Property;
};
