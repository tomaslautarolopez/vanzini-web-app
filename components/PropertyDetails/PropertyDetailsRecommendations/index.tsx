import React from "react";
import classnames from "classnames";

import { PropertyDetailsRecommendationsProps } from "./types";

import styles from "./PropertyDetailsRecommendations.module.scss";
import { useRecommendations } from "../../../hooks/useRecommendations";
import PropertyCard from "../../PropertyCard";
import Loading from "../../Loading";
import { isDevelopment } from "../../../utils/isDevelopment";

const PropertyDetailsRecommendations: React.FC<PropertyDetailsRecommendationsProps> = ({
  className,
  property,
}) => {
  const {
    loading,
    setTypeOfrecommendation,
    recommendations,
    typeOfrecommendation,
  } = useRecommendations(property);

  return (
    <div
      className={classnames(
        styles["property-details-recommendations"],
        className
      )}
    >
      <h5>Propiedades similares</h5>
      <p>Filtra segun precio, dormitorios o barrio.</p>
      <div className={styles["property-details-recommendations__filters"]}>
        <button
          className={classnames(
            styles["property-details-recommendations__filters--button"],
            "white-custom-button",
            typeOfrecommendation === "default" && styles["selected"]
          )}
          onClick={() => setTypeOfrecommendation("default")}
        >
          Recomendado
        </button>
        <button
          className={classnames(
            styles["property-details-recommendations__filters--button"],
            "white-custom-button",
            typeOfrecommendation === "price" && styles["selected"]
          )}
          onClick={() => setTypeOfrecommendation("price")}
        >
          Precio
        </button>
        {!isDevelopment(property.propertyType) && (
          <button
            className={classnames(
              styles["property-details-recommendations__filters--button"],
              "white-custom-button",
              typeOfrecommendation === "rooms" && styles["selected"]
            )}
            onClick={() => setTypeOfrecommendation("rooms")}
          >
            Dormitorios
          </button>
        )}
        <button
          className={classnames(
            styles["property-details-recommendations__filters--button"],
            "white-custom-button",
            typeOfrecommendation === "location" && styles["selected"]
          )}
          onClick={() => setTypeOfrecommendation("location")}
        >
          Barrio
        </button>
      </div>
      <div
        className={styles["property-details-recommendations__recommendations"]}
      >
        {!loading &&
          recommendations.map((recommendedProperty) => (
            <PropertyCard
              className={
                styles["property-details-recommendations__property-card"]
              }
              hideActions
              property={recommendedProperty}
              key={recommendedProperty._id}
            />
          ))}
      </div>
      {loading && (
        <Loading
          className={styles["property-details-recommendations__loading"]}
        />
      )}
    </div>
  );
};

export default PropertyDetailsRecommendations;
