import { Property } from "../../../interfaces/properties";

export type PropertyDetailsPriceProps = {
  property: Property;
  className?: string;
};
