import React from "react";
import classnames from "classnames";

import { PropertyDetailsPriceProps } from "./types";
import { useGeneratePriceString } from "../../../hooks/useGeneratePriceString";
import { numberWithDotsAndCommas } from "../../../utils/numberWithDotsAndCommas";
import { propertyHasTag } from "../../../utils/propertyHasTag";

import styles from "./PropertyDetailsPrice.module.scss";

const PropertyDetailsPrice: React.FC<PropertyDetailsPriceProps> = ({
  property,
  className,
}) => {
  const { expenses } = property;
  return (
    <div className={classnames(styles["property-details-price"], className)}>
      <h5>Precio de Venta</h5>
      <h3 className={styles["property-details-price__blue"]}>
        {useGeneratePriceString(property, "Venta")}
      </h3>
      <p>Precio de Alquiler</p>
      <h5
        className={classnames(styles["property-details-price__blue"], "fw-700")}
      >
        {useGeneratePriceString(property, "Alquiler")}
      </h5>
      <p>Expensas</p>
      <h5
        className={classnames(styles["property-details-price__blue"], "fw-700")}
      >
        {expenses ? numberWithDotsAndCommas(expenses) : "---"}
      </h5>
      <p>
        Apta crédito:{" "}
        <span className={styles["property-details-price__blue"]}>
          {propertyHasTag(property, "Apto crédito") ? "Si" : "---"}
        </span>
      </p>
      <p>
        Financiación:{" "}
        <span className={styles["property-details-price__blue"]}>
          {propertyHasTag(property, "Financiación") ? "Si" : "---"}
        </span>
      </p>
    </div>
  );
};

export default PropertyDetailsPrice;
