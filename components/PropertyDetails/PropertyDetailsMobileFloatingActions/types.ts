import { Property } from "../../../interfaces/properties";

export type PropertyDetailsMobileFloatingActionsProps = {
  property: Property;
};
