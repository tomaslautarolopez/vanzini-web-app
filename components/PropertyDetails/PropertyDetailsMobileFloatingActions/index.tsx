import React, { useContext, useRef, useState } from "react";
import { Button } from "antd";

import { PropertyDetailsMobileFloatingActionsProps } from "./types";
import ShareLikeAndNotificationButtons from "../../ShareLikeAndNotificationButtons";
import { webURL } from "../../../axios";
import { buildUrlForProperty } from "../../../utils/buildUrlForProperty";
import { ModalContext } from "../../../contexts/ModalContext";
import { useOutsideAlerter } from "../../../hooks/useOutsideAlerter";

import styles from "./PropertyDetailsMobileFloatingActions.module.scss";

const PropertyDetailsMobileFloatingActions: React.FC<PropertyDetailsMobileFloatingActionsProps> = ({
  property,
}) => {
  const shareRef = useRef<HTMLDivElement>(null);
  const [toggle, setToggle] = useState(false);
  const { setModal, setConsultModalData } = useContext(ModalContext);

  useOutsideAlerter(shareRef, () => setToggle(false));

  const propertyUrl = buildUrlForProperty(property);

  return (
    <div
      ref={shareRef}
      className={styles["property-details-mobile-floating-actions"]}
    >
      <ShareLikeAndNotificationButtons
        propertyId={property._id}
        setToggle={setToggle}
        toggle={toggle}
        url={`${webURL}${propertyUrl}`}
        shareText={`Te comparto esta propiedad desde Vanzini Propiedades: ${property.address}`}
      />
      <Button
        type="primary"
        onClick={() => {
          setModal("consultModal", true);
          setConsultModalData({
            property,
            propertiesId: [
              {
                id: property._id,
                propertyType: property.propertyType,
              },
            ],
          });
        }}
      >
        Consultar
      </Button>
    </div>
  );
};

export default PropertyDetailsMobileFloatingActions;
