import { Property } from "../../../interfaces/properties";

export type PropertyDetailsSideBarActionsProps = {
  property: Property;
};
