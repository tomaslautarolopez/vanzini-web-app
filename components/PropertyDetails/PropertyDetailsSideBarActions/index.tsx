import React, { useContext, useRef, useState } from "react";
import { useTransition, animated } from "react-spring";
import {
  BellFilled,
  BellOutlined,
  HeartFilled,
  HeartOutlined,
  LoadingOutlined,
  ShareAltOutlined,
} from "@ant-design/icons";
import { Button } from "antd";

import { PropertyDetailsSideBarActionsProps } from "./types";
import { useLikeProperty } from "../../../hooks/useLikeProperty";
import ShareButtons from "../../ShareButtons";
import { webURL } from "../../../axios";
import { buildUrlForProperty } from "../../../utils/buildUrlForProperty";
import { useOutsideAlerter } from "../../../hooks/useOutsideAlerter";
import { ModalContext } from "../../../contexts/ModalContext";
import { usePriceAlerts } from "../../../hooks/usePriceAlerts";
import { UserContext } from "../../../contexts/UserContext";

import styles from "./PropertyDetailsSideBarActions.module.scss";

const PropertyDetailsSideBarActions: React.FC<PropertyDetailsSideBarActionsProps> = ({
  property,
}) => {
  const {
    likeProperty,
    dislikeProperty,
    loading: likeLoading,
  } = useLikeProperty();
  const [toggle, setToggle] = useState(false);
  const shareRef = useRef<HTMLDivElement>(null);
  const { setAlertModalData, setModal } = useContext(ModalContext);
  const { removeUserPriceAlert, loading: alertLoading } = usePriceAlerts();

  const {
    user: { data },
  } = useContext(UserContext);
  const liked = data?.likes.has(property._id);
  const alertSetted = data?.alerts.has(property._id);

  useOutsideAlerter(shareRef, () => setToggle(false));

  const transitions = useTransition(toggle, null, {
    from: { position: "absolute", opacity: 0, bottom: 0, width: "100%" },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
  });

  const propertyUrl = buildUrlForProperty(property);

  return (
    <div className={styles["property-details-side-bar-actions"]}>
      <Button
        size="small"
        type="ghost"
        onClick={() =>
          liked ? dislikeProperty(property._id) : likeProperty(property._id)
        }
      >
        {likeLoading ? (
          <LoadingOutlined />
        ) : liked ? (
          <HeartFilled />
        ) : (
          <HeartOutlined />
        )}{" "}
        Guardar
      </Button>
      <Button
        onClick={() => {
          if (alertSetted) {
            removeUserPriceAlert(property._id);
          } else {
            setModal("alertModal", true);
            setAlertModalData({ propertyId: property._id });
          }
        }}
        size="small"
        type="ghost"
      >
        {alertLoading ? (
          <LoadingOutlined />
        ) : alertSetted ? (
          <BellFilled />
        ) : (
          <BellOutlined />
        )}
        Alerta
      </Button>
      <div
        className={styles["property-details-side-bar-actions__share-container"]}
        ref={shareRef}
      >
        {transitions.map(({ item, props, key }) =>
          item ? (
            <animated.div style={props} key={key}>
              <ShareButtons
                url={`${webURL}${propertyUrl}`}
                text={`Te comparto esta propiedad desde Vanzini Propiedades: ${property.address}`}
              />
            </animated.div>
          ) : (
            <animated.div style={props} key={key}>
              <Button
                style={{ width: "100%" }}
                onClick={() => setToggle(true)}
                size="small"
                type="ghost"
              >
                <ShareAltOutlined /> Compartir
              </Button>
            </animated.div>
          )
        )}
      </div>
    </div>
  );
};

export default PropertyDetailsSideBarActions;
