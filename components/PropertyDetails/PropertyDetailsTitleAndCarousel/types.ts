import { Property } from "../../../interfaces/properties";

export type PropertyDetailsTitleAndCarouselProps = {
  property: Property;
};
