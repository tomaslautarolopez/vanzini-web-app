import React, { useRef, useState } from "react";
import { Button, Carousel, Radio } from "antd";
import classnames from "classnames";

import { PropertyDetailsTitleAndCarouselProps } from "./types";
import FullscreenViewer from "../../FullscreenViewer";
import NoImages from "../../../assets/images/no-images.jpg";
import FullscreenButton from "../../../assets/icons/property-details/fullscreen.svg";
import { getFullAddress } from "../../../utils/getFullAddress";
import { Photo } from "../../../interfaces/properties";
import { propertyTypeToString } from "../../../utils/propertyTypeToString";
import carouselButton from "../../../assets/icons/property-card/carousel-button.svg";
import { useFadeInOutWrapper } from "../../../hooks/useFadeInOutWrapper";

import styles from "./PropertyDetailsTitleAndCarousel.module.scss";
import { useEffect } from "react";

const getPropertiesImagesByPictureType = (
  photos: Photo[],
  picturesTypes: "pictures" | "blueprints" | "video"
) => {
  switch (picturesTypes) {
    case "pictures":
      return photos.filter((photo) => !photo.is_blueprint);
    case "blueprints":
      return photos.filter((photo) => photo.is_blueprint);
    case "video":
      return [];
  }
};

const HEADER_CUSTOM_TAGS = [
  "Financiación",
  "Descuento por pago contado",
  "Contado",
  "Acepta permuta",
  "Alquilado",
  "Amoblado",
];

const HEADER_TAGS = [
  "Amenties",
  "Amoblado",
  "Apto Profesional",
  "Acepto crédito",
];

const PropertyDetailsTitleAndCarousel: React.FC<PropertyDetailsTitleAndCarouselProps> =
  ({ property }) => {
    const { photos, publicationTitle, videos } = property;

    const [currentSlide, setCurrentSlide] = useState(0);
    const [fullscren, setFullscreen] = useState(false);
    const [picturesTypes, setPicturesTypes] = useState<
      "pictures" | "blueprints" | "video"
    >("pictures");

    const videoToShow = videos?.[0];

    useEffect(() => {
      if (videoToShow) {
        setPicturesTypes("video");
      }
    }, [videoToShow?.id]);

    const carouselRef = useRef<Carousel | null>();

    const hasBlueprints = photos.some((photo) => photo.is_blueprint);

    const carouselImages = getPropertiesImagesByPictureType(
      photos,
      picturesTypes
    );

    const availableTags = [...HEADER_CUSTOM_TAGS, ...HEADER_TAGS];

    const propertyTypesTags = new Set([
      propertyTypeToString(property.propertyType),
      ...(property.subPropertiesTypes?.map(propertyTypeToString) || []),
    ]);

    const propertyTags = [
      ...(property.customTags || []),
      ...property.tags,
    ].filter((x) => availableTags.includes(x));

    const tags = [...propertyTypesTags, ...propertyTags];

    const leftArrowToogle = currentSlide !== 0;
    const rightArrowToogle =
      photos.length !== 0 && currentSlide < photos.length - 1;

    const carouselArrowLeft = useFadeInOutWrapper(
      <span
        style={{ transform: "rotate(180deg)" }}
        className={classnames(
          styles["property-details-title-and-carousel__carousel--arrow"],
          "dp12-shadow"
        )}
        onClick={() => {
          if (leftArrowToogle) carouselRef.current?.prev();
        }}
      >
        <img src={carouselButton} alt="Botton izquierda carrusel" />
      </span>,
      leftArrowToogle,
      styles["property-details-title-and-carousel__carousel--arrow---left"]
    );

    const carouselArrowRight = useFadeInOutWrapper(
      <span
        className={classnames(
          styles["property-details-title-and-carousel__carousel--arrow"],
          "dp12-shadow"
        )}
        onClick={() => {
          if (rightArrowToogle) carouselRef.current?.next();
        }}
      >
        <img src={carouselButton} alt="Botton derecha carrusel" />
      </span>,
      rightArrowToogle,
      styles["property-details-title-and-carousel__carousel--arrow---right"]
    );

    return (
      <div className={styles["property-details-title-and-carousel"]}>
        {fullscren && (
          <FullscreenViewer
            images={carouselImages.map((photo) => photo.image)}
            onClose={() => setFullscreen(false)}
            currentSlide={currentSlide}
          />
        )}
        <div
          className={styles["property-details-title-and-carousel__carousel"]}
        >
          {!!videoToShow && picturesTypes === "video" && (
            <iframe
              width="100%"
              src={videoToShow.player_url}
              height="100%"
              frameBorder="0"
              allowFullScreen
              title={videoToShow.title}
            />
          )}
          {picturesTypes !== "video" && (
            <>
              <Carousel
                dots={false}
                ref={(ref) => {
                  carouselRef.current = ref;
                }}
                beforeChange={(_, to) => setCurrentSlide(to)}
              >
                {carouselImages.length > 0 ? (
                  carouselImages.map((photo) => (
                    <div
                      key={photo.thumb}
                      className={
                        styles[
                          "property-details-title-and-carousel__carousel--photo"
                        ]
                      }
                    >
                      <img
                        src={photo.image}
                        alt="Foto de propiedad"
                        key={photo.image}
                      />
                    </div>
                  ))
                ) : (
                  <div
                    className={
                      styles[
                        "property-details-title-and-carousel__carousel--photo"
                      ]
                    }
                  >
                    <img
                      src={NoImages}
                      alt="Foto de puerta blanca con logo Vanzini"
                    />
                  </div>
                )}
              </Carousel>
              <div className={classnames(
                    styles["property-details-title-and-carousel__carousel--counter"], "dp2-shadow")}>
                    {currentSlide + 1} / {carouselImages.length}
              </div>
              <div
                className={
                  styles[
                    "property-details-title-and-carousel__carousel--actions"
                  ]
                }
              >
                <Button size="small" onClick={() => setFullscreen(true)}>
                  Full Screen
                </Button>
                <div
                  className={
                    styles[
                      "property-details-title-and-carousel__carousel--actions---carousel-controlers"
                    ]
                  }
                >
                  <Button
                    size="small"
                    onClick={() => {
                      carouselRef.current?.prev()
                      }
                    }
                  >
                    Anterior
                  </Button>
                  <Button
                    size="small"
                    onClick={() => carouselRef.current?.next()}
                  >
                    Siguiente
                  </Button>
                </div>
              </div>
              <div
                className={
                  styles[
                    "property-details-title-and-carousel__carousel--actions-mobile"
                  ]
                }
              >
                <div
                  className={classnames(
                    styles[
                      "property-details-title-and-carousel__carousel--arrow"
                    ],
                    styles[
                      "property-details-title-and-carousel__carousel--arrow---fullscreen"
                    ]
                  )}
                >
                  <img
                    onClick={() => setFullscreen(true)}
                    src={FullscreenButton}
                    alt="Icono de full screen"
                  />
                </div>
                {carouselArrowLeft}
                {carouselArrowRight}
              </div>
            </>
          )}
        </div>
        <div className={styles["property-details-title-and-carousel__header"]}>
          <Radio.Group
            value={picturesTypes}
            onChange={(e) => setPicturesTypes(e.target.value)}
            className={
              styles[
                "property-details-title-and-carousel__picture-type-selector"
              ]
            }
          >
            <Radio.Button value="pictures">Fotos</Radio.Button>
            <Radio.Button disabled={!hasBlueprints} value="blueprints">
              Planos
            </Radio.Button>
            <Radio.Button disabled={!videos?.length} value="video">
              Video
            </Radio.Button>
          </Radio.Group>
          <h3>{publicationTitle}</h3>
          <h5>{getFullAddress(property)}</h5>
          <div className={styles["property-details-title-and-carousel__tags"]}>
            {tags.map((tag) => (
              <span
                key={tag}
                className={
                  styles["property-details-title-and-carousel__tags--item"]
                }
              >
                {tag}
              </span>
            ))}
          </div>
        </div>
      </div>
    );
  };

export default PropertyDetailsTitleAndCarousel;
