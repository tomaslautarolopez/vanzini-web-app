import React, { useState } from "react";
import { Button } from "antd";

import { AboutUsLocationProps } from "./types";
import map from "../../../assets/images/AboutUs/map.png";
import OfficeBrownImage1 from "../../../assets/images/AboutUs/office/brown/office-1.png";
import OfficeBrownImage2 from "../../../assets/images/AboutUs/office/brown/office-2.png";
import OfficeBrownImage3 from "../../../assets/images/AboutUs/office/brown/office-3.png";
import OfficeFishertonImage1 from "../../../assets/images/AboutUs/office/fisherton/office-1.png";
import OfficeFishertonImage2 from "../../../assets/images/AboutUs/office/fisherton/office-2.png";
import OfficeFishertonImage3 from "../../../assets/images/AboutUs/office/fisherton/office-3.png";
import { useFadeInOutWrapper } from "../../../hooks/useFadeInOutWrapper";
import { defaultMapIcons } from "../../../constants/map";

import styles from "./AboutUsLocation.module.scss";

const AboutUsLocation: React.FC<AboutUsLocationProps> = () => {
  const [office, setOffice] = useState<"browm" | "fisherton">("browm");

  const brownComponent = useFadeInOutWrapper(
    <>
      <div className={styles["about-us-location__office--images"]}>
        <div>
          <img src={OfficeBrownImage1} />
        </div>
        <div className={styles["about-us-location__office--images---column"]}>
          <img src={OfficeBrownImage2} />
          <img src={OfficeBrownImage3} />
        </div>
      </div>
      <h6>
        Oficinas en el Centro de Rosario
        <br /> Brown 2063, Piso 3 y 4<br /> Teléfono: +54 341 425 5215
        <br /> Correo electrónico: contacto@vanzini.com.ar
      </h6>
    </>,
    office === "browm"
  );

  const fishertonComponent = useFadeInOutWrapper(
    <>
      <div className={styles["about-us-location__office--images"]}>
        <div>
          <img src={OfficeFishertonImage1} />
        </div>
        <div className={styles["about-us-location__office--images---column"]}>
          <img src={OfficeFishertonImage2} />
          <img src={OfficeFishertonImage3} />
        </div>
      </div>
      <h6>
        Sucursal Fisherton - Funes
        <br /> Schweitzer 8883, piso 1<br /> Teléfono: +54 341 425 5215
        <br /> Correo electrónico: contacto@vanzini.com.ar
      </h6>
    </>,
    office === "fisherton"
  );

  return (
    <div className={styles["about-us-location"]}>
      <span className={styles["about-us-location__tag"]}>
        ¿dónde encontrarnos?
      </span>
      <h3>
        Nos encontramos en el <b>Centro</b> y <b>Fisherton</b>
      </h3>
      <h6 className={styles["about-us-location__description"]}>
        Nuestras sucursales se encuentran ubicadas estratégicamente para que
        nuestra atención sea cada vez más personalizada y cerca.
      </h6>
      <div className={styles["about-us-location__office"]}>
        <div
          className={styles["about-us-location__office--map"]}
          style={{ backgroundImage: `url(${map})` }}
        >
          <img
            className={styles["about-us-location__office--map---brown"]}
            src={defaultMapIcons.OF}
          />
          <img
            className={styles["about-us-location__office--map---fisherton"]}
            src={defaultMapIcons.OF}
          />
        </div>
        <div className={styles["about-us-location__office--description"]}>
          <div
            className={
              styles["about-us-location__office--description---container"]
            }
          >
            {brownComponent}
            {fishertonComponent}
          </div>
          <Button
            type={office === "browm" ? "primary" : undefined}
            onClick={() => setOffice("browm")}
          >
            Sucursal Centro
          </Button>
          <Button
            type={office === "fisherton" ? "primary" : undefined}
            onClick={() => setOffice("fisherton")}
          >
            Sucursal Fisherton
          </Button>
        </div>
      </div>
    </div>
  );
};

export default AboutUsLocation;
