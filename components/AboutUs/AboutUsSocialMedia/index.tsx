import React from "react";

import { AboutUsSocialMediaProps } from "./types";
import vanzini from "../../../assets/icons/about-us/social-media/vanzini.svg";
import youtube from "../../../assets/icons/about-us/social-media/youtube.svg";
import instagram from "../../../assets/icons/about-us/social-media/instagram.svg";
import facebook from "../../../assets/icons/about-us/social-media/facebook.svg";
import spotify from "../../../assets/icons/about-us/social-media/spotify.svg";
import pinterest from "../../../assets/icons/about-us/social-media/pinterest.svg";
import { useCountProperties } from "../../../hooks/useCountProperties";
import { numberWithDotsAndCommas } from "../../../utils/numberWithDotsAndCommas";

import styles from "./AboutUsSocialMedia.module.scss";

const AboutUsSocialMedia: React.FC<AboutUsSocialMediaProps> = () => {
  const { propertiesCount } = useCountProperties(undefined);

  return (
    <div className={styles["about-us-social-media"]}>
      <span className={styles["about-us-social-media__tag"]}>
        Redes sociales{" "}
      </span>
      <h3>
        Somos <b>sociables</b>. compartimos contenido diariamente
      </h3>
      <h6 className={styles["about-us-social-media__description"]}>
        Somos sociables, compartimos diariamente contenido en todas nuestras
        redes sociales. Seguinos y sé parte de nuestra comunidad.
      </h6>
      <div className={styles["about-us-social-media__container"]}>
        <a href="https://www.youtube.com/channel/UC2RFQcXNsN-cjQhVn4JHADw">
          <div className={styles["about-us-social-media__social-media-item"]}>
            <img src={youtube} />
            <h3>Youtube</h3>
            <h6
              className={
                styles["about-us-social-media__social-media-item--description"]
              }
            >
              Conocé nuestro canal y suscribite para ver todo nuestro contenido
              audiovisual
            </h6>
            <h6
              className={
                styles["about-us-social-media__social-media-item--link"]
              }
            >
              Suscribite
            </h6>
          </div>
        </a>
        <a href="https://www.instagram.com/vanzinipropiedades/" target="_blank">
          <div className={styles["about-us-social-media__social-media-item"]}>
            <img src={instagram} />
            <h3>Instagram</h3>
            <h6
              className={
                styles["about-us-social-media__social-media-item--description"]
              }
            >
              Subimos contenido sobre arquitectura y real estate, comercial y
              entretenimiento.
            </h6>
            <h6
              className={
                styles["about-us-social-media__social-media-item--link"]
              }
            >
              Seguir
            </h6>
          </div>
        </a>
        <a href="https://www.facebook.com/vanzini.prop" target="_blank">
          <div className={styles["about-us-social-media__social-media-item"]}>
            <img src={facebook} />
            <h3>Facebook</h3>
            <h6
              className={
                styles["about-us-social-media__social-media-item--description"]
              }
            >
              Sumate a la comunidad de nuestros fans y recibí contenido
              diariamente.
            </h6>
            <h6
              className={
                styles["about-us-social-media__social-media-item--link"]
              }
            >
              Seguir
            </h6>
          </div>
        </a>
        <a
          href="https://open.spotify.com/user/b7m3f3xjx7yx8rnnez8g8heid?si=5Wb7cRrXTsySE2LbNv6-wA"
          target="_blank"
        >
          <div className={styles["about-us-social-media__social-media-item"]}>
            <img src={spotify} />
            <h3>Spotify</h3>
            <h6
              className={
                styles["about-us-social-media__social-media-item--description"]
              }
            >
              Seguí nuestras listas de música para disfrutar en cualquier
              situación.
            </h6>
            <h6
              className={
                styles["about-us-social-media__social-media-item--link"]
              }
            >
              Seguir
            </h6>
          </div>
        </a>
        <div className={styles["about-us-social-media__social-media-item"]}>
          <img src={vanzini} />
          <h3>Web</h3>
          <h6
            className={
              styles["about-us-social-media__social-media-item--description"]
            }
          >
            Nuestra web cuenta con herramientas prácticas para facilitar la
            búsqueda.
          </h6>
          <h6
            className={styles["about-us-social-media__social-media-item--link"]}
          >
            {propertiesCount !== undefined &&
              numberWithDotsAndCommas(propertiesCount)}{" "}
            propiedades
          </h6>
        </div>
        <a href="https://www.pinterest.es/vanzinipropiedades/" target="_blank">
          <div className={styles["about-us-social-media__social-media-item"]}>
            <img src={pinterest} />
            <h3>Pinterest</h3>
            <h6
              className={
                styles["about-us-social-media__social-media-item--description"]
              }
            >
              Fotografía artística sobre arquitectura local y de todo el mundo.{" "}
            </h6>
            <h6
              className={
                styles["about-us-social-media__social-media-item--link"]
              }
            >
              Seguir
            </h6>
          </div>
        </a>
      </div>
    </div>
  );
};

export default AboutUsSocialMedia;
