import React from "react";

import { AboutUsEmployeesProps } from "./types";
import { useGetEmployees } from "../../../hooks/useGetEmployees";
import AboutUsEmployeeCard from "../AboutUsEmployeeCard";
import LinkedIn from "../../../assets/icons/about-us/linkedin.svg";

import styles from "./AboutUsEmployees.module.scss";

const AboutUsEmployees: React.FC<AboutUsEmployeesProps> = () => {
  const { data } = useGetEmployees();

  const sellersManager = data?.find(
    (employee) => employee.employeeType === "Gerente de Ventas"
  );

  return (
    <div className={styles["about-us-employees"]}>
      <span className={styles["about-us-employees__tag"]}>NUESTRO EQUIPO</span>
      <h3>
        Contactate con nuestro <b>equipo de ventas</b>
      </h3>
      <h6 className={styles["about-us-employees__description"]}>
        Nuestro equipo de agentes inmobiliarios está disponible por correo y
        whatsapp, listo para brindarte la mejor atención.
      </h6>
      <div className={styles["about-us-employees__employees"]}>
        {data
          ?.filter((employee) => employee.employeeType !== "Gerente de Ventas")
          .map((employee) => (
            <AboutUsEmployeeCard employee={employee} key={employee._id} />
          ))}
        {sellersManager && (
          <div className={styles["about-us-employees__sellers-manager"]}>
            <img
              className={styles["about-us-employees__sellers-manager--picture"]}
              src={sellersManager.pictureUrl}
            />
            <h5 className={styles["about-us-employees__sellers-manager--name"]}>
              {sellersManager.name}
            </h5>
            <h5 className={styles["about-us-employees__sellers-manager--role"]}>
              {sellersManager.employeeType}
            </h5>
            <div
              className={styles["about-us-employees__sellers-manager--actions"]}
            >
              <a
                target="_blank"
                rel="noopener noreferrer"
                href={`mailto:${sellersManager.email}`}
              >
                <button>Solicitar reunión</button>
              </a>
            </div>
          </div>
        )}
        <div className={styles["about-us-employees__linked-in"]}>
          <img
            className={styles["about-us-employees__linked-in--picture"]}
            src={LinkedIn}
          />
          <h5 className={styles["about-us-employees__linked-in--name"]}>
            Vanzini-prop
          </h5>
          <h5 className={styles["about-us-employees__linked-in--role"]}>
            Visitá nuestra pagina de Linkdin
          </h5>
          <div className={styles["about-us-employees__linked-in--actions"]}>
            <a
              target="_blank"
              href="https://www.linkedin.com/company/vanzini-prop/"
            >
              <button>Ir a Linkdin</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutUsEmployees;
