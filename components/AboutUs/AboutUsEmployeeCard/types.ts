import { Employee } from "../../../interfaces/employee";

export type AboutUsEmployeeCardProps = {
  employee: Employee;
};
