import React from "react";
import { Button } from "antd";

import { AboutUsEmployeeCardProps } from "./types";
import { isMobileWeb } from "../../../utils/isMobileWeb";

import styles from "./AboutUsEmployeeCard.module.scss";

const AboutUsEmployeeCard: React.FC<AboutUsEmployeeCardProps> = ({
  employee,
}) => {
  return (
    <div className={styles["about-us-employee-card"]}>
      <img
        className={styles["about-us-employee-card__picture"]}
        src={employee.pictureUrl}
      />
      <h5 className={styles["about-us-employee-card__name"]}>
        {employee.name}
      </h5>
      <h5 className={styles["about-us-employee-card__role"]}>
        {employee.employeeType}
      </h5>
      <div className={styles["about-us-employee-card__actions"]}>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={`mailto:${employee.email}`}
        >
          <Button size="small" className="email">
            Correo
          </Button>
        </a>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={
            isMobileWeb()
              ? `https://api.whatsapp.com/send?phone=${employee.phonenumber}`
              : `https://web.whatsapp.com/send?phone=${employee.phonenumber}`
          }
        >
          <Button size="small">Whatsapp</Button>
        </a>
      </div>
    </div>
  );
};

export default AboutUsEmployeeCard;
