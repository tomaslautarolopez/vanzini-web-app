import React, { useContext, useEffect } from "react";
import { Button, Form, Input, Select } from "antd";

import { AssessFormProps } from "./types";
import { UserContext } from "../../contexts/UserContext";
import OnGuardEmployeeContact from "../OnGuardEmployeeContact";
import { ConsultForm, useConsult } from "../../hooks/useConsult";

import styles from "./AssessForm.module.scss";

const { TextArea } = Input;
const { Option } = Select;

const TIPLOGY_OPTIONS = [
  "casa",
  "pasillo",
  "departamento",
  "terreno",
  "galpon",
  "local",
  "oficina",
  "cochera",
  "campo",
  "emprendimiento",
  "loteo",
];

const AssessForm: React.FC<AssessFormProps> = ({ onCancel }) => {
  const {
    user: { data },
  } = useContext(UserContext);
  const [form] = Form.useForm();
  const { loading, makeConsult } = useConsult();

  useEffect(() => {
    form.setFieldsValue({
      name: data?.name,
      email: data?.email,
    });
  }, [data]);

  return (
    <div className={styles["assess-form"]}>
      <OnGuardEmployeeContact consultType="ASSES" />
      <Form
        id="assess-form"
        name="assess-form"
        layout="vertical"
        onFinish={(value) => {
          const consult: ConsultForm = {
            consultText: `Dirección: ${value.address}. \n Tipología: ${value.typology}. \n Detalles: ${value.consultText} \n `,
            email: value.email,
            name: value.name,
            phonenumber: value.phonenumber,
          };

          makeConsult(consult, "ASSES", onCancel);
        }}
        requiredMark={false}
        form={form}
      >
        <Form.Item
          name="name"
          label="NOMBRE"
          rules={[
            {
              required: true,
              message: "Por favor, ingrese su nombre.",
            },
          ]}
        >
          <Input
            placeholder="Nombre y apelldio"
            style={{ width: "100%", height: "32px" }}
          />
        </Form.Item>
        <Form.Item
          name="email"
          label="EMAIL"
          rules={[
            {
              required: true,
              message: "Por favor, ingrese un email.",
            },
            {
              type: "email",
              message: "Debes ingresar un email válido.",
            },
          ]}
        >
          <Input
            placeholder="nombre@email.com"
            style={{ width: "100%", height: "32px" }}
          />
        </Form.Item>
        <Form.Item name="phonenumber" label="TELÉFONO">
          <Input
            placeholder="3411-1111"
            style={{ width: "100%", height: "32px" }}
          />
        </Form.Item>
        <Form.Item name="address" label="DIRECCIÓN">
          <Input
            placeholder="Marcos Paz 3637"
            style={{ width: "100%", height: "32px" }}
          />
        </Form.Item>
        <Form.Item
          name="typology"
          label="TIPOLOGÍA"
          rules={[
            {
              required: true,
              message:
                "Por favor, elige el tipo de propiedad que deseas tazar.",
            },
          ]}
        >
          <Select style={{ width: "100%" }}>
            {TIPLOGY_OPTIONS.map((type) => (
              <Option value={type}>{type}</Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item name="consultText">
          <TextArea placeholder="Más información" />
        </Form.Item>
        <Button
          loading={loading}
          type="primary"
          htmlType="submit"
          style={{ width: "100%" }}
        >
          ENVIAR
        </Button>
      </Form>
    </div>
  );
};

export default AssessForm;
