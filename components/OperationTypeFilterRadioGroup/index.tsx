import React, { useRef } from 'react';
import { Radio } from 'antd';
import { OperationTypeFilterRadioGroupProps } from './types';
import { operationTypeOptions } from '../../constants/filters';
import { useCountProperties } from '../../hooks/useCountProperties';
import { Filters } from '../../contexts/FiltersContext';

const OperationTypeFilterRadioGroup: React.FC<OperationTypeFilterRadioGroupProps> =
  (props) => {
    const { operationType, setOperationType } = props;
    const filter = useRef<Filters>({
      operationType: 'Alquiler',
    });

    const { propertiesCount: rentPropertiesCount } = useCountProperties(
      filter.current
    );

    return (
      <Radio.Group
        value={operationType}
        onChange={(e) => setOperationType(e.target.value)}
      >
        {operationTypeOptions.map((option) => (
          <Radio value={option.value} key={option.value}>
            {option.value === 'Alquiler'
              ? `${option.label} (${rentPropertiesCount || 0} propiedades)`
              : option.label}
          </Radio>
        ))}
      </Radio.Group>
    );
  };

export default OperationTypeFilterRadioGroup;
