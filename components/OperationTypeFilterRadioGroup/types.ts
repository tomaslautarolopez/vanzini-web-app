import { OperationType } from "../../contexts/FiltersContext";

export type OperationTypeFilterRadioGroupProps = {
  operationType?: OperationType;
  setOperationType: (value: React.SetStateAction<OperationType | undefined>) => void;
};
