import React, { useContext } from "react";
import { Modal } from "antd";

import { AssessModalProps } from "./types";
import { ModalContext } from "../../contexts/ModalContext";
import AssessForm from "../AssessForm";

import styles from "./AssessModal.module.scss";

const AssessModal: React.FC<AssessModalProps> = () => {
  const {
    modals: { assessModal },
    setModal,
  } = useContext(ModalContext);

  const onCancel = () => setModal("assessModal", false);

  return (
    <Modal
      destroyOnClose
      className={styles["assess-modal"]}
      visible={assessModal}
      footer={null}
      onCancel={onCancel}
    >
      <AssessForm onCancel={onCancel} />
    </Modal>
  );
};

export default AssessModal;
