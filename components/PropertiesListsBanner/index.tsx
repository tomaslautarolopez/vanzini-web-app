import React from "react";
import { useRouter } from "next/router";

import { PropertiesListsBannerProps } from "./types";
import Banner from "../Banner";
import { COLORS } from "../Banner/types";

const PropertiesListsBanner: React.FC<PropertiesListsBannerProps> = () => {
  const { push } = useRouter();

  return (
    <Banner
      color={COLORS.BLUE}
      buttonContent={"Ir a listados"}
      onClick={() => push("/listados")}
      textContent={
        <>
          Selección destacada.
          <br /> Ingresá a ver nuestros listados.
        </>
      }
      titleContent={
        <>
          <b>Selección destacada.</b> Ingresá a ver nuestros listados.
        </>
      }
    />
  );
};

export default PropertiesListsBanner;
