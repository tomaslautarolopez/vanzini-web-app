import React from "react";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import classnames from "classnames";

import { ThanksForConsultLayoutProps } from "./types";

import styles from "./ThanksForConsultLayout.module.scss";

const ThanksForConsultLayout: React.FC<ThanksForConsultLayoutProps> = ({
  background,
  children,
  contentClassName,
  hideBackButton,
}) => {
  const router = useRouter();

  const backButton = !hideBackButton && (
    <button
      onClick={() => router.back()}
      className={styles["thanks-for-consult-layout__content--back-button"]}
    >
      <ArrowLeftOutlined /> ATRAS
    </button>
  );

  return (
    <div className={styles["thanks-for-consult-layout"]}>
      {!hideBackButton && (
        <div
          className={
            styles["thanks-for-consult-layout__back-button-container-mobile"]
          }
        >
          {backButton}
        </div>
      )}
      <div className={styles["thanks-for-consult-layout__content"]}>
        <div
          className={
            styles["thanks-for-consult-layout__content--elements-container"]
          }
        >
          <div
            className={classnames(
              styles["thanks-for-consult-layout__content--elements"],
              contentClassName
            )}
          >
            <div
              className={
                styles[
                  "thanks-for-consult-layout__back-button-container-desktop"
                ]
              }
            >
              {backButton}
            </div>
            {children}
          </div>
        </div>
        <div className={styles["thanks-for-consult-layout__background"]}>
          <img src={background} alt="Ciudad de Rosario" />
        </div>
      </div>
    </div>
  );
};

export default ThanksForConsultLayout;
