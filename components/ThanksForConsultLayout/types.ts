export type ThanksForConsultLayoutProps = {
  background: string;
  contentClassName?: string;
  hideBackButton?: boolean;
};
