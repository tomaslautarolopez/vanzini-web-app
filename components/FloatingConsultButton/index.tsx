import React, { useContext } from "react";
import classnames from "classnames";

import { FloatingConsultButtonProps } from "./types";
import Send from "../../assets/icons/consults/send.svg";
import { ModalContext } from "../../contexts/ModalContext";

import styles from "./FloatingConsultButton.module.scss";

const FloatingConsultButton: React.FC<FloatingConsultButtonProps> = ({
  className,
  showOnMobile,
}) => {
  const { setModal } = useContext(ModalContext);

  return (
    <div
      onClick={() => setModal("consultModal", true)}
      className={classnames(
        styles["floating-consult-button"],
        !showOnMobile && styles["floating-consult-button__hide-modile"],
        "dp8-shadow",
        className
      )}
    >
      <img src={Send} alt="Icono de avion de papel" />
    </div>
  );
};

export default FloatingConsultButton;
