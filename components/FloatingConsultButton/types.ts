export type FloatingConsultButtonProps = {
  className?: string;
  showOnMobile?: boolean;
};
