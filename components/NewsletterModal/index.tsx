import React, { useContext } from "react";

import { NewsletterModalProps } from "./types";
import ModalWithImage from "../ModalWithImage";
import NewsletterBg from "../../assets/images/Newsletter/newsletter-bg.jpg";
import FlagIcon from "../../assets/icons/general/flag.svg";
import NewsletterForm from "../NewsletterForm";
import { ModalContext } from "../../contexts/ModalContext";

import styles from "./NewsletterModal.module.scss";

const NewsletterModal: React.FC<NewsletterModalProps> = ({}) => {
  const {
    modals: { newsletterModal },
    setModal,
  } = useContext(ModalContext);

  return (
    <ModalWithImage
      style={{
        width: 840,
      }}
      imageClassname={styles["newsletter-modal__image"]}
      image={NewsletterBg}
      altImage="Barquito de papel escultura Rosario"
      visible={newsletterModal}
      onCancel={() => setModal("newsletterModal", false)}
      footer={null}
      header={
        <h6 className={styles["newsletter-modal__header"]}>
          <img src={FlagIcon} alt="Escudo de armas vanzini" /> NEWSLETTER
        </h6>
      }
    >
      <div className={styles["newsletter-modal"]}>
        <h3>
          Suscribite y recibi las
          <br /> últimas ofertas!
        </h3>
        <p>Enterate primero de las últimas ofertas y propiedades en venta.</p>
        <NewsletterForm callback={() => setModal("newsletterModal", false)} />
        <h6 className={styles["newsletter-modal__footer"]}>
          Suscribiendote aceptás los{" "}
          <a href="/terminos-y-condiciones" target="_blank">
            términos y condiciones de uso.
          </a>
        </h6>
      </div>
    </ModalWithImage>
  );
};

export default NewsletterModal;
