import React from "react";
import { Form, Input } from "antd";

import { NewsletterFormProps } from "./types";
import { useNewsletter } from "../../hooks/useNewsletter";

import styles from "./NewsletterForm.module.scss";
import { LoadingOutlined } from "@ant-design/icons";

const NewsletterForm: React.FC<NewsletterFormProps> = ({ callback }) => {
  const {
    suscribeToNewsletter,
    loading: { suscribing },
  } = useNewsletter();

  return (
    <Form
      id="newsletter-form"
      name="newsletter-form"
      layout="vertical"
      onFinish={({ email }) => suscribeToNewsletter(email, callback)}
    >
      <Form.Item
        name="email"
        label="EMAIL"
        rules={[
          {
            required: true,
            message: "Por favor, ingrese su email.",
          },
        ]}
      >
        <Input
          placeholder="nombre@email.com"
          style={{ width: "100%", height: "32px" }}
        />
      </Form.Item>
      <button className={styles["newsletter-form__button"]} type="submit">
        QUIERO RECIBIR OFERTAS
        {suscribing && <LoadingOutlined />}
      </button>
    </Form>
  );
};

export default NewsletterForm;
