import React, { useContext } from "react";
import classnames from "classnames";
import Link from "next/link";

import { ModalContext } from "../../contexts/ModalContext";
import { PageFooterProps } from "./types";

import VanziniLogo from "../../assets/icons/page-footer/vanzini-logo.svg";
import FacebookLogo from "../../assets/icons/page-footer/Facebook.svg";
import SpotifyLogo from "../../assets/icons/page-footer/Spotify.svg";
import YoutubeLogo from "../../assets/icons/page-footer/Youtube.svg";
import PinterestLogo from "../../assets/icons/page-footer/Pinterest.svg";
import InstagramLogo from "../../assets/icons/page-footer/Instagram.svg";

import styles from "./PageFooter.module.scss";

const PageFooter: React.FC<PageFooterProps> = ({ className }) => {
  const { setModal } = useContext(ModalContext);

  return (
    <div className={classnames(styles["page-footer"], className)}>
      <div className={styles["page-footer__info"]}>
        <div
          className={classnames(
            styles["page-footer__info--column"],
            styles["page-footer__info--logo"]
          )}
        >
          <img src={VanziniLogo} alt="Logo inmobiliaria Vanzini en blanco" />
          <p>
            Desde 1965 liderando el mercado inmobiliario de
            <br /> Rosario y alrededores. Expertos en venta de
            <br /> propiedades.
          </p>
          <div className={styles["page-footer__info--social-media"]}>
            <a
              rel="noreferrer"
              href="https://www.facebook.com/vanzini.prop"
              target="_blank"
            >
              <img src={FacebookLogo} />
            </a>
            <a
              rel="noreferrer"
              href="https://open.spotify.com/user/b7m3f3xjx7yx8rnnez8g8heid?si=5Wb7cRrXTsySE2LbNv6-wA"
              target="_blank"
            >
              <img src={SpotifyLogo} />
            </a>
            <a
              rel="noreferrer"
              href="https://www.youtube.com/user/VanziniPropiedades"
              target="_blank"
            >
              <img src={YoutubeLogo} />
            </a>
            <a
              rel="noreferrer"
              href="https://www.pinterest.es/vanzinipropiedades/"
              target="_blank"
            >
              <img src={PinterestLogo} />
            </a>
            <a
              rel="noreferrer"
              href="https://www.instagram.com/vanzinipropiedades/"
              target="_blank"
            >
              <img src={InstagramLogo} />
            </a>
          </div>
        </div>
        <div
          className={classnames(
            styles["page-footer__info--column"],
            styles["page-footer__info--data"]
          )}
        >
          <h6 className="fw-700">SERVICIOS</h6>
          <Link href="/feed">
            <a>Comprar</a>
          </Link>
          <Link href="/tasaciones">
            <a>Vender</a>
          </Link>
          <Link href="/feed/emprendimientos?operacion=Venta&antiguedad=en-construccion&moneda=ARS">
            <a>Invertir</a>
          </Link>
          <Link href="/?operacion=Alquiler">
            <a>Alquilar</a>
          </Link>
        </div>
        <div
          className={classnames(
            styles["page-footer__info--column"],
            styles["page-footer__info--data"]
          )}
        >
          <h6 className="fw-700">EMPRESA</h6>
          <Link href="/listados">
            <a>Clasificados</a>
          </Link>
          <Link href="/mapa">
            <a>Mapa</a>
          </Link>
          <a onClick={() => setModal("newsletterModal", true)}>Newsletters</a>
          <Link href="/nosotros">
            <a>Nosotros</a>
          </Link>
        </div>
        <div
          className={classnames(
            styles["page-footer__info--column"],
            styles["page-footer__info--data"],
            styles["page-footer__info--data---address"]
          )}
        >
          <h6 className="fw-700">CONTACTO</h6>
          <a
            rel="noreferrer"
            href="https://g.page/vanzini-propiedades?share"
            target="_blank"
          >
            Oficinas Centro
            <br /> Brown 2063, Piso 3 y 4.
          </a>
          <a
            rel="noreferrer"
            href="https://g.page/vanzinipropiedadesfisherton?share"
            target="_blank"
          >
            Oficinas Fisherton
            <br /> Schweitzer 8883, Aldea Lago
          </a>
          <h6>
            Teléfono
            <br /> +54 341 425 5215
          </h6>
          <a href="mailto:web@vanzini.com.ar">
            Correo electrónico
            <br /> contacto@vanzini.com.ar
          </a>
        </div>
      </div>
      <div className={styles["page-footer__footer"]}>
        <h6 className="fw-500">
          Vanzini Black S.A.© Todos los Derechos Reservados
        </h6>
        <a href="/terminos-y-condiciones" target="_blank">
          Términos y condiciones de uso
        </a>
      </div>
    </div>
  );
};

export default PageFooter;
