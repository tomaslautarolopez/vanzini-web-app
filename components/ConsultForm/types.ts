import { Location } from "../../interfaces/employee";
import { PropertiesTypes } from "../../interfaces/properties";

export type ConsultFormProps = {
  propertiesId?: {
    id: string;
    propertyType: PropertiesTypes;
    units?: string[];
  }[];
  onCancel?: () => void;
  location?: Location;
};
