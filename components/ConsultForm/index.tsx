import React, { useContext, useEffect, useRef } from 'react';
import { Button, Form, Input } from 'antd';

import { ConsultFormProps } from './types';
import { useGetPropertyAddress } from '../../hooks/useGetPropertyAddress';
import { UserContext } from '../../contexts/UserContext';
import {
  ConsultForm as ConsultFormType,
  useConsult,
} from '../../hooks/useConsult';
import { isDevelopment as isDevelopmentFun } from '../../utils/isDevelopment';
import OnGuardEmployeeContact from '../OnGuardEmployeeContact';

import styles from './ConsultForm.module.scss';

const TextArea = Input.TextArea;

const generateUnitString = (units?: string[]) => {
  return units?.length ? `\n    Unidades: ${units.join(' - ')}` : '';
};

const ConsultForm: React.FC<ConsultFormProps> = ({
  propertiesId,
  onCancel,
  location,
}) => {
  const pIds = propertiesId?.map((pi) => pi.id);
  const consultTextRef = useRef<any>(null);
  const propertiesAddress = useGetPropertyAddress(pIds || []);
  const {
    user: { data },
  } = useContext(UserContext);
  const [form] = Form.useForm();
  const { loading, makeConsult } = useConsult();

  const areDevelopments = propertiesId?.some(({ propertyType }) =>
    isDevelopmentFun(propertyType)
  );

  const consultType = areDevelopments
    ? 'DEVELOPMENT'
    : propertiesId?.length
    ? 'PROPERTY'
    : 'GENERAL';

  useEffect(() => {
    if (propertiesAddress.length) {
      setTimeout(() => {
        consultTextRef.current?.focus({
          cursor: 'start',
        });
      }, 1000);

      form.setFieldsValue({
        consultText: `Hola me gustaria recibir más informacion sobre: \n${propertiesAddress
          .map(
            (addr, index) =>
              `- ${addr}${generateUnitString(propertiesId?.[index]?.units)}`
          )
          .join('\n')}`,
      });
    }
  }, [form, propertiesAddress, propertiesId]);

  useEffect(() => {
    form.setFieldsValue({
      name: data?.name,
      email: data?.email,
    });
  }, [data]);

  return (
    <div className={styles['consult-form']}>
      <OnGuardEmployeeContact
        location={location}
        consultType={consultType}
        pIds={pIds}
      />
      <Form
        id="consult-form"
        name="consult-form"
        layout="vertical"
        onFinish={(value) => {
          const consult: ConsultFormType = {
            consultText: value.consultText,
            email: value.email,
            name: value.name,
            phonenumber: value.phonenumber,
            propertyId: pIds,
          };

          makeConsult(consult, consultType, onCancel);
        }}
        requiredMark={false}
        form={form}
      >
        <Form.Item
          name="name"
          label="NOMBRE"
          rules={[
            {
              required: true,
              message: 'Por favor, ingrese su nombre.',
            },
          ]}
        >
          <Input
            placeholder="Nombre y apelldio"
            style={{ width: '100%', height: '32px' }}
          />
        </Form.Item>
        <Form.Item
          name="email"
          label="EMAIL"
          rules={[
            {
              required: true,
              message: 'Por favor, ingrese un email.',
            },
            {
              type: 'email',
              message: 'Debes ingresar un email válido.',
            },
          ]}
        >
          <Input
            placeholder="nombre@email.com"
            style={{ width: '100%', height: '32px' }}
          />
        </Form.Item>
        <Form.Item name="phonenumber" label="TELÉFONO">
          <Input
            placeholder="3411-1111"
            style={{ width: '100%', height: '32px' }}
          />
        </Form.Item>
        <Form.Item name="consultText">
          <TextArea
            ref={consultTextRef}
            placeholder="Más información"
            rows={4}
          />
        </Form.Item>
        <Button
          loading={loading}
          type="primary"
          htmlType="submit"
          style={{ width: '100%' }}
        >
          ENVIAR
        </Button>
      </Form>
    </div>
  );
};

export default ConsultForm;
