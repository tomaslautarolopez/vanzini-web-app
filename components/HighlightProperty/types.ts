import { HighlightProperty } from "../../hooks/useGetHighlightProperties";

export type HighlightPropertyProps = {
  property: HighlightProperty;
  className?: string;
};
