import React, { useRef, useState } from "react";
import { Button } from "antd";
import classnames from "classnames";

import { HighlightPropertyProps } from "./types";
import { amenitiesTags } from "../../constants/tags";
import box from "../../assets/icons/highlight-property/box.svg";
import badge from "../../assets/icons/highlight-property/badge.svg";
import { propertyTypeToString } from "../../utils/propertyTypeToString";
import { getFullAddress } from "../../utils/getFullAddress";
import ShareLikeAndNotificationButtons from "../ShareLikeAndNotificationButtons";
import { buildUrlForProperty } from "../../utils/buildUrlForProperty";
import { webURL } from "../../axios";
import { useOutsideAlerter } from "../../hooks/useOutsideAlerter";
import { useGeneratePriceString } from "../../hooks/useGeneratePriceString";
import ResponsiveLink from "../ResponsiveLink";

import styles from "./HighlightProperty.module.scss";
import { numberWithDotsAndCommas } from "../../utils/numberWithDotsAndCommas";

const HighlightProperty: React.FC<HighlightPropertyProps> = ({
  property,
  className,
}) => {
  const { propertyType, tags, totalSurfaceMax, highlightTitle } = property;

  const [toggle, setToggle] = useState(false);
  const propertyUrl = buildUrlForProperty(property);
  const shareRef = useRef<HTMLDivElement>(null);

  useOutsideAlerter(shareRef, () => setToggle(false));

  const aditionalTags = (
    propertyType !== "LA" && propertyType !== "LOT"
      ? tags.filter((tag) => amenitiesTags.includes(tag))
      : tags
  ).slice(0, 6);

  const moreImages = property.photos?.slice(1, 4);

  const priceString = useGeneratePriceString(property, "Venta");

  return (
    <div className={classnames(styles["highlight-property"], className)}>
      <div className={styles["highlight-property__column-right"]}>
        <ResponsiveLink
            className={styles["highlight-property__image"]}
            as={propertyUrl}
            href="/propiedades/[operationType-propertyType]/[propertySlug]"
        >
          <img
            className={styles["highlight-property__image"]}
            src={property.photos?.[0]?.image}
          />
        </ResponsiveLink>
        <div className={styles["highlight-property__image-footer"]}>
          <h6 className={styles["highlight-property__image-footer--surface"]}>
            <img src={box} />
            {numberWithDotsAndCommas(totalSurfaceMax)}m<sup>2</sup> tot.
          </h6>
          <h6 className={styles["highlight-property__image-footer--price"]}>
            {priceString}
          </h6>
        </div>
      </div>
      <div className={styles["highlight-property__column-center"]}>
          <div className={styles["highlight-property__more-images"]}>
            {moreImages?.map((image) => (
              <ResponsiveLink
              className={styles["highlight-property__more-images"]}
              as={propertyUrl}
              href="/propiedades/[operationType-propertyType]/[propertySlug]"
              >
                <img key={image.image} src={image.image} />
              </ResponsiveLink>
            ))}
          </div>
        
        <h6 className={styles["highlight-property__price"]}>{priceString}</h6>
      </div>
      <div className={styles["highlight-property__column-left"]}>
        <div>
          <div className={styles["highlight-property__batch"]}>
            <img src={badge} />
            SUPER DESTACADO
          </div>
          <small className={styles["highlight-property__type"]}>
            {propertyTypeToString(property.propertyType)}
          </small>
          <ResponsiveLink
            className={styles["highlight-property__actions--link"]}
            as={propertyUrl}
            href="/propiedades/[operationType-propertyType]/[propertySlug]"
          >
            <h3 className={styles["highlight-property__titlte"]}>
              {highlightTitle}
            </h3>
          </ResponsiveLink>
          <h6 className={styles["highlight-property__address"]}>
            {getFullAddress(property)}
          </h6>
          <h6>Amenities:</h6>
          <h6 className={styles["highlight-property__amenities"]}>
            {aditionalTags.join(" • ")}
          </h6>
        </div>
        <div className={styles["highlight-property__actions"]} ref={shareRef}>
          <ShareLikeAndNotificationButtons
            propertyId={property._id}
            setToggle={setToggle}
            toggle={toggle}
            url={`${webURL}${propertyUrl}`}
            shareText={`Te comparto esta propiedad desde Vanzini Propiedades: ${property.address}`}
          />
          <ResponsiveLink
            className={styles["highlight-property__actions--link"]}
            as={propertyUrl}
            href="/propiedades/[operationType-propertyType]/[propertySlug]"
          >
            <Button size="small">Ver ficha</Button>
          </ResponsiveLink>
        </div>
      </div>
    </div>
  );
};

export default HighlightProperty;
