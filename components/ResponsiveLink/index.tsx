import React from "react";
import Link from "next/link";
import { ResponsiveLinkProps } from "./types";
import { useRef } from "react";
import { useEffect } from "react";
import { isMobileWeb } from "../../utils/isMobileWeb";

const ResponsiveLink: React.FC<ResponsiveLinkProps> = ({
  href,
  as,
  children,
  className,
}) => {
  const linkRef = useRef<HTMLAnchorElement>(null);

  useEffect(() => {
    if (isMobileWeb() && linkRef.current) {
      linkRef.current.setAttribute("target", "");
    }
  }, []);

  return (
    <Link key="mobile-link" href={href} as={as} passHref>
      <a target="_blank" ref={linkRef} className={className}>
        {children}
      </a>
    </Link>
  );
};

export default ResponsiveLink;
