export type ResponsiveLinkProps = {
  href: string;
  as: string;
  className?: string;
};
