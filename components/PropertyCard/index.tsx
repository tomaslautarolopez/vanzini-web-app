import React, { useContext, useEffect, useRef, useState } from "react";
import classnames from "classnames";
import { Button, Carousel } from "antd";

import { PropertyCardProps } from "./types";
import styles from "./PropertyCard.module.scss";
import { getFullAddress } from "../../utils/getFullAddress";
import { propertyTypeToString } from "../../utils/propertyTypeToString";
import carouselButton from "../../assets/icons/property-card/carousel-button.svg";
import { useFadeInOutWrapper } from "../../hooks/useFadeInOutWrapper";
import NoImages from "../../assets/images/no-images.jpg";
import { ModalContext } from "../../contexts/ModalContext";
import { useOutsideAlerter } from "../../hooks/useOutsideAlerter";
import { buildUrlForProperty } from "../../utils/buildUrlForProperty";
import { webURL } from "../../axios";
import PropertyCardSkeleton from "../PropertyCardSkeleton";
import ShareLikeAndNotificationButtons from "../ShareLikeAndNotificationButtons";
import { useGeneratePriceString } from "../../hooks/useGeneratePriceString";
import { isMobileWeb } from "../../utils/isMobileWeb";
import { isDevelopment } from "../../utils/isDevelopment";
import { FiltersContext } from "../../contexts/FiltersContext";
import ResponsiveLink from "../ResponsiveLink";
import { UserContext } from "../../contexts/UserContext";
import { EyeOutlined } from "@ant-design/icons";
import { numberWithDotsAndCommas } from "../../utils/numberWithDotsAndCommas";

const PropertyCard: React.FC<PropertyCardProps> = ({
  property,
  hideActions,
  className,
  mapCard,
}) => {
  const carouselRef = useRef<Carousel | null>();
  const [currentSlide, setCurrentSlide] = useState(0);
  const [carouselHovered, setCarouselHovered] = useState<boolean>(
    isMobileWeb() || false
  );
  const { filters } = useContext(FiltersContext);
  const { setModal, setConsultModalData } = useContext(ModalContext);
  const [toggle, setToggle] = useState(false);
  const firstImageRef = useRef(null);
  const shareRef = useRef<HTMLDivElement>(null);
  const { hasPropertyBeenSeen } = useContext(UserContext);

  useOutsideAlerter(shareRef, () => setToggle(false));

  const photos = property.photos
    .filter((photo) => !photo.is_blueprint)
    .slice(0, 5);

  const [isDownloadingPicture, setIsDownloadingPicture] = useState(
    !!(photos && photos.length)
  );

  const leftArrowToogle = currentSlide !== 0 && carouselHovered;
  const rightArrowToogle =
    photos.length !== 0 && currentSlide < photos.length - 1 && carouselHovered;

  const propertyUrl = buildUrlForProperty(property);

  useEffect(() => {
    const img = firstImageRef.current;
    if (img && (img as any).complete) {
      setIsDownloadingPicture(false);
    }
  }, []);

  const carouselArrowLeft = useFadeInOutWrapper(
    <span
      style={{ transform: "rotate(180deg)" }}
      className={classnames(
        styles["property-card__carousel--arrow"],
        "dp12-shadow"
      )}
      onClick={() => {
        if (leftArrowToogle) carouselRef.current?.prev();
      }}
    >
      <img src={carouselButton} alt="Botton izquierda carrusel" />
    </span>,
    leftArrowToogle
  );
  const carouselArrowRight = useFadeInOutWrapper(
    <span
      className={classnames(
        styles["property-card__carousel--arrow"],
        "dp12-shadow"
      )}
      onClick={() => {
        if (rightArrowToogle) carouselRef.current?.next();
      }}
    >
      <img src={carouselButton} alt="Botton derecha carrusel" />
    </span>,
    rightArrowToogle
  );

  const priceString = useGeneratePriceString(
    property,
    filters.operationType || "Venta"
  );

  return !isDownloadingPicture ? (
    <div
      className={classnames(
        styles["property-card"],
        className,
        mapCard && styles["property-card__map-card"]
      )}
    >
      <div
        className={styles["property-card__carousel"]}
        onMouseEnter={() => setCarouselHovered(true)}
        onMouseLeave={() => setCarouselHovered(false)}
      >
        <Carousel
          dots={false}
          ref={(ref) => {
            carouselRef.current = ref;
          }}
          beforeChange={(_, to) => setCurrentSlide(to)}
        >
          {photos.length > 0 ? (
            photos.map((photo) => (
              <div
                key={photo.thumb}
                className={classnames(
                  styles["property-card__carousel--photo"],
                  mapCard && styles["property-card__map-card--photo"]
                )}
              >
                <img
                  src={`${photo.image}?w=${
                    isMobileWeb() && typeof window !== "undefined"
                      ? window.innerWidth
                      : "368"
                  }`}
                  alt="Foto de propiedad"
                />
              </div>
            ))
          ) : (
            <div className={styles["property-card__carousel--photo"]}>
              <img
                src={NoImages}
                alt="Foto de puerta blanca con logo Vanzini"
              />
            </div>
          )}
        </Carousel>
        <div
          className={styles["property-card__carousel--control"]}
          style={{ left: 16 }}
        >
          {carouselArrowLeft}
        </div>
        <div
          className={styles["property-card__carousel--control"]}
          style={{ right: 16 }}
        >
          {carouselArrowRight}
        </div>
        <div className={styles["property-card__carousel--footer"]}>
          <div>
            {!isDevelopment(property.propertyType) && (
              <>
                <small className="fw-700">SUPERFICIE</small>
                <p className="fw-700">
                  {numberWithDotsAndCommas(property.totalSurfaceMax)}m<sup>2</sup>
                </p>
              </>
            )}
          </div>
          {priceString !== "CONSULTAR PRECIO" ? (
            <div className={styles["property-card__carousel--footer---price"]}>
              <small className="fw-700 text-uppercase">
                PRECIO DE {filters.operationType || "VENTA"}
              </small>
              <p className="fw-700">{priceString}</p>
            </div>
          ) : (
            <div className="fw-700">
              <p>CONSULTAR PRECIO</p>
            </div>
          )}
        </div>
        <ResponsiveLink
          as={propertyUrl}
          href="/propiedades/[operationType-propertyType]/[propertySlug]"
        >
          <div className={styles["property-card__carousel--bg"]} />
        </ResponsiveLink>
        {hasPropertyBeenSeen(property._id) && (
          <div className={styles["property-card__seen-label"]}>
            <EyeOutlined
              className={styles["property-card__seen-label--icon"]}
            />
            Visto
          </div>
        )}
      </div>
      <div className={styles["property-card__footer"]}>
        <div>
          <small className={styles["property-card__footer--property-type"]}>
            {propertyTypeToString(property.propertyType)}
          </small>
          <ResponsiveLink
            as={propertyUrl}
            href="/propiedades/[operationType-propertyType]/[propertySlug]"
          >
            <h5 className={styles["property-card__footer--title"]}>
              {property.publicationTitle}
            </h5>
          </ResponsiveLink>
          <p className={styles["property-card__footer--address"]}>
            {getFullAddress(property)}
          </p>
        </div>
        {!hideActions && (
          <div className={styles["property-card__actions"]} ref={shareRef}>
            <ShareLikeAndNotificationButtons
              propertyId={property._id}
              setToggle={setToggle}
              toggle={toggle}
              url={`${webURL}${propertyUrl}`}
              shareText={`Te comparto esta propiedad desde Vanzini Propiedades: ${property.address}`}
            />
            <Button
              className={styles["property-card__actions--consult-button"]}
              onClick={() => {
                setModal("consultModal", true);
                setConsultModalData({
                  propertiesId: [
                    {
                      id: property._id,
                      propertyType: property.propertyType,
                    },
                  ],
                  property,
                });
              }}
              size="small"
            >
              Consultar
            </Button>
          </div>
        )}
      </div>
    </div>
  ) : (
    <>
      <img
        ref={firstImageRef}
        style={{ display: "none" }}
        src={photos && photos[0].image}
        onLoad={() => {
          setIsDownloadingPicture(false);
        }}
      />
      <PropertyCardSkeleton />
    </>
  );
};

export default PropertyCard;
