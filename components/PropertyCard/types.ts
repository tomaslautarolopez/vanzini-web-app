import { Property } from "../../interfaces/properties";

export type PropertyCardProps = {
  property: Property;
  className?: string;
  hideActions?: boolean;
  mapCard?: boolean;
};
