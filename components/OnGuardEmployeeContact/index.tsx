import React, { useContext } from "react";
import classnames from "classnames";
import { OnGuardEmployeeContactProps } from "./types";
import { isMobileWeb } from "../../utils/isMobileWeb";
import { useWhatsappShare } from "../../hooks/useWhatsappShare";
import OnGuardEmployee from "../OnGuardEmployee";

import styles from "./OnGuardEmployeeContact.module.scss";
import { EmployeeContext } from "../../contexts/EmployeeContext";
import { Location } from "../../interfaces/employee";

const OnGuardEmployeeContact: React.FC<OnGuardEmployeeContactProps> = ({
  consultType,
  pIds,
  location = Location.HEAD_OFFICE,
}) => {
  const { employeesOnGuard } = useContext(EmployeeContext);

  const onGuardEmployee = employeesOnGuard[location];

  const phonenumber = onGuardEmployee?.phonenumber;
  const { text } = useWhatsappShare(consultType || "GENERAL", pIds || []);

  const whatsappLink = isMobileWeb()
    ? `https://api.whatsapp.com/send?phone=${phonenumber}&text=${text}`
    : `https://web.whatsapp.com/send?phone=${phonenumber}&text=${text}`;

  return (
    <div className={styles["on-guard-employee-contact"]}>
      {onGuardEmployee && (
        <div className={styles["on-guard-employee-contact__on-guard-employee"]}>
          <OnGuardEmployee onGuardEmployee={onGuardEmployee} />
          <button
            className={classnames(
              styles[
                "on-guard-employee-contact__on-guard-employee--whatsapp-button"
              ],
              "dp2-shadow"
            )}
            onClick={() => window.open(whatsappLink, "_blank")}
          >
            WHATSAPP
          </button>
        </div>
      )}
      <p className={styles["on-guard-employee-contact__legend"]}>
        También podes enviar tu consulta por correo
      </p>
    </div>
  );
};

export default OnGuardEmployeeContact;
