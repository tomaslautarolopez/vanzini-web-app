import { ConsultType } from "../../hooks/useConsult";
import { Location } from "../../interfaces/employee";

export type OnGuardEmployeeContactProps = {
  consultType?: ConsultType;
  pIds?: string[];
  location?: Location;
};
