import React, { useContext, useEffect } from "react";
import { Modal } from "antd";

import { ConsultModalProps } from "./types";
import styles from "./ConsultModal.module.scss";
import ConsultForm from "../ConsultForm";
import { ModalContext } from "../../contexts/ModalContext";
import { propertyLocationFinder } from "../../utils/propertyLocationFinder";

const ConsultModal: React.FC<ConsultModalProps> = () => {
  const {
    modals: { consultModal },
    setModal,
    setConsultModalData,
    modalsData: {
      consultModal: { propertiesId, property },
    },
  } = useContext(ModalContext);

  useEffect(() => {
    if (!consultModal) {
      setConsultModalData({ propertiesId: [], property: undefined });
    }
  }, [consultModal]);

  const onCancel = () => setModal("consultModal", false);

  const location = property && propertyLocationFinder(property);

  return (
    <Modal
      destroyOnClose
      className={styles["consult-modal"]}
      visible={consultModal}
      footer={null}
      onCancel={onCancel}
    >
      <ConsultForm
        location={location}
        propertiesId={propertiesId}
        onCancel={onCancel}
      />
    </Modal>
  );
};

export default ConsultModal;
