import React, { useContext } from "react";
import { Modal } from "antd";

import { LogOutModalProps } from "./types";
import { ModalContext } from "../../contexts/ModalContext";
import { UserContext } from "../../contexts/UserContext";

import styles from "./LogOutModal.module.scss";

const LogOutModal: React.FC<LogOutModalProps> = () => {
  const {
    modals: { logOutModal },
    setModal,
  } = useContext(ModalContext);

  const { logout: logOut } = useContext(UserContext);

  return (
    <Modal
      className={styles["log-out-modal"]}
      visible={logOutModal}
      onCancel={() => setModal("logOutModal", false)}
      footer={null}
      style={{ width: 320 }}
      closable={false}
    >
      <div className={styles["log-out-modal__content"]}>
        <h3>
          ¿Estás seguro que
          <br /> querés cerrar tu sesión?
        </h3>
        <p>
          Si cerras tu sesión podes volver a iniciar sesion en cualquier
          momento.
        </p>
        <div className={styles["log-out-modal__content--actions"]}>
          <button
            className="white-custom-button"
            onClick={() => setModal("logOutModal", false)}
          >
            Cancelar
          </button>
          <button
            className="red-custom-button"
            onClick={() => logOut(() => setModal("logOutModal", false))}
          >
            Cerrar sesión
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default LogOutModal;
