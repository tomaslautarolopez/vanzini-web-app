import React from "react";

import { AssessQualityPicturesProps } from "./types";
import property1 from "../../../assets/images/Assess/properties/property-1.jpg";
import property2 from "../../../assets/images/Assess/properties/property-2.jpg";
import property3 from "../../../assets/images/Assess/properties/property-3.jpg";

import styles from "./AssessQualityPictures.module.scss";

const AssessQualityPictures: React.FC<AssessQualityPicturesProps> = () => {
  return (
    <div className={styles["assess-quality-pictures"]}>
      <span className={styles["assess-quality-pictures__tag"]}>
        ¿qué nos distingue?
      </span>
      <h3>
        Relevamiento fotografico de <b>alta calidad</b>
      </h3>
      <h6 className={styles["assess-quality-pictures__description"]}>
        Todas nuestras propiedades son relevadas con fotografías de alta
        calidad, para mostrar sus características de la mejor manera posible.
      </h6>
      <div className={styles["assess-quality-pictures__properties"]}>
        <div className={styles["assess-quality-pictures__property-card"]}>
          <img
            className={styles["assess-quality-pictures__property-card--image"]}
            src={property1}
          />
          <div
            className={styles["assess-quality-pictures__property-card--footer"]}
          >
            <small>Departamento</small>
            <h5>Mega departamento con vista al rio</h5>
            <p>Av. del Huerto 1223, Rosario</p>
          </div>
        </div>

        <div className={styles["assess-quality-pictures__property-card"]}>
          <img
            className={styles["assess-quality-pictures__property-card--image"]}
            src={property2}
          />
          <div
            className={styles["assess-quality-pictures__property-card--footer"]}
          >
            <small>Casa</small>
            <h5>Casa 3 dormitorios con vista al lago</h5>
            <p>Av. Newbery 8800 ' 45 - Country del Lago</p>
          </div>
        </div>

        <div className={styles["assess-quality-pictures__property-card"]}>
          <img
            className={styles["assess-quality-pictures__property-card--image"]}
            src={property3}
          />
          <div
            className={styles["assess-quality-pictures__property-card--footer"]}
          >
            <small>Oficina</small>
            <h5>Oficinas premium con vista al río</h5>
            <p>Av. Madres de Plaza de Mayo 3020</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AssessQualityPictures;
