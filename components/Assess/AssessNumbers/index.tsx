import React from "react";

import { AssessNumbersProps } from "./types";
import numbersBg from "../../../assets/images/Assess/numbers-bg.jpg";
import check from "../../../assets/icons/assess/check.svg";

import styles from "./AssessNumbers.module.scss";

const AssessNumbers: React.FC<AssessNumbersProps> = () => {
  return (
    <div className={styles["assess-numbers"]}>
      <span className={styles["assess-numbers__tag"]}>Nuestros numeros</span>
      <div className={styles["assess-numbers__desktop"]}>
        <h3>
          El respaldo de una empresa basada en el <b>analisis</b> y la{" "}
          <b>estadistica</b>
        </h3>
        <h6 className={styles["assess-numbers__description"]}>
          Sabemos que el éxito de una venta se basa en el análisis de los datos
          que da el mercado, por eso llevamos estadísticas precisas de cada
          acción comercial.
        </h6>
        <div className={styles["assess-numbers__numbers-container"]}>
          <img
            className={styles["assess-numbers__numbers-background"]}
            src={numbersBg}
          />
          <div className={styles["assess-numbers__numbers"]}>
            <h3>
              Más conectados, <b>más resultados</b>
            </h3>
            <h6>
              Somos un equipo de personas especializada en la venta y
              comercializacion de propiedades.
            </h6>
            <div className={styles["assess-numbers__numbers--check-list"]}>
              <span>
                <h3>
                  <img src={check} />
                  110.000
                </h3>
                <h6>
                  Visitas web
                  <br /> mensuales
                </h6>
              </span>
              <span>
                <h3>
                  <img src={check} />
                  90.000
                </h3>
                <h6>
                  Suscriptores en
                  <br /> newsletter
                </h6>
              </span>
              <span>
                <h3>
                  <img src={check} />
                  40.000
                </h3>
                <h6>
                  Seguidores en
                  <br /> nuestras Redes
                </h6>
              </span>
              <span>
                <h3>
                  <img src={check} />
                  12
                </h3>
                <h6>
                  Portales
                  <br /> inmobiliarios
                </h6>
              </span>
              <span>
                <h3>
                  <img src={check} />
                  12
                </h3>
                <h6>
                  Asesores en
                  <br /> ventas
                </h6>
              </span>
              <span>
                <h3>
                  <img src={check} />6
                </h3>
                <h6>
                  Espcialistas en
                  <br /> marketing
                </h6>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className={styles["assess-numbers__mobile"]}>
        <img
          src={numbersBg}
          className={styles["assess-numbers__mobile--image"]}
        />
        <div className={styles["assess-numbers__mobile--numbers"]}>
          <h3>
            Más conectados, <b>más resultados</b>
          </h3>
          <h6>
            Somos un equipo de personas especializada en la venta y
            comercializacion de propiedades.
          </h6>
          <div className={styles["assess-numbers__mobile--check-list"]}>
            <span>
              <h3>
                <img src={check} />
                110.000
              </h3>
              <h6>
                Visitas web
                <br /> mensuales
              </h6>
            </span>
            <span>
              <h3>
                <img src={check} />
                90.000
              </h3>
              <h6>
                Suscriptores en
                <br /> newsletter
              </h6>
            </span>
            <span>
              <h3>
                <img src={check} />
                40.000
              </h3>
              <h6>
                Seguidores en
                <br /> nuestras Redes
              </h6>
            </span>
            <span>
              <h3>
                <img src={check} />
                12
              </h3>
              <h6>
                Portales
                <br /> inmobiliarios
              </h6>
            </span>
            <span>
              <h3>
                <img src={check} />
                12
              </h3>
              <h6>
                Asesores en
                <br /> ventas
              </h6>
            </span>
            <span>
              <h3>
                <img src={check} />6
              </h3>
              <h6>
                Espcialistas en
                <br /> marketing
              </h6>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AssessNumbers;
