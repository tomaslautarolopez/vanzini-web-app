import React, { useContext } from "react";
import { Button } from "antd";

import filledCheck from "../../../assets/icons/assess/assess-form/filled-check.svg";
import outlinedCheck from "../../../assets/icons/assess/assess-form/outlined-check.svg";
import { AssessHowToProps } from "./types";
import cityImage from "../../../assets/images/Assess/city.png";
import { ModalContext } from "../../../contexts/ModalContext";

import styles from "./AssessHowTo.module.scss";

const AssessHowTo: React.FC<AssessHowToProps> = () => {
  const { setModal } = useContext(ModalContext);

  return (
    <div className={styles["assess-how-to"]}>
      <span className={styles["assess-how-to__tag"]}>¿Como vender?</span>
      <h3>
        Seguí estos simples pasos para <b>vender tu propiedad</b>
      </h3>
      <h6 className={styles["assess-how-to__description"]}>
        Recibí una tasación profesional y conocé el valor de tu propiedad de
        manera sencilla y rápida.
      </h6>
      <div className={styles["assess-how-to__how-to"]}>
        <img
          className={styles["assess-how-to__how-to--image"]}
          src={cityImage}
        />
        <div className={styles["assess-how-to__flow"]}>
          <div className={styles["assess-how-to__flow--item"]}>
            <h3>
              <img src={filledCheck} /> Llená el formulario
            </h3>
            <div className={styles["assess-how-to__flow--item---step"]}>
              <div className={styles["assess-how-to__flow--blue-bar"]} />
              <h6>
                Completa con la mayor
                <br /> cantidad de datos.
              </h6>
            </div>
          </div>
          <div className={styles["assess-how-to__flow--item"]}>
            <h3>
              <img src={outlinedCheck} /> Hablá con un agente
            </h3>
            <div className={styles["assess-how-to__flow--item---step"]}>
              <div className={styles["assess-how-to__flow--light-blue-bar"]} />
              <h6>
                Se estaran contactando para
                <br /> coordinar una visita.
              </h6>
            </div>
          </div>
          <div className={styles["assess-how-to__flow--item"]}>
            <h3>
              <img src={outlinedCheck} /> Documentación
            </h3>
            <div className={styles["assess-how-to__flow--item---step"]}>
              <div className={styles["assess-how-to__flow--light-blue-bar"]} />
              <h6>
                Al momento de la visita conta
                <br /> con toda la documentación.
              </h6>
            </div>
          </div>
        </div>
        <div className={styles["assess-how-to__proceed"]}>
          <h3>
            Tasamos tu propiedad
            <br /> <b>en 48hs</b>
          </h3>
          <h5>
            Tenemos las herramientas digitales para valuar y poner a la venta tu
            propiedad sin tener que moverte de tu casa.
          </h5>
          <Button onClick={() => setModal("assessModal", true)} type="primary">
            Comenzar tasación
          </Button>
        </div>
      </div>
    </div>
  );
};

export default AssessHowTo;
