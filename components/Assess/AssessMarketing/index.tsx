import React from "react";

import { AssessMarketingProps } from "./types";
import professionals from "../../../assets/icons/assess/marketing/professionals.svg";
import experts from "../../../assets/icons/assess/marketing/experts.svg";
import atenttion from "../../../assets/icons/assess/marketing/atenttion.svg";

import styles from "./AssessMarketing.module.scss";

const AssessMarketing: React.FC<AssessMarketingProps> = () => {
  return (
    <div className={styles["assess-marketing"]}>
      <span className={styles["assess-marketing__tag"]}>
        ¿Por que Vender con nosotros?
      </span>
      <h3>
        Vender tu propiedad nunca fue tan <b>sencillo</b>
      </h3>
      <h6 className={styles["assess-marketing__description"]}>
        Liderando el mercado inmobiliaria por mas de 65 años en la ciudad de
        Rosario y en sus alrededores.
      </h6>
      <div className={styles["assess-marketing__flow-contaier"]}>
        <div className={styles["assess-marketing__flow"]}>
          <img src={professionals} />
          <h3>Vendedores profesionales</h3>
          <h6>
            Todos nuestros vendedores cuentan con un entrenamiento en las areas
            contables, notariales y tecnicas.
          </h6>
        </div>
        <div className={styles["assess-marketing__flow"]}>
          <img src={experts} />
          <h3>Expertos en Rosario</h3>
          <h6>
            Mas de 65 años vendiendo en la ciudad de Rosario y alrededores
            avalan nuestra experiencia en el mercado.
          </h6>
        </div>
        <div className={styles["assess-marketing__flow"]}>
          <img src={atenttion} />
          <h3>Atención personalizada</h3>
          <h6>
            Contamos con un vasto equipo de vendedores para tener un
            asesoramiento y seguimiento más personalizado.
          </h6>
        </div>
      </div>
    </div>
  );
};

export default AssessMarketing;
