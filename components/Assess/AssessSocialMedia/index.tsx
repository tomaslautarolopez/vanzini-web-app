import React, { useContext } from "react";

import { AssessSocialMediaProps } from "./types";
import vanzini from "../../../assets/icons/assess/social-media/vanzini.svg";
import email from "../../../assets/icons/assess/social-media/email.svg";
import googleAds from "../../../assets/icons/assess/social-media/google.svg";
import mercadolibre from "../../../assets/icons/assess/social-media/mercadolibre.svg";
import zonaprop from "../../../assets/icons/assess/social-media/zonaprop.svg";
import argenprop from "../../../assets/icons/assess/social-media/argenprop.svg";
import { ModalContext } from "../../../contexts/ModalContext";
import { useCountProperties } from "../../../hooks/useCountProperties";
import { numberWithDotsAndCommas } from "../../../utils/numberWithDotsAndCommas";

import styles from "./AssessSocialMedia.module.scss";

const AssessSocialMedia: React.FC<AssessSocialMediaProps> = () => {
  const { propertiesCount } = useCountProperties(undefined);
  const { setModal } = useContext(ModalContext);

  return (
    <div className={styles["assess-social-media"]}>
      <span className={styles["assess-social-media__tag"]}>
        ¿dónde se publica?
      </span>
      <h3>
        El mayor <b>alcance digital</b> de toda la region
      </h3>
      <h6 className={styles["assess-social-media__description"]}>
        Nuestro equipo de marketing realiza campañas digitales en los medios mas
        importantes para que tenga una mayor difusión y velocidad de venta.
      </h6>
      <div className={styles["assess-social-media__container"]}>
        <div
          className={styles["assess-social-media__social-media-item"]}
          onClick={() => setModal("newsletterModal", true)}
        >
          <img src={email} />
          <h3>Email marketing</h3>
          <h6
            className={
              styles["assess-social-media__social-media-item--description"]
            }
          >
            Contamos con una base de datos geniuna, segmentada e interesada en
            el rubro.
          </h6>
          <h6
            className={styles["assess-social-media__social-media-item--link"]}
          >
            Suscribirte
          </h6>
        </div>
        <div className={styles["assess-social-media__social-media-item"]}>
          <img src={vanzini} />
          <h3>Web</h3>
          <h6
            className={
              styles["assess-social-media__social-media-item--description"]
            }
          >
            Nuestra web cuenta con herramientas prácticas para facilitar la
            búsqueda.
          </h6>
          <h6
            className={styles["assess-social-media__social-media-item--link"]}
          >
            {propertiesCount !== undefined &&
              numberWithDotsAndCommas(propertiesCount)}{" "}
            propiedades
          </h6>
        </div>
        <a href="https://g.page/vanzini-propiedades/review?rc" target="_blank">
          <div className={styles["assess-social-media__social-media-item"]}>
            <img src={googleAds} />
            <h3>Google ads</h3>
            <h6
              className={
                styles["assess-social-media__social-media-item--description"]
              }
            >
              Realizamos campañas de marketing en la búsqueda de google y en su
              red de display.
            </h6>
            <h6
              className={styles["assess-social-media__social-media-item--link"]}
            >
              review
            </h6>
          </div>
        </a>
        <a
          href="https://tienda.mercadolibre.com.ar/vanzini-propiedades"
          target="_blank"
        >
          <div className={styles["assess-social-media__social-media-item"]}>
            <img src={mercadolibre} />
            <h3>Mercado Libre</h3>
            <h6
              className={
                styles["assess-social-media__social-media-item--description"]
              }
            >
              Visitá nuestro perfíl en el portal más conocido de compra y venta.
            </h6>
            <h6
              className={styles["assess-social-media__social-media-item--link"]}
            >
              ir al perfil
            </h6>
          </div>
        </a>
        <a
          href="https://www.zonaprop.com.ar/inmobiliarias/vanzini-propiedades_17806323-inmuebles.html"
          target="_blank"
        >
          <div className={styles["assess-social-media__social-media-item"]}>
            <img src={zonaprop} />
            <h3>Zonaprop</h3>
            <h6
              className={
                styles["assess-social-media__social-media-item--description"]
              }
            >
              Tenemos una gran presencia en el mayor portal inmobiliario de
              Argentina.
            </h6>
            <h6
              className={styles["assess-social-media__social-media-item--link"]}
            >
              ir al perfil
            </h6>
          </div>
        </a>
        <a
          href="https://www.argenprop.com/Vanzini-Propiedades/inmuebles-anunciante-173974?"
          target="_blank"
        >
          <div className={styles["assess-social-media__social-media-item"]}>
            <img src={argenprop} />
            <h3>Argenprop</h3>
            <h6
              className={
                styles["assess-social-media__social-media-item--description"]
              }
            >
              Todas nuestras propiedades se publican en uno de los sitios
              inmobiliarios más populares.
            </h6>
            <h6
              className={styles["assess-social-media__social-media-item--link"]}
            >
              ir al perfil
            </h6>
          </div>
        </a>
      </div>
    </div>
  );
};

export default AssessSocialMedia;
