import React, { useContext } from "react";
import { Checkbox, Form, Input } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

import { SignUpModalProps } from "./types";
import ModalWithImage from "../ModalWithImage";
import UserIcon from "../../assets/icons/general/user.svg";
import SignUpBg from "../../assets/images/SignUp/sign-up-bg.jpg";
import { UserContext, UserSignUpForm } from "../../contexts/UserContext";
import { useNewsletter } from "../../hooks/useNewsletter";
import { ModalContext } from "../../contexts/ModalContext";

import styles from "./SignUpModal.module.scss";

const SignUpModal: React.FC<SignUpModalProps> = () => {
  const {
    signUp,
    user: {
      loading: { isSignUpLoading },
    },
  } = useContext(UserContext);
  const { suscribeToNewsletter } = useNewsletter();
  const {
    modals: { signUpModal },
    setModal,
  } = useContext(ModalContext);

  return (
    <ModalWithImage
      style={{
        width: 840,
      }}
      image={SignUpBg}
      altImage="Fuente Rosario Argentina"
      visible={signUpModal}
      onCancel={() => setModal("signUpModal", false)}
      footer={null}
      header={
        <h6 className={styles["sign-up-modal__header"]}>
          <img src={UserIcon} alt="Icono de usuario" /> REGISTRARTE
        </h6>
      }
    >
      <div className={styles["sign-up-modal"]}>
        <h3>Te damos la bienvenida!</h3>
        <p>
          Completa los datos y empeza a buscar con todas las mejores
          herramientas digitales
        </p>
        <Form
          onFinish={({
            newsletter,
            ...userForm
          }: UserSignUpForm & { newsletter: boolean }) => {
            signUp(userForm, () => {
              if (newsletter) {
                suscribeToNewsletter(userForm.email, () => {
                  setModal("signUpModal", false);
                });
              } else {
                setModal("signUpModal", false);
              }
            });
          }}
          className={styles["sign-up-modal__form"]}
          layout="vertical"
        >
          <Form.Item
            rules={[
              {
                required: true,
                message: "Debes ingresar un email",
              },
              {
                type: "email",
                message: "Debes ingresar un email válido",
              },
            ]}
            label="EMAIL"
            name="email"
          >
            <Input placeholder="nombre@email.com" />
          </Form.Item>
          <Form.Item
            rules={[
              {
                required: true,
                message: "Debes ingresar un nombre",
              },
            ]}
            label="NOMBRE"
            name="name"
          >
            <Input placeholder="Nombre Completo" />
          </Form.Item>
          <Form.Item
            label="CONTRASEÑA"
            name="password"
            rules={[
              {
                required: true,
                message: "Debes ingresar una contraseña",
              },
            ]}
          >
            <Input.Password placeholder="*********" />
          </Form.Item>
          <Form.Item
            label="REPETIR CONTRASEÑA"
            name="repassword"
            rules={[
              {
                required: true,
                message: "Debes repetir tu contraseña",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject("Las contraseñas deben coincidir");
                },
              }),
            ]}
          >
            <Input.Password placeholder="*********" />
          </Form.Item>
          <Form.Item name="newsletter" valuePropName="checked">
            <Checkbox>Suscribirme al newsletter</Checkbox>
          </Form.Item>
          <button className="black-custom-button" type="submit">
            REGISTRARTE {isSignUpLoading && <LoadingOutlined />}
          </button>
        </Form>
        <h6 className={styles["sign-up-modal__footer"]}>
          Registrandote aceptás los{" "}
          <a href="/terminos-y-condiciones" target="_blank">
            términos y condiciones de uso.
          </a>
        </h6>
      </div>
    </ModalWithImage>
  );
};

export default SignUpModal;
