import React from "react";
import classnmaes from "classnames";
import { Modal } from "antd";

import { ModalWithImageProps } from "./types";

import styles from "./ModalWithImage.module.scss";

const ModalWithImage: React.FC<ModalWithImageProps> = ({
  image,
  altImage,
  className,
  children,
  header,
  imageClassname,
  ...rest
}) => {
  return (
    <Modal
      footer={null}
      className={classnmaes(styles["modal-with-image"], className)}
      {...rest}
    >
      <div className={styles["modal-with-image__container"]}>
        <div className={styles["modal-with-image__left"]}>
          <div className={styles["modal-with-image__header-mobile"]}>
            {header}
          </div>
          <img
            className={classnmaes(styles["modal-with-image__left--image"], imageClassname)}
            src={image}
            alt={altImage}
          />
        </div>
        <div className={styles["modal-with-image__right"]}>
          <div className={styles["modal-with-image__header-desktop"]}>
            {header}
          </div>
          {children}
        </div>
      </div>
    </Modal>
  );
};

export default ModalWithImage;
