import { ModalProps } from "antd/lib/modal";
import { ReactNode } from "react";

export type ModalWithImageProps = {
  image: string;
  altImage: string;
  imageClassname?: string;
  header?: ReactNode;
} & ModalProps;
