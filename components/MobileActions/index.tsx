import React from "react";
import classnames from "classnames";
import { Button } from "antd";

import { MobileActionsProps } from "./types";
import styles from "./MobileActions.module.scss";

const MobileActions: React.FC<MobileActionsProps> = ({
  className,
  actions,
}) => {
  return (
    <div className={classnames(styles["mobile-actions"], className)}>
      {actions.map(({ img, label, action }) => (
        <Button
          key={label}
          className={styles["mobile-actions--button"]}
          size="small"
          onClick={action}
        >
          <img src={img} /> {label}
        </Button>
      ))}
    </div>
  );
};

export default MobileActions;
