import React from "react";

export type MobileActionsProps = {
  className?: string;
  actions: {
    label: string;
    img: string;
    action: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  }[];
};
