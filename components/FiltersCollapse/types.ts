export type FiltersCollapseProps = {
  className?: string;
  title: string;
  showCollapse?: boolean;
};
