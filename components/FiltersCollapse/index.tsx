import React, { useState } from "react";
import { Collapse } from "antd";
import classnames from "classnames";

import { FiltersCollapseProps } from "./types";

import styles from "./FiltersCollapse.module.scss";
import { DownOutlined } from "@ant-design/icons";

const { Panel } = Collapse;

const FiltersCollapse: React.FC<FiltersCollapseProps> = ({
  children,
  title,
  className,
  showCollapse,
}) => {
  const [activeKey, setActiveKey] = useState<undefined | "1">("1");

  return (
    <div className={classnames(styles["filters-collapse"], className)}>
      <h5>{title}</h5>
      <Collapse activeKey={activeKey} className="filters-collapse__collpase">
        <Panel header="" key="1">
          {children}
        </Panel>
      </Collapse>
      {showCollapse && (
        <button
          className={classnames(
            styles["filters-collapse__button"],
            activeKey && styles["open"]
          )}
          onClick={() => setActiveKey(activeKey ? undefined : "1")}
        >
          {activeKey ? "Ocultar" : "Mostrar"}{" "}
          <DownOutlined
            className={classnames(
              styles["filters-collapse__anchor"],
              activeKey && styles["open"]
            )}
          />
        </button>
      )}
    </div>
  );
};

export default FiltersCollapse;
