import React from "react";

import { NewsletterSuscribeHeroProps } from "./types";
import ThanksForConsultLayout from "../../components/ThanksForConsultLayout";
import background from "../../assets/images/SuscribeToNewsletter/suscribe-to-newsletter-bg.jpg";
import FlagIcon from "../../assets/icons/general/flag.svg";
import NewsletterForm from "../../components/NewsletterForm";

import styles from "./NewsletterSuscribeHero.module.scss";

const NewsletterSuscribeHero: React.FC<NewsletterSuscribeHeroProps> = () => {
  return (
    <div className={styles["newsletter-suscribe-hero"]}>
      <ThanksForConsultLayout
        hideBackButton
        background={background}
        contentClassName={styles["newsletter-suscribe-hero__content"]}
      >
        <h6 className={styles["newsletter-suscribe-hero__header"]}>
          <img src={FlagIcon} alt="Escudo de armas vanzini" /> NEWSLETTER
        </h6>
        <h3 className="fw-700">Últimas novedades inmobiliarias!</h3>
        <p>
          Querés enterarte de las últimas propieades a la venta y las mejores
          promociones
          <br />
          inmobiliarias? Suscribite a nuestro newsletter.
        </p>
        <div
          className={
            styles["newsletter-suscribe-hero__content--newsletter-form"]
          }
        >
          <NewsletterForm />
          <h6>
            Suscribiendote aceptás{" "}
            <a href="/terminos-y-condiciones" target="_blank">
              términos y condiciones de uso.
            </a>
          </h6>
        </div>
      </ThanksForConsultLayout>
    </div>
  );
};

export default NewsletterSuscribeHero;
