import React, { useState } from "react";
import Lightbox from "react-image-lightbox-rotate";

import { FullscreenViewerProps } from "./types";
import styles from "./FullscreenViewer.module.scss";

const FullscreenViewer: React.FC<FullscreenViewerProps> = ({
  images,
  onClose,
  currentSlide,
}) => {
  const [index, setIndex] = useState(currentSlide || 0);

  return (
    <>
      <div className={styles["fullscreen-viewer__counter"]}>
        {index + 1} / {images.length}
      </div>
      <Lightbox
        mainSrc={images[index]}
        nextSrc={images[(index + 1) % images.length]}
        prevSrc={images[(index - 1) % images.length]}
        onCloseRequest={onClose}
        onPreMovePrevRequest={() => setIndex((index - 1) % images.length)}
        onPreMoveNextRequest={() => setIndex((index + 1) % images.length)}
      />
    </>
  );
};

export default FullscreenViewer;
