export type FullscreenViewerProps = {
  images: Array<string>;
  onClose: () => void;
  currentSlide?: number;
};
