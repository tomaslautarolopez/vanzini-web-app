import React, { useContext, useEffect, useState } from "react";
import { Form, Input } from "antd";

import UserIcon from "../../assets/icons/general/user.svg";
import { ForgotPasswordModalProps } from "./types";
import ModalWithImage from "../ModalWithImage";
import { ModalContext } from "../../contexts/ModalContext";
import { LoadingOutlined } from "@ant-design/icons";
import background from "../../assets/images/ForgotPassword/forgot-password-bg.jpg";
import { usePasswordRecovery } from "../../hooks/usePasswordRecovery";

import styles from "./ForgotPasswordModal.module.scss";

const ForgotPasswordForm: React.FC<{
  recoverPassword: (email: string) => void;
  signUpModal: () => void;
  recovering: boolean;
  setEmail: (email: string) => void;
}> = ({ recoverPassword, signUpModal, recovering, setEmail }) => (
  <>
    <h3>¿Te olvidaste la contraseña?</h3>
    <p>Ingresa tu email y te enviaremos los pasos para cambiarla</p>
    <Form
      onFinish={(form) => {
        setEmail(form.email);
        recoverPassword(form.email);
      }}
      className={styles["forgot-password-modal__form"]}
      layout="vertical"
    >
      <Form.Item
        rules={[
          {
            required: true,
            message: "Debes ingresar un email",
          },
          {
            type: "email",
            message: "Debes ingresar un email válido",
          },
        ]}
        label="EMAIL"
        name="email"
      >
        <Input placeholder="nombre@email.com" />
      </Form.Item>
      <button className="black-custom-button" type="submit">
        RECUPERAR {recovering && <LoadingOutlined />}
      </button>
    </Form>
    <h6 className={styles["forgot-password-modal__footer"]}>
      No tengo cuenta,{" "}
      <a
        onClick={() => {
          signUpModal();
        }}
      >
        registrarme.
      </a>
    </h6>
  </>
);

const ForgotPasswordSuccess: React.FC<{
  LogInModal: () => void;
  email: string;
  signUpModal: () => void;
}> = ({ email, LogInModal, signUpModal }) => (
  <>
    <div>
      <h3>Te enviamos un email</h3>
      <p>{email} revisa tu correo y segui los pasos</p>
    </div>
    <div>
      <button
        onClick={() => LogInModal()}
        className="black-custom-button"
        type="submit"
      >
        INICIAR SESIÓN
      </button>
      <h6 className={styles["forgot-password-modal__footer"]}>
        No tengo cuenta,{" "}
        <a
          onClick={() => {
            signUpModal();
          }}
        >
          registrarme.
        </a>
      </h6>
    </div>
  </>
);

const ForgotPasswordModal: React.FC<ForgotPasswordModalProps> = () => {
  const {
    setModal,
    modals: { forgotPasswordModal },
  } = useContext(ModalContext);
  const [email, setEmail] = useState<string>("");

  const {
    loading: { recovering: recoveringLoading },
    recoverPassword,
    success: { recovering: recoveringSuccess },
    resetState,
  } = usePasswordRecovery();

  useEffect(() => {
    if (!forgotPasswordModal) {
      resetState();
    }
  }, [forgotPasswordModal]);

  return (
    <ModalWithImage
      style={{
        width: 840,
      }}
      image={background}
      altImage="Fuente Rosario Argentina"
      visible={forgotPasswordModal}
      imageClassname={styles["forgot-password-modal__image"]}
      onCancel={() => setModal("forgotPasswordModal", false)}
      footer={null}
      header={
        <h6 className={styles["forgot-password-modal__header"]}>
          <img src={UserIcon} alt="Icono de usuario" /> RECUPERAR CONTRASEÑA
        </h6>
      }
    >
      <div className={styles["forgot-password-modal"]}>
        {recoveringSuccess ? (
          <ForgotPasswordSuccess
            signUpModal={() => setModal("signUpModal", true)}
            email={email}
            LogInModal={() => setModal("logInModal", true)}
          />
        ) : (
          <ForgotPasswordForm
            setEmail={setEmail}
            signUpModal={() => setModal("signUpModal", true)}
            recovering={recoveringLoading}
            recoverPassword={recoverPassword}
          />
        )}
      </div>
    </ModalWithImage>
  );
};

export default ForgotPasswordModal;
