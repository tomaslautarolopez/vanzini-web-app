export type SortDrawerProps = {
  visible: boolean;
  setVisible: (visible: boolean) => void;
};
