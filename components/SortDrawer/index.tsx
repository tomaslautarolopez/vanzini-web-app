import React, { useContext } from "react";
import classnames from "classnames";
import { Drawer } from "antd";

import { SortDrawerProps } from "./types";
import { FiltersContext, Sort } from "../../contexts/FiltersContext";
import {
  leastPriceFilterSort,
  mostPriceFilterSort,
} from "../../utils/sortOperations";

import styles from "./SortDrawer.module.scss";

const SortDrawer: React.FC<SortDrawerProps> = ({ visible, setVisible }) => {
  const {
    setSort,
    filters: { operationType, sort },
  } = useContext(FiltersContext);

  const applySort = (value: Sort) => {
    setSort(value);
    setVisible(false);
  };

  return (
    <Drawer
      visible={visible}
      onClose={() => setVisible(false)}
      placement="top"
      height="100vh"
    >
      <div className={styles["sort-drawer"]}>
        <div>
          <h3 className="fw-700">ORDENAR</h3>
          <p>Podes ordenar el resultado de tus búsquedas</p>
          <div
            className={classnames(
              styles["sort-drawer__option"],
              (sort === "operationSell.priceUSDmin:ASC" ||
                sort === "operationRent.priceUSDmin:ASC") &&
                styles["press"],
              (sort === "operationSell.priceUSDmin:ASC" ||
                sort === "operationRent.priceUSDmin:ASC") &&
                styles["selected"]
            )}
            onClick={() => {
              applySort(
                operationType
                  ? leastPriceFilterSort(operationType)
                  : "operationSell.priceUSDmin:ASC"
              );
            }}
          >
            Menor precio {operationType ? `(${operationType})` : ""}
          </div>
          <div
            className={classnames(
              styles["sort-drawer__option"],
              (sort === "operationRent.priceUSDmax:DESC" ||
                sort === "operationSell.priceUSDmax:DESC") &&
                styles["press"],
              (sort === "operationRent.priceUSDmax:DESC" ||
                sort === "operationSell.priceUSDmax:DESC") &&
                styles["selected"]
            )}
            onClick={() => {
              applySort(
                operationType
                  ? mostPriceFilterSort(operationType)
                  : "operationSell.priceUSDmax:DESC"
              );
            }}
          >
            Mayor precio {operationType ? `(${operationType})` : ""}
          </div>
          <div
            className={classnames(
              styles["sort-drawer__option"],
              sort === "creationDate:DESC" && styles["selected"],
              sort === "creationDate:DESC" && styles["press"]
            )}
            onClick={() => {
              applySort("creationDate:DESC");
            }}
          >
            Recien publicados{" "}
          </div>
          <div
            className={classnames(
              styles["sort-drawer__option"],
              sort === "totalSurfaceMax:DESC" && styles["selected"],
              sort === "totalSurfaceMax:DESC" && styles["press"]
            )}
            onClick={() => {
              applySort("totalSurfaceMax:DESC");
            }}
          >
            Mas m <sup>2</sup>
          </div>
          <div
            className={classnames(
              styles["sort-drawer__option"],
              sort === "totalSurfaceMax:ASC" && styles["selected"],
              sort === "totalSurfaceMax:ASC" && styles["press"]
            )}
            onClick={() => {
              applySort("totalSurfaceMax:ASC");
            }}
          >
            Menos m <sup>2</sup>
          </div>
        </div>
      </div>
    </Drawer>
  );
};

export default SortDrawer;
