import React, { useContext, useEffect, useState } from "react";
import Link from "next/link";
import { Button, Input, Avatar } from "antd";
import classnames from "classnames";
import debounce from "lodash.debounce";
import { useRouter } from "next/router";

import { NavBarProps } from "./types";
import Logo from "../../assets/icons/general/vanzini-logo.svg";
import LogoWhite from "../../assets/icons/general/vanzini-logo-white.svg";
import Glass from "../../assets/icons/nav-bar/glass.svg";
import Cross from "../../assets/icons/nav-bar/cross.svg";
import { FiltersContext } from "../../contexts/FiltersContext";
import { UserContext } from "../../contexts/UserContext";
import { ModalContext } from "../../contexts/ModalContext";
import ProfilePopup from "../ProfilePopup";

import styles from "./NavBar.module.scss";

const NavBar: React.FC<NavBarProps> = () => {
  const { pathname } = useRouter();
  const [scrolledHome, setScrolledHome] = useState(false);
  const [address, setAddress] = useState<string | undefined>();
  const { changeFilters, filters, constructUrlWithFilter } = useContext(
    FiltersContext
  );
  const { setModal } = useContext(ModalContext);
  const { user } = useContext(UserContext);
  const picture = user.data?.picture;
  const name = user.data?.name;

  useEffect(() => {
    const listener = debounce(() => {
      if (window.scrollY > 518) {
        setScrolledHome(true);
      } else {
        setScrolledHome(false);
      }
    }, 100);

    window.addEventListener("scroll", listener);

    return () => {
      window.removeEventListener("scroll", listener);
    };
  }, []);

  return (
    <div
      className={classnames(
        styles["nav-bar"],
        pathname === "/" && !scrolledHome && styles["nav-bar__home"],
        pathname === "/" && scrolledHome && styles["nav-bar__shadow"],
        pathname !== "/feed" &&
          pathname !== "/feed/[propertyType]" &&
          pathname !== "/" &&
          pathname !== "/mapa" &&
          styles["nav-bar__shadow"]
      )}
    >
      <div className={styles["nav-bar__container"]}>
        <Link href={constructUrlWithFilter("/feed")}>
          <a>
            <div className={styles["nav-bar__logo"]}>
              <img
                src={pathname === "/" && !scrolledHome ? LogoWhite : Logo}
                alt="Logo de inmobiliaria Vanzini"
              />
            </div>
          </a>
        </Link>
        <div className={styles["nav-bar__search"]}>
          <form
            onSubmit={(event) => {
              event.preventDefault();
              changeFilters({
                ...filters,
                address,
              });
            }}
          >
            <Input
              placeholder="Buscar por dirección"
              className={classnames(
                styles["nav-bar__search--inputs"],
                "dp2-shadow"
              )}
              value={address}
              onChange={(event) => setAddress(event.target.value)}
              addonBefore={
                <img
                  onClick={() => {
                    changeFilters({
                      ...filters,
                      address,
                    });
                  }}
                  className={styles["nav-bar__search--glass"]}
                  src={Glass}
                  alt="Lupa azul"
                />
              }
              addonAfter={
                <img
                  className={styles["nav-bar__search--close"]}
                  onClick={() => {
                    setAddress(undefined);
                    changeFilters({
                      ...filters,
                      address: undefined,
                    });
                  }}
                  src={Cross}
                  alt="Cruz negra"
                />
              }
            />
          </form>
        </div>
        <div className={styles["nav-bar__menu"]}>
          <Link href={constructUrlWithFilter("/mapa")}>
            <div
              className={classnames(
                styles["nav-bar__menu--item"],
                pathname === "/mapa" && styles["current-path"]
              )}
            >
              <a href="/mapa">Mapa</a>
            </div>
          </Link>
          <Link href="/tasaciones">
            <div
              className={classnames(
                styles["nav-bar__menu--item"],
                pathname === "/tasaciones" && styles["current-path"]
              )}
            >
              <a href="/tasaciones">Tasaciones</a>
            </div>
          </Link>
          <Link href="/nosotros">
            <div
              className={classnames(
                styles["nav-bar__menu--item"],
                pathname === "/nosotros" && styles["current-path"]
              )}
            >
              <a href="/nosotros">Nosotros</a>
            </div>
          </Link>
          <Link href="/listados">
            <div
              className={classnames(
                styles["nav-bar__menu--item"],
                pathname === "/listados" && styles["current-path"]
              )}
            >
              <a href="/listados">Listados</a>
            </div>
          </Link>
          {user.data ? (
            <ProfilePopup>
              {picture ? (
                <Avatar size={40} src={picture} />
              ) : (
                <Avatar
                  size={40}
                  src={picture}
                  style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
                >
                  {name ? name[0].toUpperCase() : "User"}
                </Avatar>
              )}
            </ProfilePopup>
          ) : (
            <Button
              className={classnames(
                pathname === "/" &&
                  !scrolledHome &&
                  styles["nav-bar__home--log-in-button"]
              )}
              onClick={() => setModal("logInModal", true)}
            >
              Ingresar
            </Button>
          )}
        </div>
      </div>
    </div>
  );
};

export default NavBar;
