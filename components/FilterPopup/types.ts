export type FilterPopupProps = {
  label: string;
  title: string;
  highlighted?: boolean;
  applyOnVisibleChange?: boolean
  onApplyFilter: () => void;
  onClearFilter: () => void;
  className?: string;
  customButton?: React.ReactElement;
};
