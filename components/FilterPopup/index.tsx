import React, { useState } from "react";
import { Button, Popover } from "antd";
import classnames from "classnames";
import {
  DeleteOutlined,
  DownOutlined,
  FilterOutlined,
} from "@ant-design/icons";

import { FilterPopupProps } from "./types";
import styles from "./FilterPopup.module.scss";

const FilterPopup: React.FC<FilterPopupProps> = ({
  label,
  children,
  title,
  highlighted,
  onApplyFilter,
  onClearFilter,
  applyOnVisibleChange,
  className,
  customButton,
}) => {
  const [visible, setVisible] = useState(false);

  return (
    <Popover
      placement="bottom"
      visible={visible}
      onVisibleChange={(visible) => {
        setVisible(visible);
        if (!visible && applyOnVisibleChange) {
          onApplyFilter();
        }
      }}
      getPopupContainer={() =>
        document.getElementById(`filter-popup__${label}`) || document.body
      }
      content={
        <div className={styles["filter-popup__content"]}>
          <div className={styles["filter-popup__content--title"]}>
            <h5 className="fw-700">{title}</h5>
          </div>
          <div className={styles["filter-popup__content--options"]}>
            {children}
          </div>
          <div className={styles["filter-popup__content--actions"]}>
            <Button
              size="small"
              className={styles["ant-btn"]}
              type="dashed"
              danger
              disabled={!highlighted}
              onClick={onClearFilter}
            >
              <DeleteOutlined /> Borrar
            </Button>
            <Button
              size="small"
              onClick={() => {
                onApplyFilter();
                setVisible(false);
              }}
              className={styles["ant-btn"]}
            >
              <FilterOutlined />
              Aplicar
            </Button>
          </div>
        </div>
      }
      trigger="click"
    >
      {customButton || (
        <button
          className={classnames(
            className,
            styles["filter-popup"],
            highlighted && styles["highlighted"]
          )}
          id={`filter-popup__${label}`}
        >
          {label} <DownOutlined style={{ width: 16 }} />
        </button>
      )}
    </Popover>
  );
};

FilterPopup.defaultProps = {
  applyOnVisibleChange: true,
};

export default FilterPopup;
