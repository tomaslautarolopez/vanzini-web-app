import { PropertiesList } from "../../hooks/useGetPropertiesListForFeed";

export type PropertiesListCardProps = {
  propertiesList: PropertiesList;
  className?: string;
};
