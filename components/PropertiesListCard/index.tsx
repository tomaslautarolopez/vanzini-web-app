import React, { useRef, useState } from "react";
import { Button } from "antd";
import classnames from "classnames";
import { useTransition, animated } from "react-spring";
import { ArrowRightOutlined, ShareAltOutlined } from "@ant-design/icons";
import Link from "next/link";

import { useOutsideAlerter } from "../../hooks/useOutsideAlerter";
import ShareButtons from "../ShareButtons";
import { webURL } from "../../axios";
import { PropertiesListCardProps } from "./types";

import styles from "./PropertiesListCard.module.scss";

const PropertiesListCard: React.FC<PropertiesListCardProps> = ({
  propertiesList,
  className,
}) => {
  const shareRef = useRef<HTMLDivElement>(null);
  const [toggle, setToggle] = useState(false);

  useOutsideAlerter(shareRef, () => setToggle(false));

  const transitions = useTransition(toggle, null, {
    from: { position: "absolute", opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
  });

  return (
    <div className={classnames(styles["properties-list-card"], className)}>
      <div className={styles["properties-list-card__image"]}>
        <img src={propertiesList.portrait} />
      </div>
      <div className={styles["properties-list-card__info"]}>
        <div>
          <h3>{propertiesList.title}</h3>
          <p>{propertiesList.description}</p>
        </div>
        <div className={styles["properties-list-card__info--actions"]}>
          <div ref={shareRef} style={{ display: "flex", alignItems: "center" }}>
            {transitions.map(({ item, props }) =>
              item ? (
                <animated.div style={props}>
                  <ShareButtons
                    url={`${webURL}/listados/${propertiesList.slug}`}
                    text={`Te comparto esta lista de propiedades de Vanzini Inmobiliaria: ${propertiesList.title}`}
                  />
                </animated.div>
              ) : (
                <animated.div style={props}>
                  <Button type="dashed" onClick={() => setToggle(true)}>
                    <ShareAltOutlined /> Compartir
                  </Button>
                </animated.div>
              )
            )}
          </div>
          <Link href={`/listados/${propertiesList.slug}`}>
            <Button>
              Ingresar <ArrowRightOutlined />
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default PropertiesListCard;
