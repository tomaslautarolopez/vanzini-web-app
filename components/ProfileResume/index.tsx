import React, { useContext } from "react";
import { Avatar } from "antd";
import { BellOutlined, HeartOutlined, TeamOutlined } from "@ant-design/icons";
import classnames from "classnames";
import Link from "next/link";

import { UserContext } from "../../contexts/UserContext";
import logout from "../../assets/icons/menu-mobile/logout.svg";
import { ModalContext } from "../../contexts/ModalContext";
import { ProfileResumeProps } from "./types";

import styles from "./ProfileResume.module.scss";

const ProfileResume: React.FC<ProfileResumeProps> = () => {
  const {
    user: { data },
  } = useContext(UserContext);
  const { setModal } = useContext(ModalContext);

  const likes = data?.likes;
  const alerts = data?.alerts;

  return (
    data && (
      <div className={styles["profile-resume"]}>
        <div className={styles["profile-resume__user"]}>
          {data.picture ? (
            <Avatar size={64} src={data.picture} />
          ) : (
            <Avatar
              size={64}
              src={data.picture}
              style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
            >
              {data.name ? data.name[0].toUpperCase() : "User"}
            </Avatar>
          )}
          <div className={styles["profile-resume__user--data"]}>
            <h5>{data.name}</h5>
            <p>{data.email}</p>
          </div>
        </div>
        <div className={styles["profile-resume__info"]}>
          <Link href="/perfil">
            <h6
              className={classnames(
                likes && likes?.size > 0 && styles["with-content"]
              )}
            >
              <HeartOutlined className={styles["profile-resume__info--icon"]} />
              Guardados
              {likes && likes?.size > 0 && <label>{likes.size}</label>}
            </h6>
          </Link>
          <Link href="/perfil">
            <h6
              className={classnames(
                alerts && alerts?.size > 0 && styles["with-content"]
              )}
            >
              <BellOutlined className={styles["profile-resume__info--icon"]} />
              Alertas
              {alerts && alerts?.size > 0 && <label>{alerts.size}</label>}
            </h6>
          </Link>
          <Link href="/perfil">
            <h6>
              <TeamOutlined className={styles["profile-resume__info--icon"]} />
              Perfil
            </h6>
          </Link>
          <h6 onClick={() => setModal("logOutModal", true)}>
            <img
              src={logout}
              className={styles["profile-resume__info--icon"]}
            />
            Cerrar sesión
          </h6>
        </div>
      </div>
    )
  );
};

export default ProfileResume;
