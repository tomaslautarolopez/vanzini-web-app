import { BellOutlined } from "@ant-design/icons";
import React, { useEffect, useState } from "react";
import ModalWithImage from "../ModalWithImage";
import AlertBg from "../../assets/images/Alert/alert-bg.jpg";
import { RemoveAlertModalProps } from "./types";
import { useRouter } from "next/router";
import { usePriceAlerts } from "../../hooks/usePriceAlerts";

import styles from "./RemoveAlertModal.module.scss";
import Link from "next/link";

const RemoveAlertModal: React.FC<RemoveAlertModalProps> = () => {
  const {
    query: { alertRemoveToken, propertyId },
  } = useRouter();

  const { removeEmailPriceAlert } = usePriceAlerts();
  const [visible, setVisible] = useState<boolean>();

  useEffect(() => {
    if (alertRemoveToken && propertyId) {
      removeEmailPriceAlert(
        alertRemoveToken as string,
        propertyId as string,
        () => setVisible(true)
      );
    }
  }, [alertRemoveToken, propertyId]);

  return (
    <ModalWithImage
      style={{
        width: 840,
      }}
      image={AlertBg}
      imageClassname={styles["remove-alert-modal__image"]}
      altImage="Barquito de papel escultura Rosario"
      visible={visible}
      onCancel={() => setVisible(false)}
      footer={null}
      header={
        <h6 className={styles["remove-alert-modal__header"]}>
          <BellOutlined style={{ fontSize: 24, marginRight: 8 }} /> ALERTA
        </h6>
      }
    >
      <div className={styles["remove-alert-modal"]}>
        <h3>Acabas de eliminar una alerta de precio!</h3>
        <p>
          Recuerda que puedes volver a crear una alerta de precio sobre esta
          propiedad cuando quieras
        </p>
        <Link href="/feed">
          <button
            className="black-custom-button"
            onClick={() => setVisible(false)}
          >
            SEGUIR BUSCANDO
          </button>
        </Link>
      </div>
    </ModalWithImage>
  );
};

export default RemoveAlertModal;
