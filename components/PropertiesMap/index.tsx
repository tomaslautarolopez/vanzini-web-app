import React, { useContext, useEffect, useRef, useState } from "react";
import { MapContainer, Marker, Popup, TileLayer, useMap } from "react-leaflet";
import { icon, LatLngBounds, Marker as MakerType } from "leaflet";
import MarkerClusterGroup from "react-leaflet-markercluster";
import { useRouter } from "next/router";
import { Button } from "antd";

import {
  defaultMapIcons,
  likedMapIcons,
  hoveredMapIcons,
} from "../../constants/map";
import { PropertiesMapProps } from "./types";
import { isMobileWeb } from "../../utils/isMobileWeb";
import MapPopup from "../MapPopup";
import { UserContext } from "../../contexts/UserContext";
import { Property } from "../../interfaces/properties";
import target from "../../assets/icons/map/target.svg";

import styles from "./PropertiesMap.module.scss";

import "leaflet/dist/leaflet.css";
import "react-leaflet-markercluster/dist/styles.min.css";

const CenteringComponetAndBounds: React.FC<{
  centeredProperty?: Property;
  setSearchedBounds: (bounds: LatLngBounds) => void;
}> = ({ centeredProperty, setSearchedBounds }) => {
  const map = useMap();

  useEffect(() => {
    if (centeredProperty) {
      const center = {
        lat: centeredProperty.geoLocation.coordinates[0],
        lng: centeredProperty.geoLocation.coordinates[1],
      };

      setTimeout(() => {
        map.flyTo(center, 15);
      }, 1000);
    } else if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        map.flyTo({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });
      });
    }
  }, [centeredProperty]);

  return (
    <Button
      className={styles["properties-map__search-in-zone--button"]}
      onClick={() => {
        setSearchedBounds(map.getBounds());
      }}
    >
      Buscar en este lugar <img src={target} />
    </Button>
  );
};

const PropertiesMap: React.FC<PropertiesMapProps> = ({
  mapProperties,
  setMobilePropertyClicked,
  setSearchedBounds,
  hoveredProperty,
}) => {
  const {
    user: { data: userData },
  } = useContext(UserContext);
  const [popupOpen, setPopupOpen] = useState(false);

  const {
    query: { propiedad },
  } = useRouter();

  const centeredProperty = mapProperties.find(
    (property) => property._id === propiedad
  );

  const markerRef = useRef<MakerType | null>(null);

  const mobile = isMobileWeb();

  useEffect(() => {
    if (markerRef.current && !popupOpen) {
      setTimeout(() => {
        if (mobile && centeredProperty) {
          setMobilePropertyClicked(centeredProperty);
        } else if (centeredProperty) {
          markerRef.current?.openPopup();
        }
        setPopupOpen(true);
      }, 1500);
    }
  });

  return (
    <MapContainer
      zoom={12}
      center={{ lat: -32.94361111, lng: -60.65277778 }}
      style={{ width: "100%", height: "100%", zIndex: 0 }}
      zoomControl={false}
    >
      <CenteringComponetAndBounds
        setSearchedBounds={setSearchedBounds}
        centeredProperty={centeredProperty}
      />
      <TileLayer
        url="https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}"
        subdomains={["mt0", "mt1", "mt2", "mt3"]}
        tileSize={256}
      />
      <MarkerClusterGroup>
        {mapProperties
          .filter((property) => property._id !== propiedad)
          .map((property) => {
            const { propertyType, publicationTitle, geoLocation } = property;
            const center = {
              lat: geoLocation.coordinates[0],
              lng: geoLocation.coordinates[1],
            };

            const liked = userData?.likes.has(property._id);

            const iconUrl = !liked
              ? defaultMapIcons[propertyType] || defaultMapIcons.HO
              : likedMapIcons[propertyType] || likedMapIcons.HO;

            return (
              <Marker
                eventHandlers={{
                  click: () => {
                    if (mobile) {
                      setMobilePropertyClicked(property);
                    }
                  },
                }}
                key={property._id}
                icon={icon({
                  iconUrl,
                  iconSize: [55, 55],
                  iconAnchor: [22.5, 5],
                  popupAnchor: [0, 0],
                })}
                position={center}
                title={publicationTitle}
              >
                {!mobile && (
                  <Popup>
                    <MapPopup property={property} liked={liked} />
                  </Popup>
                )}
              </Marker>
            );
          })}
      </MarkerClusterGroup>
      {hoveredProperty && (
        <Marker
          ref={markerRef}
          eventHandlers={{
            click: () => {
              if (mobile) {
                setMobilePropertyClicked(hoveredProperty);
              }
            },
          }}
          key={hoveredProperty._id}
          icon={icon({
            iconUrl:
              hoveredMapIcons[hoveredProperty.propertyType] ||
              hoveredMapIcons.HO,
            iconSize: [55, 55],
            iconAnchor: [22.5, 5],
            popupAnchor: [0, 0],
          })}
          position={{
            lat: hoveredProperty.geoLocation.coordinates[0],
            lng: hoveredProperty.geoLocation.coordinates[1],
          }}
          title={hoveredProperty.publicationTitle}
        >
          {!mobile && (
            <Popup>
              <MapPopup
                property={hoveredProperty}
                liked={userData?.likes.has(hoveredProperty._id)}
              />
            </Popup>
          )}
        </Marker>
      )}
      {centeredProperty && (
        <Marker
          ref={markerRef}
          eventHandlers={{
            click: () => {
              if (mobile) {
                setMobilePropertyClicked(centeredProperty);
              }
            },
          }}
          key={centeredProperty._id}
          icon={icon({
            iconUrl:
              defaultMapIcons[centeredProperty.propertyType] ||
              defaultMapIcons.HO,
            iconSize: [55, 55],
            iconAnchor: [22.5, 5],
            popupAnchor: [0, 0],
          })}
          position={{
            lat: centeredProperty.geoLocation.coordinates[0],
            lng: centeredProperty.geoLocation.coordinates[1],
          }}
          title={centeredProperty.publicationTitle}
        >
          {!mobile && (
            <Popup>
              <MapPopup
                property={centeredProperty}
                liked={userData?.likes.has(centeredProperty._id)}
              />
            </Popup>
          )}
        </Marker>
      )}
    </MapContainer>
  );
};

export default PropertiesMap;
