import { LatLngBounds } from "leaflet";
import { Property } from "../../interfaces/properties";

export type PropertiesMapProps = {
  mapProperties: Property[];
  setMobilePropertyClicked: (property: Property) => void;
  setSearchedBounds: (bounds: LatLngBounds) => void;
  hoveredProperty?: Property;
};
