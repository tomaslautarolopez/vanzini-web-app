import { BellOutlined } from "@ant-design/icons";
import React, { useContext } from "react";
import { AlertModalProps } from "./types";
import ModalWithImage from "../ModalWithImage";
import { ModalContext } from "../../contexts/ModalContext";
import AlertBg from "../../assets/images/Alert/alert-bg.jpg";
import AlertForm from "../AlertForm";

import styles from "./AlertModal.module.scss";

const AlertModal: React.FC<AlertModalProps> = () => {
  const {
    modals: { alertModal },
    setModal,
    modalsData: { alertModal: alertModalData },
  } = useContext(ModalContext);

  return (
    <ModalWithImage
      style={{
        width: 840,
      }}
      image={AlertBg}
      imageClassname={styles["alert-modal__image"]}
      altImage="Barquito de papel escultura Rosario"
      visible={alertModal}
      onCancel={() => setModal("alertModal", false)}
      footer={null}
      header={
        <h6 className={styles["alert-modal__header"]}>
          <BellOutlined style={{ fontSize: 24, marginRight: 8 }} /> ALERTA
        </h6>
      }
    >
      <div className={styles["alert-modal"]}>
        <h3>Alerta si baja de precio!</h3>
        <p>
          Cuando esta propiedad baje de precio te estaremos enviando un email
          para notificarte
        </p>
        {alertModalData.propertyId && (
          <AlertForm
            propertyId={alertModalData.propertyId}
            callback={() => setModal("alertModal", false)}
          />
        )}
        <h6 className={styles["alert-modal__footer"]}>
          Al crear esta alerta aceptás los{" "}
          <a href="/terminos-y-condiciones" target="_blank">
            términos y condiciones de uso.
          </a>
        </h6>
      </div>
    </ModalWithImage>
  );
};

export default AlertModal;
