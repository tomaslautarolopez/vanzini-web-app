import React from "react";

export enum COLORS {
  MAGENTA = "magenta",
  BLUE = "blue",
  GRAY = "gray",
}

export type BannerProps = {
  className?: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  buttonContent?: React.ReactNode;
  titleContent?: React.ReactNode;
  textContent?: React.ReactNode;
  color: COLORS;
};
