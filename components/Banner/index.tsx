import React from "react";
import classnames from "classnames";

import { BannerProps, COLORS } from "./types";
import styles from "./Banner.module.scss";

const getBackgroundsColors = (color: COLORS) => {
  switch (color) {
    case COLORS.BLUE:
      return {
        buttonBg: "var(--color-blue-7)",
        containerBg: "var(--color-blue-8)",
      };

    case COLORS.GRAY:
      return {
        buttonBg: "var(--color-gray-8)",
        containerBg: "var(--color-gray-9)",
      };

    case COLORS.MAGENTA:
      return {
        buttonBg: "var(--color-magenta-5)",
        containerBg: "var(--color-magenta-6)",
      };
  }
};

const Banner: React.FC<BannerProps> = ({
  onClick,
  buttonContent,
  titleContent,
  textContent,
  className,
  color,
}) => {
  const { buttonBg, containerBg } = getBackgroundsColors(color);

  return (
    <div
      className={classnames(styles["banner"], className)}
      style={{
        backgroundColor: containerBg,
      }}
    >
      <div className={styles["banner__container"]}>
        <h5>{titleContent}</h5>
        <p>{textContent}</p>
        <button
          style={{
            backgroundColor: buttonBg,
          }}
          onClick={onClick}
          className={styles["banner__button"]}
        >
          {buttonContent}
        </button>
      </div>
    </div>
  );
};

export default Banner;
