import React from "react";
import { useRouter } from "next/router";

import { AssessBannerProps } from "./types";
import Banner from "../Banner";
import { COLORS } from "../Banner/types";

const AssessBanner: React.FC<AssessBannerProps> = () => {
  const { push } = useRouter();
  return (
    <Banner
      color={COLORS.GRAY}
      buttonContent={"Vender"}
      onClick={() => push("/tasaciones")}
      textContent={
        <>
          ¿Querés vender tu propiedad?
          <br /> Asesorate con nosotros.
        </>
      }
      titleContent={
        <>
          <b>¿Querés vender tu propiedad?</b> Asesorate con nosotros.
        </>
      }
    />
  );
};

export default AssessBanner;
