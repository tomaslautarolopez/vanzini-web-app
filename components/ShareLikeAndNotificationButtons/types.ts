export type ShareLikeAndNotificationButtonsProps = {
  url: string;
  shareText: string;
  propertyId: string;
  toggle: boolean;
  setToggle: (toggle: boolean) => void;
};
