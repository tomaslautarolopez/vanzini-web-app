import React, { useContext } from "react";
import { useTransition, animated } from "react-spring";
import {
  BellFilled,
  BellOutlined,
  HeartFilled,
  HeartOutlined,
  LoadingOutlined,
  ShareAltOutlined,
} from "@ant-design/icons";

import { useLikeProperty } from "../../hooks/useLikeProperty";
import { ShareLikeAndNotificationButtonsProps } from "./types";
import ShareButtons from "../ShareButtons";
import { ModalContext } from "../../contexts/ModalContext";
import { usePriceAlerts } from "../../hooks/usePriceAlerts";
import { UserContext } from "../../contexts/UserContext";

import styles from "./ShareLikeAndNotificationButtons.module.scss";

const ShareLikeAndNotificationButtons: React.FC<ShareLikeAndNotificationButtonsProps> = ({
  url,
  shareText,
  propertyId,
  toggle,
  setToggle,
}) => {
  const transitions = useTransition(toggle, null, {
    from: { position: "absolute", opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
  });
  const {
    likeProperty,
    dislikeProperty,
    loading: likeLoading,
  } = useLikeProperty();
  const { setModal, setAlertModalData } = useContext(ModalContext);
  const { removeUserPriceAlert, loading: alertLoading } = usePriceAlerts();

  const {
    user: { data },
  } = useContext(UserContext);
  const liked = data?.likes.has(propertyId);
  const alertSetted = data?.alerts.has(propertyId);

  return (
    <div className={styles["share-like-and-notification-buttons"]}>
      {transitions.map(({ item, props, key }) =>
        item ? (
          <animated.div style={props} key={key}>
            <ShareButtons url={url} text={shareText} />
          </animated.div>
        ) : (
          <animated.div style={props} key={key}>
            {likeLoading ? (
              <LoadingOutlined
                className={styles["share-like-and-notification-buttons__icon"]}
              />
            ) : liked ? (
              <HeartFilled
                onClick={() => {
                  dislikeProperty(propertyId);
                }}
                className={styles["share-like-and-notification-buttons__icon"]}
              />
            ) : (
              <HeartOutlined
                onClick={() => {
                  likeProperty(propertyId);
                }}
                className={styles["share-like-and-notification-buttons__icon"]}
              />
            )}

            <ShareAltOutlined
              onClick={() => setToggle(true)}
              className={styles["share-like-and-notification-buttons__icon"]}
            />
            {alertLoading ? (
              <LoadingOutlined
                className={styles["share-like-and-notification-buttons__icon"]}
              />
            ) : alertSetted ? (
              <BellFilled
                onClick={() => {
                  removeUserPriceAlert(propertyId);
                }}
                className={styles["share-like-and-notification-buttons__icon"]}
              />
            ) : (
              <BellOutlined
                onClick={() => {
                  setModal("alertModal", true);
                  setAlertModalData({ propertyId });
                }}
                className={styles["share-like-and-notification-buttons__icon"]}
              />
            )}
          </animated.div>
        )
      )}
    </div>
  );
};

export default ShareLikeAndNotificationButtons;
