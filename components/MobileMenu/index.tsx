import React, { useContext, useState } from "react";
import { Collapse } from "antd";
import { DownOutlined } from "@ant-design/icons";
import Link from "next/link";
import classnames from "classnames";

import { MobileMenuProps } from "./types";
import { ModalContext } from "../../contexts/ModalContext";
import { propertyTypeOptions } from "../../constants/filters";

import LogoShield from "../../assets/icons/general/vanzini-shield.svg";

import login from "../../assets/icons/menu-mobile/login.svg";
import map from "../../assets/icons/menu-mobile/map.svg";
import like from "../../assets/icons/menu-mobile/like.svg";
import appraisal from "../../assets/icons/menu-mobile/appraisal.svg";
import send from "../../assets/icons/menu-mobile/send.svg";
import mail from "../../assets/icons/menu-mobile/mail.svg";
import tag from "../../assets/icons/menu-mobile/tag.svg";
import bullet from "../../assets/icons/menu-mobile/bullet.svg";
import aboutUs from "../../assets/icons/menu-mobile/about-us.svg";
import logout from "../../assets/icons/menu-mobile/logout.svg";

import Facebook from "../../assets/icons/menu-mobile/social-media/Facebook.svg";
import Instagram from "../../assets/icons/menu-mobile/social-media/Instagram.svg";
import Pinterest from "../../assets/icons/menu-mobile/social-media/Pinterest.svg";
import Spotify from "../../assets/icons/menu-mobile/social-media/Spotify.svg";
import Youtube from "../../assets/icons/menu-mobile/social-media/Youtube.svg";

import styles from "./MobileMenu.module.scss";
import { FiltersContext } from "../../contexts/FiltersContext";

const { Panel } = Collapse;

const MobileMenu: React.FC<MobileMenuProps> = ({ onClose }) => {
  const { setModal } = useContext(ModalContext);
  const [activeKey, setActiveKey] = useState<undefined | "1">();
  const { constructUrlWithFilter } = useContext(FiltersContext);

  return (
    <div className={styles["mobile-menu"]}>
      <div className={styles["mobile-menu__menu"]}>
        <h6>
          <img src={LogoShield} alt="Escudo de armas vanzini" /> MENU
        </h6>
        <h5
          onClick={() => {
            setModal("logInModal", true);
            onClose();
          }}
        >
          <img src={login} /> Ingresar
        </h5>
        <Link href={constructUrlWithFilter("/mapa")}>
          <h5
            onClick={() => {
              onClose();
            }}
          >
            <img src={map} /> Mapa
          </h5>
        </Link>
        <h5>
          <img src={like} /> Guardados
        </h5>
        <Link href="/tasaciones">
          <h5 onClick={() => onClose()}>
            <img src={appraisal} /> Vender
          </h5>
        </Link>
        <h5
          onClick={() => {
            setModal("consultModal", true);
            onClose();
          }}
        >
          <img src={send} /> Consulta
        </h5>
        <h5
          onClick={() => {
            setModal("newsletterModal", true);
            onClose();
          }}
        >
          <img src={mail} /> Suscribirme al Newsletter
        </h5>
        <h5
          onClick={() => {
            if (activeKey) {
              setActiveKey(undefined);
            } else {
              setActiveKey("1");
            }
          }}
        >
          <img src={tag} /> Categorías{" "}
          <DownOutlined
            className={classnames(
              styles["mobile-menu__collpase--anchor"],
              activeKey && styles["open"]
            )}
          />
          <Collapse activeKey={activeKey} className="mobile-menu__collpase">
            <Panel header="" key="1">
              <ul className={styles["mobile-menu__collpase--categories"]}>
                {propertyTypeOptions.map(({ label, query }) => (
                  <Link href={`/feed/${query}`}>
                    <li onClick={() => onClose()}>{label}</li>
                  </Link>
                ))}
              </ul>
            </Panel>
          </Collapse>
        </h5>
        <Link href="/listados">
          <h5 onClick={() => onClose()}>
            <img src={bullet} /> Listados
          </h5>
        </Link>
        <Link href="/nosotros">
          <h5 onClick={() => onClose()}>
            <img src={aboutUs} /> Nosotros
          </h5>
        </Link>
        <h5
          onClick={() => {
            setModal("logOutModal", true);

            onClose();
          }}
        >
          <img src={logout} /> Cerrar Sesión
        </h5>
      </div>

      <div className={styles["mobile-menu__social-media"]}>
        <h5>Seguinos</h5>

        <a href="https://www.facebook.com/vanzini.prop" target="_blank">
          <img src={Facebook} />
        </a>
        <a
          href="https://open.spotify.com/user/b7m3f3xjx7yx8rnnez8g8heid?si=5Wb7cRrXTsySE2LbNv6-wA"
          target="_blank"
        >
          <img src={Spotify} />
        </a>
        <a
          href="https://www.youtube.com/user/VanziniPropiedades"
          target="_blank"
        >
          <img src={Youtube} />
        </a>
        <a href="https://www.pinterest.es/vanzinipropiedades/" target="_blank">
          <img src={Pinterest} />
        </a>
        <a href="https://www.instagram.com/vanzinipropiedades/" target="_blank">
          <img src={Instagram} />
        </a>
      </div>
    </div>
  );
};

export default MobileMenu;
