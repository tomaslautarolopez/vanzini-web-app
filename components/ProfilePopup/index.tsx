import React from "react";
import { Popover } from "antd";

import { ProfilePopupProps } from "./types";

import ProfileResume from "../ProfileResume";

const ProfilePopup: React.FC<ProfilePopupProps> = ({ children }) => {
  return (
    <Popover
      content={<ProfileResume />}
      placement="bottom"
      getPopupContainer={() => {
        return document.getElementById("profile-popup-id") as HTMLElement;
      }}
    >
      <div id="profile-popup-id">{children}</div>
    </Popover>
  );
};

export default ProfilePopup;
