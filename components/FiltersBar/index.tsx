import React from "react";
import { Checkbox, Radio, InputNumber, Select, Button } from "antd";
import classnames from "classnames";
import { DeleteOutlined } from "@ant-design/icons";

import { FiltersBarProps } from "./types";
import styles from "./FiltersBar.module.scss";
import FilterPopup from "../FilterPopup";
import {
  ageOptions,
  propertyTypeOptions,
  roomAmountOptions,
  amenitiesOptions,
  otherOptions,
} from "../../constants/filters";
import { useFilters } from "../../hooks/useFilters";
import OperationTypeFilterRadioGroup from "../OperationTypeFilterRadioGroup";

const popUpClass = styles["filters-bar__filter-popup"];

const { Option } = Select;

const FiltersBar: React.FC<FiltersBarProps> = ({ className }) => {
  const {
    resetFilters,
    countryOptions,
    secondSubDivisionOptions,
    firstSubDivisionOptions,
    country,
    setCountry,
    firstSubDivision,
    setFirstSubDivision,
    secondSubDivision,
    setSecondSubDivision,
    currency,
    setCurrency,
    fromPrice,
    setFromPrice,
    toPrice,
    setToPrice,
    operationType,
    setOperationType,
    propertyType,
    setPropertyType,
    age,
    setAge,
    roomAmount,
    setRoomAmount,
    amenities,
    setAmenities,
    other,
    setOther,
    applyFilters,
  } = useFilters();

  return (
    <div className={classnames(styles["filters-bar"], className)}>
      <div className={styles["filters-bar__container"]}>
        <span className={styles["filters-bar__label"]}>Filtros</span>
        <FilterPopup
          className={popUpClass}
          onApplyFilter={applyFilters}
          onClearFilter={() => setOperationType("Venta")}
          title="Tipo de operación"
          label="Operación"
          highlighted={!!operationType?.length}
        >
          <OperationTypeFilterRadioGroup
            operationType={operationType}
            setOperationType={setOperationType}
          />
        </FilterPopup>
        <FilterPopup
          className={popUpClass}
          onApplyFilter={applyFilters}
          onClearFilter={() => setPropertyType([])}
          title="Tipología"
          label="Tipología"
          highlighted={!!propertyType.length}
        >
          <Checkbox.Group
            value={propertyType}
            onChange={(values: any) => setPropertyType(values)}
            className={styles["filters-bar__options"]}
          >
            {propertyTypeOptions.map((option) => (
              <Checkbox value={option.value} key={option.value}>
                {option.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </FilterPopup>
        <FilterPopup
          className={popUpClass}
          onApplyFilter={applyFilters}
          onClearFilter={() => setRoomAmount([])}
          title="Dormitorios"
          label="Dormitorios"
          highlighted={!!roomAmount.length}
        >
          <Checkbox.Group
            value={roomAmount}
            onChange={(values: any) => setRoomAmount(values)}
            className={styles["filters-bar__options"]}
          >
            {roomAmountOptions.map((option) => (
              <Checkbox value={option.value} key={option.value}>
                {option.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </FilterPopup>
        <FilterPopup
          className={popUpClass}
          onApplyFilter={applyFilters}
          onClearFilter={() => {
            setToPrice(undefined);
            setFromPrice(undefined);
            setOther([]);
          }}
          title="Precio"
          label="Precio"
          highlighted={!!fromPrice || !!toPrice || !!other.length}
        >
          <div className={styles["filters-bar__options--price"]}>
            <Radio.Group
              value={currency}
              onChange={(e) => setCurrency(e.target.value)}
              className={styles["ant-radio-group"]}
            >
              <Radio value={"ARS"}>Pesos</Radio>
              <Radio value={"USD"}>U$D</Radio>
            </Radio.Group>
            <div className={styles["filters-bar__options--price-inputs"]}>
              <InputNumber
                value={fromPrice}
                onChange={(value) => setFromPrice(Number(value))}
                style={{ width: 100, textAlign: "center" }}
                placeholder="Desde"
              />
              {" - "}
              <InputNumber
                value={toPrice}
                onChange={(value) => setToPrice(Number(value))}
                className="site-input-right"
                style={{
                  width: 100,
                  textAlign: "center",
                }}
                placeholder="Hasta"
              />
            </div>
            <Checkbox.Group
              value={other}
              onChange={(values: any) => setOther(values)}
            >
              {otherOptions.map((option) => (
                <div key={option.value}>
                  <Checkbox
                    value={option.value}
                    className={styles["ant-checkbox-wrapper"]}
                  >
                    {option.label}
                  </Checkbox>
                </div>
              ))}
            </Checkbox.Group>
          </div>
        </FilterPopup>
        <FilterPopup
          className={popUpClass}
          onApplyFilter={applyFilters}
          onClearFilter={() => setAge([])}
          title="Antigüedad"
          label="Antigüedad"
          highlighted={!!age.length}
        >
          <Checkbox.Group
            value={age}
            onChange={(values: any) => setAge(values)}
            className={styles["filters-bar__options"]}
          >
            {ageOptions.map((option) => (
              <Checkbox value={option.value} key={option.value}>
                {option.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </FilterPopup>
        <FilterPopup
          className={popUpClass}
          onApplyFilter={applyFilters}
          onClearFilter={() => {
            setCountry("Argentina");
            setFirstSubDivision(undefined);
            setSecondSubDivision(undefined);
          }}
          title="Ubicación"
          label="Ubicación"
          highlighted={!!country || !!firstSubDivision || !!secondSubDivision}
        >
          <div>
            <Radio.Group
              value={country}
              onChange={(e) => setCountry(e.target.value)}
              className={styles["ant-radio-group"]}
            >
              {countryOptions.map((option) => (
                <Radio value={option} key={option}>
                  {option}
                </Radio>
              ))}
            </Radio.Group>
            <div
              id="location-filter-bar-fsd"
              className={styles["filters-bar__options--location-select"]}
            >
              <div>Ciudad</div>
              <Select
                style={{ width: "100%" }}
                disabled={!firstSubDivisionOptions}
                placeholder="Rosario"
                onChange={(value: string) => setFirstSubDivision(value)}
                getPopupContainer={() =>
                  document.getElementById("location-filter-bar-fsd") ||
                  document.body
                }
              >
                {firstSubDivisionOptions &&
                  firstSubDivisionOptions.map(
                    ({ value, amount }: { value: string; amount: number }) =>
                      value !== "amount" && (
                        <Option value={value} key={value}>
                          {value} ({amount})
                        </Option>
                      )
                  )}
              </Select>
            </div>
            <div
              id="location-filter-bar-ssd"
              className={styles["filters-bar__options--location-select"]}
            >
              <div>Barrio</div>
              <Select
                getPopupContainer={() =>
                  document.getElementById("location-filter-bar-ssd") ||
                  document.body
                }
                style={{ width: "100%" }}
                disabled={!secondSubDivisionOptions}
                placeholder="Centro"
                onChange={(value: string) => setSecondSubDivision(value)}
              >
                {secondSubDivisionOptions &&
                  secondSubDivisionOptions.map(
                    ({ value, amount }: { value: string; amount: number }) =>
                      value !== "amount" && (
                        <Option value={value} key={value}>
                          {value} ({amount})
                        </Option>
                      )
                  )}
              </Select>
            </div>
          </div>
        </FilterPopup>
        <FilterPopup
          className={popUpClass}
          onApplyFilter={applyFilters}
          onClearFilter={() => setAmenities([])}
          title="Amenities"
          label="Amenities"
          highlighted={!!amenities.length}
        >
          <Checkbox.Group
            value={amenities}
            onChange={(values: any) => setAmenities(values)}
            className={styles["filters-bar__options"]}
          >
            {amenitiesOptions.map((option) => (
              <Checkbox value={option.value} key={option.value}>
                {option.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </FilterPopup>
        <Button type="dashed" onClick={resetFilters}>
          <DeleteOutlined /> Borrar Filtros
        </Button>
      </div>
    </div>
  );
};

export default FiltersBar;
