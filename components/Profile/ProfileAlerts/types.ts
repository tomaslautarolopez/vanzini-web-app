import { Property } from "../../../interfaces/properties";

export type ProfileAlertsProps = {};

export interface TableProps {
    key: string;
    totalSurface: string;
    address: string;
    typology: string;
    roomAmounts: string;
    previousPrice: Property;
    currentPrice: Property;
  }
  