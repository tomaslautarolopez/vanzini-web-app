import { useEffect, useState } from "react";
import { Sort } from "../../../contexts/FiltersContext";
import { fetchProperties } from "../../../data-fetch/fetchProperties";
import { Property } from "../../../interfaces/properties";

export const useProfileAlerts = (alerts: Set<string> | undefined) => {
  const [alertsProperties, setAlertsProperties] = useState<Property[]>([]);
  const [sort, setSort] = useState<Sort | undefined>();
  const [loading, setLoading] = useState(false);

  const getLikedProperties = async () => {
    setLoading(true);
    try {
      const { properties } = await fetchProperties(0, undefined, {
        id: alerts && [...alerts],
        sort,
      });

      setAlertsProperties(properties);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  };

  useEffect(() => {
    if (alerts && alerts.size > 0) {
      getLikedProperties();
    }
  }, [alerts, sort]);

  return {
    alertsProperties,
    setSort,
    loading,
  };
};
