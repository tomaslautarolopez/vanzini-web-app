import React, { useContext, useState } from "react";
import { Popover, Table } from "antd";
import { DownOutlined, MoreOutlined } from "@ant-design/icons";
import { Breakpoint } from "antd/lib/_util/responsiveObserve";
import { Pagination } from "antd";

import { ProfileAlertsProps, TableProps } from "./types";
import { useProfileAlerts } from "./hooks";
import { UserContext } from "../../../contexts/UserContext";
import SortPopup from "../../SortPopup";
import { useGeneratePriceFromOperation } from "../../../hooks/useGeneratePriceString";
import { propertyTypeToString } from "../../../utils/propertyTypeToString";
import { ModalContext } from "../../../contexts/ModalContext";
import { buildRoomsAmountString } from "../../../utils/buildRoomsAmountString";
import { getFullAddress } from "../../../utils/getFullAddress";
import PropertySimpleMenu from "../../PropertySimpleMenu";
import { priceSorter } from "../../../utils/priceSorter";
import { Property } from "../../../interfaces/properties";

import styles from "./ProfileAlerts.module.scss";
import { numberWithDotsAndCommas } from "../../../utils/numberWithDotsAndCommas";

const PAGE_SIZE = 8;

const ProfileAlerts: React.FC<ProfileAlertsProps> = () => {
  const {
    user: { data },
  } = useContext(UserContext);
  const { alertsProperties, setSort } = useProfileAlerts(data?.alerts);
  const [page, setPage] = useState(1);
  const { setModal, setConsultModalData } = useContext(ModalContext);

  const columns = [
    {
      title: "DIRECCION",
      dataIndex: "address",
    },
    {
      title: "TOTAL",
      dataIndex: "totalSurface",
      sorter: (a: TableProps, b: TableProps) =>
        a.totalSurface.localeCompare(b.totalSurface),
      responsive: ["lg", "xl", "xxl"] as Breakpoint[],
    },
    {
      title: "TIPOLOGÍA",
      dataIndex: "typology",
      sorter: (a: TableProps, b: TableProps) =>
        a.typology.localeCompare(b.typology),
      responsive: ["lg", "xl", "xxl"] as Breakpoint[],
    },
    {
      title: "DORMITORIOS",
      dataIndex: "roomAmounts",
      sorter: (a: TableProps, b: TableProps) =>
        a.roomAmounts.localeCompare(b.roomAmounts),
      responsive: ["lg", "xl", "xxl"] as Breakpoint[],
    },
    {
      title: "ANTES",
      dataIndex: "previousPrice",
      className: styles["profile-alerts__table--price-previous"],
      render: (prop: Property) => {
        const priceString = useGeneratePriceFromOperation(
          prop.lastOperationSell,
          prop.webPrice
        );

        if (priceString === "CONSULTAR PRECIO") {
          return (
            <span
              onClick={() => {
                setModal("consultModal", true);
                setConsultModalData({
                  property: prop,
                  propertiesId: [
                    {
                      id: prop._id,
                      propertyType: prop.propertyType,
                    },
                  ],
                });
              }}
              style={{
                cursor: "pointer",
                color: "#1890ff",
              }}
            >
              Consultar
            </span>
          );
        } else {
          return priceString;
        }
      },
      sorter: (a: TableProps, b: TableProps) =>
        priceSorter(
          a.currentPrice.lastOperationSell,
          b.currentPrice.lastOperationSell
        ),
    },
    {
      title: "AHORA",
      dataIndex: "currentPrice",
      render: (prop: Property) => {
        const priceString = useGeneratePriceFromOperation(
          prop.operationSell,
          prop.webPrice
        );

        if (priceString === "CONSULTAR PRECIO") {
          return (
            <span
              onClick={() => {
                setModal("consultModal", true);
                setConsultModalData({
                  property: prop,
                  propertiesId: [
                    {
                      id: prop._id,
                      propertyType: prop.propertyType,
                    },
                  ],
                });
              }}
              style={{
                cursor: "pointer",
                color: "#1890ff",
              }}
            >
              Consultar
            </span>
          );
        } else {
          return priceString;
        }
      },
      className: styles["profile-alerts__table--price-current"],
      sorter: (a: TableProps, b: TableProps) =>
        priceSorter(a.currentPrice.operationSell, b.currentPrice.operationSell),
    },
    {
      dataIndex: "actions",
    },
  ];

  const tableData: TableProps[] = alertsProperties
    .slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE)
    .map((prop) => {
      return {
        key: `${prop._id}_${prop.propertyType}`,
        address: getFullAddress(prop),
        roomAmounts: buildRoomsAmountString(prop),
        totalSurface: `${numberWithDotsAndCommas(prop.totalSurfaceMax)}m²`,
        typology: propertyTypeToString(prop.propertyType),
        actions: (
          <div className={styles["profile-alerts__table--actions"]}>
            <Popover
              trigger="click"
              style={{
                zIndex: 10,
              }}
              placement="left"
              content={
                <div className={styles["profile-alerts__table--actions-menu"]}>
                  <PropertySimpleMenu hideShare property={prop} />
                </div>
              }
            >
              <button>
                <MoreOutlined />
              </button>
            </Popover>
          </div>
        ),
        currentPrice: prop,
        previousPrice: prop,
      };
    });

  return (
    <div className={styles["profile-alerts"]}>
      <div className={styles["profile-alerts__header"]}>
        <h5>Mis Alertas</h5>
        <p>
          En esta seccion encontraras todas las propiedades que has seleccionado
          para recibir emails en caso que su precio se modifique. Se el primero
          en enterarte!
        </p>
      </div>
      <div className={styles["profile-alerts__action"]}>
        <h6 className={styles["profile-alerts__action--title"]}>
          ALERTAS CREADAS
        </h6>
        <SortPopup setLocalSort={setSort}>
          <h6 className={styles["profile-alerts__action--sort"]}>
            Acciones <DownOutlined />
          </h6>
        </SortPopup>
      </div>
      <div className={styles["profile-alerts__alerts-properties"]}>
        <Table
          showSorterTooltip={false}
          className="property-details-developments-tables__table"
          columns={columns}
          dataSource={tableData}
          pagination={false}
        />
      </div>
      <div>
        <Pagination
          onChange={(page) => {
            setPage(page);
          }}
          className={styles["profile-alerts__pagination"]}
          defaultCurrent={1}
          defaultPageSize={PAGE_SIZE}
          total={alertsProperties.length}
        />
      </div>
    </div>
  );
};

export default ProfileAlerts;
