import React, { useState } from "react";
import classnames from "classnames";
import { useTransition, animated } from "react-spring";
import Link from "next/link";
import { CloseOutlined, MoreOutlined } from "@ant-design/icons";

import { ProfilePropertyCardMobileProps } from "./types";
import NoImages from "../../../assets/images/no-images.jpg";
import { getFullAddress } from "../../../utils/getFullAddress";
import { useGeneratePriceString } from "../../../hooks/useGeneratePriceString";
import { buildUrlForProperty } from "../../../utils/buildUrlForProperty";
import PropertySimpleMenu from "../../PropertySimpleMenu";

import styles from "./ProfilePropertyCardMobile.module.scss";

const ProfilePropertyCardMobile: React.FC<ProfilePropertyCardMobileProps> = ({
  className,
  property,
}) => {
  const { photos, _id } = property;

  const [toggle, setToggle] = useState(true);
  const transitions = useTransition(toggle, null, {
    from: { position: "absolute", opacity: 0, width: "100%" },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
  });

  return (
    <div
      className={classnames(styles["profile-property-card-mobile"], className)}
      key={`profile-property-card-mobile-${_id}`}
    >
      <div className={styles["profile-property-card-mobile__image"]}>
        <Link href={buildUrlForProperty(property)}>
          <a>
            <img src={photos?.[0]?.thumb || NoImages} />
          </a>
        </Link>
      </div>
      <div className={styles["profile-property-card-mobile__container"]}>
        {transitions.map(({ item, props }) =>
          item ? (
            <animated.div style={props} key={`${_id}-info`}>
              <div className={styles["profile-property-card-mobile__info"]}>
                <div>
                  <p
                    className={
                      styles["profile-property-card-mobile__info--address"]
                    }
                  >
                    {getFullAddress(property)}
                  </p>
                  <p
                    className={
                      styles["profile-property-card-mobile__info--price"]
                    }
                  >
                    {useGeneratePriceString(property, "Venta")}
                  </p>
                </div>
                <div
                  className={
                    styles["profile-property-card-mobile__info--actions"]
                  }
                >
                  <button
                    className={styles["profile-property-card-mobile__button"]}
                    onClick={() => setToggle(false)}
                  >
                    <MoreOutlined />
                  </button>
                </div>
              </div>
            </animated.div>
          ) : (
            <animated.div style={props} key={`${_id}-actions`}>
              <div className={styles["profile-property-card-mobile__actions"]}>
                <PropertySimpleMenu property={property} hideDetails />
                <button
                  className={styles["profile-property-card-mobile__button"]}
                  onClick={() => setToggle(true)}
                >
                  <CloseOutlined />
                </button>
              </div>
            </animated.div>
          )
        )}
      </div>
    </div>
  );
};

export default ProfilePropertyCardMobile;
