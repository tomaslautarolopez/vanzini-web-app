import { Property } from "../../../interfaces/properties";

export type ProfilePropertyCardMobileProps = {
  property: Property;
  className?: string;
};
