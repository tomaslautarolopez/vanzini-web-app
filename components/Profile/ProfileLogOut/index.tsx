import React, { useContext } from "react";

import { ProfileLogOutProps } from "./types";
import { ModalContext } from "../../../contexts/ModalContext";

import styles from "./ProfileLogOut.module.scss";

const ProfileLogOut: React.FC<ProfileLogOutProps> = () => {
  const { setModal } = useContext(ModalContext);

  return (
    <div className={styles["profile-log-out"]}>
      <div className={styles["profile-newsletter__title"]}>
        <h5>Cerrar sesión </h5>
      </div>
      <h6>
        Si cerras tu sesión podes volver a iniciar sesion en cualquier momento.
      </h6>
      <button
        className="red-custom-button"
        onClick={() => {
          setModal("logOutModal", true);
        }}
      >
        Cerrar sesión
      </button>
    </div>
  );
};

export default ProfileLogOut;
