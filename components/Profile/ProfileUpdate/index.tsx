import React, { useContext } from "react";
import { Form, Input, Button } from "antd";

import { ProfileUpdateProps } from "./types";
import { useUpdateUser } from "../../../hooks/useUpdateUser";

import styles from "./ProfileUpdate.module.scss";
import { UserContext } from "../../../contexts/UserContext";

const ProfileUpdate: React.FC<ProfileUpdateProps> = () => {
  const {
    loading: { info },
    updateUserInfo,
  } = useUpdateUser();
  const {
    user: { data },
  } = useContext(UserContext);

  return (
    <div className={styles["profile-update"]}>
      <div className={styles["profile-update__header"]}>
        <h5>Mi Perfil</h5>
        <p>Todos los datos personales se encuentran en esta seccion.</p>
      </div>
      <div className={styles["profile-update__form"]}>
        <div className={styles["profile-update__form--title"]}>
          <h5>Datos personales</h5>
        </div>
        <Form
          layout="vertical"
          onFinish={(form) => updateUserInfo({ name: form.name })}
        >
          <Form.Item
            name="name"
            label="NOMBRE"
            rules={[
              {
                required: true,
                message: "Por favor, ingrese su nombre completo.",
              },
            ]}
          >
            <Input
              placeholder={data?.name}
              style={{ width: "100%", height: "32px" }}
            />
          </Form.Item>
          <Button
            type="ghost"
            size="small"
            htmlType="submit"
            className={styles["profile-update__form--submit"]}
            loading={info}
          >
            Guardar cambios
          </Button>
        </Form>
      </div>
    </div>
  );
};

export default ProfileUpdate;
