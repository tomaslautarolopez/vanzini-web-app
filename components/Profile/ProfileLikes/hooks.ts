import { useEffect, useState } from "react";
import { Sort } from "../../../contexts/FiltersContext";
import { fetchProperties } from "../../../data-fetch/fetchProperties";
import { Property } from "../../../interfaces/properties";

export const useProfileLikes = (likes: Set<string> | undefined) => {
  const [likedProperties, setLikedProperties] = useState<Property[]>([]);
  const [sort, setSort] = useState<Sort | undefined>();
  const [loading, setLoading] = useState(false);

  const getLikedProperties = async () => {
    setLoading(true);
    try {
      const { properties } = await fetchProperties(0, undefined, {
        id: likes && [...likes],
        sort,
      });

      setLikedProperties(properties);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  };

  useEffect(() => {
    if (likes && likes.size > 0) {
      getLikedProperties();
    } else {
      setLikedProperties([])
    }
  }, [likes, sort]);

  return {
    likedProperties,
    setSort,
    loading,
  };
};
