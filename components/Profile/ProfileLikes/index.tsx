import React, { useContext, useState } from "react";
import { Button } from "antd";
import { DownOutlined } from "@ant-design/icons";

import { ProfileLikesProps } from "./types";
import { useProfileLikes } from "./hooks";
import { UserContext } from "../../../contexts/UserContext";
import SortPopup from "../../SortPopup";
import ProfilePropertyCard from "../ProfilePropertyCard";

import styles from "./ProfileLikes.module.scss";

const ProfileLikes: React.FC<ProfileLikesProps> = () => {
  const {
    user: { data },
  } = useContext(UserContext);
  const { likedProperties, setSort } = useProfileLikes(data?.likes);
  const [showProperties, setShowProperties] = useState(5);

  return (
    <div className={styles["profile-likes"]}>
      <div className={styles["profile-likes__header"]}>
        <h5>Mis Favoritos</h5>
        <p>
          En esta seccion se guardaran todas las propiedades que mas te han
          gustado, no te olvides de poner una alerta por suba o baja de precio
          para ser el primero en enterarte.
        </p>
      </div>
      <div className={styles["profile-likes__action"]}>
        <h6 className={styles["profile-likes__action--title"]}>GUARDADOS</h6>
        <SortPopup setLocalSort={setSort}>
          <h6 className={styles["profile-likes__action--sort"]}>
            Acciones <DownOutlined />
          </h6>
        </SortPopup>
      </div>
      <div className={styles["profile-likes__liked-properties"]}>
        {likedProperties.slice(0, showProperties).map((property) => (
          <ProfilePropertyCard
            key={`profile-likes__liked-properties--property-card-${property._id}`}
            className={styles["profile-likes__liked-properties--property-card"]}
            property={property}
          />
        ))}
        {showProperties < likedProperties.length && (
          <Button
            type="ghost"
            size="small"
            className={styles["profile-likes__liked-properties--show-more"]}
            onClick={() => setShowProperties(showProperties + 5)}
          >
            Ver más
          </Button>
        )}
      </div>
    </div>
  );
};

export default ProfileLikes;
