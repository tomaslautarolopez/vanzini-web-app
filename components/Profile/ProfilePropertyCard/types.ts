import { Property } from "../../../interfaces/properties";

export type ProfilePropertyCardProps = {
  property: Property;
  className?: string;
};
