import React from "react";

import { ProfilePropertyCardProps } from "./types";
import ProfilePropertyCardDesktop from "../ProfilePropertyCardDesktop";
import { isMobileWeb } from "../../../utils/isMobileWeb";
import ProfilePropertyCardMobile from "../ProfilePropertyCardMobile";

const ProfilePropertyCard: React.FC<ProfilePropertyCardProps> = ({
  property,
  className,
}) => {
  return isMobileWeb() ? (
    <ProfilePropertyCardMobile
      property={property}
      className={className}
    />
  ) : (
    <ProfilePropertyCardDesktop
      property={property}
      className={className}
    />
  );
};

export default ProfilePropertyCard;
