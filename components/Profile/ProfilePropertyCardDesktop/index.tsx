import React, { useContext } from "react";
import classnames from "classnames";
import { Popconfirm } from "antd";
import {
  BellFilled,
  BellOutlined,
  CloseOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import Link from "next/link";

import NoImages from "../../../assets/images/no-images.jpg";
import { propertyTypeToString } from "../../../utils/propertyTypeToString";
import { getFullAddress } from "../../../utils/getFullAddress";
import { useGeneratePriceString } from "../../../hooks/useGeneratePriceString";
import { useLikeProperty } from "../../../hooks/useLikeProperty";
import { ModalContext } from "../../../contexts/ModalContext";
import Consult from "../../../assets/icons/consult/send.svg";
import { ProfilePropertyCardDesktopProps } from "./types";
import { usePriceAlerts } from "../../../hooks/usePriceAlerts";
import { UserContext } from "../../../contexts/UserContext";
import { buildUrlForProperty } from "../../../utils/buildUrlForProperty";

import styles from "./ProfilePropertyCardDesktop.module.scss";
import { numberWithDotsAndCommas } from "../../../utils/numberWithDotsAndCommas";

const ProfilePropertyCardDesktop: React.FC<ProfilePropertyCardDesktopProps> = ({
  property,
  className,
}) => {
  const { setModal, setConsultModalData, setAlertModalData } = useContext(
    ModalContext
  );
  const [visible, setVisible] = React.useState(false);

  const {
    user: { data },
  } = useContext(UserContext);
  const alertSetted = data?.alerts.has(property._id);

  const { photos, propertyType, totalSurfaceMax, _id } = property;
  const { dislikeProperty, loading: likeLoading } = useLikeProperty();
  const { removeUserPriceAlert, loading: alertLoading } = usePriceAlerts();

  return (
    <div
      className={classnames(styles["profile-property-card-desktop"], className)}
    >
      <div className={styles["profile-property-card-desktop__image"]}>
        <Link href={buildUrlForProperty(property)}>
          <a>
            <img src={photos?.[0]?.thumb || NoImages} />
          </a>
        </Link>
      </div>
      <div className={styles["profile-property-card-desktop__info"]}>
        <div>
          <small
            className={
              styles["profile-property-card-desktop__info--property-type"]
            }
          >
            {propertyTypeToString(propertyType)}
          </small>
          <Link href={buildUrlForProperty(property)}>
            <a>
              <h5>{property.publicationTitle}</h5>
            </a>
          </Link>
          <h6
            className={styles["profile-property-card-desktop__info--address"]}
          >
            {getFullAddress(property)}
          </h6>
          <h6
            className={
              styles["profile-property-card-desktop__info--price-and-surface"]
            }
          >
            {numberWithDotsAndCommas(totalSurfaceMax)}m² | {useGeneratePriceString(property, "Venta")}
          </h6>
        </div>
        <div className={styles["profile-property-card-desktop__info--actions"]}>
          <button
            onClick={() => {
              if (alertSetted) {
                removeUserPriceAlert(property._id);
              } else {
                setModal("alertModal", true);
                setAlertModalData({ propertyId: property._id });
              }
            }}
            className={
              styles["profile-property-card-desktop__info--actions---bell"]
            }
          >
            {alertLoading ? (
              <LoadingOutlined />
            ) : alertSetted ? (
              <BellFilled />
            ) : (
              <BellOutlined />
            )}
          </button>
          <Popconfirm
            title="Estas seguro que quieres eliminar esta propiedad de tus guardados?"
            onConfirm={() => dislikeProperty(_id, () => setVisible(false))}
            okText="Si"
            cancelText="No"
            placement="left"
            onCancel={() => setVisible(false)}
            okButtonProps={{ loading: likeLoading }}
            visible={visible}
          >
            <button onClick={() => setVisible(true)}>
              <CloseOutlined />
            </button>
          </Popconfirm>
          <button
            onClick={() => {
              setModal("consultModal", true);
              setConsultModalData({
                property,
                propertiesId: [
                  {
                    id: property._id,
                    propertyType: property.propertyType,
                  },
                ],
              });
            }}
          >
            <img src={Consult} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProfilePropertyCardDesktop;
