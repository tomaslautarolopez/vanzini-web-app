import { Property } from "../../../interfaces/properties";

export type ProfilePropertyCardDesktopProps = {
  property: Property;
  className?: string;
};
