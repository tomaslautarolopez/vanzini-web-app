import React, { useContext } from "react";
import { LoadingOutlined } from "@ant-design/icons";

import { ProfileNewsletterProps } from "./types";
import { useNewsletter } from "../../../hooks/useNewsletter";
import { UserContext } from "../../../contexts/UserContext";

import styles from "./ProfileNewsletter.module.scss";

const ProfileNewsletter: React.FC<ProfileNewsletterProps> = () => {
  const {
    user: { data },
  } = useContext(UserContext);

  const {
    isSuscribe,
    suscribeToNewsletter,
    unsuscribeToNewsletter,
    loading: { suscribing, unsuscribing },
  } = useNewsletter(data?.email);

  return (
    <div className={styles["profile-newsletter"]}>
      <div className={styles["profile-newsletter__title"]}>
        <h5>
          {isSuscribe ? "Desuscripción Newsletter" : "Suscripción Newsletter"}
        </h5>
      </div>
      <h6>
        {isSuscribe
          ? "Podes volverte a suscribirte cuando quieras a nuestro newsletter. Tevamos a extrañar."
          : "Queres enterarte de las últimas propieades a la venta y mejores promociones inmobiliaria. Suscribite a nuestro newsletter."}
      </h6>
      <button
        className="magenta-custom-button"
        onClick={() => {
          if (isSuscribe && data?.email) {
            unsuscribeToNewsletter(data.email);
          } else if (data?.email) {
            suscribeToNewsletter(data?.email);
          }
        }}
      >
        {isSuscribe ? "Desuscribirte" : "Suscribirte"}
        {(suscribing || unsuscribing) && <LoadingOutlined />}
      </button>
    </div>
  );
};

export default ProfileNewsletter;
