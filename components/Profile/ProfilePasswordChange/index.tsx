import React from "react";
import { Form, Input, Button } from "antd";

import { ProfilePasswordChangeProps } from "./types";

import styles from "./ProfilePasswordChange.module.scss";
import { useUpdateUser } from "../../../hooks/useUpdateUser";

const ProfilePasswordChange: React.FC<ProfilePasswordChangeProps> = () => {
  const {
    updateUserPassword,
    loading: { password },
  } = useUpdateUser();

  return (
    <div className={styles["profile-password-change"]}>
      <div className={styles["profile-password-change__title"]}>
        <h5>Cambiar constraseña</h5>
      </div>
      <Form
        layout="vertical"
        onFinish={(form) => updateUserPassword(form.password)}
      >
        <Form.Item
          label="NUEVA CONTRASEÑA"
          name="password"
          rules={[
            {
              required: true,
              message: "Debes ingresar una contraseña",
            },
          ]}
        >
          <Input.Password placeholder="*********" />
        </Form.Item>
        <Form.Item
          label="CONFIRMAR CONTRASEÑA"
          name="repassword"
          rules={[
            {
              required: true,
              message: "Debes repetir tu contraseña",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject("Las contraseñas deben coincidir");
              },
            }),
          ]}
        >
          <Input.Password placeholder="*********" />
        </Form.Item>
        <Button
          type="ghost"
          size="small"
          htmlType="submit"
          className={styles["profile-password-change__submit"]}
          loading={password}
        >
          Guardar cambios
        </Button>
      </Form>
    </div>
  );
};

export default ProfilePasswordChange;
