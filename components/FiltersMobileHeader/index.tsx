import React from "react";
import { DeleteOutlined } from "@ant-design/icons";

import { FiltersMobileHeaderProps } from "./types";
import FiltersTags from "../FiltersTags";

import styles from "./FiltersMobileHeader.module.scss";

const FiltersMobileHeader: React.FC<FiltersMobileHeaderProps> = ({
  clearFilters,
  tags,
}) => {
  return (
    <div className={styles["filters-mobile-header"]}>
      <div className={styles["filters-mobile-header__header"]}>
        <h5>Filtros Seleccionados</h5>
        <DeleteOutlined
          style={{ fontSize: 21 }}
          onClick={() => clearFilters()}
        />
      </div>
      <FiltersTags
        className={styles["filters-mobile-header__tags"]}
        tags={tags}
      />
    </div>
  );
};

export default FiltersMobileHeader;
