export type FiltersMobileHeaderProps = {
  clearFilters: () => void;
  tags: Array<{
    label: string;
    value: string | number;
    onRemove: (value: string | number) => undefined;
  }>;
};
