import { useContext, useEffect } from "react";

import { API_URL_SOCIAL_LOG_IN } from "../../axios/apiEndPoints";
import { UserContext } from "../../contexts/UserContext";
import { baseURL } from "../../axios";

export const useLogInModal = (
  setShowLogInModal: (visible: boolean) => void
): {
  onClickLogIn: (socialMedia: string) => void;
} => {
  const { logInFederation, user } = useContext(UserContext);
  const onClickLogIn = (socialMedia: string) => {
    window.open(
      `${baseURL}${API_URL_SOCIAL_LOG_IN(socialMedia)}`,
      "popup",
      "width=600,height=600"
    );
  };

  useEffect(() => {
    if (user.data) {
      setShowLogInModal(false);
    }
  }, [user.data]);

  useEffect(() => {
    const setToken = (e: MessageEvent) => {
      if (e.data.token) {
        logInFederation(e.data.token);
      }
    };

    window.addEventListener("message", setToken, false);

    return () => {
      window.removeEventListener("message", setToken);
    };
  }, []);

  return {
    onClickLogIn,
  };
};
