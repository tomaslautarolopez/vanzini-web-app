import React, { useContext, useEffect, useState } from "react";
import { Form, Input } from "antd";
import {
  FacebookFilled,
  GoogleOutlined,
  LoadingOutlined,
} from "@ant-design/icons";

import EmailLogInBg from "../../assets/images/LogIn/email-log-in-bg.jpg";
import { LogInModalProps } from "./types";
import styles from "./LogInModal.module.scss";
import ModalWithImage from "../ModalWithImage";
import LogoShield from "../../assets/icons/general/vanzini-shield.svg";
import { useLogInModal } from "./hook";
import { UserContext } from "../../contexts/UserContext";
import { ModalContext } from "../../contexts/ModalContext";

const SocialMediaLogIn: React.FC<{
  onClickLogIn: (socialMedia: string) => void;
  setEmailLogIn: (emailLogIn: boolean) => void;
  openSignUpModal: () => void;
}> = ({ onClickLogIn, setEmailLogIn, openSignUpModal }) => (
  <>
    <h3>Vivi la experiencia completa</h3>
    <p>
      Ingresando podes guardar tus propiedades favoritas en tu perfil, consultar
      por todas, crear alertas y más
    </p>
    <button
      onClick={() => onClickLogIn("google")}
      className="white-custom-button"
    >
      <GoogleOutlined style={{ fontSize: 17, marginRight: 11 }} /> Ingresar con
      google
    </button>
    <button
      onClick={() => onClickLogIn("facebook")}
      className="white-custom-button"
    >
      <FacebookFilled style={{ fontSize: 17, marginRight: 11 }} /> Ingresar con
      facebook
    </button>
    <button onClick={() => setEmailLogIn(true)} className="black-custom-button">
      CORREO ELECTRÓNICO
    </button>
    <h6 className={styles["log-in-modal__footer"]}>
      No tengo cuenta, <a onClick={() => openSignUpModal()}> Registrarme.</a>
    </h6>
  </>
);

const EmailLogIn: React.FC<{
  emailLogIn: (email: string, password: string) => Promise<void>;
  isLoginLoading: boolean;
  openSignUpModal: () => void;
  forgotPassword: () => void;
}> = ({ emailLogIn, isLoginLoading, openSignUpModal, forgotPassword }) => (
  <>
    <h3>Ingresa y segui buscando!</h3>
    <p>Accede a más funcionalidades y herramientas para buscar</p>
    <Form
      id="log-in-form"
      name="log-in-form"
      layout="vertical"
      onFinish={({ email, password }) => emailLogIn(email, password)}
    >
      <Form.Item
        name="email"
        label="EMAIL"
        rules={[
          {
            required: true,
            message: "Por favor, ingrese su email.",
          },
        ]}
      >
        <Input
          placeholder="nombre@email.com"
          style={{ width: "100%", height: "32px" }}
        />
      </Form.Item>
      <div className={styles["log-in-modal__password-container"]}>
        <a
          onClick={() => forgotPassword()}
          className={
            styles["log-in-modal__password-container--forgot-password"]
          }
        >
          Olvide mi contraseña
        </a>
        <Form.Item
          name="password"
          label="CONTRASEÑA"
          rules={[
            {
              required: true,
              message: "Por favor, ingrese su contraseña.",
            },
          ]}
        >
          <Input.Password
            className={styles["ant-input"]}
            placeholder="******"
            style={{ width: "100%", height: "32px" }}
          />
        </Form.Item>
      </div>
      <button
        style={{ marginTop: 0 }}
        className="black-custom-button"
        type="submit"
      >
        INICIAR SESIÓN {isLoginLoading && <LoadingOutlined />}
      </button>
    </Form>
    <h6 className={styles["log-in-modal__footer"]}>
      No tengo cuenta, <a onClick={() => openSignUpModal()}>Registrarme.</a>
    </h6>
  </>
);

const LogInModal: React.FC<LogInModalProps> = () => {
  const [emailLogIn, setEmailLogIn] = useState<boolean>(false);
  const {
    modals: { logInModal },
    setModal,
  } = useContext(ModalContext);

  const setShowLogInModal = (visible: boolean) =>
    setModal("logInModal", visible);

  const { onClickLogIn } = useLogInModal(setShowLogInModal);
  const {
    login,
    user: {
      loading: { isLoginLoading },
    },
  } = useContext(UserContext);

  useEffect(() => {
    if (!logInModal) {
      setTimeout(() => {
        setEmailLogIn(false);
      }, 300);
    }
  }, [logInModal]);

  return (
    <ModalWithImage
      style={{
        width: 840,
      }}
      image={EmailLogInBg}
      imageClassname={styles["log-in-modal__image"]}
      altImage="Fuente Rosario Argentina"
      visible={logInModal}
      onCancel={() => setShowLogInModal(false)}
      footer={null}
      header={
        <h6 className={styles["log-in-modal__header"]}>
          <img src={LogoShield} alt="Escudo de armas vanzini" /> INICIAR SESION
        </h6>
      }
    >
      <div className={styles["log-in-modal"]}>
        {emailLogIn ? (
          <EmailLogIn
            forgotPassword={() => setModal("forgotPasswordModal", true)}
            openSignUpModal={() => setModal("signUpModal", true)}
            emailLogIn={login}
            isLoginLoading={isLoginLoading}
          />
        ) : (
          <SocialMediaLogIn
            openSignUpModal={() => setModal("signUpModal", true)}
            setEmailLogIn={setEmailLogIn}
            onClickLogIn={onClickLogIn}
          />
        )}
      </div>
    </ModalWithImage>
  );
};

export default LogInModal;
