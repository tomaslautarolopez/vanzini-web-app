import React from "react";
import classnames from "classnames";

import { PropertyCardSkeletonProps } from "./types";

import styles from "./PropertyCardSkeleton.module.scss";

const PropertyCardSkeleton: React.FC<PropertyCardSkeletonProps> = () => {
  return (
    <div className={styles["property-card-skeleton"]}>
      <div
        className={classnames(
          styles["property-card-skeleton__carousel"],
          "property-card-skeleton-animation"
        )}
      />
      <div className={styles["property-card-skeleton__info"]}>
        <div
          className={classnames(
            styles["property-card-skeleton__info--type"],
            "property-card-skeleton-animation"
          )}
        />
        <div
          className={classnames(
            styles["property-card-skeleton__info--title"],
            "property-card-skeleton-animation"
          )}
        />
        <div
          className={classnames(
            styles["property-card-skeleton__info--address"],
            "property-card-skeleton-animation"
          )}
        />
        <div className={styles["property-card-skeleton__actions"]}>
          <div>
            <div
              className={classnames(
                styles["property-card-skeleton__actions--share"],
                "property-card-skeleton-animation"
              )}
            />
            <div
              className={classnames(
                styles["property-card-skeleton__actions--share"],
                "property-card-skeleton-animation"
              )}
            />
            <div
              className={classnames(
                styles["property-card-skeleton__actions--share"],
                "property-card-skeleton-animation"
              )}
            />
          </div>
          <div>
            <div
              className={classnames(
                styles["property-card-skeleton__actions--consult"],
                "property-card-skeleton-animation"
              )}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PropertyCardSkeleton;
