import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();
const { ENVIROMENT } = publicRuntimeConfig;

export const SOCKETS_URLs: { [key: string]: string } = {
  development: "http://localhost:3000",
  staging: "http://www.lucioxd.com",
  production: "https://www.vanzini.com.ar",
};

export const socketPATH = ENVIROMENT ? SOCKETS_URLs[ENVIROMENT] : SOCKETS_URLs.dev;
