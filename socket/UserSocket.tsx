import io from "socket.io-client";
import { notification } from "antd";
import { DollarOutlined } from "@ant-design/icons";
import {
  LowerPriceNotification,
  Notification,
  NOTIFICATIONS_TYPE,
} from "../interfaces/notifications";
import { socketPATH } from "./contants";
import { buildUrlForProperty } from "../utils/buildUrlForProperty";

export class UserSocket {
  socket: SocketIOClient.Socket;
  addNotificationToContext: (notifications: Notification) => void;
  static instance: UserSocket;

  constructor(
    token: string,
    addNotificationToContext: (notifications: Notification) => void
  ) {
    if (UserSocket.instance) {
      UserSocket.instance.destroy();
    }
    this.onNotification = this.onNotification.bind(this);

    this.socket = io(`${socketPATH}/user`, {
      path: "/sockets",
      autoConnect: true,
      query: {
        token: token,
      },
    });

    this.addNotificationToContext = addNotificationToContext;
    this.initilializeEventHanlders();

    UserSocket.instance = this;
  }

  initilializeEventHanlders() {
    this.socket.on("notification", this.onNotification);
    this.socket.on("connection", () => {});
  }

  onLowerPriceNotification(lowerPriceNotification: LowerPriceNotification) {
    const {
      information: { property },
    } = lowerPriceNotification;

    notification.success({
      message: "Una propiedad bajo de precio!",
      icon: <DollarOutlined style={{ color: "var(--color-blue-7)" }} />,

      description: (
        <>
          <b>{property.publicationTitle}</b> bajo de precio!{" "}
          <a href={buildUrlForProperty(property)} target="_blank">
            Ver ficha
          </a>
        </>
      ),
      placement: "bottomRight",
    });
  }

  onNotification(notification: Notification) {
    this.addNotificationToContext(notification);

    switch (notification.type) {
      case NOTIFICATIONS_TYPE.LOWER_PRICE:
        this.onLowerPriceNotification(notification);
    }
  }

  destroy() {
    this.socket.disconnect();
  }
}
