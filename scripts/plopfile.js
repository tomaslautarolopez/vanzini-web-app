const componentExists = require("./utils/componentExists");

module.exports = function newFile(plop) {
  plop.setGenerator("Component", {
    description: "Create a new component",
    prompts: [
      {
        type: "input",
        name: "componentName",
        message: "Enter component name: ",
        validate: (value) => {
          if (/.+/.test(value)) {
            return componentExists(value)
              ? "A component with this name already exists"
              : true;
          }

          return "The name is required";
        },
      },
    ],
    actions: [
      {
        type: "add",
        path: "../components/{{pascalCase componentName}}/index.tsx",
        templateFile: "utils/plop-templates/component-template.hbs",
      },
      {
        type: "add",
        path: `../components/{{pascalCase componentName}}/{{pascalCase componentName}}.module.scss`,
      },
      {
        type: "add",
        path: `../components/{{pascalCase componentName}}/types.ts`,
        templateFile: "utils/plop-templates/component-types-template.hbs",
      },
    ],
  });
};
