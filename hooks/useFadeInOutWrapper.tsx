import { useTransition, animated } from "react-spring";

export const useFadeInOutWrapper = (
  children: JSX.Element,
  toogle: boolean,
  className?: string
) => {
  const transitions = useTransition(toogle, null, {
    from: { position: "absolute", opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
  });

  return transitions.map(
    ({ item, key, props }) =>
      item && (
        <animated.div className={className} key={key} style={props}>
          {children}
        </animated.div>
      )
  );
};
