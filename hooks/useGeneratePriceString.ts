import { useContext } from "react";
import { OperationType } from "../contexts/FiltersContext";
import { UserContext } from "../contexts/UserContext";
import {
  DevelopmentProperty,
  Operation,
  Property,
} from "../interfaces/properties";
import { numberWithDotsAndCommas } from "../utils/numberWithDotsAndCommas";
import { validateDevelopment } from "../utils/validators";

export const useGeneratePriceFromOperation = (
  operation?: Operation,
  webPrice?: boolean
) => {
  const {
    user: { data },
  } = useContext(UserContext);

  if (!webPrice && !data) {
    return "CONSULTAR PRECIO";
  }

  if (operation?.currencyFromTokko === "USD" && operation.priceUSDmin) {
    return `U$D ${numberWithDotsAndCommas(operation.priceUSDmin)}`;
  } else if (
    operation?.currencyFromTokko === "ARS" &&
    operation.pricePesosMin
  ) {
    return `$ ${numberWithDotsAndCommas(operation.pricePesosMin)}`;
  } else {
    return "CONSULTAR PRECIO";
  }
};

export const useCreatePriceForDevelopment = (prop: Property): string => {
  const {
    user: { data },
  } = useContext(UserContext);

  const webPrice =
    prop.webPrice ||
    prop.developmentProperties.some(
      (developmentProperty) => developmentProperty.webPrice
    );

  if (webPrice || !!data) {
    return "VER PRECIOS";
  } else {
    return "CONSULTAR PRECIOS";
  }
};

export const useCreatePriceSellForProperty = (
  prop: Property | DevelopmentProperty
): string => {
  const {
    user: { data },
  } = useContext(UserContext);

  if ((prop.webPrice || !!data) && prop.sell) {
    return useGeneratePriceFromOperation(prop.operationSell, true);
  } else if (!prop.webPrice && prop.sell) {
    return "CONSULTAR PRECIO";
  } else {
    return "---";
  }
};

export const useCreatePriceRentForProperty = (
  prop: Property | DevelopmentProperty
): string => {
  const {
    user: { data },
  } = useContext(UserContext);

  if ((prop.webPrice || !!data) && prop.rent) {
    return useGeneratePriceFromOperation(prop.operationRent, true);
  } else if (!prop.webPrice && prop.rent) {
    return "CONSULTAR PRECIO";
  } else {
    return "---";
  }
};

export const useGeneratePriceString = (
  prop: Property | DevelopmentProperty,
  operationType: OperationType
) => {
  try {
    validateDevelopment(prop);

    return useCreatePriceForDevelopment(prop);
  } catch {
    if (operationType === "Venta") {
      return useCreatePriceSellForProperty(prop);
    } else if (operationType === "Alquiler") {
      return useCreatePriceRentForProperty(prop);
    }
  }
};
