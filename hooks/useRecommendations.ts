import { useEffect, useState } from "react";
import { notification } from "antd";

import { Filters } from "../contexts/FiltersContext";
import { fetchProperties } from "../data-fetch/fetchProperties";
import { Property } from "../interfaces/properties";
import { isDevelopment } from "../utils/isDevelopment";

type RecommendationType = "default" | "price" | "location" | "rooms";

const NOTIFICATION_TYPE = "Recomendaciones";

const constructFiltersFromRecommendationType = (
  recommendationType: RecommendationType,
  property: Property
): Filters => {
  const {
    operationSell: { priceUSDmin },
    roomAmount,
    secondSubDivision,
  } = property;

  switch (recommendationType) {
    case "default":
      return {
        propertyType: [property.propertyType],
        toSellPrice: priceUSDmin * 1.2,
        fromSellPrice: priceUSDmin * 0.8,
        roomAmount: isDevelopment(property.propertyType)
          ? undefined
          : [`${roomAmount}:=`],
        secondSubDivision: secondSubDivision,
        currency: "USD",
      };
    case "rooms":
      return {
        propertyType: [property.propertyType],
        secondSubDivision: secondSubDivision,
        roomAmount: [`${roomAmount}:=`],
      };
    case "price":
      return {
        propertyType: [property.propertyType],
        toSellPrice: priceUSDmin + 10000,
        fromSellPrice: priceUSDmin - 10000,
        currency: "USD",
      };
    case "location":
      return {
        propertyType: [property.propertyType],
        secondSubDivision: secondSubDivision,
      };
    default:
      return {};
  }
};

export const useRecommendations = (property: Property) => {
  const [
    typeOfrecommendation,
    setTypeOfrecommendation,
  ] = useState<RecommendationType>("default");
  const [recommendations, setRecommendations] = useState<Property[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getRecommendations();
  }, [typeOfrecommendation, property]);

  const getRecommendations = async () => {
    setLoading(true);

    try {
      const { properties } = await fetchProperties(
        undefined,
        5,
        constructFiltersFromRecommendationType(typeOfrecommendation, property)
      );
      setRecommendations(
        properties.filter((prop) => prop._id !== property._id).slice(0, 4)
      );
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "Algo falló al tratar de buscar recomendaciones. Intentelo de nuevo mas tarde",
        placement: "bottomRight",
      });
      console.log(error);
    }

    setLoading(false);
  };

  return {
    loading,
    setTypeOfrecommendation,
    typeOfrecommendation,
    recommendations,
  };
};
