import { useContext, useEffect, useState } from "react";

import { FiltersContext } from "../contexts/FiltersContext";
import { fetchProperties } from "../data-fetch/fetchProperties";
import { Property } from "../interfaces/properties";

export const useGetProperties = (
  initialProperties?: Property[],
  initialPropertiesCount?: number
): {
  getProperties: (
    offset?: number,
    limit?: number,
    fields?: string[],
    reset?: boolean
  ) => Promise<void>;
  error: any;
  propertiesCount: number | undefined;
  properties: Property[];
  hasMore: boolean;
  fetchingProperties: boolean;
} => {
  const [properties, setProperties] = useState<Property[]>(
    initialProperties || []
  );
  const [propertiesCount, setpropertiesCount] = useState<number | undefined>(
    initialPropertiesCount
  );
  const [hasMore, setHasMore] = useState(true);
  const [error, setError] = useState<any>();
  const { filters } = useContext(FiltersContext);
  const [fetchingProperties, setFetchingProperties] = useState(false);

  useEffect(() => {
    if (initialProperties) {
      setProperties(initialProperties);
    }
    if (initialPropertiesCount) {
      setpropertiesCount(initialPropertiesCount);
    }

    setHasMore(true);
  }, [initialProperties, initialPropertiesCount]);

  const getProperties = async (
    offset?: number,
    limit?: number,
    fields?: string[],
    reset?: boolean
  ) => {
    setFetchingProperties(true);
    try {
      const { properties: newProperties, propertiesCount: newPropertiesCount } =
        await fetchProperties(offset, limit, filters, fields);

      setProperties(reset ? newProperties : [...properties, ...newProperties]);
      setpropertiesCount(newPropertiesCount);

      if (newProperties.length === 0) {
        setHasMore(false);
      }
    } catch (error) {
      setError(error);
      console.warn(error);
    }
    setFetchingProperties(false);
  };

  return {
    getProperties,
    error,
    propertiesCount,
    properties,
    hasMore,
    fetchingProperties,
  };
};
