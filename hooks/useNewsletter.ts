import { notification } from "antd";
import { useEffect, useState } from "react";

import { axiosVanziniApiInstance } from "../axios";
import {
  API_URL_SUSCRIBE_TO_NEWSLETTER,
  API_URL_UNSUSCRIBE_TO_NEWSLETTER,
  API_URL_CHECK_SUSCRIPTION_TO_NEWSLETTER,
} from "../axios/apiEndPoints";

const NOTIFICATION_TYPE = "Newsletter";

export const useNewsletter = (email?: string) => {
  const [isSuscribe, setIsSuscribe] = useState(false);
  const [loading, setLoading] = useState<{
    suscribing: boolean;
    unsuscribing: boolean;
    gettingSuscription: boolean;
  }>({
    suscribing: false,
    unsuscribing: false,
    gettingSuscription: false,
  });

  useEffect(() => {
    if (email) {
      checkNewsletterSuscription();
    }
  }, [email]);

  const checkNewsletterSuscription = async () => {
    setLoading({
      ...loading,
      gettingSuscription: true,
    });
    try {
      const {
        data: { payload },
      } = await axiosVanziniApiInstance.get(
        API_URL_CHECK_SUSCRIPTION_TO_NEWSLETTER,
        {
          params: {
            email,
          },
        }
      );

      setIsSuscribe(payload);
    } catch (error) {
      console.log(error);
    }
    setLoading({
      ...loading,
      gettingSuscription: false,
    });
  };

  const suscribeToNewsletter = async (
    email: string,
    callback?: Function
  ): Promise<void> => {
    setLoading({
      ...loading,
      suscribing: true,
    });
    try {
      await axiosVanziniApiInstance.post(API_URL_SUSCRIBE_TO_NEWSLETTER, {
        email,
      });

      window.location.href = "/gracias-por-su-suscripcion"
      callback?.();
      setIsSuscribe(true);
      notification.success({
        message: NOTIFICATION_TYPE,
        description: "Te suscribiste con éxtio al Newsletter",
        placement: "bottomRight",
      });
    } catch (error) {
      console.log(error);
      notification.error({
        message: NOTIFICATION_TYPE,
        description: "No pudimos suscribirlo al Newsletter",
        placement: "bottomRight",
      });
    }
    setLoading({
      ...loading,
      suscribing: false,
    });
  };

  const unsuscribeToNewsletter = async (
    email: string,
    callback?: Function
  ): Promise<void> => {
    setLoading({
      ...loading,
      suscribing: true,
    });
    try {
      await axiosVanziniApiInstance.put(API_URL_UNSUSCRIBE_TO_NEWSLETTER, {
        email,
      });

      callback?.();
      setIsSuscribe(false);
      notification.success({
        message: NOTIFICATION_TYPE,
        description: "Te dessuscribiste con éxtio del Newsletter",
        placement: "bottomRight",
      });
    } catch (error) {
      console.log(error);
      notification.error({
        message: NOTIFICATION_TYPE,
        description: "No pudimos dessuscribirlo del Newsletter",
        placement: "bottomRight",
      });
    }
    setLoading({
      ...loading,
      suscribing: false,
    });
  };

  return {
    loading: loading,
    suscribeToNewsletter,
    unsuscribeToNewsletter,
    isSuscribe,
  };
};
