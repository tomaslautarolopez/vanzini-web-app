import { useEffect } from "react";
import useSWR from "swr";

import { fetchPropertiesLists } from "../data-fetch/fetchPropertiesLists";
import { Property } from "../interfaces/properties";

export interface PropertiesList {
  createAt: number;
  createdBy: string;
  description: string;
  list: Property[];
  portrait: string;
  score: number;
  showInWeb: boolean;
  slug: string;
  title: string;
  updateAt: number;
  properties?: Property[];
  _id: string;
}

export const SWR_KEY_PROPERTIES_LIST_FOR_FEED =
  "SWR_KEY_PROPERTIES_LIST_FOR_FEED";

const fetcher = async (): Promise<PropertiesList[] | undefined> => {
  try {
    const propertiesList = await fetchPropertiesLists();

    return propertiesList;
  } catch (error) {
    console.error(error);
  }
};

export const useGetPropertiesListForFeed = () => {
  const { data, error, mutate } = useSWR<PropertiesList[] | undefined>(
    [SWR_KEY_PROPERTIES_LIST_FOR_FEED],
    fetcher,
    {
      revalidateOnMount: false,
    }
  );

  useEffect(() => {
    if (!data && !error) {
      mutate();
    }
  }, [data, mutate, error]);

  return { data, error, mutate };
};
