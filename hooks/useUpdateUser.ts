import { notification } from "antd";
import { useContext, useState } from "react";

import { axiosVanziniApiInstance } from "../axios";
import {
  API_URL_UPDATE_USER,
  API_URL_CHANGE_USER_PASSWORD,
} from "../axios/apiEndPoints";
import { UserContext } from "../contexts/UserContext";

export interface UserForm {
  name: string;
}

const NOTIFICATION_TYPE = "Usuario";

export const useUpdateUser = () => {
  const {
    user: { data },
    setUserData,
  } = useContext(UserContext);
  const [loading, setLoading] = useState<{
    info: boolean;
    password: boolean;
  }>({
    info: false,
    password: false,
  });

  const updateUserInfo = async (form: UserForm) => {
    setLoading({
      ...loading,
      info: true,
    });
    try {
      if (data) {
        await axiosVanziniApiInstance.put(API_URL_UPDATE_USER, form);

        notification.success({
          message: NOTIFICATION_TYPE,
          description: "Actualizamos su información personal con éxito",
          placement: "bottomRight",
        });
        setUserData({ ...data, ...form });
      }
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "No pudimos ractualizar su información personal, intentelo de nuevo mas tarde",
        placement: "bottomRight",
      });
      console.log(error);
    }
    setLoading({
      ...loading,
      info: false,
    });
  };

  const updateUserPassword = async (newPassword: string) => {
    setLoading({
      ...loading,
      password: true,
    });
    try {
      if (data) {
        await axiosVanziniApiInstance.put(API_URL_CHANGE_USER_PASSWORD, {
          newPassword,
        });

        notification.success({
          message: NOTIFICATION_TYPE,
          description: "Cambiamos su contrseña con éxito",
          placement: "bottomRight",
        });
      }
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "No pudimos cambiar su contraseña, intentelo de nuevo mas tarde",
        placement: "bottomRight",
      });
      console.log(error);
    }
    setLoading({
      ...loading,
      password: false,
    });
  };

  return {
    updateUserInfo,
    updateUserPassword,
    loading,
  };
};
