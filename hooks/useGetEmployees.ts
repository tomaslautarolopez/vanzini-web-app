import { useEffect } from "react";
import useSWR from "swr";

import { axiosVanziniApiInstance } from "../axios";
import { API_URL_EMPLOYEES_IN_WEB } from "../axios/apiEndPoints";
import { Employee } from "../interfaces/employee";

export const SWR_KEY_EMPLOYEES = "SWR_KEY_EMPLOYEES";

const fetcher = async (): Promise<Employee[] | undefined> => {
  try {
    const {
      data: { payload: employees },
    } = await axiosVanziniApiInstance.get(API_URL_EMPLOYEES_IN_WEB);

    return employees;
  } catch (error) {
    console.error(error);
  }
};

export const useGetEmployees = () => {
  const { data, error, mutate } = useSWR<Employee[] | undefined>(
    [SWR_KEY_EMPLOYEES],
    fetcher,
    {
      revalidateOnMount: false,
    }
  );

  useEffect(() => {
    if (!data && !error) {
      mutate();
    }
  }, [data, mutate, error]);

  return { data, error, mutate };
};
