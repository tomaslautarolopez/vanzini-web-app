import { useContext, useState, useRef, useEffect, useCallback } from "react";
import { axiosVanziniApiInstance } from "../axios";
import { API_URL_GET_HIGHLIGHT_PROPERTY } from "../axios/apiEndPoints";
import { Filters, FiltersContext } from "../contexts/FiltersContext";
import { Property } from "../interfaces/properties";

export type HighlightProperty = Property & {
  highlightDescription: string;
  highlightTitle: string;
};

export const useGetHighlightProperties = (
  amount: number
): {
  getHighlightProperties: (filtersToAply?: Filters) => Promise<void>;
  highligthProperties: HighlightProperty[];
} => {
  const { filters } = useContext(FiltersContext);
  const [highligthProperties, setHighligthProperties] = useState<
    HighlightProperty[]
  >([]);
  const alreadySeenProperties = useRef<string[]>([]);

  useEffect(() => {
    alreadySeenProperties.current = [];
    setHighligthProperties([]);
    getHighlightProperties(filters);
  }, [filters]);

  const getHighlightProperties = useCallback(
    async (filtersToAply?: Filters) => {
      let newHighlights: HighlightProperty[] = [];

      for (let index = 0; index < amount; index++) {
        const newHighlight = await getOneHighlightProperties(filtersToAply);
        if (newHighlight) {
          newHighlights = [...newHighlights, newHighlight];
        }
      }

      if (!filtersToAply) {
        newHighlights = newHighlights.filter(
          (newHighlight) =>
            !highligthProperties.find(
              (highligthProperty) => highligthProperty._id === newHighlight._id
            )
        );
      }

      setHighligthProperties([
        ...(filtersToAply ? [] : highligthProperties),
        ...newHighlights,
      ]);
    },
    [highligthProperties]
  );

  const getOneHighlightProperties = async (filtersToAply?: Filters): Promise<
    HighlightProperty | undefined
  > => {
    try {
      const {
        data: { payload: property },
      }: {
        data: { payload: HighlightProperty };
      } = await axiosVanziniApiInstance.get(API_URL_GET_HIGHLIGHT_PROPERTY, {
        params: {
          ...(filtersToAply || filters),
          alreadyTakeIds: alreadySeenProperties.current,
        },
      });

      if (property) {
        alreadySeenProperties.current.push(property._id);

        return property;
      }
    } catch (error) {
      console.log(error);
    }
  };

  return {
    getHighlightProperties,
    highligthProperties,
  };
};
