import { useState } from "react";
import { notification } from "antd";

import { axiosVanziniApiInstance } from "../axios";
import {
  API_URL_CONSULT_GENERAL,
  API_URL_CONSULT_PROPERTY,
  API_URL_CONSULT_DEVELOPMENT,
  API_URL_CONSULT_ASSES,
  API_URL_CONSULT_FAVS,
} from "../axios/apiEndPoints";

export interface ConsultForm {
  consultText: string;
  email: string;
  name: string;
  phonenumber: string;
  propertyId?: string[];
}

const NOTIFICATION_TYPE = "Consulta";

export type ConsultType =
  | "GENERAL"
  | "PROPERTY"
  | "DEVELOPMENT"
  | "ASSES"
  | "FAV";

const generateConsultUrl = (consultType: ConsultType) => {
  switch (consultType) {
    case "GENERAL":
      return API_URL_CONSULT_GENERAL;
    case "PROPERTY":
      return API_URL_CONSULT_PROPERTY;
    case "DEVELOPMENT":
      return API_URL_CONSULT_DEVELOPMENT;
    case "ASSES":
      return API_URL_CONSULT_ASSES;
    case "FAV":
      return API_URL_CONSULT_FAVS;
    default:
      return "";
  }
};

const generateRedirecttUrl = (consultType: ConsultType) => {
  switch (consultType) {
    case "ASSES":
      return "/gracias-por-su-tasacion";
    default:
      return "/gracias-por-su-consulta";
  }
};

export const useConsult = (): {
  loading: boolean;
  makeConsult: (
    consult: ConsultForm,
    consultType: ConsultType,
    callback?: Function
  ) => Promise<void>;
} => {
  const [loading, setLoading] = useState(false);

  const makeConsult = async (
    consult: ConsultForm,
    consultType: ConsultType,
    callback?: Function
  ) => {
    setLoading(true);
    try {
      await axiosVanziniApiInstance.put(
        generateConsultUrl(consultType),
        consult
      );

      notification.success({
        message: NOTIFICATION_TYPE,
        description: "Realizamos su consulta con éxito",
        placement: "bottomRight",
      });

      window.location.href = generateRedirecttUrl(consultType);
      callback?.();
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "No pudimos realizar su consulta, intentelo de nuevo mas tarde",
        placement: "bottomRight",
      });
      console.log(error);
    }
    setLoading(false);
  };

  return { loading, makeConsult };
};
