import { useState } from "react";

import { axiosVanziniApiInstance } from "../axios";
import {
  API_URL_RECOVER_PASSWORD,
  API_URL_RESET_PASSWORD,
} from "../axios/apiEndPoints";

export const usePasswordRecovery = () => {
  const [loading, setLoading] = useState<{
    recovering: boolean;
    resetting: boolean;
  }>({
    recovering: false,
    resetting: false,
  });
  const [success, setSuccess] = useState<{
    recovering: boolean;
    resetting: boolean;
  }>({
    recovering: false,
    resetting: false,
  });

  const resetState = () => {
    setSuccess({
      recovering: false,
      resetting: false,
    });
  };

  const recoverPassword = async (email: string) => {
    setLoading({
      ...loading,
      recovering: true,
    });
    try {
      await axiosVanziniApiInstance.post(API_URL_RECOVER_PASSWORD, {
        email,
      });

      setSuccess({
        ...success,
        recovering: true,
      });
    } catch (error) {
      console.log(error);

      setSuccess({
        ...success,
        recovering: false,
      });
    }
    setLoading({
      ...loading,
      recovering: false,
    });
  };

  const resetPassword = async (
    newPassword: string,
    token: string,
    callback?: Function
  ) => {
    setLoading({
      ...loading,
      resetting: true,
    });
    try {
      await axiosVanziniApiInstance.post(
        API_URL_RESET_PASSWORD,
        {
          newPassword,
        },
        {
          headers: {
            Authorization: `BEARER ${token}`,
          },
        }
      );

      setSuccess({
        ...success,
        resetting: true,
      });
      callback?.();
    } catch (error) {
      console.log(error);

      setSuccess({
        ...success,
        resetting: false,
      });
    }
    setLoading({
      ...loading,
      resetting: false,
    });
  };

  return {
    recoverPassword,
    resetPassword,
    success,
    loading,
    resetState,
  };
};
