import { useEffect } from "react";
import useSWR from "swr";

import { axiosVanziniApiInstance } from "../axios";
import { API_URL_EMPLOYEE_ON_GUARD } from "../axios/apiEndPoints";
import { Employee, Location } from "../interfaces/employee";

export const SWR_KEY_ON_GUARD_EMPLOYEE = "SWR_KEY_ON_GUARD_EMPLOYEE";

const fetcher = async (
  _: any,
  location: Location
): Promise<Employee | undefined> => {
  try {
    const {
      data: { payload: onGuard },
    } = await axiosVanziniApiInstance.get<{ payload: Employee }>(
      `${API_URL_EMPLOYEE_ON_GUARD}/${location}`
    );

    return onGuard;
  } catch (error) {
    console.error(error);
  }
};

export const useOnGuardEmployee = (location: Location) => {
  const { data, error, mutate } = useSWR<Employee | undefined>(
    [SWR_KEY_ON_GUARD_EMPLOYEE, location],
    fetcher,
    {
      revalidateOnMount: false,
    }
  );

  useEffect(() => {
    if (!data && !error) {
      mutate();
    }
  }, [data, mutate, error]);

  return { data, error, mutate };
};
