import { useContext, useState } from "react";
import { notification } from "antd";

import { axiosVanziniApiInstance } from "../axios";
import {
  API_URL_PRICE_ALERT_EMAIL,
  API_URL_PRICE_ALERT_USER,
} from "../axios/apiEndPoints";
import { UserContext } from "../contexts/UserContext";
import { BellFilled, BellOutlined } from "@ant-design/icons";

export const NOTIFICATION_TYPE = "Alertas";

export const usePriceAlerts = (): {
  createPriceAlert: (
    propertyId: string,
    email: string,
    callback?: Function
  ) => Promise<void>;
  removeEmailPriceAlert: (
    token: string,
    propertyId: string,
    callback?: Function
  ) => Promise<void>;
  removeUserPriceAlert: (
    propertyId: string,
    callback?: Function
  ) => Promise<void>;
  loading: boolean;
} => {
  const {
    user: { data },
    setUserData,
  } = useContext(UserContext);
  const [loading, setLoading] = useState(false);

  const removeEmailPriceAlert = async (
    token: string,
    propertyId: string,
    callback?: Function
  ) => {
    setLoading(true);
    try {
      await axiosVanziniApiInstance.delete(
        `${API_URL_PRICE_ALERT_EMAIL}/${propertyId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      callback?.();

      notification.success({
        message: NOTIFICATION_TYPE,
        description: "Alerta removida con éxito",
        placement: "bottomRight",
        icon: <BellOutlined style={{ color: "var(--color-blue-7)" }} />,
        duration: 2,
      });
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "No pudimos eliminar el alerta de precio para esta propiedad, intentelo de nuevo mas tarde",
        placement: "bottomRight",
      });
      console.log(error);
    }
    setLoading(false);
  };

  const removeUserPriceAlert = async (
    propertyId: string,
    callback?: Function
  ) => {
    setLoading(true);
    try {
      if (data) {
        await axiosVanziniApiInstance.delete(
          `${API_URL_PRICE_ALERT_USER}/${propertyId}`
        );

        const newAlerts = new Set(data.alerts);
        newAlerts.delete(propertyId);

        setUserData({ ...data, alerts: newAlerts });
        callback?.();

        notification.success({
          message: NOTIFICATION_TYPE,
          description: "Alerta removida con éxito",
          placement: "bottomRight",
          icon: <BellOutlined style={{ color: "var(--color-blue-7)" }} />,
          duration: 2,
        });
      }
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "No pudimos eliminar el alerta de precio para esta propiedad, intentelo de nuevo mas tarde",
        placement: "bottomRight",
      });
      console.log(error);
    }
    setLoading(false);
  };

  const createPriceAlert = async (
    propertyId: string,
    email: string,
    callback?: Function
  ) => {
    setLoading(true);
    try {
      await axiosVanziniApiInstance.post(API_URL_PRICE_ALERT_EMAIL, {
        propertyId,
        email,
      });

      if (data) {
        await axiosVanziniApiInstance.post(API_URL_PRICE_ALERT_USER, {
          propertyId,
        });

        const newAlerts = new Set(data.alerts);
        newAlerts.add(propertyId);

        setUserData({ ...data, alerts: newAlerts });
      }

      callback?.();

      notification.success({
        message: NOTIFICATION_TYPE,
        description: "Alerta creada con éxito",
        placement: "bottomRight",
        icon: <BellFilled style={{ color: "var(--color-blue-7)" }} />,
        duration: 2,
      });
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "No pudimos crear una alerta de precio para esta propiedad, intentelo de nuevo mas tarde",
        placement: "bottomRight",
      });
      console.log(error);
    }
    setLoading(false);
  };

  return {
    createPriceAlert,
    removeUserPriceAlert,
    removeEmailPriceAlert,
    loading,
  };
};
