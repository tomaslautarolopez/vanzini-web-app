import { useContext, useState } from "react";
import { notification } from "antd";

import { axiosVanziniApiInstance } from "../axios";
import {
  API_URL_LIKE_PROPERTY,
  API_URL_DISLIKE_PROPERTY,
} from "../axios/apiEndPoints";
import { ModalContext } from "../contexts/ModalContext";
import { UserContext } from "../contexts/UserContext";
import { HeartFilled, HeartOutlined } from "@ant-design/icons";

export const NOTIFICATION_TYPE = "Propiedades";

export const useLikeProperty = (): {
  likeProperty: (propertyId: string, callback?: Function) => Promise<void>;
  dislikeProperty: (propertyId: string, callback?: Function) => Promise<void>;
  loading: boolean;
} => {
  const {
    user: { data },
    setUserData,
  } = useContext(UserContext);
  const { setModal } = useContext(ModalContext);
  const [loading, setLoading] = useState(false);

  const likeProperty = async (propertyId: string, callback?: Function) => {
    if (data) {
      setLoading(true);
      try {
        await axiosVanziniApiInstance.put(API_URL_LIKE_PROPERTY, {
          propertyId,
        });

        const newLikes = new Set(data.likes);
        newLikes.add(propertyId);

        setUserData({ ...data, likes: newLikes });
        notification.success({
          message: NOTIFICATION_TYPE,
          description: "Guardada en favoritos éxito",
          placement: "bottomRight",
          icon: <HeartFilled style={{ color: "var(--color-magenta-7)" }} />,
          duration: 2,
        });

        callback?.();
      } catch (error) {
        notification.error({
          message: NOTIFICATION_TYPE,
          description:
            "No pudimos guardar esta propiedad, intentelo de nuevo mas tarde",
          placement: "bottomRight",
        });
        console.log(error);
      }
      setLoading(false);
    } else {
      setModal("logInModal", true);
    }
  };

  const dislikeProperty = async (propertyId: string, callback?: Function) => {
    if (data) {
      setLoading(true);
      try {
        await axiosVanziniApiInstance.put(API_URL_DISLIKE_PROPERTY, {
          propertyId,
        });

        const newLikes = new Set(data.likes);
        newLikes.delete(propertyId);

        setUserData({ ...data, likes: newLikes });
        callback?.();
        notification.success({
          message: NOTIFICATION_TYPE,
          description: "Eliminada de favoritos",
          placement: "bottomRight",
          icon: <HeartOutlined style={{ color: "var(--color-magenta-7)" }} />,
          duration: 2,
        });
      } catch (error) {
        console.log(error);
      }
      setLoading(false);
    } else {
      setModal("logInModal", true);
    }
  };

  return { likeProperty, dislikeProperty, loading };
};
