import { useState, useEffect } from "react";
import { fetchProperties } from "../data-fetch/fetchProperties";
import { Property } from "../interfaces/properties";

export const useGetPropertyAddress = (ids: string[]): string[] => {
  const [addresses, setAddresses] = useState<string[]>([]);

  const getAddresses = async () => {
    try {
      if (ids.length) {
        const { properties } = await fetchProperties(0, 1, { id: ids }, [
          "address",
        ]);
        setAddresses(properties.map((obj: Property) => obj.address));
      } else {
        setAddresses([]);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAddresses();
  }, [...ids]);

  return addresses;
};
