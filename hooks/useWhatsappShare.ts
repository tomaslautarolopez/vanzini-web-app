import { useState, useEffect } from "react";
import { API_URL_SHORTEN } from "../axios/apiEndPoints";
import { useGetPropertyAddress } from "./useGetPropertyAddress";
import { axiosVanziniApiInstance, webURL } from "../axios";
import { ConsultType } from "./useConsult";
import { Property } from "../interfaces/properties";
import { fetchProperties } from "../data-fetch/fetchProperties";
import { buildUrlForProperty } from "../utils/buildUrlForProperty";

export const useWhatsappShare = (
  consultType: ConsultType,
  ids: string[]
): {
  text: string;
  isLoading: boolean;
} => {
  const [text, setText] = useState("");
  const [isLoadingText, setisLoadingText] = useState(false);

  const adresses = useGetPropertyAddress(ids);

  useEffect(() => {
    if (!isLoadingText) {
      getText();
    }
  }, [...ids, adresses]);

  const getPreamble = () => {
    switch (consultType) {
      case "GENERAL":
        return "Hola, me comunico desde vanzini.com.ar";

      case "PROPERTY":
        return (
          "Me comunico desde la web de Vanzini Propiedades, estoy interesado" +
          ` por esta propiedad: %0A`
        );

      case "DEVELOPMENT":
        return (
          "Me comunico desde la web de Vanzini Propiedades, estoy interesado" +
          ` en este emprendimiento: %0A`
        );

      case "FAV":
        return (
          "Hola, me comunico desde la web de Vanzini Propiedades, quisiera obtener información" +
          ` de los siguientes inmuebles: %0A`
        );

      case "ASSES":
        return `Hola, me comunico desde vanzini.com.ar, quisiera tasar una propiedad`;

      default:
        return "Hola, me comunico desde vanzini.com.ar";
    }
  };

  const zip = (arr: Array<any>, ...arrs: Array<any>) =>
    arr.map((val, i) => arrs.reduce((a, arr) => [...a, arr[i]], [val]));

  const shortenURLs = async (properties: Array<Property>) => {
    try {
      setisLoadingText(true);
      const urls: string[] = properties.map(
        (property) => `${webURL}${buildUrlForProperty(property)}`
      );
      const shortenedURLPromises = urls.map((url) =>
        axiosVanziniApiInstance.post(API_URL_SHORTEN, { url })
      );
      const promisesResolved = await Promise.all(shortenedURLPromises);
      const shortenedURLs = promisesResolved.map((res) => res.data.result_url);

      setisLoadingText(false);
      return shortenedURLs;
    } catch (error) {
      console.error(error);

      setisLoadingText(false);
      return [];
    }
  };

  const getText = async () => {
    if (consultType === "GENERAL" || consultType === "ASSES") {
      setText(`${getPreamble()}`);
    } else {
      const { properties }: { properties: Property[] } = await fetchProperties(
        0,
        undefined,
        {
          id: ids,
        }
      );

      const urls: Array<string> = await shortenURLs(properties);

      const addressAndUrl = zip(adresses, urls)
        .map((arr) => arr.join(": "))
        .join("%0A");

      setText(`${getPreamble()}${addressAndUrl || ""}`);
    }
  };

  return {
    text,
    isLoading: isLoadingText,
  };
};
