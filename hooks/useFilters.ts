import { useContext, useEffect, useMemo, useState } from 'react';

import {
  CountryType,
  FiltersContext,
  OperationType,
} from '../contexts/FiltersContext';
import { useGetLocationFilters } from './useGetLocationFilters';
import { PropertiesTypes } from '../interfaces/properties';

export const useFilters = () => {
  const [firstSubDivision, setFirstSubDivision] = useState<
    string | undefined
  >();
  const [secondSubDivision, setSecondSubDivision] = useState<
    string | undefined
  >();
  const {
    changeFilters,
    filters,
    resetFilters: resetFiltersContext,
  } = useContext(FiltersContext);

  const [country, setCountry] = useState<CountryType | undefined>(filters.country);

  const [currency, setCurrency] = useState<'USD' | 'ARS' | undefined>(filters.currency);
  const [fromPrice, setFromPrice] = useState<number | undefined>(
    filters.fromSellPrice
  );
  const [toPrice, setToPrice] = useState<number | undefined>(
    filters.toSellPrice
  );
  const [operationType, setOperationType] = useState<OperationType | undefined>(
    filters.operationType
  );
  const [propertyType, setPropertyType] = useState<PropertiesTypes[]>(
    filters.propertyType || []
  );
  const [age, setAge] = useState<string[]>(filters.age || []);
  const [roomAmount, setRoomAmountState] = useState<string[]>(
    filters.roomAmount || []
  );
  const [amenities, setAmenities] = useState<string[]>(filters.tags || []);
  const [other, setOther] = useState<string[]>(filters.customTags || []);

  const setRoomAmount = (roomAmounts: string[]) => {
    if (roomAmounts.includes('0:=')) {
      const propertyTypeSet = new Set(propertyType);
      propertyTypeSet.add(PropertiesTypes.AP);
      propertyTypeSet.add(PropertiesTypes.BU);

      setPropertyType(Array.from(propertyTypeSet));
    }
    setRoomAmountState(roomAmounts);
  };

  useEffect(() => {
    const {
      country,
      firstSubDivision,
      secondSubDivision,
      currency,
      operationType,
      propertyType,
      age,
      roomAmount,
      customTags,
      tags,
      toSellPrice,
      fromSellPrice,
    } = filters;

    setPropertyType(propertyType || []);
    setAge(age || []);
    setRoomAmount(roomAmount || []);
    setAmenities(tags || []);
    setOther(customTags || []);
    setFromPrice(fromSellPrice);
    setToPrice(toSellPrice);
    setCountry(country || 'Argentina');
    setFirstSubDivision(firstSubDivision);
    setSecondSubDivision(secondSubDivision);
    setCurrency(currency || 'ARS');
    setOperationType(operationType || 'Venta');
  }, [filters]);

  const resetFilters = () => {
    setPropertyType([]);
    setAge([]);
    setRoomAmount([]);
    setAmenities([]);
    setOther([]);
    setFromPrice(undefined);
    setToPrice(undefined);
    setCountry('Argentina');
    setFirstSubDivision(undefined);
    setSecondSubDivision(undefined);
    setCurrency('ARS');
    setOperationType('Venta');

    resetFiltersContext();
  };

  const applyFilters = (ignoreEquality?: boolean) => {
    changeFilters(
      {
        ...filters,
        fromSellPrice: fromPrice,
        toSellPrice: toPrice,
        country,
        firstSubDivision,
        secondSubDivision,
        currency,
        operationType,
        propertyType,
        age,
        roomAmount,
        customTags: other,
        tags: amenities,
      },
      ignoreEquality
    );
  };

  const temporaryFilters = useMemo(
    () => ({
      country,
      firstSubDivision,
      secondSubDivision,
      currency,
      operationType,
      propertyType,
      age,
      roomAmount,
      customTags: other,
      tags: amenities,
    }),
    [
      country,
      firstSubDivision,
      secondSubDivision,
      currency,
      operationType,
      propertyType,
      age,
      roomAmount,
      other,
      amenities,
    ]
  );

  const [loadingLocation, locationOptions] = useGetLocationFilters();

  const countryOptions = Object.keys(locationOptions);

  const firstSubDivisionOptions =
    country &&
    locationOptions[country] &&
    Object.keys(locationOptions[country])
      .reduce((acc: Array<{ value: string; amount: number }>, fs: string) => {
        return [
          ...acc,
          { value: fs, amount: (locationOptions[country] as any)[fs].amount },
        ];
      }, [])
      .sort((a, b) => b.amount - a.amount);

  const secondSubDivisionOptions =
    country &&
    firstSubDivision &&
    locationOptions[country] &&
    (locationOptions[country] as any)[firstSubDivision] &&
    Object.keys((locationOptions[country] as any)[firstSubDivision])
      .reduce((acc: Array<{ value: string; amount: number }>, fs: string) => {
        return [
          ...acc,
          {
            value: fs,
            amount: (locationOptions[country] as any)[firstSubDivision][fs]
              .amount,
          },
        ];
      }, [])
      .sort((a, b) => b.amount - a.amount);

  return {
    resetFilters,
    loadingLocation,
    countryOptions,
    secondSubDivisionOptions,
    firstSubDivisionOptions,
    country,
    setCountry,
    firstSubDivision,
    setFirstSubDivision,
    secondSubDivision,
    setSecondSubDivision,
    currency,
    setCurrency,
    fromPrice,
    setFromPrice,
    toPrice,
    setToPrice,
    operationType,
    setOperationType,
    propertyType,
    setPropertyType,
    age,
    setAge,
    roomAmount,
    setRoomAmount,
    amenities,
    setAmenities,
    other,
    setOther,
    applyFilters,
    temporaryFilters,
  };
};
