import { useState, useEffect } from "react";

import { fetchProperties } from "../data-fetch/fetchProperties";
import { Filters } from "../contexts/FiltersContext";

export const useCountProperties = (
  filters: Filters | undefined
): {
  propertiesCount: number | undefined;
  isCounting: boolean;
} => {
  const [propertiesCount, setPropertiesCount] = useState<number | undefined>(
    undefined
  );
  const [isCounting, setIsCounting] = useState(false);

  useEffect(() => {
    countProperties();
  }, [filters]);

  const countProperties = async () => {
    setIsCounting(true);
    try {
      const { propertiesCount } = await fetchProperties(0, 1, filters);
      setPropertiesCount(propertiesCount);
    } catch (error) {
      console.log(error);
      setPropertiesCount(undefined);
    }
    setIsCounting(false);
  };

  return { propertiesCount, isCounting };
};
