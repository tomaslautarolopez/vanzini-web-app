import { useState, useCallback } from 'react';

type SetStateAction<T> = (prevState: T) => void;

export const useLocalStorageState = <T>(
  key: string,
  defaultValue: T
): [T, SetStateAction<T>] => {
  const [state, setState] = useState(() => {
    try {
      const localStorageValue = window.localStorage.getItem(key);
      if (localStorageValue !== null) {
        return JSON.parse(localStorageValue);
      } else {
        return defaultValue;
      }
    } catch (e) {
      return defaultValue;
    }
  });

  const setLocalStorageState = useCallback(
    (newState: T): void => {
      window.localStorage.setItem(key, JSON.stringify(newState));
      setState(newState);
    },
    [key]
  );

  return [state, setLocalStorageState];
};
