import { useState, useEffect } from "react";
import { axiosVanziniApiInstance } from "../axios";
import { API_URL_GET_LOCATION_FILTERS } from "../axios/apiEndPoints";

class LocationfilterOptions {
  private locationOptions: null | object = null;
  private static instance: LocationfilterOptions;

  constructor() {
    if (!!LocationfilterOptions.instance) {
      return LocationfilterOptions.instance;
    }

    LocationfilterOptions.instance = this;
    this.locationOptions = null;

    return this;
  }

  getOptions = async () => {
    if (this.locationOptions) {
      return this.locationOptions;
    } else {
      const {
        data: { payload: locationOptions },
      } = await axiosVanziniApiInstance.get(API_URL_GET_LOCATION_FILTERS);
      return locationOptions;
    }
  };
}

export const useGetLocationFilters = (): [
  boolean,
  { [key: string]: object | number; amount: number }
] => {
  const [locationOptions, setLocationOptions] = useState({ amount: 0 });
  const locationfilterOptions = new LocationfilterOptions();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getFilters();
  }, []);

  const getFilters = async () => {
    setLoading(true);
    try {
      const options = await locationfilterOptions.getOptions();
      setLocationOptions(options);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  };

  return [loading, locationOptions];
};
