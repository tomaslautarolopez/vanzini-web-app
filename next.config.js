const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});
const withImages = require("next-images");

module.exports = withBundleAnalyzer(
  withImages({
    publicRuntimeConfig: {
      ENVIROMENT: process.env.ENVIROMENT,
    },
  })
);
