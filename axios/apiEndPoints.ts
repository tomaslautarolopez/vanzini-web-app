/*
  Vanzini API end ppints
*/
const API_URL_PROPERTIES: string = "/properties/get-properties";
const API_URL_GET_LOCATION_FILTERS: string = "/properties/location-filters";
const API_URL_SOCIAL_LOG_IN: (socialMedia: string) => string = (
  socialMedia: string
) => `/users/auth/${socialMedia}`;
const API_END_POINT_LOG_IN: string = "/users/log-in";
const API_END_POINT_GET_USER: string = "/users/get-user";
const API_END_POINT_SIGN_UP: string = "/users/sign-up";
const API_END_POINT_RECOVER_PASSWORD: string = "/users/recover-password";
const API_URL_LIKE_PROPERTY: string = "/users/like-property";
const API_URL_DISLIKE_PROPERTY: string = "/users/unlike-property";
const API_URL_CONSULT_GENERAL: string = "/consults/general";
const API_URL_CONSULT_PROPERTY: string = "/consults/property";
const API_URL_CONSULT_DEVELOPMENT: string = "/consults/development";
const API_URL_CONSULT_FAVS: string = "/consults/fav";
const API_URL_CONSULT_ASSES: string = "/consults/assess";
const API_URL_EMPLOYEE_ON_GUARD: string = "/employees/get-on-guard-employee";
const API_URL_ASSESS: string = "/consults/assess";
const API_URL_FAV: string = "/consults/fav";
const API_URL_SHORTEN: string = "/url-shorten/shorten";
const API_URL_SUSCRIBE_TO_NEWSLETTER: string = "/newsletter/suscribe";
const API_URL_UNSUSCRIBE_TO_NEWSLETTER: string = "/newsletter/unsuscribe";
const API_URL_CHECK_SUSCRIPTION_TO_NEWSLETTER: string =
  "/newsletter/is-suscribed";
const API_URL_GET_HIGHLIGHT_PROPERTY: string = "/properties/highlight-property";
const API_URL_GET_PROPERTIES_LIST_FOR_FEED: string =
  "/properties/lists/all-with-properties";
const API_URL_GET_PROPERTIES_LIST: string = "/properties/lists/get";
const API_URL_EMPLOYEES_IN_WEB: string = "/employees/get-employees-in-web";
const API_URL_UPDATE_USER: string = "/users/update";
const API_URL_MARK_PROPERTY_AS_SEEN: string = "/users/seen-property";
const API_URL_CHANGE_USER_PASSWORD: string = "/users/change-password";
const API_URL_RECOVER_PASSWORD: string = "/users/recover-password";
const API_URL_RESET_PASSWORD: string = "/users/update-password";
const API_URL_PRICE_ALERT_EMAIL: string = "/price-alerts/email";
const API_URL_PRICE_ALERT_USER: string = "/price-alerts/user";
const API_URL_NOTIFICATIONS: string = "/notifications";

export {
  API_URL_MARK_PROPERTY_AS_SEEN,
  API_URL_NOTIFICATIONS,
  API_URL_RESET_PASSWORD,
  API_URL_RECOVER_PASSWORD,
  API_URL_CHECK_SUSCRIPTION_TO_NEWSLETTER,
  API_URL_UNSUSCRIBE_TO_NEWSLETTER,
  API_URL_CHANGE_USER_PASSWORD,
  API_URL_UPDATE_USER,
  API_URL_CONSULT_FAVS,
  API_URL_CONSULT_ASSES,
  API_URL_EMPLOYEES_IN_WEB,
  API_URL_GET_PROPERTIES_LIST,
  API_URL_GET_PROPERTIES_LIST_FOR_FEED,
  API_URL_GET_HIGHLIGHT_PROPERTY,
  API_URL_SUSCRIBE_TO_NEWSLETTER,
  API_URL_SHORTEN,
  API_URL_EMPLOYEE_ON_GUARD,
  API_URL_CONSULT_GENERAL,
  API_URL_CONSULT_PROPERTY,
  API_URL_CONSULT_DEVELOPMENT,
  API_URL_ASSESS,
  API_URL_FAV,
  API_URL_PROPERTIES,
  API_URL_GET_LOCATION_FILTERS,
  API_URL_SOCIAL_LOG_IN,
  API_END_POINT_LOG_IN,
  API_END_POINT_GET_USER,
  API_END_POINT_SIGN_UP,
  API_END_POINT_RECOVER_PASSWORD,
  API_URL_LIKE_PROPERTY,
  API_URL_DISLIKE_PROPERTY,
  API_URL_PRICE_ALERT_EMAIL,
  API_URL_PRICE_ALERT_USER,
};
