import axios from "axios";
import getConfig from "next/config";

import { API_URLs, WEB_URLs } from "./constants";

const { publicRuntimeConfig } = getConfig();
const { ENVIROMENT } = publicRuntimeConfig;

export const baseURL = ENVIROMENT ? API_URLs[ENVIROMENT] : API_URLs.dev;

export const webURL = ENVIROMENT ? WEB_URLs[ENVIROMENT] : WEB_URLs.dev;

export const axiosVanziniApiInstance = axios.create({
  baseURL,
});
