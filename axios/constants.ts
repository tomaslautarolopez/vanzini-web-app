export const API_URLs: { [key: string]: string } = {
  development: "http://localhost:3000/api",
  staging: "http://www.lucioxd.com/api",
  production: "https://www.vanzini.com.ar/api",
};

export const WEB_URLs: { [key: string]: string } = {
  development: "http://localhost:4000",
  staging: "http://www.lucioxd.com",
  production: "https://www.vanzini.com.ar",
};
