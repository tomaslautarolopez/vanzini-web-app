import React, { createContext, useEffect, useState, useCallback } from "react";
import { notification } from "antd";
import { useRouter } from "next/router";

import { axiosVanziniApiInstance } from "../axios";
import {
  API_END_POINT_LOG_IN,
  API_END_POINT_GET_USER,
  API_END_POINT_SIGN_UP,
  API_END_POINT_RECOVER_PASSWORD,
  API_URL_NOTIFICATIONS,
  API_URL_MARK_PROPERTY_AS_SEEN,
} from "../axios/apiEndPoints";
import { useLocalStorageState } from "../hooks/useLocalStorageState";
import { AsyncData } from "../interfaces/async-data";
import { UserSocket } from "../socket/UserSocket";
import { Notification } from "../interfaces/notifications";

export interface User {
  token?: string;
  likes: Set<string>;
  email?: string;
  name?: string;
  picture?: string;
  _id?: string;
  provider: "google" | "facebook" | "local";
  alerts: Set<string>;
  seen: Set<string>;
}

export interface UserSignUpForm {
  email: string;
  password: string;
  name: string;
}

export const NOTIFICATION_TYPE = "Usuario";

class VoidContext implements UserContext {
  get user(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get login(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get logout(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get signUp(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get signUpLoading(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get recoverPassword(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get changeRecoverPassword(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get setUserData(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get logInFederation(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get userSocket(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get notifications(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get markPropertyAsSeen(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get hasPropertyBeenSeen(): never {
    throw new Error("Cannot consume context outside of provider");
  }
}

type LoginAction = (email: string, password: string) => Promise<void>;
type LogoutAction = (callback?: Function) => void;
type SignUpAction = (value: UserSignUpForm, callback?: Function) => void;
type RecoverPasswordAction = (email: string) => void;
type ChangeRecoverPasswordAction = (
  email: string,
  token: string
) => Promise<void>;
type logInFederationAction = (token: string) => void;

interface UserContext {
  user: AsyncData<User, UserLoading>;
  login: LoginAction;
  logout: LogoutAction;
  signUp: SignUpAction;
  recoverPassword: RecoverPasswordAction;
  changeRecoverPassword: ChangeRecoverPasswordAction;
  logInFederation: logInFederationAction;
  userSocket: UserSocket | undefined;
  setUserData: (User: User | null) => void;
  notifications: Notification[];
  hasPropertyBeenSeen: (propertyId: string) => boolean;
  markPropertyAsSeen: (propertyId: string) => void;
}
interface UserLoading {
  isRecoveringPassword: boolean;
  isLoginLoading: boolean;
  isSetUserLoading: boolean;
  isSignUpLoading: boolean;
}

const useUser = (): UserContext => {
  const [token, setToken] = useLocalStorageState<string | undefined>(
    "milote-token",
    undefined
  );
  const [seenProperties, setSeenProperties] = useLocalStorageState<
    string[] | undefined
  >("seen-properties", []);
  const [userSocket, setUserSocket] = useState<UserSocket | undefined>();
  const [notifications, setNotifications] = useState<Notification[]>([]);

  const [isLoginLoading, setIsLoginLoading] = useState<boolean>(false);
  const [isSetUserLoading, setIsSetUserLoading] = useState<boolean>(false);
  const [isSignUpLoading, setIsSignUpLoading] = useState<boolean>(false);
  const [isRecoveringPassword, setIsRecoveringPassword] =
    useState<boolean>(false);

  const [error, setError] = useState<any>(null);
  const [userData, setUserData] = useState<User | null>(null);

  const { pathname, push } = useRouter();

  const markPropertyAsSeen = (propertyId: string) => {
    if (userData && !userData.seen.has(propertyId)) {
      axiosVanziniApiInstance.put(
        `${API_URL_MARK_PROPERTY_AS_SEEN}/${propertyId}`
      );

      setUserData({
        ...userData,
        seen: userData.seen.add(propertyId),
      });
    }

    if (
      !userData &&
      seenProperties &&
      !seenProperties?.includes?.(propertyId)
    ) {
      setSeenProperties([...seenProperties, propertyId]);
    }
  };

  const hasPropertyBeenSeen = (propertyId: string) => {
    if(userData && userData.seen.has(propertyId)) {
      return true
    }

    if(seenProperties && seenProperties?.includes?.(propertyId)) {
      return true
    }

    return false
  }

  const addNotification = (notification: Notification) =>
    setNotifications([...notifications, notification]);

  const getNotifications = async (token: string) => {
    try {
      const {
        data: { payload: notifications },
      } = await axiosVanziniApiInstance.get(API_URL_NOTIFICATIONS, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      setNotifications(notifications);
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "No pudimos obtener sus notificaciones, intentelo de nuevo mas tarde",
        placement: "bottomRight",
      });
      console.log(error);
    }
  };

  const changeRecoverPassword = async (
    password: string,
    token: string
  ): Promise<void> => {
    setIsRecoveringPassword(true);

    try {
      await axiosVanziniApiInstance.post(
        `${API_END_POINT_RECOVER_PASSWORD}confirm/`,
        {
          password,
          token,
        }
      );

      notification.success({
        message: NOTIFICATION_TYPE,
        description: "Logramos cambiar su contraseña",
        placement: "bottomRight",
      });
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "No pudimos cambiar su contraseña, intentelo de nuevo mas tarde",
        placement: "bottomRight",
      });
      console.error(error);
      setIsRecoveringPassword(false);
      throw error;
    }

    setIsRecoveringPassword(false);
  };

  const recoverPassword = async (email: string): Promise<void> => {
    setIsRecoveringPassword(true);

    try {
      await axiosVanziniApiInstance.post(API_END_POINT_RECOVER_PASSWORD, {
        email,
      });

      notification.success({
        message: NOTIFICATION_TYPE,
        description:
          "Enviamos un email a tu cuenta para que puedas terminar el proceso de recuperar tu contraseña",
        placement: "bottomRight",
      });
    } catch (error) {
      notification.error({
        message: NOTIFICATION_TYPE,
        description:
          "No pudimos cambiar tu contraseña, intentalo de nuevo mas tarde.",
        placement: "bottomRight",
      });

      console.error(error);
    }

    setIsRecoveringPassword(false);
  };

  const setTokenFromLoginCredentials = async (
    email: string,
    password: string
  ): Promise<void> => {
    setIsLoginLoading(true);

    try {
      setError(undefined);
      const {
        data: {
          payload: { token: new_token },
        },
      } = await axiosVanziniApiInstance.post(API_END_POINT_LOG_IN, {
        email,
        password,
      });

      setUserData(null);
      setToken(new_token);
    } catch (error) {
      console.error(error);

      setToken(undefined);
    }

    setIsLoginLoading(false);
  };

  const setUserFromToken = useCallback(
    async (token: string): Promise<void> => {
      setIsSetUserLoading(true);

      try {
        const {
          data: { payload: user },
        } = await axiosVanziniApiInstance.get(API_END_POINT_GET_USER, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        axiosVanziniApiInstance.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${token}`;

        setUserData({
          ...user,
          likes: new Set(user.likes),
          alerts: new Set(user.alerts),
          seen: new Set(user.seen),
        });
      } catch (error) {
        setToken(undefined);
        console.error(error);
      }

      setIsSetUserLoading(false);
    },
    [setToken]
  );

  const logout = async (callback?: Function): Promise<void> => {
    setUserData(null);
    setToken(undefined);

    if (pathname === "/perfil") {
      push("/feed");
    }

    callback?.();
  };

  const signUp = async (signUpForm: UserSignUpForm, callback?: Function) => {
    setIsSignUpLoading(true);
    try {
      await axiosVanziniApiInstance.post(API_END_POINT_SIGN_UP, signUpForm);

      callback?.();
    } catch (error) {
      console.log(error);
      setError(error);
    }
    setIsSignUpLoading(false);
  };

  const logInFederation = (token: string) => {
    setToken(token);
  };

  useEffect(() => {
    if (token !== undefined) {
      setUserSocket(new UserSocket(token, addNotification));
      getNotifications(token);
      setUserFromToken(token);
    } else {
      setIsSetUserLoading(false);
    }
  }, [setUserFromToken, token]);

  return {
    setUserData,
    changeRecoverPassword,
    recoverPassword,
    userSocket,
    markPropertyAsSeen,
    hasPropertyBeenSeen,
    user: {
      data: userData,
      loading: {
        isRecoveringPassword,
        isLoginLoading,
        isSetUserLoading,
        isSignUpLoading,
      },
      error,
    },
    notifications,
    login: setTokenFromLoginCredentials,
    logout,
    signUp,
    logInFederation,
  };
};

export const UserContext = createContext<UserContext>(new VoidContext());

export const UserContextProvider: React.FC = ({ children }) => {
  const userContext = useUser();

  return (
    <UserContext.Provider value={userContext}>{children}</UserContext.Provider>
  );
};
