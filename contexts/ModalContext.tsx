import React, { createContext, useState } from "react";
import { PropertiesTypes, Property } from "../interfaces/properties";

class VoidContext implements ModalContext {
  get modals(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get setModal(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get modalsData(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get setConsultModalData(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get setAlertModalData(): never {
    throw new Error("Cannot consume context outside of provider");
  }
}

interface Modals {
  logInModal: boolean;
  consultModal: boolean;
  signUpModal: boolean;
  newsletterModal: boolean;
  assessModal: boolean;
  logOutModal: boolean;
  forgotPasswordModal: boolean;
  resetPasswordModal: boolean;
  alertModal: boolean;
}

interface ConsultModalData {
  property: Property | undefined;
  propertiesId: {
    id: string;
    propertyType: PropertiesTypes;
    units?: string[];
  }[];
}

interface AlertModalData {
  propertyId: string | undefined;
}

interface ModalsData {
  consultModal: ConsultModalData;
  alertModal: AlertModalData;
}

interface ModalContext {
  modals: Modals;
  modalsData: ModalsData;
  setModal: (modal: keyof Modals, visible: boolean) => void;
  setConsultModalData: (consultModalData: ConsultModalData) => void;
  setAlertModalData: (alertModal: AlertModalData) => void;
}

export const ModalContext = createContext<ModalContext>(new VoidContext());

const modalInitialState: Modals = {
  logInModal: false,
  consultModal: false,
  signUpModal: false,
  newsletterModal: false,
  assessModal: false,
  logOutModal: false,
  forgotPasswordModal: false,
  resetPasswordModal: false,
  alertModal: false,
};

export const ModalContextProvider: React.FC = ({ children }) => {
  const [modals, setModalFn] = useState<Modals>(modalInitialState);
  const [modalsData, setModalsData] = useState<ModalsData>({
    alertModal: {
      propertyId: undefined,
    },
    consultModal: {
      property: undefined,
      propertiesId: [],
    },
  });

  const setModal = (key: keyof Modals, visible: boolean) => {
    setModalFn({
      ...modalInitialState,
      [key]: visible,
    });
  };

  const setConsultModalData = (consultModalData: ConsultModalData) =>
    setModalsData({
      ...modalsData,
      consultModal: consultModalData,
    });

  const setAlertModalData = (alertModalData: AlertModalData) =>
    setModalsData({
      ...modalsData,
      alertModal: alertModalData,
    });

  return (
    <ModalContext.Provider
      value={{
        modals,
        setModal,
        modalsData,
        setConsultModalData,
        setAlertModalData,
      }}
    >
      {children}
    </ModalContext.Provider>
  );
};
