import React, { createContext, useState, useEffect } from "react";
import { useRouter } from "next/router";
import qs from "qs";
import isEqual from "lodash.isequal";

import { PropertiesTypes, Property } from "../interfaces/properties";
import { ParsedUrlQuery } from "querystring";
import {
  ageOptions,
  amenitiesOptions,
  otherOptions,
  propertyTypeOptions,
  roomAmountOptions,
  sortQuery,
} from "../constants/filters";
import { objectFilter } from "../utils/objectFilter";

export type Sort =
  | "operationSell.priceUSDmin:ASC"
  | "operationRent.priceUSDmin:ASC"
  | "operationSell.priceUSDmax:DESC"
  | "operationRent.priceUSDmax:DESC"
  | "creationDate:DESC"
  | "totalSurfaceMax:DESC"
  | "totalSurfaceMax:ASC";

export type CountryType = "Argentina" | "Uruguay";

export type OperationType = "Venta" | "Alquiler";
export interface Filters {
  propertyType?: PropertiesTypes[];
  fields?: Array<[keyof Property]>;
  age?: string[];
  country?: CountryType;
  urbanLocation?: string;
  region?: string;
  firstSubDivision?: string;
  secondSubDivision?: string;
  operationType?: OperationType;
  currency?: "USD" | "ARS";
  fromSellPrice?: number;
  toSellPrice?: number;
  fromRentPrice?: number;
  toRentPrice?: number;
  roomAmount?: string[];
  constructionStatus?: String;
  sort?: Sort;
  neighborhood?: string[];
  address?: String;
  tags?: string[];
  id?: string[];
  customTags?: string[];
}

class VoidContext implements FiltersContext {
  get filters(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get changeFilters(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get resetFilters(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get setSort(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get constructUrlWithFilter(): never {
    throw new Error("Cannot consume context outside of provider");
  }
}

interface FiltersContext {
  filters: Filters;
  changeFilters: (filters: Filters, ignoreEquality?: boolean) => void;
  setSort: (sort: Sort) => void;
  resetFilters: () => void;
  constructUrlWithFilter: (path: string, newFilters?: Filters) => string;
}

const findValue = (
  params: string[],
  options: { query: string; value: string }[]
) => {
  return params.map((label) => {
    const value = options.find(
      (opt) => opt.query.toLowerCase() === label.toLowerCase()
    );
    if (!value) {
      throw new Error("Wrong page");
    } else {
      return value?.value;
    }
  });
};

const findLabel = (
  selectedOptions: string[],
  options: { value: string; query: string }[]
): string[] => {
  return selectedOptions
    .map(
      (value: String) =>
        options.find((opt) => opt.value.toLowerCase() === value.toLowerCase())
          ?.query
    )
    .filter((value) => value !== undefined) as string[];
};

export const constructQueryFromFilters = ({
  propertyType,
  roomAmount,
  age,
  tags,
  operationType,
  currency,
  toSellPrice,
  fromSellPrice,
  customTags,
  address,
  country,
  region,
  urbanLocation,
  firstSubDivision,
  secondSubDivision,
  sort,
}: Filters): {
  pathname: string;
  query: object;
} => {
  const propertyTypeParam =
    propertyType && findLabel(propertyType, propertyTypeOptions).join("-");
  const tagsParam = tags && findLabel(tags, amenitiesOptions);
  const customParam = customTags && findLabel(customTags, otherOptions);
  const orden = sort && findLabel([sort], sortQuery);
  const operationTypeParams = operationType;

  const roomAmountParams =
    roomAmount && findLabel(roomAmount, roomAmountOptions);
  const ageParams = age && findLabel(age, ageOptions);

  return {
    query: objectFilter(
      {
        orden,
        amenities: tagsParam,
        otros: customParam,
        operacion: operationTypeParams,
        habitaciones: roomAmountParams,
        antiguedad: ageParams,
        moneda: currency,
        hasta: toSellPrice,
        desde: fromSellPrice,
        search: address,
        pais: country,
        provincia: region,
        localidad: urbanLocation,
        partido: firstSubDivision,
        barrio: secondSubDivision,
      },
      (value) => !!value
    ),
    pathname: `/${propertyTypeParam || ""}`,
  };
};

export const parseFiltersFromQuery = ({
  propertyType,
  amenities,
  operacion,
  habitaciones,
  antiguedad,
  moneda,
  hasta,
  desde,
  otros,
  search,
  pais,
  partido,
  barrio,
  orden,
}: ParsedUrlQuery) => {
  return {
    sort:
      typeof orden === "string" ? findValue([orden], sortQuery)[0] : undefined,
    propertyType:
      typeof propertyType === "string"
        ? findValue(propertyType.split("-"), propertyTypeOptions)
        : [],
    tags: amenities
      ? findValue(
          typeof amenities === "string" ? [amenities] : amenities,
          amenitiesOptions
        )
      : [],
    customTags: otros
      ? findValue(typeof otros === "string" ? [otros] : otros, otherOptions)
      : [],
    fromSellPrice: desde && Number(desde),
    toSellPrice: hasta && Number(hasta),
    address: search,
    country: pais || "Argentina",
    firstSubDivision: partido,
    secondSubDivision: barrio,
    age: antiguedad
      ? findValue(
          typeof antiguedad === "string" ? [antiguedad] : antiguedad,
          ageOptions
        )
      : [],
    currency: moneda || "USD",
    operationType: (operacion as OperationType) || "Venta",
    roomAmount: habitaciones
      ? findValue(
          typeof habitaciones === "string" ? [habitaciones] : habitaciones,
          roomAmountOptions
        )
      : [],
  } as Filters;
};

const useFilters = (): FiltersContext => {
  const { query, push, pathname: nextPathname } = useRouter();
  const [filters, setFilters] = useState<Filters>(parseFiltersFromQuery(query));

  useEffect(() => {
    const newFilters = parseFiltersFromQuery(query);

    if (!isEqual(newFilters, filters)) {
      setFilters(newFilters);
    }
  }, [query, setFilters]);

  const constructUrlWithFilter = (
    path: string,
    newFilters?: Filters
  ): string => {
    const { query, pathname } = constructQueryFromFilters(
      newFilters || filters
    );

    return `${path}${pathname}?${qs.stringify(query, {
      arrayFormat: "repeat",
    })}`;
  };

  const changeFilters = (newFilters: Filters, ignoreEquality?: boolean) => {
    if (!isEqual(newFilters, filters) || ignoreEquality) {
      const pathNamePrefix =
        nextPathname === "/mapa" || nextPathname === "/mapa/[propertyType]"
          ? "/mapa"
          : "/feed";

      if (typeof window !== "undefined") {
        window.scrollTo(0, 0);
      }

      push(constructUrlWithFilter(pathNamePrefix, newFilters));
    }
  };

  const resetFilters = () => {
    changeFilters({
      country: "Argentina",
      firstSubDivision: undefined,
      secondSubDivision: undefined,
      currency: "ARS",
      operationType: "Venta",
      propertyType: [],
      age: [],
      roomAmount: [],
      customTags: [],
      tags: [],
      toSellPrice: undefined,
      fromSellPrice: undefined,
    });
  };

  const setSort = (sort: Sort) => {
    changeFilters({ ...filters, sort });
  };

  return {
    constructUrlWithFilter,
    resetFilters,
    setSort,
    filters,
    changeFilters,
  };
};

export const FiltersContext = createContext<FiltersContext>(new VoidContext());

export const FiltersContextProvider: React.FC = ({ children }) => {
  const filtersContext = useFilters();

  return (
    <FiltersContext.Provider value={filtersContext}>
      {children}
    </FiltersContext.Provider>
  );
};
