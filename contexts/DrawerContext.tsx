import React, { createContext, useState } from "react";

class VoidContext implements DrawerContext {
  get drawers(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get setDrawer(): never {
    throw new Error("Cannot consume context outside of provider");
  }
}

interface Drawers {
  filters: boolean;
}

interface DrawerContext {
  drawers: Drawers;
  setDrawer: (modal: keyof Drawers, visible: boolean) => void;
}

export const DrawerContext = createContext<DrawerContext>(new VoidContext());

export const DrawerContextProvider: React.FC = ({ children }) => {
  const [drawers, setDrawersFn] = useState<Drawers>({
    filters: false,
  });

  const setDrawer = (key: keyof Drawers, visible: boolean) =>
    setDrawersFn({
      ...drawers,
      [key]: visible,
    });

  return (
    <DrawerContext.Provider
      value={{
        drawers,
        setDrawer,
      }}
    >
      {children}
    </DrawerContext.Provider>
  );
};
