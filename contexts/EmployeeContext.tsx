import React, { createContext } from "react";
import { useOnGuardEmployee } from "../hooks/useOnGuardEmployee";
import { Employee, Location } from "../interfaces/employee";

class VoidContext implements EmployeeContext {
  get employeesOnGuard(): never {
    throw new Error("Cannot consume context outside of provider");
  }
  get refetchEmployeesOnGuard(): never {
    throw new Error("Cannot consume context outside of provider");
  }
}

interface EmployeeContext {
  employeesOnGuard: {
    [Location.FISHERTON]: Employee | undefined;
    [Location.HEAD_OFFICE]: Employee | undefined;
  };
  refetchEmployeesOnGuard: () => void;
}

export const EmployeeContext = createContext<EmployeeContext>(
  new VoidContext()
);

export const EmployeeContextProvider: React.FC = ({ children }) => {
  const { data: fishertonEmployee, mutate: fishertonMutate } =
    useOnGuardEmployee(Location.FISHERTON);
  const { data: headOfficeEmployee, mutate: headOfficeMutate } =
    useOnGuardEmployee(Location.HEAD_OFFICE);

  const refetchEmployeesOnGuard = () => {
    fishertonMutate();
    headOfficeMutate();
  };

  return (
    <EmployeeContext.Provider
      value={{
        employeesOnGuard: {
          [Location.FISHERTON]: fishertonEmployee,
          [Location.HEAD_OFFICE]: headOfficeEmployee,
        },
        refetchEmployeesOnGuard,
      }}
    >
      {children}
    </EmployeeContext.Provider>
  );
};
